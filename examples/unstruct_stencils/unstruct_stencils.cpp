#include "dsl.hpp"

using namespace EDSL;


namespace edsl {

  // dense field types:

  Gridspace ck_space(cells,levels);
  define_field_type(CK_Field,ck_space);

  Gridspace ek_space(edges,levels);
  define_field_type(EK_Field,ek_space);

  Gridspace vk_space(verts,levels);
  define_field_type(VK_Field,vk_space);

  // single-link sparse field types:

  Gridspace cvk_space(cells,verts,levels);
  define_field_type(CVK_Field,cvk_space);

  Gridspace evk_space(edges,verts,levels);
  define_field_type(EVK_Field,evk_space);

  Gridspace eck_space(edges,cells,levels);
  define_field_type(ECK_Field,eck_space);

  Gridspace vck_space(verts,cells,levels);
  define_field_type(VCK_Field,vck_space);

  Gridspace cek_space(cells,edges,levels);
  define_field_type(CEK_Field,cek_space);

  Gridspace vek_space(verts,edges,levels);
  define_field_type(VEK_Field,vek_space);

  // multi-link sparse field types:
  Gridspace ecvk_space(edges,cells.verts,levels);
  define_field_type(ECVK_Field,ecvk_space);

  // copy tests:
  void copy_cell_field(CK_Field alpha, CK_Field beta) {
    alpha = beta;
  }

  void copy_ce_field(CEK_Field alpha, CEK_Field beta) {
    alpha = beta;
  }

  void copy_vertical_field(CK_Field alpha, Field beta) {
    beta.spec(levels);
    alpha = beta;
  }

  // single-link-chain reduction tests:

  // CV chain:
  void cv_reduce(CVK_Field my_stencil, CK_Field alpha, VK_Field beta) {
    alpha = nreduce(verts, my_stencil*beta);
  }

  // EV chain:
  void ev_reduce(EVK_Field my_stencil, EK_Field alpha, VK_Field beta) {
    alpha = nreduce(verts, my_stencil*beta);
  }

  // EC chain:
  void ec_reduce(ECK_Field my_stencil, EK_Field alpha, CK_Field beta) {
    alpha = nreduce(cells, my_stencil*beta);
  }

  // VC chain:
  void vc_reduce(VCK_Field my_stencil, VK_Field alpha, CK_Field beta) {
    alpha = nreduce(cells, my_stencil*beta);
  }

  // CE chain:
  void ce_reduce(CEK_Field my_stencil, CK_Field alpha, EK_Field beta) {
    alpha = nreduce(edges, my_stencil*beta);
  }

  // VE chain:
  void ve_reduce(VEK_Field my_stencil, VK_Field alpha, EK_Field beta) {
    alpha = nreduce(edges, my_stencil*beta);
  }

  // multi-link-chain reduction tests:

  // ECV chain:
  void ecv_reduce(ECVK_Field my_stencil, EK_Field alpha, VK_Field beta) {
    alpha = nreduce(cells.verts, my_stencil*beta);
  }
  
  // ECV chain with weights:
  void ecv_reduce_with_weights1(ECVK_Field my_stencil, EK_Field alpha, VK_Field beta) {
    alpha = nreduce(cells.verts, {1, 2, 3, 4}, my_stencil*beta);
  }

  void ecv_reduce_with_weights2(ECVK_Field my_stencil, EK_Field alpha, VK_Field beta) {
    alpha = nreduce(cells.verts, {1, alpha, 3, 4}, my_stencil*beta);
  }

}
