PROGRAM test_unstruct_stencils
  ! todo: rename to cdsl_dawn_unstruct to avoid name collisions
  USE mesh_reader, ONLY: init_mesh_support, mesh_type, gen_atlas_mesh, sym2loctype
  USE dawn_unstruct
  USE dawn_unstruct_stencils
  USE example_support, ONLY: dp, init, f_abort, assert, check, undef_dp
  IMPLICIT NONE

  CHARACTER(len=255):: gridfile
  TYPE(mesh_type) :: mesh

  TYPE, ABSTRACT :: reduce_x2y_type
    CHARACTER(len=2) :: chain_str
  END TYPE reduce_x2y_type

  TYPE, EXTENDS(reduce_x2y_type) :: reduce_c2v_type
    TYPE(cv_reduce_type) :: reduce
  END TYPE reduce_c2v_type

  TYPE, EXTENDS(reduce_x2y_type) :: reduce_e2v_type
    TYPE(ev_reduce_type) :: reduce
  END TYPE reduce_e2v_type

  TYPE, EXTENDS(reduce_x2y_type) :: reduce_e2c_type
    TYPE(ec_reduce_type) :: reduce
  END TYPE reduce_e2c_type

  TYPE, EXTENDS(reduce_x2y_type) :: reduce_v2c_type
    TYPE(vc_reduce_type) :: reduce
  END TYPE reduce_v2c_type

  TYPE, EXTENDS(reduce_x2y_type) :: reduce_c2e_type
    TYPE(ce_reduce_type) :: reduce
  END TYPE reduce_c2e_type

  TYPE, EXTENDS(reduce_x2y_type) :: reduce_v2e_type
    TYPE(ve_reduce_type) :: reduce
  END TYPE reduce_v2e_type

  TYPE(reduce_c2v_type) :: reduce_c2v
  TYPE(reduce_e2v_type) :: reduce_e2v
  TYPE(reduce_e2c_type) :: reduce_e2c
  TYPE(reduce_v2c_type) :: reduce_v2c
  TYPE(reduce_c2e_type) :: reduce_c2e
  TYPE(reduce_v2e_type) :: reduce_v2e

  CHARACTER(len=*), PARAMETER :: file = &
       & __FILE__

  CALL init_test

  ! test meta data:
  CALL test_dense_meta_data
  CALL test_sparse_meta_data

  ! test data transfer with backend:
  CALL test_dense_copy
  CALL test_dense_ikb_copy
  CALL test_sparse_copy

  ! special single column field:
  CALL test_vertical_field


  ! all test cases with first order neighbors:
  CALL test_reduce_xy(reduce_c2v)
  CALL test_reduce_xy(reduce_e2v)
  CALL test_reduce_xy(reduce_e2c)
  CALL test_reduce_xy(reduce_v2c)
  CALL test_reduce_xy(reduce_c2e)
  CALL test_reduce_xy(reduce_v2e)

  ! redundant special case implementation (for debugging)
  CALL test_reduce_ce

  ! selected test cases with second order neighbors:
  CALL test_reduce_ecv

  CALL test_reduce_ecv_with_weights1

  CALL test_reduce_ecv_with_weights2

CONTAINS

  SUBROUTINE init_test
    USE iso_c_binding, ONLY : c_double
    CALL assert(dp == c_double, file, __LINE__)
    CALL GET_COMMAND_ARGUMENT(1,gridfile)
    PRINT*,'gridfile=',TRIM(gridfile)
    CALL init_mesh_support(gridfile)
    mesh = gen_atlas_mesh()
  END SUBROUTINE init_test

  SUBROUTINE test_dense_copy
    TYPE(copy_cell_field_type) :: stencil
    TYPE(dense_field_type) :: alpha, beta
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:)
    INTEGER :: dense_size, k_size

    ! init Dawn data fields
    CALL alpha%init(mesh, cdsl_loctype_cells, 1)
    CALL beta%init(mesh, cdsl_loctype_cells, 1)

    ! init Fortran fields:
    CALL alpha%get_shape(dense_size, k_size)
    ALLOCATE( a(dense_size, k_size), b(dense_size, k_size) )
    CALL meta_data%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])

    a = -1.0_dp
    CALL init(b)

    ! Fortran -> Dawn:
    CALL beta%set(b, meta_data)
    CALL alpha%set(a, meta_data)

    ! init & run stencil:
    CALL stencil%init(mesh, k_size, alpha, beta)
    CALL stencil%run()

    ! Dawn -> Fortran:
    CALL alpha%get(a, meta_data)

    ! check:
    CALL check(a ,b, 0.0_dp)
    PRINT*,'check test_dense_copy passed'
  END SUBROUTINE test_dense_copy

  SUBROUTINE test_dense_ikb_copy
    TYPE(cdsl_subset_range_type) :: all_cells, some_cells
    TYPE(dense_field_type) :: alpha
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    INTEGER  :: nproma, nlev, nblocks_cells
    INTEGER :: h_size, k_size, dense_size, ncells, nsome, m1, m2
    REAL(dp), ALLOCATABLE :: a(:,:,:), b(:,:,:)
    REAL(dp) :: tol

    nproma = 16
    nlev = 4
    ncells = get_mesh_size(mesh, cdsl_loctype_cells)
    tol = ncells * nlev * EPSILON(a)

    ! basic test:
    CALL gen_subset_range(all_cells, num_skip=0, num_elements=ncells, block_size=nproma)
    h_size = subset_range_size(all_cells)
    CALL assert(h_size == ncells, file, __LINE__)
    CALL alpha%init(mesh, cdsl_loctype_cells, nlev)
    CALL alpha%get_shape(dense_size, k_size)
    CALL assert(dense_size == ncells, file, __LINE__)
    CALL assert(k_size == nlev, file, __LINE__)
    nblocks_cells = subset_range_nblocks(all_cells)
    ALLOCATE(a(nproma, nlev, nblocks_cells), b(nproma, nlev, nblocks_cells))
    CALL meta_data%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind, cdsl_blk_dim_kind])

    CALL init(a)
    CALL alpha%set(a, meta_data)
    b = undef_dp
    CALL alpha%get(b, meta_data)
    ! check:
    CALL check(a ,b, 0.0_dp)

    ! test copy with shortened range:
    m1 = 17 ! size of skipped head
    m2 = 18 ! size of skipped tail
    CALL assert(ncells > m1+m2, file, __LINE__)
    CALL gen_subset_range(some_cells, m1+1, ncells-m1-m2, nproma)
    nsome = subset_range_size(some_cells)
    CALL assert(nsome == ncells-m1-m2, file, __LINE__)
    a = 1.0_dp
    CALL alpha%set(a, meta_data)
    b = 0.0_dp
    CALL alpha%get(b, meta_data, some_cells)
    CALL assert( ABS(SUM(b) - nlev*REAL(nsome, dp)) <= tol, file, __LINE__)
    b = 0.0_dp
    CALL alpha%set(b, meta_data, some_cells)
    CALL alpha%get(b, meta_data)
    CALL assert( ABS(SUM(b) - nlev*REAL(m1+m2, dp)) <= tol, file, __LINE__)
    PRINT*,'check test_dense_ikb_copy passed'
  END SUBROUTINE test_dense_ikb_copy

  SUBROUTINE test_sparse_copy
    INTEGER, PARAMETER :: nlev = 1
    TYPE(contab_type) :: c2e ! cells->edges
    TYPE(sparse_field_type) :: alpha, beta
    TYPE(copy_ce_field_type) :: copy
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_tr
    REAL(dp), ALLOCATABLE :: a(:,:,:), b(:,:,:)
    REAL(dp), ALLOCATABLE :: a_tr(:,:,:), b_tr(:,:,:)
    INTEGER :: dense_size, sparse_size, k_size, k, ic, iie, p

    ! init connectivity table:
    CALL c2e%init(mesh, "CE") ! todo: add loctype-chain interface
    CALL c2e%get_shape(dense_size, sparse_size)

    ! init Dawn data fields:
    CALL alpha%init(c2e, 1)
    CALL beta%init(c2e, 1)

    ! init and Fortran (meta-) data:
    CALL alpha%get_shape(dense_size, sparse_size, k_size)
    ALLOCATE( a(dense_size, sparse_size, k_size), b(dense_size, sparse_size, k_size) )
    CALL meta_data%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_sparse_dim_kind, cdsl_lev_dim_kind])

    ALLOCATE( a_tr(sparse_size, dense_size, k_size), b_tr(sparse_size, dense_size, k_size) )
    CALL meta_data_tr%init(SHAPE(a_tr), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])

    b = 0
    p = 0
    DO k = 1, nlev
      DO ic = 1, dense_size
        DO iie = 1, sparse_size
          p = p + 1
          b(ic, iie, k) = REAL((k*10000 + ic) * 100 + iie, dp)
        ENDDO
      ENDDO
    ENDDO

    DO k = 1, k_size
      b_tr(:,:,k) = TRANSPOSE(b(:,:,k))
    ENDDO

    ! check getter & setter:
    CALL beta%set(b, meta_data)
    a = undef_dp
    CALL beta%get(a, meta_data)
    CALL check(a, b, 0.0_dp)
    a_tr = undef_dp
    CALL beta%get(a_tr, meta_data_tr)
    CALL check(a_tr, b_tr, 0.0_dp)


    CALL beta%set(b_tr, meta_data_tr)
    a_tr = undef_dp
    CALL beta%get(a_tr, meta_data_tr)
    CALL check(a_tr, b_tr, 0.0_dp)

    ! set & check stencil
    a = undef_dp
    CALL alpha%set(a, meta_data)
    CALL copy%init(mesh, k_size, alpha, beta)
    CALL copy%run()
    CALL alpha%get(a, meta_data)
    CALL check(a,b, 0.0_dp)

    PRINT*,'check test_sparse_copy passed'
  END SUBROUTINE test_sparse_copy

  SUBROUTINE test_vertical_field
    TYPE(copy_vertical_field_type) :: stencil
    TYPE(dense_field_type) :: alpha
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_a
    TYPE(vertical_field_type) :: beta
    REAL(dp), ALLOCATABLE :: a(:,:), b(:)
    INTEGER :: dense_size, k_size, nlev
    INTEGER :: k

    nlev = 4

    ! init Dawn data fields
    CALL alpha%init(mesh, cdsl_loctype_cells, nlev)
    CALL assert(.NOT. beta%is_valid(), file, __LINE__)
    CALL beta%init(nlev)
    CALL assert(beta%is_valid(), file, __LINE__)

    ! get & check extends:
    k_size = -1
    CALL alpha%get_shape(dense_size, k_size)
    CALL assert(k_size == nlev, file, __LINE__)
    k_size = -1
    CALL beta%get_size(k_size)
    CALL assert(k_size == nlev, file, __LINE__)

    ! init Fortran fields:
    ALLOCATE( a(dense_size, k_size), b(k_size) )
    CALL meta_data_a%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])

    a = -1.0_dp
    DO k = 1, nlev
      b(k) = k
    ENDDO

    ! Fortran -> Dawn:
    CALL beta%set(b)
    CALL alpha%set(a, meta_data_a)

    ! init & run stencil:
    CALL stencil%init(mesh, k_size, alpha, beta)
    CALL stencil%run()

    ! Dawn -> Fortran & checks
    CALL alpha%get(a, meta_data_a)
    DO k = 1, k_size
      CALL assert(ALL(a(:,k) == b(k)), file, __LINE__)
    ENDDO
    b = -1
    CALL beta%get(b)
    CALL assert(ALL(a(1,:) == b(:)), file, __LINE__)

    PRINT*,'check test_vertical_field passed'
  END SUBROUTINE test_vertical_field

  SUBROUTINE test_reduce_xy(rxy)
    CLASS(reduce_x2y_type) :: rxy
    TYPE(contab_type) :: contab
    TYPE(dense_field_type) :: alpha, beta
    TYPE(sparse_field_type) :: stencil
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_a, meta_data_b, meta_data_s_tr
    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:), s_tr(:,:,:), a_ref(:,:)
    INTEGER, ALLOCATABLE :: x2y_map(:,:), sparse_len(:)
    REAL(dp) :: tol, rsum !unused: beta_range
    INTEGER :: dense_size, sparse_size
    INTEGER :: dense_loctype, sparse_loctype
    INTEGER :: alpha_hsize, beta_hsize, k_size
    INTEGER :: ix, iiy, iy, k

    SELECT TYPE(rxy)
    TYPE is (reduce_c2v_type)
      rxy%chain_str = "CV"
    TYPE is (reduce_e2v_type)
      rxy%chain_str = "EV"
    TYPE is (reduce_e2c_type)
      rxy%chain_str = "EC"
    TYPE is (reduce_v2c_type)
      rxy%chain_str = "VC"
    TYPE is (reduce_c2e_type)
      rxy%chain_str = "CE"
    TYPE is (reduce_v2e_type)
      rxy%chain_str = "VE"
    CLASS default
      CALL f_abort('invalid type', file, __LINE__)
    END SELECT
    dense_loctype =  sym2loctype(rxy%chain_str(1:1))
    sparse_loctype = sym2loctype(rxy%chain_str(2:2))

    ! init connectivity table:
    CALL contab%init(mesh, rxy%chain_str)
    CALL contab%get_shape(dense_size, sparse_size)

    ! init Dawn data fields:
    CALL alpha%init(mesh, dense_loctype, 1)
    CALL beta%init(mesh, sparse_loctype, 1)
    CALL stencil%init(contab, 1) ! todo: we need 2d stencils

    ! init Fortran fields:
    CALL alpha%get_shape(alpha_hsize, k_size)
    CALL assert(k_size == 1, file, __LINE__)
    CALL beta%get_shape(beta_hsize, k_size)
    CALL assert(k_size == 1, file, __LINE__)
    CALL assert(dense_size == alpha_hsize, file, __LINE__)
    ALLOCATE(a(alpha_hsize, k_size), b(beta_hsize, k_size), s_tr(sparse_size, dense_size, k_size))
    CALL meta_data_a%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_b%init(SHAPE(b), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_s_tr%init(SHAPE(s_tr), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(a_ref(alpha_hsize, k_size), x2y_map(sparse_size, dense_size), sparse_len(dense_size))
    CALL contab%get_sparse_len(sparse_len)
    a = undef_dp
    CALL init(b)

    DO k = 1, 1
      DO ix = 1, dense_size
        DO iiy = 1, sparse_size
          s_tr(iiy,ix,k) = REAL(iiy,dp)
        ENDDO
      ENDDO
    ENDDO

    ! set & check stencil
    CALL stencil%set(s_tr, meta_data_s_tr)
    CALL beta%set(b, meta_data_b)

    ! init and run kernel:
    SELECT TYPE(rxy)
    TYPE is (reduce_c2v_type)
      CALL rxy%reduce%init(mesh, k_size, stencil, alpha, beta); CALL rxy%reduce%run()
    TYPE is (reduce_e2v_type)
      CALL rxy%reduce%init(mesh, k_size, stencil, alpha, beta); CALL rxy%reduce%run()
    TYPE is (reduce_e2c_type)
      CALL rxy%reduce%init(mesh, k_size, stencil, alpha, beta); CALL rxy%reduce%run()
    TYPE is (reduce_v2c_type)
      CALL rxy%reduce%init(mesh, k_size, stencil, alpha, beta); CALL rxy%reduce%run()
    TYPE is (reduce_c2e_type)
      CALL rxy%reduce%init(mesh, k_size, stencil, alpha, beta); CALL rxy%reduce%run()
    TYPE is (reduce_v2e_type)
      CALL rxy%reduce%init(mesh, k_size, stencil, alpha, beta); CALL rxy%reduce%run()
    CLASS default
      CALL f_abort('invalid type', file, __LINE__)
    END SELECT

    ! get result:
    CALL alpha%get(a, meta_data_a)

    ! reference solution:
    CALL contab%get_map(x2y_map)
    CALL assert(MINVAL(x2y_map) >= 0, file, __LINE__)
    CALL assert(MAXVAL(x2y_map) <= beta_hsize, file, __LINE__)
    DO k = 1, 1
      DO ix = 1, dense_size

        rsum = 0.0_dp
        DO iiy = 1, sparse_len(ix)
          iy = x2y_map(iiy,ix)
          CALL assert(iy >= 1 .AND. iy <= beta_hsize, file, __LINE__)
          rsum = rsum + s_tr(iiy,ix,k) * b(iy,k)
        ENDDO
        a_ref(ix,1) = rsum

      ENDDO
    ENDDO

#if 0
    ! estimate tolerance (currently not required)
    beta_range = MAXVAL(b) - MINVAL(b)
    tol = ABS(beta_range)*EPSILON(beta_range)
#else
    tol = 0.0_dp
#endif

    CALL check(a, a_ref, tol)
    PRINT*,'check test_reduce_xy('//rxy%chain_str//') passed'

  END SUBROUTINE test_reduce_xy

  SUBROUTINE test_reduce_ce
    TYPE(contab_type) :: c2e ! cells->edges
    TYPE(dense_field_type) :: alpha, beta
    TYPE(sparse_field_type) :: stencil
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_cells, meta_data_edges, meta_data_c2e
    TYPE(ce_reduce_type) :: reduce
    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:), a_ref(:,:), s(:,:,:)
    REAL(dp) :: rsum, tol
    INTEGER :: ncells, nedges, k_size
    INTEGER :: c2e_dense_size, c2e_sparse_size
    INTEGER, ALLOCATABLE :: c2e_map(:,:)
    INTEGER :: ic, iie, ie, k

    ! init connectivity table:
    CALL c2e%init(mesh, "CE")
    CALL c2e%get_shape(c2e_dense_size, c2e_sparse_size)

    ! init Dawn data fields:
    CALL alpha%init(mesh, cdsl_loctype_cells, 1)
    CALL beta%init(mesh, cdsl_loctype_edges, 1)
    CALL stencil%init(c2e, 1) ! todo: we need 2d stencils

    ! init Fortran fields:
    CALL alpha%get_shape(ncells, k_size)
    CALL beta%get_shape(nedges, k_size)
    CALL assert(c2e_dense_size == ncells, file, __LINE__)
    ALLOCATE(a(ncells, k_size), b(nedges, k_size))
    CALL meta_data_cells%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_edges%init(SHAPE(b), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(s(c2e_sparse_size, c2e_dense_size, k_size))
    CALL meta_data_c2e%init(SHAPE(s), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(a_ref(ncells, k_size), c2e_map(c2e_sparse_size, c2e_dense_size))
    a = undef_dp
    CALL init(b)

    DO k = 1, 1
      DO ic = 1, c2e_dense_size
        DO iie = 1, c2e_sparse_size
          s(iie,ic,k) = REAL(iie,dp)
        ENDDO
      ENDDO
    ENDDO

    ! set & check stencil
    CALL stencil%set(s, meta_data_c2e)
    CALL beta%set(b, meta_data_edges)
    CALL reduce%init(mesh, k_size, stencil, alpha, beta)
    CALL reduce%run()
    CALL alpha%get(a, meta_data_cells)

    ! reference solution:
    CALL c2e%get_map(c2e_map)
    CALL assert(MINVAL(c2e_map) >= 1, file, __LINE__)
    CALL assert(MAXVAL(c2e_map) <= nedges, file, __LINE__)

    DO k = 1, 1
      DO ic = 1, c2e_dense_size

        rsum = 0.0_dp
        DO iie = 1, c2e_sparse_size
          ie = c2e_map(iie,ic)
          IF (ie == 0) CYCLE
          CALL assert(ie >= 1 .AND. ie <= nedges, file, __LINE__)
          rsum = rsum + s(iie,ic,k) * b(ie,k)
        ENDDO
        a_ref(ic,1) = rsum

      ENDDO
    ENDDO
    tol = 0.0_dp
    CALL check(a, a_ref, tol)
    PRINT*,'check test_reduce_ce passed'
  END SUBROUTINE test_reduce_ce

  SUBROUTINE test_reduce_ecv
    TYPE(contab_type) :: con ! (edges,cells.verts)
    INTEGER :: dense_size, sparse_size
    TYPE(dense_field_type) :: alpha, beta
    TYPE(sparse_field_type) :: stencil
    TYPE(ecv_reduce_type) :: reduce
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_ecv, meta_data_v, meta_data_e

    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:), a_ref(:,:), s(:,:,:)
    INTEGER, ALLOCATABLE :: map(:,:), sparse_len(:)
    INTEGER :: nedges, nverts, k_size
    INTEGER :: ie, iiv, iv, k
    REAL(dp) :: rsum

    CALL con%init(mesh, "ECV")
    CALL con%get_shape(dense_size, sparse_size)
    ALLOCATE(sparse_len(dense_size))
    CALL con%get_sparse_len(sparse_len)

    ! init Dawn data fields:
    CALL alpha%init(mesh, cdsl_loctype_edges, 1)
    CALL beta%init(mesh, cdsl_loctype_verts, 1)
    CALL stencil%init(con, 1)

    ! init Fortran fields:
    CALL alpha%get_shape(nedges, k_size)
    CALL beta%get_shape(nverts, k_size)
    CALL assert(dense_size == nedges, file, __LINE__)
    ALLOCATE(a(nedges, k_size), b(nverts, k_size))
    CALL meta_data_e%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_v%init(SHAPE(b), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(s(sparse_size, dense_size, k_size))
    CALL meta_data_ecv%init(SHAPE(s), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])

    ALLOCATE(a_ref(nedges, k_size), map(sparse_size, dense_size))
    a = undef_dp
    CALL init(b)

    DO k = 1, 1
      DO ie = 1, dense_size
        DO iiv = 1, sparse_size
          s(iiv,ie,k) = REAL(iiv,dp)
        ENDDO
      ENDDO
    ENDDO


    ! set dawn storage & run kernel
    CALL stencil%set(s, meta_data_ecv)
    CALL beta%set(b, meta_data_v)
    CALL reduce%init(mesh, k_size, stencil, alpha, beta)
    CALL reduce%run()
    CALL alpha%get(a, meta_data_e)

    ! reference values:
    CALL con%get_map(map)

    DO k = 1, 1
      DO ie = 1, nedges

        rsum = 0.0_dp
        DO iiv = 1, sparse_len(ie)
          iv = map(iiv,ie)
          CALL assert(iv >= 1 .AND. iv <= nverts, file, __LINE__)
          rsum = rsum + s(iiv,ie,k) * b(iv,k)
        ENDDO
        a_ref(ie,1) = rsum

      ENDDO
    ENDDO

    CALL check(a, a_ref, relative_tolerance=0.0_dp)
    PRINT*,'check test_reduce_ecv passed'
  END SUBROUTINE test_reduce_ecv

  SUBROUTINE test_reduce_ecv_with_weights1
    TYPE(contab_type) :: con ! (edges,cells.verts)
    INTEGER :: dense_size, sparse_size
    TYPE(dense_field_type) :: alpha, beta
    TYPE(sparse_field_type) :: stencil
    TYPE(ecv_reduce_with_weights1_type) :: reduce
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_ecv, meta_data_v, meta_data_e
    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:), a_ref(:,:), s(:,:,:)
    INTEGER, ALLOCATABLE :: map(:,:), sparse_len(:)
    INTEGER :: nedges, nverts, k_size
    INTEGER :: ie, iiv, iv, k
    REAL(dp) :: rsum, weights(4)

    CALL con%init(mesh, "ECV")
    CALL con%get_shape(dense_size, sparse_size)
    ALLOCATE(sparse_len(dense_size))
    CALL con%get_sparse_len(sparse_len)

    ! init Dawn data fields:
    CALL alpha%init(mesh, cdsl_loctype_edges, 1)
    CALL beta%init(mesh, cdsl_loctype_verts, 1)
    CALL stencil%init(con, 1)

    ! init Fortran fields:
    CALL alpha%get_shape(nedges, k_size)
    CALL beta%get_shape(nverts, k_size)
    CALL assert(dense_size == nedges, file, __LINE__)
    ALLOCATE(a(nedges, k_size), b(nverts, k_size))
    CALL meta_data_e%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_v%init(SHAPE(b), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(s(sparse_size, dense_size, k_size))
    CALL meta_data_ecv%init(SHAPE(s), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(a_ref(nedges, k_size), map(sparse_size, dense_size))
    a = undef_dp
    CALL init(b)

    DO k = 1, 1
      DO ie = 1, dense_size
        DO iiv = 1, sparse_size
          s(iiv,ie,k) = REAL(iiv,dp)
        ENDDO
      ENDDO
    ENDDO

    ! set dawn storage & run kernel
    CALL stencil%set(s, meta_data_ecv)
    CALL beta%set(b, meta_data_v)
    CALL reduce%init(mesh, k_size, stencil, alpha, beta)
    CALL reduce%run()
    CALL alpha%get(a, meta_data_e)

    ! reference values:
    CALL con%get_map(map)
    weights = [1, 2, 3, 4]
    DO k = 1, 1
      DO ie = 1, nedges

        rsum = 0.0_dp
        call assert(sparse_len(ie) == size(weights), file, __LINE__)
        DO iiv = 1, sparse_len(ie)
          iv = map(iiv,ie)
          CALL assert(iv >= 1 .AND. iv <= nverts, file, __LINE__)
          rsum = rsum + s(iiv,ie,k) * b(iv,k) * weights(iiv)
        ENDDO
        a_ref(ie,1) = rsum

      ENDDO
    ENDDO

    CALL check(a, a_ref, relative_tolerance=0.0_dp)
    PRINT*,'check test_reduce_ecv_with_weights1 passed'
  END SUBROUTINE test_reduce_ecv_with_weights1

  SUBROUTINE test_reduce_ecv_with_weights2
    TYPE(contab_type) :: con ! (edges,cells.verts)
    INTEGER :: dense_size, sparse_size
    TYPE(dense_field_type) :: alpha, beta
    TYPE(sparse_field_type) :: stencil
    TYPE(ecv_reduce_with_weights2_type) :: reduce
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_ecv, meta_data_v, meta_data_e

    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:), a_ref(:,:), s(:,:,:), initial_a(:,:)
    INTEGER, ALLOCATABLE :: map(:,:), sparse_len(:)
    INTEGER :: nedges, nverts, k_size
    INTEGER :: ie, iiv, iv, k
    REAL(dp) :: rsum, weights(4)

    CALL con%init(mesh, "ECV")
    CALL con%get_shape(dense_size, sparse_size)
    ALLOCATE(sparse_len(dense_size))
    CALL con%get_sparse_len(sparse_len)

    ! init Dawn data fields:
    CALL alpha%init(mesh, cdsl_loctype_edges, 1)
    CALL beta%init(mesh, cdsl_loctype_verts, 1)
    CALL stencil%init(con, 1)

    ! init Fortran fields:
    CALL alpha%get_shape(nedges, k_size)
    CALL beta%get_shape(nverts, k_size)
    CALL assert(dense_size == nedges, file, __LINE__)
    ALLOCATE(a(nedges, k_size), b(nverts, k_size))
    CALL meta_data_e%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_v%init(SHAPE(b), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(s(sparse_size, dense_size, k_size))
    CALL meta_data_ecv%init(SHAPE(s), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(a_ref(nedges, k_size), initial_a(nedges, k_size), map(sparse_size, dense_size))
    a = undef_dp
    CALL init(initial_a)
    initial_a = initial_a + 1
    CALL init(b)

    DO k = 1, 1
      DO ie = 1, dense_size
        DO iiv = 1, sparse_size
          s(iiv,ie,k) = REAL(iiv,dp)
        ENDDO
      ENDDO
    ENDDO


    ! set dawn storage & run kernel
    CALL stencil%set(s, meta_data_ecv)
    CALL alpha%set(initial_a, meta_data_e)
    CALL beta%set(b, meta_data_v)
    CALL reduce%init(mesh, k_size, stencil, alpha, beta)
    CALL reduce%run()
    CALL alpha%get(a,  meta_data_e)

    ! reference values:
    CALL con%get_map(map)
    weights = [1, 2, 3, 4]
    DO k = 1, 1
      DO ie = 1, nedges

        rsum = 0.0_dp
        call assert(sparse_len(ie) == size(weights), file, __LINE__)
        weights(2) = initial_a(ie,k)
        DO iiv = 1, sparse_len(ie)
          iv = map(iiv,ie)
          CALL assert(iv >= 1 .AND. iv <= nverts, file, __LINE__)
          rsum = rsum + s(iiv,ie,k) * b(iv,k) * weights(iiv)
        ENDDO
        a_ref(ie,1) = rsum

      ENDDO
    ENDDO

    CALL check(a, a_ref, relative_tolerance=0.0_dp)
    PRINT*,'check test_reduce_ecv_with_weights2 passed'
  END SUBROUTINE test_reduce_ecv_with_weights2

  SUBROUTINE test_dense_meta_data
    TYPE(dense_field_type) :: alpha
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_a
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_b1, meta_data_b2, meta_data_b3
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_c1, meta_data_c2, meta_data_c3
    INTEGER, PARAMETER :: nlev = 1, nproma=3
    REAL(dp), ALLOCATABLE :: a(:,:,:), b1(:,:,:), b2(:,:,:), b3(:,:,:), c1(:,:,:), c2(:,:,:), c3(:,:,:)
    INTEGER :: nblocks, dense_size, npromz
    INTEGER :: ia, ic, k, k_size

    ! init Dawn data field
    CALL alpha%init(mesh, cdsl_loctype_cells, nlev)

    CALL alpha%get_shape(dense_size, k_size)
    CALL assert(nlev == k_size, file, __LINE__)

    CALL gen_block_coords(dense_size, nproma, nblocks, npromz)
    CALL assert((nblocks-1)*nproma + npromz  == dense_size, file, __LINE__)

    ALLOCATE( a(nproma, nblocks, nlev), &
         ! different positions of level dim:
         &    b1(nproma, nblocks, nlev), &
         &    b2(nproma, nlev, nblocks), &
         &    b3(nlev, nproma, nblocks), &
         ! swap idx and blk dims:
         &    c1(nblocks, nproma, nlev), &
         &    c2(nblocks, nlev, nproma), &
         &    c3(nlev, nblocks, nproma) &
         & )

    CALL meta_data_a%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_blk_dim_kind, cdsl_lev_dim_kind])

    CALL meta_data_b1%init(SHAPE(b1), [cdsl_idx_dim_kind, cdsl_blk_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_b2%init(SHAPE(b2), [cdsl_idx_dim_kind, cdsl_lev_dim_kind, cdsl_blk_dim_kind])
    CALL meta_data_b3%init(SHAPE(b3), [cdsl_lev_dim_kind, cdsl_idx_dim_kind, cdsl_blk_dim_kind])

    CALL meta_data_c1%init(SHAPE(c1), [cdsl_blk_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_c2%init(SHAPE(c2), [cdsl_blk_dim_kind, cdsl_lev_dim_kind, cdsl_idx_dim_kind])
    CALL meta_data_c3%init(SHAPE(c3), [cdsl_lev_dim_kind, cdsl_blk_dim_kind, cdsl_idx_dim_kind])

    a = 0.0_dp
    b1 = -1
    b2 = -2
    b3 = -3
    c1 = -4
    c2 = -5
    c3 = -6

    DO k = 1, nlev
      DO ic = 1, nblocks
        DO ia = 1, nproma
          a(ia, ic, k) = REAL((k*100000 + ic) * 100 + ia, dp)
        ENDDO
      ENDDO
      ic = nblocks
      DO ia = npromz+1, nproma
        a(ia, ic, k) = 0.0_dp
        b1(ia, ic, k) = 0.0_dp
        b2(ia, k, ic) = 0.0_dp
        b3(k, ia, ic) = 0.0_dp
        c1(ic, ia, k) = 0.0_dp
        c2(ic, k, ia) = 0.0_dp
        c3(k, ic, ia) = 0.0_dp
      ENDDO
    ENDDO

    CALL alpha%set(a, meta_data_a)
    CALL alpha%get(b1, meta_data_b1)
    CALL alpha%get(b2, meta_data_b2)
    CALL alpha%get(b3, meta_data_b3)
    CALL alpha%get(c1, meta_data_c1)
    CALL alpha%get(c2, meta_data_c2)
    CALL alpha%get(c3, meta_data_c3)

    DO k = 1, nlev
      CALL check(a(:,:,k), b1(:,:,k), relative_tolerance=0.0_dp)
      CALL check(a(:,:,k), b2(:,k,:), relative_tolerance=0.0_dp)
      CALL check(a(:,:,k), b3(k,:,:), relative_tolerance=0.0_dp)
    ENDDO

    DO ic = 1, nblocks
      DO ia = 1, nproma

        CALL check(a(ia,ic,:), c1(ic,ia,:), relative_tolerance=0.0_dp)
        CALL check(a(ia,ic,:), c2(ic,:,ia), relative_tolerance=0.0_dp)
        CALL check(a(ia,ic,:), c3(:,ic,ia), relative_tolerance=0.0_dp)
      ENDDO
    ENDDO

    PRINT*,'check test_dense_meta_data passed'
  END SUBROUTINE test_dense_meta_data

  SUBROUTINE test_sparse_meta_data
    TYPE(contab_type) :: c2e ! cells->edges
    TYPE(sparse_field_type) :: stencil
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_b1, meta_data_b2, meta_data_b3
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_c1, meta_data_c2, meta_data_c3
    INTEGER, PARAMETER :: nlev = 2
    REAL(dp), ALLOCATABLE :: a(:,:,:), b1(:,:,:), b2(:,:,:), b3(:,:,:), c1(:,:,:), c2(:,:,:), c3(:,:,:)
    INTEGER :: c2e_dense_size, c2e_sparse_size
    INTEGER :: iie, ic, k

    ! init connectivity table:
    CALL c2e%init(mesh, "CE")
    CALL c2e%get_shape(c2e_dense_size, c2e_sparse_size)

    ALLOCATE( a(c2e_sparse_size, c2e_dense_size, nlev), &
         ! different positions of level dim:
         &    b1(c2e_sparse_size, c2e_dense_size, nlev), &
         &    b2(c2e_sparse_size, nlev, c2e_dense_size), &
         &    b3(nlev, c2e_sparse_size, c2e_dense_size), &
         ! swap sparse and dense dims:
         &    c1(c2e_dense_size, c2e_sparse_size, nlev), &
         &    c2(c2e_dense_size, nlev, c2e_sparse_size), &
         &    c3(nlev, c2e_dense_size, c2e_sparse_size) &
         & )

    CALL meta_data_b1%init(SHAPE(b1), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_b2%init(SHAPE(b2), [cdsl_sparse_dim_kind, cdsl_lev_dim_kind, cdsl_idx_dim_kind])
    CALL meta_data_b3%init(SHAPE(b3), [cdsl_lev_dim_kind, cdsl_sparse_dim_kind, cdsl_idx_dim_kind])

    CALL meta_data_c1%init(SHAPE(c1), [cdsl_idx_dim_kind, cdsl_sparse_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_c2%init(SHAPE(c2), [cdsl_idx_dim_kind, cdsl_lev_dim_kind, cdsl_sparse_dim_kind])
    CALL meta_data_c3%init(SHAPE(c3), [cdsl_lev_dim_kind, cdsl_idx_dim_kind, cdsl_sparse_dim_kind])

    a = 0
    DO k = 1, nlev
      DO ic = 1, c2e_dense_size
        DO iie = 1, c2e_sparse_size
          a(iie, ic, k) = REAL((k*10000 + ic) * 100 + iie, dp)
        ENDDO
      ENDDO
    ENDDO

    b1 = -1
    b2 = -2
    b3 = -3
    c1 = -4
    c2 = -5
    c3 = -6

    CALL stencil%init(c2e, nlev)
    CALL stencil%set(a, meta_data_b1)
    CALL stencil%get(b1, meta_data_b1)
    CALL stencil%get(b2, meta_data_b2)
    CALL stencil%get(b3, meta_data_b3)
    CALL stencil%get(c1, meta_data_c1)
    CALL stencil%get(c2, meta_data_c2)
    CALL stencil%get(c3, meta_data_c3)

    DO k = 1, nlev
      CALL check(a(:,:,k), b1(:,:,k), relative_tolerance=0.0_dp)
      CALL check(a(:,:,k), b2(:,k,:), relative_tolerance=0.0_dp)
      CALL check(a(:,:,k), b3(k,:,:), relative_tolerance=0.0_dp)
    ENDDO

    DO ic = 1, c2e_dense_size
      DO iie = 1, c2e_sparse_size

        CALL check(a(iie,ic,:), c1(ic,iie,:), relative_tolerance=0.0_dp)
        CALL check(a(iie,ic,:), c2(ic,:,iie), relative_tolerance=0.0_dp)
        CALL check(a(iie,ic,:), c3(:,ic,iie), relative_tolerance=0.0_dp)
      ENDDO
    ENDDO

    PRINT*,'check test_sparse_meta_data passed'
  END SUBROUTINE test_sparse_meta_data

END PROGRAM test_unstruct_stencils
