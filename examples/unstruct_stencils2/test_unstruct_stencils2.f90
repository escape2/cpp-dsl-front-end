PROGRAM test_unstruct_stencils
  ! todo: rename to cdsl_dawn_unstruct to avoid name collisions
  USE mesh_reader, ONLY: init_mesh_support, mesh_type, gen_atlas_mesh, sym2loctype
  USE dawn_unstruct
  USE dawn_unstruct_stencils2
  USE example_support, ONLY: dp, init, assert, check, undef_dp
  IMPLICIT NONE

  CHARACTER(len=255):: gridfile
  TYPE(mesh_type) :: mesh

  CHARACTER(len=*), PARAMETER :: file = &
       & __FILE__

  CALL init_test

  CALL test_ec_reduce2
  CALL test_offset_reduce
  CALL test_globals

CONTAINS

  SUBROUTINE init_test
    USE iso_c_binding, ONLY : c_double
    CALL assert(dp == c_double, file, __LINE__)
    CALL GET_COMMAND_ARGUMENT(1,gridfile)
    CALL init_mesh_support(gridfile)
    mesh = gen_atlas_mesh()
  END SUBROUTINE init_test

  SUBROUTINE test_ec_reduce2
    TYPE(contab_type) :: e2c_con ! edges->cells
    TYPE(dense_field_type) :: alpha, beta
    TYPE(sparse_field_type) :: stencil
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_edges, meta_data_cells, meta_data_e2c
    TYPE(ec_reduce2_type) :: reduce
    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:), a_ref(:,:), s(:,:,:)
    REAL(dp) :: rsum, tol
    INTEGER :: ncells, nedges, k_size
    INTEGER :: e2c_dense_size, e2c_sparse_size
    INTEGER, ALLOCATABLE :: e2c_map(:,:)
    INTEGER :: ie, iic, ic, k


    ! init connectivity table:
    CALL e2c_con%init(mesh, "EC")
    CALL e2c_con%get_shape(e2c_dense_size, e2c_sparse_size)

    ! init Dawn data fields:
    CALL alpha%init(mesh, cdsl_loctype_edges, 1)
    CALL beta%init(mesh, cdsl_loctype_cells, 1)
    CALL stencil%init(e2c_con, 1)

    ! init Fortran fields:
    CALL alpha%get_shape(nedges, k_size)
    CALL beta%get_shape(ncells, k_size)
    CALL assert(e2c_dense_size == nedges, file, __LINE__)
    ALLOCATE(a(nedges, k_size), b(ncells, k_size))

    CALL meta_data_edges%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    CALL meta_data_cells%init(SHAPE(b), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(s(e2c_sparse_size, e2c_dense_size, k_size))
    CALL meta_data_e2c%init(SHAPE(s), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    ALLOCATE(a_ref(nedges, k_size), e2c_map(e2c_sparse_size, e2c_dense_size))
    a = undef_dp
    CALL init(b)
    b = 1.0_dp
    DO k = 1, 1
      DO ie = 1, e2c_dense_size
        DO iic = 1, e2c_sparse_size
          s(iic,ie,k) = REAL(ie*0 + iic,dp)
        ENDDO
      ENDDO
    ENDDO

    ! set & check stencil
    CALL stencil%set(s, meta_data_e2c)

    CALL beta%set(b, meta_data_cells)

    CALL reduce%init(mesh, k_size, stencil, alpha, beta)

    CALL reduce%run()

    CALL alpha%get(a, meta_data_edges)

    ! reference solution:
    CALL e2c_con%get_map(e2c_map)
    CALL assert(MINVAL(e2c_map) >= 1, file, __LINE__)
    CALL assert(MAXVAL(e2c_map) <= ncells, file, __LINE__)

    DO k = 1, 1
      DO ie = 1, e2c_dense_size

        rsum = 0.0_dp
        DO iic = 1, e2c_sparse_size
          ic = e2c_map(iic,ie)
          IF (ic == 0) CYCLE
          CALL assert(ic >= 1 .AND. ic <= ncells, file, __LINE__)
          rsum = rsum + s(iic,ie,k) * b(ic,k)
        ENDDO
        a_ref(ie,1) = rsum * 2

      ENDDO
    ENDDO
    tol = 0.0_dp
    CALL check(a, a_ref, tol)

    PRINT*,'check test_ec_reduce2 passed'

  END SUBROUTINE test_ec_reduce2

  SUBROUTINE test_offset_reduce
    INTEGER, PARAMETER :: nlev = 1
    TYPE(contab_type) :: ece_con, ec_con
    TYPE(offset_reduce_type) :: reduce
    TYPE(cdsl_unstruct_meta_data_type) :: e_md, ec_md, ece_md
    REAL(dp), ALLOCATABLE :: raw_diam_coeff(:,:,:), e2c_aux(:,:,:), out_vn_e(:,:), prism_thick_e(:,:), ref(:,:)
    TYPE(dense_field_type) :: out_vn_e_cdsl, prism_thick_e_cdsl
    TYPE(sparse_field_type) :: raw_diam_coeff_cdsl, e2c_aux_cdsl
    INTEGER, ALLOCATABLE :: ec_map(:,:), ece_map(:,:)
    REAL(dp) :: s, t, weight, reltol
    INTEGER :: nedges, offsets(4)
    INTEGER :: ec_dense_size, ec_sparse_size
    INTEGER :: ece_dense_size, ece_sparse_size
    INTEGER :: ie, ie2, is, k

    ! init connectivity tables:
    CALL ec_con%init(mesh, "EC")
    CALL ec_con%get_shape(ec_dense_size, ec_sparse_size)
    nedges = ec_dense_size
    CALL ece_con%init(mesh, "ECE")
    CALL ece_con%get_shape(ece_dense_size, ece_sparse_size)
    CALL assert(ece_dense_size == nedges, file, __LINE__)

    ! init Fortran fields:
    ALLOCATE(raw_diam_coeff(ece_sparse_size, nedges, nlev), &
         &   e2c_aux(ec_sparse_size, nedges, nlev), &
         &   out_vn_e(nedges, nlev), &
         &   prism_thick_e(nedges, nlev) )

    e2c_aux = undef_dp
    prism_thick_e = undef_dp
    raw_diam_coeff = undef_dp

    DO k = 1, nlev
      DO ie = 1, nedges
        prism_thick_e(ie,k) = REAL(ie*10 + k, dp)
        DO is = 1, ece_sparse_size
          raw_diam_coeff(is, ie, k) = REAL(ie*100 + k*10 + is, dp)
        ENDDO
        DO is = 1, ec_sparse_size
          e2c_aux(is, ie, k) = REAL(ie*100 + k*10 + is, dp)
        ENDDO
      ENDDO
    ENDDO

    out_vn_e = undef_dp

    ! int meta data:
    CALL e_md%init( SHAPE(out_vn_e), [cdsl_idx_dim_kind, cdsl_lev_dim_kind] )
    CALL ec_md%init( SHAPE(e2c_aux), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind] )
    CALL ece_md%init( SHAPE(raw_diam_coeff), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind] )

    ! init cdsl data fields:
    CALL out_vn_e_cdsl%init(mesh, cdsl_loctype_edges, nlev)
    CALL prism_thick_e_cdsl%init(mesh, cdsl_loctype_edges, nlev) ! todo: use this info to impl. check of dense meta dataxs
    CALL raw_diam_coeff_cdsl%init(ece_con, nlev)
    CALL e2c_aux_cdsl%init(ec_con, nlev)

    ! set cdsl data fields
    CALL e2c_aux_cdsl%set(e2c_aux, ec_md)
    CALL raw_diam_coeff_cdsl%set(raw_diam_coeff, ece_md)
    CALL prism_thick_e_cdsl%set(prism_thick_e, e_md)
    CALL out_vn_e_cdsl%set(out_vn_e, e_md)


    ! reference solution:
    ALLOCATE(ec_map(ec_sparse_size, nedges), ece_map(ece_sparse_size, nedges), ref(nedges, nlev))
    CALL out_vn_e_cdsl%get(ref, e_md)

    CALL ec_con%get_map(ec_map)
    CALL ece_con%get_map(ece_map)
    CALL assert(ece_sparse_size == 4, file, __LINE__)
    ref = undef_dp
    offsets = [0, 0, 1, 1]
    DO k = 1, nlev
      DO ie = 1, nedges
        s = 0.0_dp
        DO is = 1, ece_sparse_size
          weight = e2c_aux(1 + offsets(is), ie, k)
          ie2 = ece_map(is, ie)
          t = raw_diam_coeff(is,ie,k) * prism_thick_e(ie2,k) * weight
          s = s + t

        ENDDO
        ref(ie,k) = s
      ENDDO
    ENDDO

    ! init & run kernel
    CALL reduce%init(mesh, nlev, raw_diam_coeff_cdsl, out_vn_e_cdsl, prism_thick_e_cdsl, e2c_aux_cdsl)
    CALL reduce%run()

    ! get result:
    CALL out_vn_e_cdsl%get(out_vn_e, e_md)

    reltol = 1.0E-15_dp
    CALL check(out_vn_e, ref, reltol)

    PRINT*,'check test_offset_reduce passed'

  END SUBROUTINE test_offset_reduce

  SUBROUTINE test_globals
    INTEGER, PARAMETER :: nlev = 2
    TYPE(dense_field_type) :: alpha, beta
    TYPE(cdsl_unstruct_meta_data_type) :: e_md
    TYPE(test_globals_type) :: tglob
    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:)
    REAL(dp) :: g1, g2
    INTEGER:: nedges, k_size

    ! init Dawn data fields:
    CALL alpha%init(mesh, cdsl_loctype_edges, nlev)
    CALL beta%init(mesh, cdsl_loctype_edges, nlev)

    ! init Fortran fields:
    CALL alpha%get_shape(nedges, k_size)
    ALLOCATE(a(nedges, k_size), b(nedges, k_size))
    CALL tglob%init(mesh, k_size, alpha, beta)

    ! init meta data:
    CALL e_md%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])

    ! check init values:
    g1 = tglob%get_first_double()
    g2 = tglob%get_second_double()
    CALL assert(g1 == 1.0_dp .and. g2 == 2.0_dp, file, __LINE__)

    ! check getter & setter:
    CALL tglob%set_first_double(3.0_dp)
    CALL tglob%set_second_double(4.0_dp)
    g1 = tglob%get_first_double()
    g2 = tglob%get_second_double()
    CALL assert(g1 == 3.0_dp .and. g2 == 4.0_dp, file, __LINE__)

    ! check CDSL access
    CALL tglob%run()
    a = 0
    b = 0
    CALL alpha%get(a, e_md)
    CALL beta%get(b, e_md)
    CALL assert(ALL(a == g1 .AND. b == g2), file, __LINE__)

    PRINT*,'check test_g lobals passed'

  END SUBROUTINE test_globals

END PROGRAM test_unstruct_stencils
