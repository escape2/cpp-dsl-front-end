
#include <stdio.h>
#include "driver-includes/defs.hpp"
#include "cuda_copy.h"

__global__ void cuda_copy_dawn_float_data_kernel(dawn::float_type* data_out,
                                                 const dawn::float_type* data_in,
                                                 int data_size, int tchunk_size) {
  int tid = blockIdx.x;
  printf("***cuda: tid=%d\n",tid);
  int istart = tchunk_size * tid;
  int iend = istart + tchunk_size -1;
  if ( istart >= data_size) return;
  if ( iend >- data_size) iend = data_size -1;

  for (int i = 0; i < 4; i++) {
    printf("***cuda: i=%d, in=%f\n",i,data_in[i]);
  }

  for (int i = istart; i <= iend; i++) {
    data_out[i] = data_in[i];
  }

  for (int i = 0; i < 4; i++) {
    printf("***cuda: i=%d, out=%f\n",i,data_out[i]);
  }

}


extern "C" {

  void cuda_copy_c(dawn::float_type* data_out,
                   const dawn::float_type* data_in,
                   int data_size) {
    int tchunk_size = data_size;
    printf("***cuda_copy_c mark0, data_size=%d\n",data_size);
    cuda_copy_dawn_float_data_kernel<<<1,1>>>(data_out, data_in, data_size, tchunk_size);
      printf("***cuda_copy_c mark1\n");
  //   cuda_copy_dawn_float_data(*field_out_pt, *field_in_pt);
      cudaDeviceSynchronize();
  }

}
