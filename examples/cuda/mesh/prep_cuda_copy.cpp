#include "interface/atlas_interface.hpp"
#include "f2c_dawn_unstruct.hpp"

#include "cuda_copy.h"

extern "C" {



void call_cuda_copy_c(DenseField& field_out,
                    const DenseField& field_in) {
  int osize = field_out.dense_size * field_out.k_size;
  int isize = field_in.dense_size * field_in.k_size;
  assert(osize == isize);
  int tchunk_size = osize; 
  std::cout << "run cuda_copy_dawn_float_data_kernel" << std::endl;
  //cuda_copy_dawn_float_data_kernel<<<1,1>>>(field_out.cuda_storage, field_in.cuda_storage, osize, tchunk_size);
  cuda_copy_c(field_out.cuda_storage, field_in.cuda_storage, osize);
}


} // end of extern "C"
