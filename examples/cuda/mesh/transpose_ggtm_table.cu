
#include <cstdio>

__global__ void transpose_gpu_table_kernel(const int* gpu_table, int *tr_gpu_table,
                                           int dense_size, int sparse_size, int tchunk_size) {

  int tid = blockIdx.x;
  int istart = tchunk_size * tid;
  int iend = istart + tchunk_size -1;

  if ( istart >= dense_size) return;
  if ( iend >- dense_size) iend = dense_size -1;
  
  for (int i = istart; i <= iend; i++) {
    for (int j = 0; j < sparse_size; j++) {
      int p = i * sparse_size + j;
      int q = j * dense_size + i;
      tr_gpu_table[q] = gpu_table[p];
    }
  }

#ifdef GPU_DEBUG_MODE
  for (int i = istart; i <4; i++) {
    for (int j = 0; j < sparse_size; j++) {
      int p = i * sparse_size + j;
      int q = j * dense_size + i;
      printf("from kernel: i=%d, j=%d, val=%d, tr_val=%d\n",i,j,gpu_table[p], tr_gpu_table[q]);
    }
  }
#endif

}

void transpose_gpu_table(const int* gpu_table, int *tr_gpu_table, int dense_size, int sparse_size, int tchunk_size) {
  transpose_gpu_table_kernel<<<1,1>>>(gpu_table, tr_gpu_table, dense_size, sparse_size, tchunk_size);
}

extern"C" {

  void transpose_gpu_table_c(const int* gpu_table, int *tr_gpu_table, int dense_size, int sparse_size, int tchunk_size) {
    transpose_gpu_table(gpu_table, tr_gpu_table, dense_size, sparse_size, tchunk_size);
  }

}
