extern "C" {

  void cuda_copy_c(dawn::float_type* data_out,
                   const dawn::float_type* data_in,
                   int data_size);

}
