MODULE f2c_get_device_con_map
  USE iso_c_binding, ONLY : c_int, c_ptr
  IMPLICIT NONE

  INTERFACE

    TYPE(c_ptr) FUNCTION f2c_atlas2ggtm_f(cdsl_mesh_pt) &
         & BIND(c, name='atlas2ggtm_c')
      USE iso_c_binding, ONLY: c_ptr
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: cdsl_mesh_pt
    END FUNCTION f2c_atlas2ggtm_f

  END INTERFACE

END MODULE f2c_get_device_con_map
