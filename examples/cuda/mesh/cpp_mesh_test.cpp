#include <memory>

#include <atlas/mesh.h>

#include "driver-includes/unstructured_interface.hpp"
#include "interface/atlas_interface.hpp"

#include "f2c_dawn_unstruct.hpp"
#include "cuda_helpers.hpp"

void transpose_gpu_table(const int* gpu_table, int *tr_gpu_table, int dense_size, int sparse_size, int tchunk_size);


void check_mesh(const cdsl::Mesh *cdsl_mesh_pt,const std::string &chain_str) {

  atlas::Mesh &atlas_mesh = *cdsl_mesh_pt->get_atlas_mesh_pt();
  Contab con(atlas_mesh, chain_str);
  auto chain = con.chain;
  size_t dense_size = con.dense_size;
  size_t sparse_size = con.sparse_size;
  size_t table_size = dense_size * sparse_size;

  dawn::GlobalGpuTriMesh &ggtm = *cdsl_mesh_pt->get_ggtm_pt();
  assert(dense_size == get_dense_GlobalGpuTriMesh_size(ggtm, chain));
  assert(sparse_size == get_sparse_GlobalGpuTriMesh_size(chain));

  bool includeCenter = false;
  int* gpu_table = ggtm.NeighborTables[{chain, includeCenter}];

  auto tr_gpu_table = unique_cuda_malloc<int>(table_size);

 
  int tchunk_size = 1;
  transpose_gpu_table(gpu_table, tr_gpu_table.get(), dense_size, sparse_size, tchunk_size);

  auto cpu_table = std::make_unique<int[]>(table_size);
 

  auto tr_cpu_table = std::make_unique<int[]>(table_size);
  gpuErrchk(cudaMemcpy(cpu_table.get(), gpu_table, table_size * sizeof(int), cudaMemcpyDeviceToHost));
  gpuErrchk(cudaMemcpy(tr_cpu_table.get(), tr_gpu_table.get(), table_size * sizeof(int), cudaMemcpyDeviceToHost));

  for(int i=0; i<dense_size; i++) {
    for(int j=0; j<sparse_size; j++) {
      int p = i*sparse_size + j;
      int q = j*dense_size + i;
      assert(cpu_table[p] == tr_cpu_table[q]);
    }
  }


  std::cout << "chain:" << chain_str
            << ", dense_size = " << dense_size
            << ", sparse_size = " << sparse_size
            << ", status: passed"
            << std::endl;

}

extern "C" {


  void cpp_check_mesh_c(const cdsl::Mesh *cdsl_mesh_pt) {
    for (auto chain : {"EC","EV", "CE", "CV", "VC", "VE", "ECV", "ECE"} ) {
      check_mesh(cdsl_mesh_pt, chain);
    }
  }

}
