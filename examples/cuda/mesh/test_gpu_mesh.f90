PROGRAM test_global_mesh
  USE iso_c_binding, ONLY: c_ptr
  USE mesh_reader, ONLY: init_mesh_support, mesh_type, gen_atlas_mesh, sym2loctype
  USE dawn_unstruct
  USE example_support, ONLY: dp, init, assert, check
  IMPLICIT NONE

  CHARACTER(len=255):: gridfile
  TYPE(mesh_type) :: mesh

  CHARACTER(len=*), PARAMETER :: file = &
       & __FILE__

  INTERFACE
    TYPE(c_ptr) FUNCTION f2c_atlas2ggtm_f(cdsl_mesh_pt) &
         & BIND(c, name='atlas2ggtm_c')
      USE iso_c_binding, ONLY: c_ptr
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: cdsl_mesh_pt
    END FUNCTION f2c_atlas2ggtm_f
  END INTERFACE

  INTERFACE
    SUBROUTINE check_mesh_f(cdsl_mesh_pt) &
         & BIND(c, name='cpp_check_mesh_c')
      USE iso_c_binding, ONLY: c_ptr
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: cdsl_mesh_pt
    END SUBROUTINE check_mesh_f
  END INTERFACE

  TYPE ggtm_type
    type(c_ptr) :: cpt
  END TYPE ggtm_type

  CALL init_test

  CALL test_gpu_mesh

CONTAINS

  SUBROUTINE cpp_mesh_test(cdsl_mesh)
    TYPE(mesh_type) :: cdsl_mesh
    CALL check_mesh_f(cdsl_mesh%cpt)
  END SUBROUTINE cpp_mesh_test

  SUBROUTINE init_test
    USE iso_c_binding, ONLY : c_double
    CALL assert(dp == c_double, file, __LINE__)
    CALL GET_COMMAND_ARGUMENT(1,gridfile)
    PRINT*,'gridfile=',TRIM(gridfile)
    CALL init_mesh_support(gridfile)
    mesh = gen_atlas_mesh()
  END SUBROUTINE init_test

  SUBROUTINE test_gpu_mesh
    CALL cpp_mesh_test(mesh)
    PRINT*,'test_gpu_mesh passed'
  END SUBROUTINE test_gpu_mesh

END PROGRAM test_global_mesh
