#include <iostream>
#include "driver-includes/defs.hpp"

void host_copy_dawn_float_data(dawn::float_type* data_out,
                               const dawn::float_type* data_in,
                               int data_size) {

  int istart = 0;
  int iend = data_size -1;

  for (int i = istart; i <= iend; i++) {
    data_out[i] = data_in[i];
  }

}


extern "C" {

  void host_copy_dawn_float_data_c(dawn::float_type* data_out,
                                   const dawn::float_type* data_in,
                                   int data_size) {
    std::cout << "run host_copy_dawn_float_data_kernel" << std::endl;
    host_copy_dawn_float_data(data_out, data_in, data_size);
  }

}
