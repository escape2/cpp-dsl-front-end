
#include <iostream>

#include "driver-includes/cuda_utils.hpp"

#define vec_size 32

__global__ void scale_x_to_y(int *x, int *y) {
  int i = blockIdx.x;
  if (i < vec_size) {
    y[i] = 2 * x[i];
  }
}

int main() {

  int host_x[vec_size], host_y[vec_size];
  int *dev_x, *dev_y;

  gpuErrchk(cudaMalloc((void **)&dev_x, vec_size * sizeof(int)));
  gpuErrchk(cudaMalloc((void **)&dev_y, vec_size * sizeof(int)));

  for (int i = 0; i < vec_size; ++i) {
    host_x[i] = i;
    host_y[i] = 0;
  }

  gpuErrchk(cudaMemcpy(dev_x, host_x, vec_size * sizeof(int), cudaMemcpyHostToDevice));

  scale_x_to_y<<<vec_size, 1>>>(dev_x, dev_y);

  gpuErrchk(cudaMemcpy(host_y, dev_y, vec_size * sizeof(int), cudaMemcpyDeviceToHost));

  int nerr = 0;
  for (int i = 0; i < vec_size; ++i) {
    if (host_y[i] != 2 * host_x[i])
      nerr++;
  }

  gpuErrchk(cudaFree(dev_x));
  gpuErrchk(cudaFree(dev_y));

  if (nerr) {
    std::cerr << "nerr = " << nerr << std::endl;
    return 1;
  }

  std::cerr << "test_cuda passed." << std::endl;

  return 0;
}
