#!/bin/bash

set -e

if [[ ! -x "$CDSL_CUDA_LAUNCHER" ]]; then
    echo "Cannot execute $CDSL_CUDA_LAUNCHER." 1>&2
    exit 1
fi
echo "CDSL_CUDA_LAUNCHER=$CDSL_CUDA_LAUNCHER"

echo "CDSL_ROOT=$CDSL_ROOT"
if [[ -z "$CDSL_ROOT" ]]; then
    echo "$0: CDSL_ROOT is not set."
    exit 1
fi

CDSL_DOWNLOAD_DIR=$CDSL_ROOT/.download

# set gridfile:
GRIDFILE=${GRIDFILE:-$(ls -1 $CDSL_DOWNLOAD_DIR | grep '.nc$'| head -n1)}

if [[ -z $GRIDFILE ]]; then
    echo "Downloading gridfile from http://icon-downloads.mpimet.mpg.de:" 1>&2
    GRIDFILE="icon_grid_0030_R02B03_G.nc"
    # expect about 4.4 MB of file size
    wget 'http://icon-downloads.mpimet.mpg.de/grids/public/mpim/0030'/$GRIDFILE 1>&2
    mv $GRIDFILE  $CDSL_DOWNLOAD_DIR/
else
    echo "Using exisiting GRIDFILE: $CDSL_DOWNLOAD_DIR/$GRIDFILE"    
fi

GRIDPATH=$CDSL_DOWNLOAD_DIR/$GRIDFILE

if [ ! -f "$GRIDPATH" ]; then
    echo "$GRIDPATH does not exist."
    exit 1
fi

echo "[cmd]=[$CDSL_CUDA_LAUNCHER $* $GRIDPATH]"
$CDSL_CUDA_LAUNCHER $* $GRIDPATH
echo "normal end"
exit 0

