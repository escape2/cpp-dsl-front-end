#include "dsl.hpp"
using namespace EDSL;

namespace edsl_extension {
  // place elemental C++ functions here that are not yet supported by the toolchain

  double not_fortran_sign_version(double a, double b) {
    // this version does not seem to handle negative zero like Fortran
    return b >= 0 ? fabs(a) : -fabs(a);
  }

  EDSL_EXTENSION_FUN inline double f_sign(double a, double b) {
    return copysign(a,b);
  }

}

using namespace edsl_extension;

namespace edsl {
  // tested: abs, sqrt, sin, cos, exp, log
  // name changes (Fortran -> C++): min -> fmin, max -> fmax, abs -> fabs
  
  void elemental_math(Field alpha, Field beta) {
    alpha.spec(latitudes,longitudes,levels);
    beta.spec(latitudes,longitudes,levels);
    vertical_region(start_level, end_level) {
      compute_on(latitudes,longitudes) {
        alpha = log(exp(0.0001*beta)) + f_sign(42.0,-1.0);
      }
    }
  }
}
