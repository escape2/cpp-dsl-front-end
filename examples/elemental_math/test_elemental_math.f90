PROGRAM test_elemental_math
  USE dawn_struct
  USE dawn_elemental_math
  USE example_support, ONLY: dp, undef_dp, init, check, assert
  IMPLICIT NONE
  INTEGER, PARAMETER :: total_ni=8, total_nj=10, total_nk=4
  INTEGER, PARAMETER :: n_zero_halo = 0
  INTEGER, PARAMETER :: zero_halos(2,2) = n_zero_halo

  CHARACTER(len=*), PARAMETER :: file = &
       & __FILE__

  CALL test

CONTAINS

  SUBROUTINE test
    TYPE(domain_type) :: dom
    TYPE(elemental_math_type) :: stencil
    TYPE(meta_data_3d_type) :: meta_data_3d
    TYPE(storage_struct3d_double_type) :: sx, sy ! = stencil_rhs, stencil_lhs
    REAL(dp) :: y(total_ni, total_nj, total_nk)
    REAL(dp) :: y_ref(total_ni, total_nj, total_nk)
    REAL(dp) :: x(total_ni, total_nj, total_nk)
    INTEGER :: halos(2,2)

    ! init domain
    CALL dom%init([total_ni, total_nj, total_nk])
    CALL dom%set_halos(zero_halos)

    ! check halos:
    halos = dom%get_halos()
    CALL assert( ALL(halos == 0), file, __LINE__)

    ! init stencil & data
    CALL stencil%init(dom)
    CALL meta_data_3d%init(dom)
    CALL sx%init(meta_data_3d, undef_dp)
    CALL sy%init(meta_data_3d, undef_dp)
    CALL init(x)

    y_ref = LOG(EXP(0.0001_dp*x)) + sign(42.0_dp, -1.0_dp)

    ! x,y -> Dawn
    CALL sx%set(x)

    ! run stencil:
    CALL stencil%run(sy, sx)

    ! Dawn -> y
    y = undef_dp
    CALL sy%get(y)

    ! compare results
    CALL check(y_ref, y)

    PRINT*,'test_elemental_math: test passed'
  END SUBROUTINE test

END PROGRAM test_elemental_math

