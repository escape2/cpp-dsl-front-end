MODULE example_support
  IMPLICIT NONE
  INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(12,307)
  REAL(dp), PARAMETER :: undef_dp = -(HUGE(1.0_dp)/4)
  INTEGER, PARAMETER :: sp = SELECTED_REAL_KIND(6, 37)
  REAL(sp), PARAMETER :: undef_sp = -(HUGE(1.0_sp)/4)
  REAL(dp), PARAMETER :: default_relative_tolerance = 1.e-15_dp

  PRIVATE

  PUBLIC :: sp, undef_sp, dp, undef_dp
  PUBLIC :: init, check, assert, f_abort

  INTERFACE init
    MODULE PROCEDURE init_3d
    MODULE PROCEDURE init_2d
    MODULE PROCEDURE init_1d
  END INTERFACE init

  INTERFACE check
    MODULE PROCEDURE check_3d
    MODULE PROCEDURE check_2d
    MODULE PROCEDURE check_1d
  END INTERFACE check

  CHARACTER(len=*), PARAMETER :: this_file = __FILE__

CONTAINS

  SUBROUTINE init_3d(f)
    REAL(dp), INTENT(out) :: f(:,:,:)
    INTEGER :: i0, j0, k0
    INTEGER :: i, j, k
    i0 = SIZE(f,1)/2
    j0 = SIZE(f,2)/2
    k0 = SIZE(f,3)/2
    DO k = 1, SIZE(f,3)
      DO j = 1, SIZE(f,2)
        DO i = 1, SIZE(f,1)
          f(i,j,k) = REAL( (i-i0) + (j-j0)*100 + (k-k0)*10000, dp)
        ENDDO
      ENDDO
    ENDDO
  END SUBROUTINE init_3d

  SUBROUTINE init_2d(f)
    REAL(dp), INTENT(out) :: f(:,:)
    INTEGER :: i0, j0
    INTEGER :: i, j
    i0 = SIZE(f,1)/2
    j0 = SIZE(f,2)/2
    DO j = 1, SIZE(f,2)
      DO i = 1, SIZE(f,1)
        f(i,j) = REAL( (i-i0) + (j-j0)*100, dp)
      ENDDO
    ENDDO
  END SUBROUTINE init_2d

  SUBROUTINE init_1d(f)
    REAL(dp), INTENT(out) :: f(:)
    INTEGER :: k0
    INTEGER :: k
    k0 = SIZE(f)/2
    DO k = 1, SIZE(f)
      f(k) = REAL( (k-k0)*10000, dp)
    ENDDO
  END SUBROUTINE init_1d

  SUBROUTINE f_abort(reason, file, line)
    CHARACTER(len=*), INTENT(in) :: reason
    CHARACTER(len=*), INTENT(in) :: file
    INTEGER, INTENT(in) :: line
    PRINT*,'f_abort:',reason
    PRINT*,'file:',file
    PRINT*,'line:',line
    STOP 1
  END SUBROUTINE f_abort

  SUBROUTINE assert(cond, file, line)
    LOGICAL, INTENT(in) :: cond
    CHARACTER(len=*), INTENT(in) :: file
    INTEGER, INTENT(in) :: line
    IF (cond) RETURN
    CALL f_abort('assert call failed:', file, line)
  END SUBROUTINE assert

  SUBROUTINE check_3d(f, g, relative_tolerance)
    REAL(dp), INTENT(in) :: f(:,:,:), g(:,:,:)
    REAL(dp), OPTIONAL, INTENT(in) :: relative_tolerance ! relative tolerance
    REAL(dp) :: rel_tol
    INTEGER :: i, j, k
    IF (PRESENT(relative_tolerance)) THEN
      rel_tol = relative_tolerance
    ELSE
      rel_tol =default_relative_tolerance
    ENDIF
    DO k = 1, SIZE(f,3)
      DO j = 1, SIZE(f,2)
        DO i = 1, SIZE(f,1)
          IF (ABS(f(i,j,k) - g(i,j,k)) > ABS(f(i,j,k))*rel_tol) THEN
            PRINT*,'mismatch: i,j,k,f,g=',i,j,k, f(i,j,k), g(i,j,k)
            CALL f_abort('check_3d failed', this_file, __LINE__)
          ENDIF
        ENDDO
      ENDDO
    ENDDO
  END SUBROUTINE check_3d

  SUBROUTINE check_2d(f, g, relative_tolerance)
    REAL(dp), INTENT(in) :: f(:,:), g(:,:)
    REAL(dp), OPTIONAL, INTENT(in) :: relative_tolerance ! relative tolerance
    REAL(dp) :: rel_tol
    INTEGER :: i, j
    IF (PRESENT(relative_tolerance)) THEN
      rel_tol = relative_tolerance
    ELSE
      rel_tol =default_relative_tolerance
    ENDIF
    DO j = 1, SIZE(f,2)
      DO i = 1, SIZE(f,1)
        IF (ABS(f(i,j) - g(i,j)) > ABS(f(i,j))*rel_tol) THEN
          PRINT*,'mismatch: i,j,f,g=',i,j, f(i,j), g(i,j)
          CALL f_abort('check_2d failed', this_file, __LINE__)
        ENDIF
      ENDDO
    ENDDO
  END SUBROUTINE check_2d

  SUBROUTINE check_1d(f, g, relative_tolerance)
    REAL(dp), INTENT(in) :: f(:), g(:)
    REAL(dp), OPTIONAL, INTENT(in) :: relative_tolerance ! relative tolerance
    REAL(dp) :: rel_tol
    INTEGER :: k
    IF (PRESENT(relative_tolerance)) THEN
      rel_tol = relative_tolerance
    ELSE
      rel_tol =default_relative_tolerance
    ENDIF
    DO k = 1, SIZE(f)
      IF (ABS(f(k) - g(k)) > ABS(f(k))*rel_tol) THEN
        PRINT*,'mismatch: k,f,g=',k, f(k), g(k)
        CALL f_abort('check_1d failed', this_file, __LINE__)
      ENDIF
    ENDDO
  END SUBROUTINE check_1d

END MODULE example_support
