#include "dsl.hpp"

using namespace EDSL;


namespace edsl {

  // dense field types:

  Gridspace ck_space(cells,levels);
  define_field_type(CK_Field,ck_space);

  // single-link sparse field types:

  Gridspace cek_space(cells,edges,levels);
  define_field_type(CEK_Field,cek_space);

  // copy tests:
#if 1
  void copy_cell_field(CK_Field alpha, CK_Field beta) {
    alpha = beta;
  }
#endif
  void copy_ce_field(CEK_Field gamma, CEK_Field delta) {
    gamma = delta;
  }

}
