fix_generated_property()

set(CDSL ${CDSL_ROOT})


# cdsl toolchain test:

set(KERNEL_BASE_NAME unstruct_copy_cdsl)
set(backend "CUDAIco")

add_custom_command(OUTPUT
  dawn_${KERNEL_BASE_NAME}.cu
  f_itf_dawn_${KERNEL_BASE_NAME}.f90
  c_itf_dawn_${KERNEL_BASE_NAME}.cpp
  COMMAND ${CDSL_PRG} -b ${backend} -o dawn_${KERNEL_BASE_NAME}.cu ${KERNEL_BASE_NAME}.cpp
  DEPENDS ${KERNEL_BASE_NAME}.cpp
  )

string(TOUPPER ${KERNEL_BASE_NAME}_KERNEL MY_KERNEL)
add_custom_target(${MY_KERNEL}
  DEPENDS
  dawn_${KERNEL_BASE_NAME}.cu
  f_itf_dawn_${KERNEL_BASE_NAME}.f90
  c_itf_dawn_${KERNEL_BASE_NAME}.cpp
  )

set(TEST_SOURCES
  test_toolchain.f90
  ${CDSL}/support/mesh_support.f90
  ${CDSL}/support/mesh_reader.f90
  ${CDSL}/support/dawn_unstruct.f90
  ${CDSL}/support/f2c_dawn_unstruct.f90
  ${CDSL}/examples/.support/example_support.f90
  f_itf_dawn_${KERNEL_BASE_NAME}.f90
  dawn_${KERNEL_BASE_NAME}.cu
  c_itf_dawn_${KERNEL_BASE_NAME}.cpp
  )  

set(MY_TEST_EXE test_toolchain.x)
add_executable(${MY_TEST_EXE} ${TEST_SOURCES})
add_dependencies(${MY_TEST_EXE} ${MY_KERNEL})

target_include_directories(${MY_TEST_EXE} PUBLIC
  .
  ${CDSL_NETCDFF_INCLUDE_DIR}
  ${CDSL}/support
  ${CDSL_DAWN_INCLUDE_DIR}
  ${atlas_BASE_DIR}/include
  ${eckit_BASE_DIR}/include
  ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES}
  )

target_link_libraries(${MY_TEST_EXE} PUBLIC
  cdslUnstruct
  ${CDSL_NETCDFF_LIB}
  )

add_test(NAME cuda_toolchain_test
  COMMAND ${CMAKE_COMMAND} -E
  env CDSL_ROOT=${CDSL_ROOT} CDSL_CUDA_LAUNCHER=${CDSL_CUDA_LAUNCHER}
  ./run.sh ./${MY_TEST_EXE}
  )
