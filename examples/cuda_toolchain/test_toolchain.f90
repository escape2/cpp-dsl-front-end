PROGRAM test_toolchain
  USE mesh_reader, ONLY: init_mesh_support, mesh_type, gen_atlas_mesh, sym2loctype
  USE dawn_unstruct
  USE example_support, ONLY: dp, init, assert, check
  USE dawn_unstruct_copy_cdsl, ONLY: copy_cell_field_type, copy_ce_field_type
  IMPLICIT NONE

  CHARACTER(len=255):: gridfile
  TYPE(mesh_type) :: mesh

  CHARACTER(len=*), PARAMETER :: file = &
       & __FILE__

  CALL init_test
  CALL test_dense_copy_kernel
  CALL test_sparse_copy_kernel

CONTAINS

  SUBROUTINE init_test
    USE iso_c_binding, ONLY : c_double
    !PRINT*,'test_toolchain:init_test: start'
    CALL assert(dp == c_double, file, __LINE__)
    CALL GET_COMMAND_ARGUMENT(1,gridfile)
    !PRINT*,'gridfile=',TRIM(gridfile)
    CALL init_mesh_support(gridfile)
    mesh = gen_atlas_mesh()
    !PRINT*,'test_toolchain:init_test: end'
  END SUBROUTINE init_test

  SUBROUTINE test_dense_copy_kernel
    INTEGER, parameter :: nlev = 1
    TYPE(dense_field_type) :: alpha, beta
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data_cells
    TYPE(copy_cell_field_type) :: kernel
    INTEGER :: ncells, k_size, k, i
    REAL(dp), ALLOCATABLE :: a(:,:), b(:,:)
    CALL alpha%init(mesh, cdsl_loctype_cells, nlev)
    CALL beta%init(mesh, cdsl_loctype_cells, nlev)
    CALL alpha%get_shape(ncells, k_size)
    CALL assert(k_size == nlev, file, __LINE__)

    ALLOCATE(a(ncells, k_size), b(ncells, k_size))
    CALL meta_data_cells%init(SHAPE(a), [cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    DO k = 1, nlev
      DO i = 1, ncells
        b(i,k) = REAL(k*10000 + i,dp)
      ENDDO
    ENDDO

    CALL beta%set(b, meta_data_cells)
    a = -b

    CALL alpha%set(a, meta_data_cells)
    CALL kernel%init(mesh, k_size, alpha, beta)
    CALL kernel%run()
    CALL beta%get(a, meta_data_cells)

    CALL assert( all(a == b) , file, __LINE__)
    PRINT*,'test_dense_copy_kernel passed'
  END SUBROUTINE test_dense_copy_kernel

  SUBROUTINE test_sparse_copy_kernel
    INTEGER, parameter :: nlev = 1
    TYPE(sparse_field_type) :: gamma, delta
    TYPE(cdsl_unstruct_meta_data_type) :: cek_meta_data
    TYPE(contab_type) :: ce_con ! cells->edges
    TYPE(copy_ce_field_type) :: kernel
    REAL(dp), ALLOCATABLE :: g(:,:,:), d(:,:,:)
    INTEGER :: dense_size, sparse_size, i, j, k

    ! init connectivity table:
    CALL ce_con%init(mesh, "CE")
    CALL ce_con%get_shape(dense_size, sparse_size)


    ! init sparse fields:
    CALL gamma%init(ce_con, nlev)
    CALL delta%init(ce_con, nlev)

    ALLOCATE(g(sparse_size, dense_size, nlev), d(sparse_size, dense_size, nlev))

    CALL cek_meta_data%init(SHAPE(g), [cdsl_sparse_dim_kind, cdsl_idx_dim_kind, cdsl_lev_dim_kind])
    DO k = 1, nlev
      DO i = 1, dense_size
        DO j = 1, sparse_size
          d(j,i,k) = REAL(0 + (j-1) + (i-1)*10 + (k-1)*100000, dp)
        ENDDO
      ENDDO
    ENDDO

    CALL delta%set(d, cek_meta_data)

    g = -d
    CALL gamma%set(g, cek_meta_data)

    CALL kernel%init(mesh, nlev, gamma, delta)
    CALL kernel%run() ! here "gamma = delta" should happen
    CALL gamma%get(g, cek_meta_data)

    CALL assert( ALL(g == d) , file, __LINE__)

    PRINT*,'test_sparse_copy_kernel passed'
  END SUBROUTINE test_sparse_copy_kernel

END PROGRAM test_toolchain
