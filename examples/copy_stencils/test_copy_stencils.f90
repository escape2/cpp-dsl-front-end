PROGRAM test_copy_stencils
  USE dawn_struct
  USE dawn_copy_stencils
  USE example_support, ONLY: sp, dp, undef_dp, init, check, assert
  IMPLICIT NONE
  TYPE(domain_type) :: dom
  INTEGER, PARAMETER :: total_ni=10, total_nj=20, total_nk=30*0+1
  INTEGER, PARAMETER :: unit_halo_size = 1
  INTEGER, PARAMETER :: unit_halos(2,2) = unit_halo_size
  INTEGER :: initial_halo_size, halos(2,2)
#ifdef CDSL_HAS_FLEX_STRUCT_HALOS
  LOGICAL, PARAMETER :: have_flexible_halos = .TRUE.
#else
  LOGICAL, PARAMETER ::have_flexible_halos = .FALSE. ! vanilla Dawn CUDA backend limitation
#endif


  CHARACTER(len=*), PARAMETER :: file = &
       & __FILE__

  ! Show Dawn's default halo size:
  initial_halo_size = get_dawn_initial_halo_size()
  IF (initial_halo_size /= 3 ) PRINT*,'Note: changed initial_halo_size=',initial_halo_size

  ! init domain
  CALL dom%init([total_ni, total_nj, total_nk])

  ! get domain halos:
  halos = dom%get_halos()
  CALL assert(ALL(halos == initial_halo_size), file, __LINE__)

  IF (have_flexible_halos) THEN
    ! set domain halos:
    CALL dom%set_halos(unit_halos)
  ENDIF

  ! tests:

  CALL test_setget_globals()
  IF (have_flexible_halos) THEN
    CALL test_set_halos
  ELSE
    PRINT*,'*** test_set_halos skipped - requires CDSL_HAS_FLEX_STRUCT_HALOS'
  ENDIF
  CALL test_copy_global_var
  CALL test_2d
  CALL test_3d(implicit_iteration=.FALSE.)
  CALL test_3d(implicit_iteration=.TRUE.)
  CALL test_2d_to_3d
  CALL test_1d_to_3d
  CALL test_init_storage

CONTAINS


  SUBROUTINE test_setget_globals()
    USE iso_c_binding, ONLY: c_bool, c_float, c_int, c_double
    TYPE(copy_global_var_type) :: stencil
    LOGICAL :: log_var
    INTEGER :: int_var
    REAL(sp) :: sp_var
    REAL(dp) :: dp_var
    INTEGER :: nh

    IF (have_flexible_halos) THEN
      ! set domain halos:
      CALL dom%set_halos(unit_halos)
      nh = 1
    ELSE
      nh = initial_halo_size
    ENDIF

    ! init stencil:
    CALL stencil%init(dom)

    ! check state of global variables:
    log_var = stencil%get_g_bool_var()
    CALL assert(.NOT. log_var, file, __LINE__)

    int_var = stencil%get_g_int_var()
    CALL assert(int_var == 42, file, __LINE__)

    sp_var = stencil%get_g_float_var()
    CALL assert( ABS(sp_var - 1.0_sp) <= EPSILON(sp_var), file, __LINE__)

    dp_var = stencil%get_g_double_var()
    CALL assert( ABS(dp_var - 2.0_sp) <= 2*EPSILON(dp_var), file, __LINE__)

    ! modify & check:
    CALL stencil%set_g_bool_var(.TRUE.)
    log_var = stencil%get_g_bool_var()
    CALL assert(log_var, file, __LINE__)

    CALL stencil%set_g_int_var(2*int_var)
    int_var = stencil%get_g_int_var()
    CALL assert(int_var == 2*42, file, __LINE__)

    CALL  stencil%set_g_float_var(4*sp_var)
    sp_var = stencil%get_g_float_var()
    CALL assert( ABS(sp_var - 4.0_sp) <= 4*EPSILON(sp_var), file, __LINE__)

    CALL  stencil%set_g_double_var(4*dp_var)
    dp_var = stencil%get_g_double_var()
    CALL assert( ABS(dp_var - 8.0_sp) <= 8*EPSILON(dp_var), file, __LINE__)

    PRINT*,'test_copy_stencils: test_globals passed'
  END SUBROUTINE test_setget_globals

  SUBROUTINE nh_subtest(tni,tnj,nh, dawn_lhs)
    INTEGER, INTENT(in) :: tni, tnj, nh
    TYPE(storage_struct2d_double_type) :: dawn_lhs

    TYPE(domain_type) :: test_dom
    TYPE(meta_data_2d_type) :: meta_data_2d
    TYPE(storage_struct2d_double_type) :: dawn_rhs
    REAL(dp) :: y(tni, tnj), res(tni, tnj), ref(tni, tnj)
    TYPE(copy_stencil_2d_type) :: stencil

    CALL test_dom%init(tni, tnj, 1)
    CALL test_dom%set_halos(nh*unit_halos)
    CALL stencil%init(test_dom)
    CALL meta_data_2d%init(tni, tnj)
    CALL dawn_rhs%init(meta_data_2d, 2.0_dp, name='rhs')

    y = 1

    ref = y
    ref(nh+1:tni-nh,nh+1:tnj-nh) = 2

    CALL dawn_lhs%set(y)
    CALL stencil%run(dawn_lhs, dawn_rhs)
    CALL dawn_lhs%get(res)
    CALL check(ref, res)

  END SUBROUTINE nh_subtest


  SUBROUTINE test_set_halos
    INTEGER, PARAMETER :: tni=10, tnj=10
    TYPE(meta_data_2d_type) :: meta_data_2d
    TYPE(storage_struct2d_double_type) :: dawn_lhs
    INTEGER :: nhalo
    CALL meta_data_2d%init(tni, tnj)
    CALL dawn_lhs%init(meta_data_2d, name='lhs')
    DO nhalo = 0, 5
      CALL nh_subtest(tni,tnj,nhalo,dawn_lhs)
    ENDDO
    PRINT*,'test_copy_stencils: test_set_halos passed'
  END SUBROUTINE test_set_halos


  SUBROUTINE test_copy_global_var
    INTEGER :: nh
    REAL(dp), PARAMETER :: s = 16.0_dp
    TYPE(copy_global_var_type) :: stencil
    REAL(dp) :: f(total_ni, total_nj), ref(total_ni, total_nj)
    REAL(dp) :: g(total_ni, total_nj)
    TYPE(meta_data_2d_type) :: meta_data_2d
    TYPE(storage_struct2d_double_type) :: f_dawn, g_dawn
    REAL(dp) :: t


    ! init:
    IF (have_flexible_halos) THEN
      CALL dom%set_halos(unit_halos)
      nh = unit_halo_size
    ELSE
      nh = initial_halo_size
    ENDIF
    CALL stencil%init(dom)
    CALL meta_data_2d%init(dom)
    CALL init(ref)
    CALL g_dawn%init(meta_data_2d, name='g')

    f = ref

    ! modify core of ref
    ref(1+nh:total_ni-nh,1+nh:total_nj-nh) = ref(1+nh:total_ni-nh,1+nh:total_nj-nh) + s

    ! f -> Dawn
    CALL f_dawn%init(meta_data_2d, name='f', init_array=f)

    ! copy back and check:
    g = -1
    CALL f_dawn%get(g)
    CALL check(f, g)

    ! Dawn: g_double_var = s
    CALL stencil%set_g_double_var(s)
    t  = stencil%get_g_double_var()
    CALL assert(s == t, file, __LINE__)

    ! run:
    CALL stencil%run(f_dawn)

    ! Dawn -> f
    CALL f_dawn%get(f)

    ! check if f and ref match:
    CALL check(ref, f)

    PRINT*,'test_copy_stencils: test_copy_global_var passed'
  END SUBROUTINE test_copy_global_var

  SUBROUTINE test_2d
    INTEGER :: nh
    TYPE(copy_stencil_2d_type) :: stencil
    REAL(dp) :: f(total_ni, total_nj), ref(total_ni, total_nj)
    REAL(dp) :: g(total_ni, total_nj)
    TYPE(meta_data_2d_type) :: meta_data_2d
    TYPE(storage_struct2d_double_type) :: f_dawn, g_dawn
    INTEGER :: i,j

    ! init:
    IF (have_flexible_halos) THEN
      CALL dom%set_halos(unit_halos)
      nh = 1
    ELSE
      nh = initial_halo_size
    ENDIF
    CALL stencil%init(dom)
    CALL meta_data_2d%init(dom)
    CALL assert(.NOT. f_dawn%is_initialized(), file, __LINE__)
    CALL f_dawn%init(meta_data_2d, name='f')
    CALL assert(f_dawn%is_initialized(), file, __LINE__)
    CALL g_dawn%init(meta_data_2d, name='g')

    DO j = 1, total_nj
      DO i = 1, total_ni
        ref(i,j) = REAL(j*100 + i, dp)
      ENDDO
    ENDDO

    ! set halos of f to undef:
    f = undef_dp
    f(1+nh:total_ni-nh,1+nh:total_nj-nh) = ref(1+nh:total_ni-nh,1+nh:total_nj-nh)

    ! set core of g to undef:
    g = ref
    g(1+nh:total_ni-nh,1+nh:total_nj-nh) = undef_dp

    ! f,g -> Dawn
    CALL f_dawn%set(f)
    CALL g_dawn%set(g)

    ! run stencil: g_dawn := f_dawn (core points only)
    CALL stencil%run(g_dawn, f_dawn)

    ! Dawn -> g
    CALL g_dawn%get(g)

    ! check if core and halos points of g are as expected:
    CALL check(ref, g)

    PRINT*,'test_copy_stencils: test_2d passed'
  END SUBROUTINE test_2d

  SUBROUTINE test_3d(implicit_iteration)
    LOGICAL, INTENT(in) :: implicit_iteration
    INTEGER :: nh
    TYPE(copy_stencil_3d_type) :: stencil
    TYPE(copy_stencil_3d_implicit_type) :: implicit_stencil
    REAL(dp) :: x(total_ni, total_nj, total_nk), ref(total_ni, total_nj, total_nk)
    REAL(dp) :: y(total_ni, total_nj, total_nk)
    TYPE(meta_data_3d_type) :: meta_data_3d
    TYPE(storage_struct3d_double_type) :: sx, sy ! = stencil_rhs, stencil_lhs

    ! init:
    IF (have_flexible_halos) THEN
      CALL dom%set_halos(unit_halos)
      nh = unit_halo_size
    ELSE
      nh = initial_halo_size
    ENDIF

    IF (implicit_iteration) THEN
      CALL implicit_stencil%init(dom)
    ELSE
      CALL stencil%init(dom)
    ENDIF

    CALL meta_data_3d%init(dom)
    CALL sx%init(meta_data_3d, init_value=0.0_dp, name='x')
    CALL sy%init(meta_data_3d, init_value=0.0_dp, name='y')

    ! init ref:
    CALL init(ref)

    ! set halos of f to undef
    x = undef_dp
    x(1+nh:total_ni-nh,1+nh:total_nj-nh,:) = ref(1+nh:total_ni-nh,1+nh:total_nj-nh,:)

    ! set core of g to undef:
    y = ref
    y(1+nh:total_ni-nh,1+nh:total_nj-nh,:) = undef_dp

    ! x,y -> Dawn
    CALL sx%set(x)
    CALL sy%set(y)

    ! run stencil: sy := sx (core points only)
    IF (implicit_iteration) THEN
      CALL implicit_stencil%run(sy, sx)
    ELSE
      CALL stencil%run(sy, sx)
    ENDIF

    ! Dawn -> y
    CALL sy%get(y)

    ! check if core and halo points of g are as expected:
    CALL check(ref, y)
    IF (implicit_iteration) THEN
      PRINT*,'test_copy_stencils: test_3d(implicit_iteration) passed'
    ELSE
      PRINT*,'test_copy_stencils: test_3d(explicit_iteration) passed'
    ENDIF
  END SUBROUTINE test_3d

  SUBROUTINE test_2d_to_3d
    INTEGER :: nh
    TYPE(copy_stencil_2d_to_3d_type) :: stencil
    REAL(dp) :: x(total_ni, total_nj), ref(total_ni, total_nj, total_nk)
    REAL(dp) :: y(total_ni, total_nj, total_nk)
    TYPE(meta_data_2d_type) :: meta_data_2d
    TYPE(meta_data_3d_type) :: meta_data_3d
    TYPE(storage_struct3d_double_type) :: lhs
    TYPE(storage_struct2d_double_type) :: rhs
    INTEGER :: k

    ! init:
    IF (have_flexible_halos) THEN
      CALL dom%set_halos(unit_halos)
      nh = unit_halo_size
    ELSE
      nh = initial_halo_size
    ENDIF
    CALL stencil%init(dom)
    CALL meta_data_2d%init(dom)
    CALL meta_data_3d%init(dom)
    CALL lhs%init(meta_data_3d, init_value=0.0_dp, name='lhs')
    CALL rhs%init(meta_data_2d, name='rhs')

    ! init ref:
    CALL init(x)
    DO k = 1, total_nk
      ref(:,:,k) = x
    ENDDO

    ! set halos of x to undef
    x = undef_dp
    x(1+nh:total_ni-nh,1+nh:total_nj-nh) = ref(1+nh:total_ni-nh,1+nh:total_nj-nh,1)

    ! set core of y to undef
    y = ref
    y(1+nh:total_ni-nh,1+nh:total_nj-nh,:) = undef_dp

    ! x,y -> Dawn
    CALL rhs%set(x)
    CALL lhs%set(y)

    ! run stencil: lhs := rhs (core points only)
    CALL stencil%run(lhs, rhs)

    ! Dawn -> y
    CALL lhs%get(y)

    ! check if core and halo points of g are as expected:
    CALL check(ref, y)

    PRINT*,'test_copy_stencils: test_2d_to_3d passed'
  END SUBROUTINE test_2d_to_3d

  SUBROUTINE test_1d_to_3d
    INTEGER :: nh
    TYPE(copy_stencil_1d_to_3d_type) :: stencil
    REAL(dp) :: x(total_nk), x2(total_nk), ref(total_ni, total_nj, total_nk)
    REAL(dp) :: y(total_ni, total_nj, total_nk)
    TYPE(meta_data_column_type) :: meta_data_column
    TYPE(meta_data_3d_type) :: meta_data_3d
    TYPE(storage_struct3d_double_type) :: lhs
    TYPE(storage_column_double_type) :: rhs
    INTEGER :: i,j

    ! init dawn objects:
    IF (have_flexible_halos) THEN
      CALL dom%set_halos(unit_halos)
      nh = unit_halo_size
    ELSE
      nh = initial_halo_size
    ENDIF
    CALL stencil%init(dom)
    CALL meta_data_3d%init(dom)
    CALL meta_data_column%init(dom)
    CALL lhs%init(meta_data_3d, init_value=0.0_dp, name='lhs')
    CALL rhs%init(meta_data_column, name='rhs')

    ! init ref:
    CALL init(x)
    DO i = 1, total_ni
      DO j = 1, total_nj
        ref(i,j,:) = x
      ENDDO
    ENDDO

    ! set core of y to undef
    y = ref
    y(1+nh:total_ni-nh,1+nh:total_nj-nh,:) = undef_dp

    ! x,y -> Dawn
    CALL rhs%set(x)
    CALL lhs%set(y)

    ! test column get function:
    x2 = undef_dp
    CALL rhs%get(x2)
    CALL check(x, x2)

    ! run stencil: lhs := rhs (core points only)
    CALL stencil%run(lhs, rhs)

    ! Dawn -> y
    CALL lhs%get(y)

    ! check if core and halo points of g are as expected:
    CALL check(ref, y)

    PRINT*,'test_copy_stencils: test_1d_to_3d passed'
  END SUBROUTINE test_1d_to_3d

  SUBROUTINE test_init_storage
    REAL(dp) :: ref(total_ni, total_nj, total_nk)
    REAL(dp) :: y(total_ni, total_nj, total_nk)
    TYPE(meta_data_3d_type) :: meta_data_3d
    TYPE(storage_struct3d_double_type) :: sy
    REAL(dp), PARAMETER :: start_value = 42.0_dp

    ! Note: halos do not matter in this test
    !IF (have_flexible_halos) THEN
    !  CALL dom%set_halos(unit_halos)
    !ENDIF
    CALL meta_data_3d%init(dom)
    CALL sy%init(meta_data_3d, init_value=start_value)

    ref = start_value

    ! Dawn > y
    y = undef_dp
    CALL sy%get(y)

    ! check:
    CALL check(ref, y)
    PRINT*,'test_copy_stencils: test_init_storage passed'

  END SUBROUTINE test_init_storage

END PROGRAM test_copy_stencils
