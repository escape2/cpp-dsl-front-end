#include "dsl.hpp"

using namespace EDSL;

namespace edsl {

  bool g_bool_var = false;
  int g_int_var = 42;
  float g_float_var = 1.0;
  double g_double_var = 2.0;

  void copy_global_var(Field alpha) {
    alpha.spec(latitudes,longitudes);
    compute_on(latitudes,longitudes) {
      alpha = alpha + g_double_var;
    }
  }

  void copy_stencil_2d(Field alpha, Field beta) {
    alpha.spec(latitudes,longitudes);
    beta.spec(latitudes,longitudes);
    compute_on(latitudes,longitudes) {
      alpha = beta;
    }
  }

  void copy_stencil_3d(Field alpha, Field beta) {
    alpha.spec(latitudes,longitudes,levels);
    beta.spec(latitudes,longitudes,levels);
    vertical_region(start_level,end_level) {
      compute_on(latitudes,longitudes) {
        alpha = beta;
      }
    }
  }

  void copy_stencil_3d_implicit(Field alpha, Field beta) {
    alpha.spec(latitudes,longitudes,levels);
    beta.spec(latitudes,longitudes,levels);
    alpha = beta;
  }

  void copy_stencil_2d_to_3d(Field alpha, Field beta) {
    alpha.spec(latitudes,longitudes,levels);
    beta.spec(latitudes,longitudes);
    vertical_region(start_level,end_level) {
      compute_on(latitudes,longitudes) {
        alpha = beta;
      }
    }
  }

  void copy_stencil_1d_to_3d(Field alpha, Field beta) {
    alpha.spec(latitudes,longitudes,levels);
    beta.spec(levels);
    vertical_region(start_level,end_level) {
      compute_on(latitudes,longitudes) {
        alpha = beta;
      }
    }
  }

} //end of namespace edsl
