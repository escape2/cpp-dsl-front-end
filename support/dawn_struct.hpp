
#pragma once

#include "f2c_dawn_common_c.hpp"

// debug support
#include <iostream>

// #ifndef DAWN_GENERATED
// #define DAWN_GENERATED
// #endif

// #ifndef GRIDTOOLS_DAWN_HALO_EXTENT
// #define GRIDTOOLS_DAWN_HALO_EXTENT 3
// #endif

// #include "driver-includes/domain.hpp"
// #include "driver-includes/gridtools_includes.hpp"


// template<typename T>
// T *create_dawn_meta_data_x(int total_ni, int total_nj, int total_nk) {
//   T *md_pt = new T(total_ni, total_nj, total_nk);
//   return md_pt;
// }

// #if 0
// template<typename T>
// void delete_dawn_meta_data(T *md_pt) {
//     delete md_pt;
// }
// #endif

enum BackendDirection { to_backend, from_backend};

template<typename M>
M *create_dawn_meta_data(int total_ni, int total_nj, int total_nk) {
  M *md_pt = new M(total_ni, total_nj, total_nk);
  return md_pt;
}


template<typename M>
void delete_dawn_meta_data(M *md_pt) {
  delete md_pt;
}

template<typename M, typename S>
S *create_dawn_storage(M *md_pt, double *init_value, std::string *name) {
    if (md_pt == NULL) return NULL;
    S *s_pt;
    if (init_value) {
      s_pt = new S(*md_pt, *init_value, name ? *name : "");
    } else {
      s_pt = new S(*md_pt, name ? *name : "");
    }
    return s_pt;
}

template<typename S>
void delete_dawn_storage(S *s_pt) {
  delete s_pt;
}


// copy 2d window:
template<typename S, // storage type
         typename F, // field element type
         int D> // direction
int copy_win_struct2d(F *f, int *f_shape, int f_win[2][2],
                      S *storage_pt) {
  if (!(storage_pt and f and f_win)) return F2C_E_INVALID_ARGUMENT;
  const int fni = f_shape[0];
  const int fnj = f_shape[1];
  const int imin = f_win[0][0]-1;
  const int imax = f_win[0][1]-1;
  const int jmin = f_win[1][0]-1;
  const int jmax = f_win[1][1]-1;
  const auto stl = storage_pt->total_lengths();
  assert(imin >= 0 and imax < fni and jmin >= 0 and jmax < fnj);    
  assert(fni == stl[0] and fnj == stl[1] and stl[2] == 1);
#if 0
  std::cout << "copy_win_struct2d: "
            << "imin=" << imin
            << ", imax=" << imax
            << ", jmin=" << jmin
            << ", jmax=" << jmax
            << std::endl;
#endif
  auto view = make_host_view(*storage_pt);
  for(int j = jmin; j <= jmax; ++j) {
    const int pi0 = fni*j;

    // nvcc 10.0 cannot deal with constexpr if
    if (D == to_backend) {
      for(int i = imin; i <= imax; ++i) {
        const int p = pi0 + i;
        view(i,j,0) = f[p];
      }
    } else if (D == from_backend) {
      for(int i = imin; i <= imax; ++i) {
        const int p = pi0 + i;
        f[p] = view(i,j,0);
      }
    } else {
      assert(false);
      //die("copy_win_struct2d: invalid direction");
    }

  }
  return 0;
}


// copy 3d window:
template<typename S, // storage type
         typename F, // field element type
         int D> // direction
int copy_win_struct3d(F *f, int *f_shape, int f_win[3][2],
                      S *storage_pt) {
    if (!(storage_pt and f and f_win)) return F2C_E_INVALID_ARGUMENT;
    //std::cout << "********** mark 3" << std::endl;
    const int fni = f_shape[0];
    const int fnj = f_shape[1];
    const int fnk = f_shape[2];
    const int imin = f_win[0][0]-1;
    const int imax = f_win[0][1]-1;
    const int jmin = f_win[1][0]-1;
    const int jmax = f_win[1][1]-1;
    const int kmin = f_win[2][0]-1;
    const int kmax = f_win[2][1]-1;
    const auto stl = storage_pt->total_lengths();
    assert(imin >= 0 and imax < fni and
           jmin >= 0 and jmax < fnj and
           kmin >= 0 and kmax < fnk);    
    assert(fni == stl[0] and fnj == stl[1] and stl[2] == fnk);
    auto view = make_host_view(*storage_pt);
    for(int k = kmin; k <= kmax; ++k) {
      const int pj0 = fni*fnj*k;
      for(int j = jmin; j <= jmax; ++j) {
        const int pi0 = pj0 + fni*j;

        if (D == to_backend) {
          for(int i = imin; i <= imax; ++i) {
            const int p = pi0 + i;
            view(i,j,k) = f[p];
          }
        } else if (D == from_backend) {
          for(int i = imin; i <= imax; ++i) {
            const int p = pi0 + i;
            f[p] = view(i,j,k);
          }
        } else {
          assert(false);
        }
        
      }
    }
    return 0;
}

// copy column window to Dawn:
template<typename S, // storage type
         typename F, // field element type
         int D> // direction
int copy_win_column(F *f, int f_size, int f_win[2],
                    S *storage_pt) {
  if (!(storage_pt and f and f_win)) return F2C_E_INVALID_ARGUMENT;
  const int fnk = f_size;
  const int kmin = f_win[0]-1;
  const int kmax = f_win[1]-1;
  const auto stl = storage_pt->total_lengths();
  assert(kmin >= 0 and kmax < fnk);    
  assert(stl[0] == 1 and stl[1] == 1 and stl[2] == fnk);
  auto view = make_host_view(*storage_pt);

  if (D == to_backend) {
    for(int k = kmin; k <= kmax; ++k) {
      view(0,0,k) = f[k];
    }
  } else if (D == from_backend) {
    for(int k = kmin; k <= kmax; ++k) {
      f[k] = view(0,0,k);
    }
  } else {
    assert(false);
  }

  return 0;
}
