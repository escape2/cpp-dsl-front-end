

#ifndef DAWN_GENERATED
#define DAWN_GENERATED
#endif

#ifndef GRIDTOOLS_DAWN_HALO_EXTENT
#define GRIDTOOLS_DAWN_HALO_EXTENT 3
#endif

#include "driver-includes/domain.hpp"
#include "driver-includes/gridtools_includes.hpp"
#include "dawn_struct.hpp"
#include "f2c_dawn_common_c.hpp"

#include <iostream>
#include <typeinfo>
#include <typeindex>
#include <string>

#include "name_mangling.h"

enum Dawn_gt_backend_enum {
                           GT_backend_kind = 1,
                           GTCU_backend_kind,
                           GTMC_backend_kind };


//

// for debugging:
#include <iostream>

namespace gtd = gridtools::dawn;

#if 0
class ext_meta_data_ij_t  {
  std::type_info ti;
  void* f2c_ptr;
};
#endif

struct F2Cretval {
  // object pointer
  void* obj_ptr;
  // integer representation of type parameter used in storage types
  int gt_dawn_halo_extend;
  int backend_kind;
};

extern"C" {
  
  //#include "f2c_dawn_c.hpp"

  // domain:

  gtd::domain *CPP_NAME_MANGLING(f2c_create_dawn_domain_c)(int total_ni, int total_nj, int total_nk) {
    gtd::domain *dom_pt = new gtd::domain(total_ni, total_nj, total_nk);
    return dom_pt;
  }

  void CPP_NAME_MANGLING(f2c_delete_dawn_domain_c)(gtd::domain *dom_pt) {
    delete dom_pt;
  }

  int CPP_NAME_MANGLING(f2c_get_dawn_domain_shape_c)(gtd::domain *dom_pt, int dom_shape[3]) {
    if (dom_pt == NULL) return F2C_E_INVALID_ARGUMENT;
    dom_shape[0] = dom_pt->isize();
    dom_shape[1] = dom_pt->jsize();
    dom_shape[2] = dom_pt->ksize();
    return 0;
  }

  // halos:

  int CPP_NAME_MANGLING(f2c_get_dawn_domain_halos_c)(gtd::domain *dom_pt, int dom_halos[2][2]) {
    if (dom_pt == NULL) return F2C_E_INVALID_ARGUMENT;
    dom_halos[0][0] = dom_pt->iminus();
    dom_halos[0][1] = dom_pt->iplus();
    dom_halos[1][0] = dom_pt->jminus();
    dom_halos[1][1] = dom_pt->jplus();
    return 0;
  }

  int CPP_NAME_MANGLING(f2c_set_dawn_domain_halos_c)(gtd::domain *dom_pt, int dom_halos[2][2]) {
    if (dom_pt == NULL) return F2C_E_INVALID_ARGUMENT;
    dom_pt->set_halos(dom_halos[0][0], dom_halos[0][1], dom_halos[1][0], dom_halos[1][1], 0, 0);
    return 0;
  }

  int CPP_NAME_MANGLING(f2c_get_dawn_initial_halo_size_c)() {
    gtd::halo dawn_halos;
    return dawn_halos.value;
  }

  // 2d meta data:

  gtd::meta_data_ij_t *CPP_NAME_MANGLING(f2c_create_dawn_meta_data_2d_c)(int total_ni, int total_nj) {
    return create_dawn_meta_data<gtd::meta_data_ij_t>(total_ni, total_nj, 1);
  }

  void CPP_NAME_MANGLING(f2c_delete_dawn_meta_data_2d_c)(gtd::meta_data_ij_t *md_pt) {
    delete_dawn_meta_data<gtd::meta_data_ij_t>(md_pt);
  }

  // 3d meta data:

  gtd::meta_data_t *CPP_NAME_MANGLING(f2c_create_dawn_meta_data_3d_c)(int total_ni, int total_nj, int total_nk) {
    return create_dawn_meta_data<gtd::meta_data_t>(total_ni, total_nj, total_nk);
  }

  void CPP_NAME_MANGLING(f2c_delete_dawn_meta_data_3d_c)(gtd::meta_data_t *md_pt) {
    delete_dawn_meta_data<gtd::meta_data_t>(md_pt);
  }

  // column meta data:

  gtd::meta_data_k_t *CPP_NAME_MANGLING(f2c_create_dawn_meta_data_column_c)(int total_nk) {
    return create_dawn_meta_data<gtd::meta_data_k_t>(1, 1, total_nk);
  }

  void CPP_NAME_MANGLING(f2c_delete_dawn_meta_data_column_c)(gtd::meta_data_k_t *md_pt) {
    delete md_pt;
  }
  
  // 2d storage:
  gtd::storage_ij_t *CPP_NAME_MANGLING(f2c_create_dawn_storage_2d_c)(gtd::meta_data_ij_t *md_pt, double *init_value, std::string *name) {
    return create_dawn_storage<gtd::meta_data_ij_t, gtd::storage_ij_t>(md_pt, init_value, name);
  }
 
  void CPP_NAME_MANGLING(f2c_delete_dawn_storage_2d_c)(gtd::storage_ij_t *s_pt) {
    delete s_pt;
  }

  // 3d storage:

  gtd::storage_t *CPP_NAME_MANGLING(f2c_create_dawn_storage_3d_c)(gtd::meta_data_t *md_pt, double *init_value, std::string *name) {
    return create_dawn_storage<gtd::meta_data_t, gtd::storage_t>(md_pt, init_value, name);
  }

  void CPP_NAME_MANGLING(f2c_delete_dawn_storage_3d_c)(gtd::storage_t *s_pt) {
    delete_dawn_storage<gtd::storage_t>(s_pt);
  }

  // column storage:

  gtd::storage_k_t *CPP_NAME_MANGLING(f2c_create_dawn_storage_column_c)(gtd::meta_data_k_t *md_pt, double *init_value, std::string *name) {
    return create_dawn_storage<gtd::meta_data_k_t,  gtd::storage_k_t>(md_pt, init_value, name);
  }

  void CPP_NAME_MANGLING(f2c_delete_dawn_storage_column_c)(gtd::storage_k_t *s_pt) {
    delete s_pt;
  }

  // copy 2d window to Dawn:
  int CPP_NAME_MANGLING(f2c_copy_win_to_dawn_struct2d_double_c)(double *f, int *f_shape, int f_win[2][2],
                                             gtd::storage_ij_t *storage_pt) {
    return copy_win_struct2d<gtd::storage_ij_t, double, to_backend>(f, f_shape, f_win, storage_pt);
  }

  // copy 2d window from Dawn:
  int CPP_NAME_MANGLING(f2c_copy_win_from_dawn_struct2d_double_c)(double *f, int *f_shape, int f_win[2][2],
                                               gtd::storage_ij_t *storage_pt) {
    return copy_win_struct2d<gtd::storage_ij_t, double, from_backend>(f, f_shape, f_win, storage_pt);
  }

  // copy 3d window to Dawn:
  int CPP_NAME_MANGLING(f2c_copy_win_to_dawn_struct3d_double_c)(double *f, int *f_shape, int f_win[3][2],
                                             gtd::storage_t *storage_pt) {
    return copy_win_struct3d<gtd::storage_t, double, to_backend>(f, f_shape, f_win, storage_pt);
  }

  // copy 3d window from Dawn:
  int CPP_NAME_MANGLING(f2c_copy_win_from_dawn_struct3d_double_c)(double *f, int *f_shape, int f_win[3][2],
                                             gtd::storage_t *storage_pt) {
    return copy_win_struct3d<gtd::storage_t, double, from_backend>(f, f_shape, f_win, storage_pt);
  }

  // copy column window to Dawn:
  int CPP_NAME_MANGLING(f2c_copy_win_to_dawn_column_double_c)(double *f, int f_size, int f_win[2],
                                           gtd::storage_k_t *storage_pt) {
    return copy_win_column<gtd::storage_k_t, double, to_backend>(f, f_size, f_win, storage_pt);
  }

  // copy column window from Dawn:
  int CPP_NAME_MANGLING(f2c_copy_win_from_dawn_column_double_c)(double *f, int f_size, int f_win[2],
                                             gtd::storage_k_t *storage_pt) {
    return copy_win_column<gtd::storage_k_t, double, from_backend>(f, f_size, f_win, storage_pt);
  }


}
