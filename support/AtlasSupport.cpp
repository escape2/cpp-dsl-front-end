//===--------------------------------------------------------------------------------*- C++ -*-===//
//                          _
//                         | |
//                       __| | __ ___      ___ ___
//                      / _` |/ _` \ \ /\ / / '_  |
//                     | (_| | (_| |\ V  V /| | | |
//                      \__,_|\__,_| \_/\_/ |_| |_| - Compiler Toolchain
//
//
//  This file is distributed under the MIT License (MIT).
//  See Dawn:LICENSE.txt for details.
//
//===------------------------------------------------------------------------------------------===//


//// The following /*  */ comment block contains the Dawn:LICENSE.txt referenced above:

/*
MIT License

Copyright (c) 2020 mroethlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

//// Implementation is derived from https://github.com/dawn-ico/AtlasUtilities/utils/AtlasFromNetcdf.* files.

#include <assert.h>
#include <iostream>
#include <math.h>

#include <atlas/mesh/Mesh.h>
#include <atlas/mesh/ElementType.h>
#include <atlas/mesh/Elements.h>
#include <atlas/mesh/Nodes.h>
#include <atlas/util/CoordinateEnums.h>

#include "AtlasSupport.hpp"

namespace {

// dummy partition identifier. always zero throughout since this reader does not support
// partitioning (yet)
const int global_part_id = 0;
    
// Pre-allocate neighborhood table, snippet taken from
// https://github.com/ecmwf/atlas/blob/98ff1f20dc883ba2dfd7658196622b9bc6d6fceb/src/atlas/mesh/actions/BuildEdges.cc#L73
template <typename ConnectivityT>
void AllocNbhTable(ConnectivityT& connectivity, const MeshMapSpecs &x2y) {
  assert(x2y.is_valid());
  const int numElements = x2y.dense_size;
  const int nbhPerElem = x2y.sparse_size;
  std::vector<int> init(numElements * nbhPerElem, connectivity.missing_value());
  connectivity.add(numElements, nbhPerElem, init.data());
}

template <typename ConnectivityT>
void AddNeighborList(ConnectivityT& connectivity, const MeshMapSpecs &x2y,
                     int idx_shift) {
  const int nx = x2y.dense_size;
  const int ny = x2y.sparse_size;
  const int ystep = x2y.sparse_step;
  assert(ystep == 1 or ystep == nx);
  const bool step1_case = (ystep == 1);
  const int miss_val = connectivity.missing_value();
  assert(miss_val < 0);
  for(int ix = 0; ix < nx; ix++) {
    atlas::idx_t y[ny];
    if (step1_case) {
      for(int iiy = 0; iiy < ny; iiy++) {
        int iy = x2y.map[ix * ny + iiy] + idx_shift;
        y[iiy] = iy < 0 ? miss_val : iy;
      }
    } else {
      for(int iiy = 0; iiy < ny; iiy++) {
        int iy = x2y.map[iiy * nx + ix] + idx_shift;
        y[iiy] = iy < 0 ? miss_val : iy;
      }
    }
    connectivity.set(ix, y);
  }

}

void AddVerts2Mesh(atlas::Mesh &mesh, const double vlat[], const double vlon[], int nvertices, int idx_shift) {
  int numNodes = nvertices;
  assert(idx_shift == -1);
  // define nodes and associated properties for Atlas meshs
  mesh.nodes().resize(numNodes);
  atlas::mesh::Nodes& nodes = mesh.nodes();
  auto lonlat = atlas::array::make_view<double, 2>(nodes.lonlat());

  // we currently don't care about parts, so myPart is always 0 and remotde_idx == glb_idx
  auto glb_idx_node = atlas::array::make_view<atlas::gidx_t, 1>(nodes.global_index());
  auto remote_idx = atlas::array::make_indexview<atlas::idx_t, 1>(nodes.remote_index());
  auto part = atlas::array::make_view<int, 1>(nodes.partition());

  // no ghosts currently (ghost = false always) and no flags are set
  auto ghost = atlas::array::make_view<int, 1>(nodes.ghost());
  auto flags = atlas::array::make_view<int, 1>(nodes.flags());

  auto radToLat = [](double rad) { return rad / (0.5 * M_PI) * 90; };
  auto radToLon = [](double rad) { return rad / (M_PI)*180; };

  for(int nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
    lonlat(nodeIdx, atlas::LON) = radToLon(vlon[nodeIdx]);
    lonlat(nodeIdx, atlas::LAT) = radToLat(vlat[nodeIdx]);

    glb_idx_node(nodeIdx) = nodeIdx;
    remote_idx(nodeIdx) = nodeIdx;

    part(nodeIdx) = global_part_id;
    ghost(nodeIdx) = false;
    atlas::mesh::Nodes::Topology::reset(flags(nodeIdx));
  }
}
  
void AddTCells2Mesh(atlas::Mesh &mesh, int ncells) {
  // define cells and associated properties
  mesh.cells().add(new atlas::mesh::temporary::Triangle(), ncells);
  auto cells_part = atlas::array::make_view<int, 1>(mesh.cells().partition());
  atlas::array::ArrayView<atlas::gidx_t, 1> glb_idx_cell =
    atlas::array::make_view<atlas::gidx_t, 1>(mesh.cells().global_index());

  for(size_t cellIdx = 0; cellIdx < ncells; cellIdx++) {
    glb_idx_cell[cellIdx] = cellIdx;
    cells_part(cellIdx) = global_part_id;
  }
}

} // namespace

bool MeshMapSpecs::is_valid() const {
  return map and dense_size>0 and sparse_size>0 and (sparse_step == 1 or sparse_step == dense_size);
}


// atlas mesh creation with specified connectivity:
// - if anything goes wrong return nullptr
atlas::Mesh* CreateAtlasMesh(int ncells, int nedges, int nverts,
                             const double vlat[], const double vlon[],
                             const MeshMapSpecs &c2v, const MeshMapSpecs &e2v,
                             const MeshMapSpecs &e2c, const MeshMapSpecs &v2c,
                             const MeshMapSpecs &v2e, const MeshMapSpecs &c2e,
                             int start_idx) {
  assert(c2v.dense_size == ncells && c2e.dense_size == ncells);
  assert(e2v.dense_size == nedges && e2c.dense_size == nedges);
  assert(v2c.dense_size == nverts && v2e.dense_size == nverts);
  const int idx_shift = 0 - start_idx;
  atlas::Mesh *mesh_pt = new atlas::Mesh;
  if (!mesh_pt) return nullptr;

  auto &mesh  = *mesh_pt;

  // add vertices to mesh:
  assert(nverts>0);
  AddVerts2Mesh(mesh, vlat, vlon, nverts, idx_shift);

  // add cells to mesh:
  assert(c2v.sparse_size == 3);
  AddTCells2Mesh(mesh, ncells);

  // add edges to mesh:
  mesh.edges().add(new atlas::mesh::temporary::Line(), nedges);

  // c2v:
  auto &c2v_con = mesh.cells().node_connectivity();
  AllocNbhTable(c2v_con, c2v);
  AddNeighborList(c2v_con, c2v, idx_shift);
  
  // e2v:
  auto &e2v_con = mesh.edges().node_connectivity();
  AllocNbhTable(e2v_con, e2v);
  AddNeighborList(e2v_con, e2v, idx_shift);

  // e2c:
  auto &e2c_con = mesh.edges().cell_connectivity();
  AllocNbhTable(e2c_con, e2c);
  AddNeighborList(e2c_con, e2c, idx_shift);

  // v2c:
  auto &v2c_con = mesh.nodes().cell_connectivity();
  AllocNbhTable(v2c_con, v2c);
  AddNeighborList(v2c_con, v2c, idx_shift);

  // v2e:
  auto &v2e_con = mesh.nodes().edge_connectivity();
  AllocNbhTable(v2e_con, v2e);
  AddNeighborList(v2e_con, v2e, idx_shift);
  
  // c2e:
  auto &c2e_con = mesh.cells().edge_connectivity();
  AllocNbhTable(c2e_con, c2e);
  AddNeighborList(c2e_con, c2e, idx_shift);

  return mesh_pt;
}
