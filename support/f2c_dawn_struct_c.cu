#ifndef __CUDACC__
#error "usage of unexpected non-cuda compiler"
#endif

#ifndef GRIDTOOLS_DAWN_CUDA
#define GRIDTOOLS_DAWN_CUDA
#endif

#ifndef BOOST_PP_VARIADICS
#define BOOST_PP_VARIADICS 1
#endif

#include "f2c_dawn_struct_c.cpp"
