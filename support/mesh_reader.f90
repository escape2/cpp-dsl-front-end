MODULE mesh_reader
  USE mesh_support, ONLY: dp, mesh_map_type, mesh_type, sym2loctype, create_atlas_mesh, &
       & count_con, set_meta, check_inversion

  PUBLIC

  INTEGER :: ncells, nedges, nverts
  INTEGER, DIMENSION(:), ALLOCATABLE :: cells, edges, verts
  TYPE(mesh_map_type) :: c2e, e2c, c2v, v2c, v2e, e2v, v2v

  ! coords:
  REAL(dp), ALLOCATABLE :: vlon(:), vlat(:)
  PRIVATE :: assert

CONTAINS

  SUBROUTINE assert(cond, line)
    LOGICAL, INTENT(in) :: cond
    INTEGER, INTENT(in) :: line
    IF (cond) RETURN
    PRINT*,'*** ASSERT CALL FAILED:'
    PRINT*,'*** file:',__FILE__
    PRINT*,'*** line:',line
    STOP
  END SUBROUTINE assert

  SUBROUTINE init_mesh_support(filename)
    CHARACTER(len=*), INTENT(in) :: filename
    CALL read_data(filename)
    CALL complete_data
  END SUBROUTINE init_mesh_support

  SUBROUTINE check_data
    CALL check_inversion(c2e, e2c)
    CALL check_inversion(e2c, c2e)
    CALL check_inversion(c2v, v2c)
    CALL check_inversion(v2c, c2v)
    CALL check_inversion(v2e, e2v)
  END SUBROUTINE check_data

  SUBROUTINE complete_data
    CALL derive_e2v()
  END SUBROUTINE complete_data

  SUBROUTINE read_data(filename)
    USE netcdf
    CHARACTER(len=*), INTENT(in) :: filename
    INTEGER :: ncid
    INTEGER :: vlon_num, vlat_num

    CALL check(nf90_open(filename, nf90_nowrite, ncid))

    CALL get_var_int_1d(ncid, 'cell_index', cells, ncells)
    CALL get_var_int_1d(ncid, 'edge_index', edges, nedges)
    CALL get_var_int_1d(ncid, 'vertex_index', verts, nverts)

    CALL get_var_dp_1d(ncid, 'vlat', vlat, vlat_num)
    CALL assert(vlat_num == nverts, __LINE__)
    CALL get_var_dp_1d(ncid, 'vlon', vlon, vlon_num)
    CALL assert(vlon_num == nverts, __LINE__)

    CALL set_meta(c2e,'C','E')
    CALL get_var_int_2d(ncid, 'edge_of_cell', c2e, ncells)
    CALL count_con(c2e, 3)

    CALL set_meta(e2c,'E','C')
    CALL get_var_int_2d(ncid, 'adjacent_cell_of_edge', e2c, nedges)
    CALL count_con(e2c, 2)

    CALL set_meta(c2v,'C','V')
    CALL get_var_int_2d(ncid, 'vertex_of_cell', c2v, ncells)
    CALL count_con(c2v, 3)

    CALL set_meta(v2c,'V','C')
    CALL get_var_int_2d(ncid, 'cells_of_vertex', v2c, nverts)
    CALL count_con(v2c, 6)

    CALL set_meta(v2e,'V','E')
    CALL get_var_int_2d(ncid, 'edges_of_vertex', v2e, nverts)
    CALL count_con(v2e, 6)

    CALL set_meta(V2v,'V','V')
    CALL get_var_int_2d(ncid, 'vertices_of_vertex', v2v, nverts)
    CALL count_con(v2v, 6)

    CALL check(nf90_close(ncid))

  CONTAINS

    SUBROUTINE get_var_int_1d(ncid, name, var, var_size)
      INTEGER, INTENT(in) :: ncid
      CHARACTER(len=*), INTENT(in) :: name
      INTEGER, INTENT(out) :: var_size
      INTEGER, ALLOCATABLE, INTENT(out) :: var(:)
      INTEGER :: ndims, dimids(4), dim_len, varid

      CALL check(nf90_inq_varid(ncid,  name, varid))
      dimids=-1
      CALL check(nf90_inquire_variable(ncid, varid, ndims=ndims, dimids=dimids))
      CALL assert(ndims == 1, __LINE__)
      CALL check(nf90_inquire_dimension(ncid, dimids(1), len=dim_len))
      ALLOCATE(var(dim_len))
      CALL check(nf90_get_var(ncid, varid, var))
      var_size = dim_len
    END SUBROUTINE get_var_int_1d

    SUBROUTINE get_var_dp_1d(ncid, name, var, var_size)
      INTEGER, INTENT(in) :: ncid
      CHARACTER(len=*), INTENT(in) :: name
      INTEGER, INTENT(out) :: var_size
      REAL(dp), ALLOCATABLE, INTENT(out) :: var(:)
      INTEGER :: ndims, dimids(4), dim_len, varid

      CALL check(nf90_inq_varid(ncid,  name, varid))
      dimids=-1
      CALL check(nf90_inquire_variable(ncid, varid, ndims=ndims, dimids=dimids))
      CALL assert(ndims == 1, __LINE__)
      CALL check(nf90_inquire_dimension(ncid, dimids(1), len=dim_len))
      ALLOCATE(var(dim_len))
      CALL check(nf90_get_var(ncid, varid, var))
      var_size = dim_len
    END SUBROUTINE get_var_dp_1d

    SUBROUTINE get_var_int_2d(ncid, name, x2y, expected_dense_size)
      INTEGER, INTENT(in) :: ncid, expected_dense_size
      CHARACTER(len=*), INTENT(in) :: name
      TYPE(mesh_map_type), INTENT(inout) :: x2y
      INTEGER, ALLOCATABLE :: tmp(:,:)
      INTEGER :: idim, ndims, dimids(4), dim_len(2), varid, dense_size, sparse_size

      CALL check(nf90_inq_varid(ncid,  name, varid))
      dimids=-1
      CALL check(nf90_inquire_variable(ncid, varid, ndims=ndims, dimids=dimids))
      CALL assert(ndims == 2, __LINE__)
      DO idim = 1, 2
        CALL check(nf90_inquire_dimension(ncid, dimids(idim), len=dim_len(idim)))
      ENDDO

      IF (dim_len(2) < dim_len(1)) THEN
        dense_size = dim_len(1)
        sparse_size = dim_len(2)
        ALLOCATE(x2y%map(sparse_size, dense_size))
        ALLOCATE(tmp(dense_size,sparse_size))
        CALL check(nf90_get_var(ncid, varid, tmp))
        x2y%map = TRANSPOSE(tmp)
      ELSE
        dense_size = dim_len(2)
        sparse_size = dim_len(1)
        ALLOCATE(x2y%map(sparse_size, dense_size))
        CALL check(nf90_get_var(ncid, varid, x2y%map))
      ENDIF

      CALL assert(dense_size == expected_dense_size, __LINE__)
      x2y%dense_size = dense_size
      x2y%sparse_size = sparse_size
      x2y%dense_dim_pos = 2
      x2y%is_valid = .TRUE.
    END SUBROUTINE get_var_int_2d

    SUBROUTINE check(istat)
      INTEGER, INTENT(in) :: istat
      IF(istat /= nf90_noerr) THEN
        PRINT *, TRIM(nf90_strerror(istat))
        STOP 2
      ENDIF
    END SUBROUTINE check

  END SUBROUTINE read_data

  SUBROUTINE derive_e2v
    INTEGER, PARAMETER :: con_ub = 2
    INTEGER :: e2v_map(con_ub,nedges), e2v_n(nedges)
    INTEGER :: iv, iie, ie, max_con, n
    e2v_n = 0
    e2v_map = 0
    max_con = 0
    DO iv = 1,nverts
      DO iie = 1, SIZE(v2e%map,1)
        ie = v2e%map(iie, iv)
        IF (ie < 1) Cycle
        n = e2v_n(ie)
        n = n + 1
        CALL assert(n <= con_ub, __LINE__)
        max_con = MAX(max_con, n)
        e2v_map(n,ie) = iv
        e2v_n(ie) = n
      ENDDO
    ENDDO
    CALL assert(max_con == 2, __LINE__)
    CALL assert(MINVAL(e2v_n) == 2, __LINE__)
    ALLOCATE(e2v%map(max_con,nedges), e2v%sparse_len(nedges))
    e2v%map = e2v_map(1:max_con,:)
    e2v%sparse_size = max_con
    e2v%dense_dim_pos = 2
    e2v%sparse_len = e2v_n
  END SUBROUTINE derive_e2v

  FUNCTION gen_atlas_mesh() RESULT(mesh)
    TYPE(mesh_type) :: mesh
    mesh = create_atlas_mesh(ncells, nedges, nverts, vlat, vlon, c2v, e2v, e2c, v2c, v2e, c2e)
  END FUNCTION gen_atlas_mesh

END MODULE mesh_reader
