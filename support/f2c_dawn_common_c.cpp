// backend independent dawn support for structured grids
// - domain default halo values depend on GRIDTOOLS_DAWN_HALO_EXTENT
// - domain type is independent of halos

#include "driver-includes/domain.hpp"
#include "f2c_dawn_common_c.hpp"

namespace gtd = gridtools::dawn;

extern"C" {

#if 0
  f2c_error_msg_t f2c_error_msg[] =
    {
     { F2C_E_SUCCESS, "No error" },
     { F2C_E_INVALID_ARGUMENT, "Invalid argument" }
    };
#endif
  // string:
  
  std::string* f2c_create_string_c(char const *name) {
    std::string* str_pt = new std::string(name ? std::string(name) : "");
    return str_pt;
  }

  void f2c_delete_string_c(std::string* str_pt) {
    delete str_pt;
  }



}
