MODULE dawn_unstruct
  USE iso_c_binding, ONLY : c_ptr, c_null_ptr, c_null_char, c_double,  c_associated
  USE mesh_support, ONLY: cdsl_loctype0_sym, cdsl_loctype1_sym, cdsl_loctype2_sym, &
       & cdsl_loctype_cells, cdsl_loctype_edges, cdsl_loctype_verts, &
       & mesh_type
  USE f2c_dawn_unstruct
  IMPLICIT NONE

  PUBLIC

  ! dim kinds:
  INTEGER, PARAMETER :: cdsl_idx_dim_kind = 0 ! idx dim of horizontal (idx,blk) block index layout
  INTEGER, PARAMETER :: cdsl_blk_dim_kind = 1 ! blk dim of horizontal (idx,blk) block index layout
  INTEGER, PARAMETER :: cdsl_lev_dim_kind = 2 ! vertical dim
  INTEGER, PARAMETER :: cdsl_sparse_dim_kind = 3 ! sparse dim

  ! Access to C Contab object:
  TYPE contab_type
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE, NON_OVERRIDABLE :: init => create_contab_from_str
    PROCEDURE, NON_OVERRIDABLE :: get_shape => get_contab_shape
    PROCEDURE, NON_OVERRIDABLE :: get_sparse_len => get_contab_sparse_len
    PROCEDURE, NON_OVERRIDABLE :: get_map => get_contab_map
    FINAL :: delete_contab
  END TYPE contab_type

  TYPE cdsl_unstruct_meta_data_type
    TYPE(c_ptr) :: cpt = c_null_ptr
    INTEGER, ALLOCATABLE :: data_shape(:) ! for usage checks only
  CONTAINS
    PROCEDURE, NON_OVERRIDABLE :: init => create_unstruct_meta_data
    FINAL :: delete_unstruct_meta_data
  END TYPE cdsl_unstruct_meta_data_type

  TYPE dense_field_type
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE, NON_OVERRIDABLE :: is_valid => dense_field__is_valid
    PROCEDURE, NON_OVERRIDABLE :: init => create_dense_field
    PROCEDURE, NON_OVERRIDABLE :: get_shape => get_dense_field_shape
    PROCEDURE, NON_OVERRIDABLE :: set_2d => copy_to_dawn_dense_double_2d
    PROCEDURE, NON_OVERRIDABLE :: set_3d => copy_to_dawn_dense_double_3d
    PROCEDURE, NON_OVERRIDABLE :: get_2d => copy_from_dawn_dense_double_2d
    PROCEDURE, NON_OVERRIDABLE :: get_3d => copy_from_dawn_dense_double_3d
    GENERIC :: set => set_2d, set_3d
    GENERIC :: get => get_2d, get_3d
    FINAL :: delete_dense_field
  END TYPE dense_field_type

  TYPE sparse_field_type
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE, NON_OVERRIDABLE :: init => create_sparse_field
    PROCEDURE, NON_OVERRIDABLE :: get_shape => get_sparse_field_shape
    PROCEDURE, NON_OVERRIDABLE :: set_3d => copy_to_dawn_sparse_double_3d
    PROCEDURE, NON_OVERRIDABLE :: set_4d => copy_to_dawn_sparse_double_4d
    PROCEDURE, NON_OVERRIDABLE :: get_3d => copy_from_dawn_sparse_double_3d
    PROCEDURE, NON_OVERRIDABLE :: get_4d => copy_from_dawn_sparse_double_4d
    GENERIC :: set => set_3d, set_4d
    GENERIC :: get => get_3d, get_4d
    FINAL :: delete_sparse_field
  END TYPE sparse_field_type

  TYPE vertical_field_type
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE, NON_OVERRIDABLE :: init => create_vertical_field
    PROCEDURE, NON_OVERRIDABLE :: get_size => get_vertical_field_size
    PROCEDURE, NON_OVERRIDABLE :: set => copy_to_dawn_vertical_field_double
    PROCEDURE, NON_OVERRIDABLE :: get => copy_from_dawn_vertical_field_double
    PROCEDURE, NON_OVERRIDABLE :: is_valid => vertical_field_is_valid
    FINAL :: delete_vertical_field
  END TYPE vertical_field_type

  PRIVATE :: assert

CONTAINS

  SUBROUTINE assert(cond, error_msg, line)
    LOGICAL, INTENT(in) :: cond
    CHARACTER(len=*), INTENT(in) :: error_msg
    INTEGER, OPTIONAL, INTENT(in) :: line
    IF (cond) RETURN
    PRINT*,'*** assert call failed:'
    PRINT*,'*** file: ',__FILE__
    IF (PRESENT(line)) PRINT*,'*** line: ',line
    PRINT*,'*** error: ',error_msg
    STOP 1
  END SUBROUTINE assert

  SUBROUTINE create_contab_from_str(contab, mesh, chain_str, check_format)
    CLASS(contab_type) :: contab
    TYPE(mesh_type), INTENT(in) :: mesh
    CHARACTER(len=*), INTENT(in) :: chain_str
    LOGICAL, OPTIONAL, INTENT(in) :: check_format
    LOGICAL :: check
    CHARACTER(len=1) :: ci, cip1
    INTEGER i
    IF (PRESENT(check_format)) THEN
      check = check_format
    ELSE
      check = .TRUE.
    ENDIF
    DO i = 1, LEN(chain_str)
      ci = chain_str(i:i)
      CALL assert( ci == cdsl_loctype0_sym .OR. &
           &       ci == cdsl_loctype1_sym .OR. &
           &       ci == cdsl_loctype2_sym, "invalid chain_str symbol: "//ci )

      IF (i == LEN(chain_str)) CYCLE
      cip1 = chain_str(i+1:i+1)
      CALL assert(ci /= cip1, "invalid chain link: "//ci//"->"//cip1)
    ENDDO

    contab%cpt = f2c_create_contab_from_str_f(mesh%cpt, TRIM(chain_str)//c_null_char)
  END SUBROUTINE create_contab_from_str

  SUBROUTINE get_contab_shape(contab, dense_size, sparse_size)
    CLASS(contab_type), INTENT(in) :: contab
    INTEGER :: dense_size, sparse_size
    CALL f2c_get_contab_shape_f(contab%cpt, dense_size, sparse_size)
  END SUBROUTINE get_contab_shape

  SUBROUTINE get_contab_sparse_len(contab, array)
    CLASS(contab_type), INTENT(in) :: contab
    INTEGER :: array(:)
    CALL f2c_get_contab_sparse_len_f(contab%cpt, array, SIZE(array))
  END SUBROUTINE get_contab_sparse_len

  SUBROUTINE get_contab_map(contab, array)
    CLASS(contab_type), INTENT(in) :: contab
    INTEGER :: array(:,:)
    CALL f2c_get_contab_map_f(contab%cpt, array, SHAPE(array), 2)
  END SUBROUTINE get_contab_map

  SUBROUTINE delete_contab(contab)
    TYPE(contab_type) :: contab
    CALL f2c_delete_contab_f(contab%cpt)
    contab%cpt = c_null_ptr
  END SUBROUTINE delete_contab

  INTEGER FUNCTION get_mesh_size(mesh, loctype) RESULT(num_elements)
    TYPE(mesh_type), INTENT(in) :: mesh
    INTEGER, INTENT(in) :: loctype
    num_elements = f2c_get_mesh_size_f(mesh%cpt, loctype)
  END FUNCTION get_mesh_size

  SUBROUTINE create_dense_field(field, mesh, loctype, k_size)
    CLASS(dense_field_type) :: field
    TYPE(mesh_type), INTENT(in) :: mesh
    INTEGER, INTENT(in) :: loctype
    INTEGER, INTENT(in) :: k_size
    CALL assert(.NOT. C_ASSOCIATED(field%cpt),'create_dense_field: already created')
    field%cpt = f2c_create_dense_field_f(mesh%cpt, loctype, k_size)
  END SUBROUTINE create_dense_field

  LOGICAL FUNCTION dense_field__is_valid(field) RESULT(init_state)
    CLASS(dense_field_type) :: field
    init_state = C_ASSOCIATED(field%cpt)
  END FUNCTION dense_field__is_valid

  SUBROUTINE delete_dense_field(dense_field)
    TYPE(dense_field_type) :: dense_field
    CALL f2c_delete_dense_field_f(dense_field%cpt)
    dense_field%cpt = c_null_ptr
  END SUBROUTINE delete_dense_field

  SUBROUTINE get_dense_field_shape(field, h_size, k_size)
    CLASS(dense_field_type) :: field
    INTEGER, INTENT(out) :: h_size, k_size
    CALL f2c_get_dense_field_shape_f(field%cpt, h_size, k_size)
  END SUBROUTINE get_dense_field_shape

  SUBROUTINE create_vertical_field(field, k_size)
    CLASS(vertical_field_type) :: field
    INTEGER, INTENT(in) :: k_size
    CALL assert(.NOT. C_ASSOCIATED(field%cpt),'create_vertical_field: already created')
    field%cpt = f2c_create_vertical_field_f(k_size)
  END SUBROUTINE create_vertical_field

  LOGICAL FUNCTION vertical_field_is_valid(field)
    CLASS(vertical_field_type), INTENT(in) :: field
    vertical_field_is_valid  = C_ASSOCIATED(field%cpt)
  END FUNCTION vertical_field_is_valid

  SUBROUTINE delete_vertical_field(vertical_field)
    TYPE(vertical_field_type) :: vertical_field
    CALL f2c_delete_vertical_field_f(vertical_field%cpt)
    vertical_field%cpt = c_null_ptr
  END SUBROUTINE delete_vertical_field

  SUBROUTINE get_vertical_field_size(field, k_size)
    CLASS(vertical_field_type) :: field
    INTEGER, INTENT(out) :: k_size
    CALL f2c_get_vertical_field_size_f(field%cpt, k_size)
  END SUBROUTINE get_vertical_field_size

  SUBROUTINE create_sparse_field(field, contab, k_size)
    CLASS(sparse_field_type) :: field
    TYPE(contab_type), INTENT(in) :: contab
    INTEGER, INTENT(in) :: k_size
    CALL assert(.NOT. C_ASSOCIATED(field%cpt),'create_sparse_field: already created')
    field%cpt = f2c_create_sparse_field_f(contab%cpt, k_size)
  END SUBROUTINE create_sparse_field

  SUBROUTINE delete_sparse_field(sparse_field)
    TYPE(sparse_field_type) :: sparse_field
    CALL f2c_delete_sparse_field_f(sparse_field%cpt)
    sparse_field%cpt = c_null_ptr
  END SUBROUTINE delete_sparse_field

  SUBROUTINE get_sparse_field_shape(field, dense_size, sparse_size, k_size)
    CLASS(sparse_field_type), INTENT(in) :: field
    INTEGER, INTENT(out) :: dense_size, sparse_size, k_size
    CALL f2c_get_sparse_field_shape_f(field%cpt, dense_size, sparse_size, k_size)
  END SUBROUTINE get_sparse_field_shape

  SUBROUTINE copy_to_dawn_vertical_field_double(field, array)
    CLASS(vertical_field_type) :: field
    REAL(c_double), INTENT(in) :: array(:)
    INTEGER :: k_size, ierr
    k_size = SIZE(array)
    ierr = f2c_copy_vertical_field_double_f(field%cpt, array, k_size, to_backend)
    CALL assert(ierr == 0, "copy_to_dawn_vertical_field_double", __LINE__)
  END SUBROUTINE copy_to_dawn_vertical_field_double

  SUBROUTINE copy_from_dawn_vertical_field_double(field, array)
    CLASS(vertical_field_type) :: field
    REAL(c_double) :: array(:)
    INTEGER :: k_size, ierr
    k_size = SIZE(array)
    ierr = f2c_copy_vertical_field_double_f(field%cpt, array, k_size, from_backend)
    CALL assert(ierr == 0, "copy_from_dawn_vertical_field_double", __LINE__)
  END SUBROUTINE copy_from_dawn_vertical_field_double

  ! dense_field_type:

  SUBROUTINE copy_to_dawn_dense_double(field, array, array_shape, meta_data, subset_range)
    CLASS(dense_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(in) :: array(*)
    INTEGER, INTENT(in) :: array_shape(:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type),  OPTIONAL, INTENT(in) :: subset_range
    INTEGER :: ierr
    CALL assert(SIZE(array_shape) == SIZE(meta_data%data_shape), 'copy_to_dawn_dense_double: data shape and meta data do not match')
    CALL assert(ALL(array_shape == meta_data%data_shape), 'copy_to_dawn_dense_double: data shape and meta data do not match')
    CALL assert(C_ASSOCIATED(field%cpt),'copy_to_dawn_dense_double: field not initialized')
    CALL assert(C_ASSOCIATED(meta_data%cpt),'copy_to_dawn_dense_double: meta_data not initialized')
    ierr = f2c_copy_dense_double_f(field%cpt, array, meta_data%cpt, subset_range, to_backend)
    CALL assert(ierr == 0, "copy_to_dawn_dense_double: failed")
  END SUBROUTINE copy_to_dawn_dense_double


  SUBROUTINE copy_to_dawn_dense_double_2d(field, array, meta_data, subset_range)
    CLASS(dense_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(in) :: array(:,:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type),  OPTIONAL, INTENT(in) :: subset_range
    CALL copy_to_dawn_dense_double(field, array, SHAPE(array), meta_data, subset_range)
  END SUBROUTINE copy_to_dawn_dense_double_2d


  SUBROUTINE copy_to_dawn_dense_double_3d(field, array, meta_data, subset_range)
    CLASS(dense_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(in) :: array(:,:,:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type),  OPTIONAL, INTENT(in) :: subset_range
    CALL copy_to_dawn_dense_double(field, array, SHAPE(array), meta_data, subset_range)
  END SUBROUTINE copy_to_dawn_dense_double_3d

  SUBROUTINE copy_from_dawn_dense_double(field, array, array_shape, meta_data, subset_range)
    CLASS(dense_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(inout) :: array(*)
    INTEGER, INTENT(in) :: array_shape(:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    INTEGER :: ierr
    CALL assert(SIZE(array_shape) == SIZE(meta_data%data_shape), &
         & 'copy_from_dawn_dense_double: data shape and meta data do not match')
    CALL assert(ALL(array_shape == meta_data%data_shape), &
         & 'copy_from_dawn_dense_double: data shape and meta data do not match')
    CALL assert(C_ASSOCIATED(field%cpt),'copy_from_dawn_dense_double: field not initialized')
    CALL assert(C_ASSOCIATED(meta_data%cpt),'copy_from_dawn_dense_double: meta_data not initialized')
    ierr = f2c_copy_dense_double_f(field%cpt, array, meta_data%cpt, subset_range, from_backend)
    CALL assert(ierr == 0, "copy_from_dawn_dense_double: failed")
  END SUBROUTINE copy_from_dawn_dense_double

  SUBROUTINE copy_from_dawn_dense_double_2d(field, array, meta_data, subset_range)
    CLASS(dense_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(inout) :: array(:,:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    CALL copy_from_dawn_dense_double(field, array, SHAPE(array), meta_data, subset_range)
  END SUBROUTINE copy_from_dawn_dense_double_2d

  SUBROUTINE copy_from_dawn_dense_double_3d(field, array, meta_data, subset_range)
    CLASS(dense_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(inout) :: array(:,:,:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    CALL copy_from_dawn_dense_double(field, array, SHAPE(array), meta_data, subset_range)
  END SUBROUTINE copy_from_dawn_dense_double_3d

  ! sparse_field_type:

  SUBROUTINE copy_to_dawn_sparse_double(field, array, array_shape, meta_data, subset_range)
    CLASS(sparse_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(in) :: array(*)
    INTEGER, INTENT(in) :: array_shape(:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    CHARACTER(len=*), PARAMETER :: msg_prefix = 'copy_to_dawn_sparse_double: '
    INTEGER :: ierr
    CALL assert(SIZE(array_shape) == SIZE(meta_data%data_shape), msg_prefix//'data shape and meta data do not match')
    CALL assert(ALL(array_shape == meta_data%data_shape), msg_prefix//'data shape and meta data do not match')
    CALL assert(C_ASSOCIATED(field%cpt),msg_prefix//'field not initialized')
    CALL assert(C_ASSOCIATED(meta_data%cpt),msg_prefix//'meta_data not initialized')
    ierr = f2c_copy_sparse_double_f(field%cpt, array, meta_data%cpt, subset_range, to_backend)
    CALL assert(ierr == 0, msg_prefix//"failed")
  END SUBROUTINE copy_to_dawn_sparse_double

  SUBROUTINE copy_to_dawn_sparse_double_3d(field, array, meta_data, subset_range)
    CLASS(sparse_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(in) :: array(:,:,:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    CALL copy_to_dawn_sparse_double(field, array, shape(array), meta_data, subset_range)
  END SUBROUTINE copy_to_dawn_sparse_double_3d

  SUBROUTINE copy_to_dawn_sparse_double_4d(field, array, meta_data, subset_range)
    CLASS(sparse_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(in) :: array(:,:,:,:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    CALL copy_to_dawn_sparse_double(field, array, shape(array), meta_data, subset_range)
  END SUBROUTINE copy_to_dawn_sparse_double_4d

  SUBROUTINE copy_from_dawn_sparse_double(field, array, meta_data, subset_range)
    CLASS(sparse_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(inout) :: array(*)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    CHARACTER(len=*), PARAMETER :: msg_prefix = 'copy_from_dawn_sparse_double: '
    INTEGER :: ierr
    CALL assert(C_ASSOCIATED(field%cpt),msg_prefix//'field not initialized')
    CALL assert(C_ASSOCIATED(meta_data%cpt),msg_prefix//'meta_data not initialized')
    ierr = f2c_copy_sparse_double_f(field%cpt, array, meta_data%cpt, subset_range, from_backend)
    CALL assert(ierr == 0, msg_prefix//"failed")
  END SUBROUTINE copy_from_dawn_sparse_double

  SUBROUTINE copy_from_dawn_sparse_double_3d(field, array, meta_data, subset_range)
    CLASS(sparse_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(inout) :: array(:,:,:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    CALL copy_from_dawn_sparse_double(field, array, meta_data, subset_range)
  END SUBROUTINE copy_from_dawn_sparse_double_3d

  SUBROUTINE copy_from_dawn_sparse_double_4d(field, array, meta_data, subset_range)
    CLASS(sparse_field_type), INTENT(in) :: field
    REAL(c_double), INTENT(inout) :: array(:,:,:,:)
    TYPE(cdsl_unstruct_meta_data_type) :: meta_data
    TYPE(cdsl_subset_range_type), OPTIONAL, INTENT(in) :: subset_range
    CALL copy_from_dawn_sparse_double(field, array, meta_data, subset_range)
  END SUBROUTINE copy_from_dawn_sparse_double_4d

  ! subset_range_type:

  INTEGER FUNCTION subset_range_size(subset_range) RESULT(num)
    TYPE(cdsl_subset_range_type), INTENT(in) :: subset_range
    INTEGER :: nblocks

    nblocks = subset_range%end_block - subset_range%start_block + 1
    IF (nblocks < 1) THEN
      num = 0
      RETURN
    ENDIF
    num = subset_range%end_index - subset_range%start_index + 1
    IF (nblocks == 1) RETURN
    num  = num + (nblocks -1) * subset_range%block_size
  END FUNCTION subset_range_size

  INTEGER FUNCTION subset_range_nblocks(subset_range) RESULT(nblocks)
    TYPE(cdsl_subset_range_type), INTENT(in) :: subset_range
    nblocks = subset_range%end_block - subset_range%start_block + 1
  END FUNCTION subset_range_nblocks

  SUBROUTINE gen_block_coords(flat_pos, block_size, iblock, idx)
    INTEGER, INTENT(in) :: flat_pos, block_size
    INTEGER, INTENT(out) :: iblock, idx
    ! assuming 1-based semantics for flat_pos, iblock, idx
    INTEGER :: flat_pos_c, iblock_c, idx_c ! zero-based intermediate coords

    flat_pos_c = flat_pos - 1
    iblock_c = flat_pos_c / block_size
    idx_c = flat_pos_c - iblock_c * block_size

    iblock = iblock_c + 1
    idx = idx_c + 1

  END SUBROUTINE gen_block_coords

  SUBROUTINE gen_subset_range(subset_range, num_skip, num_elements, block_size)
    TYPE(cdsl_subset_range_type), INTENT(out) :: subset_range
    INTEGER, INTENT(in) :: num_skip, num_elements
    INTEGER, INTENT(in) :: block_size
    INTEGER :: blk_size
    blk_size = block_size
    IF (num_elements == 0) THEN
      subset_range%start_block = 1
      subset_range%start_index = 1
      subset_range%end_block = -1
      subset_range%end_index = -1
      subset_range%block_size = 0
      RETURN
    ENDIF
    CALL gen_block_coords(num_skip+1, blk_size, subset_range%start_block, subset_range%start_index)
    CALL gen_block_coords(num_skip+num_elements, blk_size, subset_range%end_block, subset_range%end_index)
    subset_range%block_size = blk_size
  END SUBROUTINE gen_subset_range

  SUBROUTINE show_subset_range(subset_range)
    TYPE(cdsl_subset_range_type), INTENT(in) :: subset_range
    PRINT*,'show_subset_range:'
    PRINT*,'sblock_size =',subset_range%block_size
    PRINT*,'start_block, start_index =',subset_range%start_block, subset_range%start_index
    PRINT*,'end_block, end_index =',subset_range%end_block, subset_range%end_index
  END SUBROUTINE show_subset_range

  ! unstruct_meta_data_type:

  SUBROUTINE create_unstruct_meta_data(umd, data_shape, dim_kinds)
    CLASS(cdsl_unstruct_meta_data_type) :: umd
    INTEGER, INTENT(in) :: data_shape(:)
    INTEGER, INTENT(in) :: dim_kinds(:)
    INTEGER :: idim, ndims, dim_kind, dim_size, flat_step
    INTEGER :: idx_dim_pos, blk_dim_pos, lev_dim_pos, sparse_dim_pos

    INTEGER :: dense_idx_size, dense_idx_step
    INTEGER :: dense_blk_size, dense_blk_step
    INTEGER :: sparse_size, sparse_step
    INTEGER :: lev_size, lev_step

    CALL assert(.NOT. C_ASSOCIATED(umd%cpt),'create_unstruct_meta_data: object already created')

    ndims = SIZE(data_shape)
    CALL assert(SIZE(dim_kinds) == ndims, &
         & "cdsl_init_unstruct_meta_data: size mismatch between data_shape and dim_kinds", __LINE__)

    ! copy data_shape for usage checks:
    ALLOCATE(umd%data_shape(SIZE(data_shape)))
    umd%data_shape = data_shape

    ! unstruct_meta_data:
    lev_size=1
    lev_step=1
    dense_blk_size=1
    dense_blk_step=1
    dense_idx_size=0
    dense_idx_step=1
    sparse_size=1
    sparse_step=1

    ! dim positions:
    idx_dim_pos =  0
    blk_dim_pos =  0
    lev_dim_pos =  0
    sparse_dim_pos =  0

    flat_step = 1
    DO idim = 1, ndims
      dim_kind = dim_kinds(idim)
      dim_size = data_shape(idim)
      CALL assert(dim_size > 0 , "cdsl_init_unstruct_meta_data: invalid dim_shape", __LINE__)

      SELECT CASE (dim_kind)
      CASE (cdsl_idx_dim_kind)
        CALL assert(idx_dim_pos == 0 , "cdsl_init_unstruct_meta_data: invalid dim_kinds", __LINE__)
        idx_dim_pos = idim
        dense_idx_size = dim_size
        dense_idx_step = flat_step
      CASE (cdsl_blk_dim_kind)
        CALL assert(blk_dim_pos == 0 , "cdsl_init_unstruct_meta_data: invalid dim_kinds", __LINE__)
        blk_dim_pos =  idim
        dense_blk_size = dim_size
        dense_blk_step = flat_step
      CASE (cdsl_lev_dim_kind)
        CALL assert(lev_dim_pos == 0 , "cdsl_init_unstruct_meta_data: invalid dim_kinds", __LINE__)
        lev_dim_pos =  idim
        lev_size = dim_size
        lev_step = flat_step
      CASE (cdsl_sparse_dim_kind)
        CALL assert(sparse_dim_pos == 0 , "cdsl_init_unstruct_meta_data: invalid dim_kinds", __LINE__)
        sparse_dim_pos =  idim
        sparse_size = dim_size
        sparse_step = flat_step
      CASE default
        CALL assert(.FALSE., &
             & "cdsl_init_unstruct_meta_data: invalid dim_kind", __LINE__)
      END SELECT

      flat_step = flat_step * dim_size

    ENDDO

    ! check consistency:
    CALL assert(idx_dim_pos > 0, "cdsl_create_unstruct_meta_data: cdsl_idx_dim_kind missing", __LINE__)
#ifdef LOCAL_DEBUG
    PRINT*,'cdsl_create_unstruct_meta_data:'
    PRINT*,'lev_size       =', lev_size
    PRINT*,'lev_step       =', lev_step
    PRINT*,'dense_blk_size =', dense_blk_size
    PRINT*,'dense_blk_step =', dense_blk_step
    PRINT*,'dense_idx_size =', dense_idx_size
    PRINT*,'dense_idx_step =', dense_idx_step
    PRINT*,'sparse_size    =', sparse_size
    PRINT*,'sparse_step    =', sparse_step
#endif
    umd%cpt = f2c_create_unstruct_meta_data( &
         & lev_size, lev_step, &
         & dense_blk_size, dense_blk_step, &
         & dense_idx_size, dense_idx_step, &
         & sparse_size, sparse_step &
         & )
    !todo : add default range to meta data
    CALL assert(C_ASSOCIATED(umd%cpt),'create_unstruct_meta_data: failed')

  END SUBROUTINE create_unstruct_meta_data

  SUBROUTINE delete_unstruct_meta_data(umd)
    TYPE(cdsl_unstruct_meta_data_type) :: umd
    CALL f2c_delete_unstruct_meta_data_f(umd%cpt)
    umd%cpt = c_null_ptr
    DEALLOCATE(umd%data_shape)
  END SUBROUTINE delete_unstruct_meta_data

END MODULE dawn_unstruct
