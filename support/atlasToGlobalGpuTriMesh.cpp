// source code origintes from git@github.com:dawn-ico/icondusk-e2e.git
// file utils/atlasToGlobalGpuTriMesh.cpp
// original LICENSE file: see following /* ... */ comment block

/*
MIT License

Copyright (c) 2020 MeteoSwiss-APN

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// changes relative to the original version:
// - adapted to compile with gcc7.1
// - added function get_sparse_GlobalGpuTriMesh_size

#include "atlasToGlobalGpuTriMesh.h"

#include "driver-includes/unstructured_interface.hpp"
#include "interface/atlas_interface.hpp"

#include <unordered_map>
#include <utility>




static std::map<std::vector<dawn::LocationType>, size_t> sizeCatalogue() {
  static std::map<std::vector<dawn::LocationType>, size_t> sizeMap({
      {{dawn::LocationType::Edges, dawn::LocationType::Cells}, 2},
      {{dawn::LocationType::Edges, dawn::LocationType::Vertices}, 2},
      {{dawn::LocationType::Cells, dawn::LocationType::Edges}, 3},
      {{dawn::LocationType::Cells, dawn::LocationType::Vertices}, 3},
      {{dawn::LocationType::Vertices, dawn::LocationType::Edges}, 6},
      {{dawn::LocationType::Vertices, dawn::LocationType::Cells}, 6},
      {{dawn::LocationType::Edges, dawn::LocationType::Cells, dawn::LocationType::Vertices}, 4},
      {{dawn::LocationType::Edges, dawn::LocationType::Cells, dawn::LocationType::Edges}, 4},
      {{dawn::LocationType::Cells, dawn::LocationType::Edges, dawn::LocationType::Cells}, 3},
  });
  return sizeMap;
}

static void addNbhListToGlobalMesh(const atlas::Mesh& atlasMesh, dawn::GlobalGpuTriMesh& globalMesh,
                                   const std::vector<dawn::LocationType>& chain, size_t size,
                                   bool includeCenter) {

  auto denseSize = [&atlasMesh](dawn::LocationType denseLoc) {
    switch(denseLoc) {
    case dawn::LocationType::Edges:
      return atlasMesh.edges().size();
    case dawn::LocationType::Cells:
      return atlasMesh.cells().size();
    case dawn::LocationType::Vertices:
      return atlasMesh.nodes().size();
    default:
      assert(false);
      return -1;
    }
  };

  int* nbhListGpuPtr = nullptr;

  gpuErrchk(cudaMalloc((void**) &nbhListGpuPtr,
                       sizeof(int) * denseSize(chain[0]) * size));
  dawn::generateNbhTable<atlasInterface::atlasTag>(atlasMesh, chain, denseSize(chain[0]), size,
                                                   nbhListGpuPtr, includeCenter);
                                                   
  globalMesh.NeighborTables[dawn::UnstructuredIterationSpace{chain, includeCenter}] = nbhListGpuPtr;
}

dawn::GlobalGpuTriMesh atlasToGlobalGpuTriMesh(const atlas::Mesh& mesh) {

#if defined(__GNUC__) && (__GNUC__ * 10 +  __GNUC_MINOR__) <= 71
  // workaround
  dawn::GlobalGpuTriMesh retMesh;
  retMesh.NumEdges = mesh.edges().size();
  retMesh.NumCells = mesh.cells().size();
  retMesh.NumVertices =  mesh.nodes().size();
  retMesh.EdgeStride = mesh.edges().size();
  retMesh.CellStride = mesh.cells().size();
  retMesh.VertexStride = mesh.nodes().size();

#else
  // not working for gcc 7.1: error "sorry, unimplemented: non-trivial designated initializers not supported"
  dawn::GlobalGpuTriMesh retMesh = {.NumEdges = mesh.edges().size(),
                                    .NumCells = mesh.cells().size(),
                                    .NumVertices = mesh.nodes().size(),
                                    .EdgeStride = mesh.edges().size(),
                                    .CellStride = mesh.cells().size(),
                                    .VertexStride = mesh.nodes().size()};
#endif


  for(const auto [chain, size] : sizeCatalogue()) {
    addNbhListToGlobalMesh(mesh, retMesh, chain, size, false);
    addNbhListToGlobalMesh(mesh, retMesh, chain, size + 1, true);
  }
  return retMesh;
}


size_t get_sparse_GlobalGpuTriMesh_size(const std::vector<dawn::LocationType>& chain) {
  auto size_map = sizeCatalogue();
  size_t sparse_size = size_map[chain];
  return sparse_size;
}

size_t get_dense_GlobalGpuTriMesh_size(const dawn::GlobalGpuTriMesh &ggtm, const std::vector<dawn::LocationType>& chain) {
  if (chain.size() < 1) return 0;

  switch(chain[0]) {
  case dawn::LocationType::Edges:
    return ggtm.NumEdges;
  case dawn::LocationType::Cells:
    return ggtm.NumCells;
  case dawn::LocationType::Vertices:
    return ggtm.NumVertices;
  default:
    assert(false);
    return 0;
  }
}

