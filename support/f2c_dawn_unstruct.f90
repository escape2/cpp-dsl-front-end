MODULE f2c_dawn_unstruct
  USE iso_c_binding, ONLY : c_int, c_ptr
  IMPLICIT NONE

  PUBLIC

  INTEGER(c_int), PARAMETER :: to_backend = 0
  INTEGER(c_int), PARAMETER :: from_backend = 1

  !TODO: only use ptr to C-struct and move type to dawn_unstruct.f90
  TYPE, BIND(c) :: cdsl_subset_range_type
    ! describes index range within a blocked index layout (e.g., as used in ICON)
    INTEGER(C_INT) :: start_block = 1
    INTEGER(C_INT) :: start_index = 1
    INTEGER(C_INT) :: end_block = 0
    INTEGER(C_INT) :: end_index = 0
    ! block_size = 0 means: use whatever the full range is and override the other components
    INTEGER(C_INT) :: block_size = 1
  END TYPE cdsl_subset_range_type

  TYPE(cdsl_subset_range_type), PARAMETER :: cdsl_full_range = cdsl_subset_range_type(&
       & start_block=1, start_index=1, end_block=0, end_index=0, block_size=0)

  INTERFACE

    TYPE(c_ptr) FUNCTION f2c_create_mesh_from_netcdf_f(filename) &
         & BIND(c, name='f2c_create_mesh_from_netcdf_c')
      USE iso_c_binding, ONLY: c_ptr, c_char
      IMPLICIT NONE
      CHARACTER(len=1,kind=c_char), DIMENSION(*), INTENT(in) :: filename
    END FUNCTION f2c_create_mesh_from_netcdf_f

    TYPE(c_ptr) FUNCTION f2c_create_mesh_explicit_f(ncells, nedges, nverts, &
         & vlat, vlon, &
         & c2v_map, c2v_sparse_size, c2v_sparse_step, &
         & e2v_map, e2v_sparse_size, e2v_sparse_step, &
         & e2c_map, e2c_sparse_size, e2c_sparse_step, &
         & v2c_map, v2c_sparse_size, v2c_sparse_step, &
         & v2e_map, v2e_sparse_size, v2e_sparse_step, &
         & c2e_map, c2e_sparse_size, c2e_sparse_step) &
         & BIND(c, name='f2c_create_mesh_explicit_c')
      USE iso_c_binding, ONLY: c_ptr, c_double, c_int
      IMPLICIT NONE
      REAL(c_double) :: vlat(*), vlon(*)
      INTEGER(c_int) :: c2v_map(*), e2v_map(*), e2c_map(*), v2c_map(*), v2e_map(*), c2e_map(*)
      INTEGER(c_int), VALUE :: ncells, nedges, nverts
      INTEGER(c_int), VALUE :: c2v_sparse_size, c2v_sparse_step
      INTEGER(c_int), VALUE :: e2v_sparse_size, e2v_sparse_step
      INTEGER(c_int), VALUE :: e2c_sparse_size, e2c_sparse_step
      INTEGER(c_int), VALUE :: v2c_sparse_size, v2c_sparse_step
      INTEGER(c_int), VALUE :: v2e_sparse_size, v2e_sparse_step
      INTEGER(c_int), VALUE :: c2e_sparse_size, c2e_sparse_step
    END FUNCTION f2c_create_mesh_explicit_f

    TYPE(c_ptr) FUNCTION f2c_create_contab_from_str_f(mesh_pt, chain_str) &
         & BIND(c, name='f2c_create_contab_from_str_c')
      USE iso_c_binding, ONLY: c_ptr, c_char
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: mesh_pt
      CHARACTER(len=1,kind=c_char), DIMENSION(*), INTENT(in):: chain_str
    END FUNCTION f2c_create_contab_from_str_f

    SUBROUTINE f2c_get_contab_shape_f(contab_pt, dense_size, sparse_size) &
         & BIND(c, name='f2c_get_contab_shape_c')
      USE iso_c_binding, ONLY: c_ptr, c_char, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: contab_pt
      INTEGER(c_int) :: dense_size, sparse_size
    END SUBROUTINE f2c_get_contab_shape_f

    SUBROUTINE f2c_get_contab_sparse_len_f(contab_pt, array, array_size) &
         & BIND(c, name='f2c_get_contab_sparse_len_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: contab_pt
      INTEGER(c_int) :: array(*)
      INTEGER(c_int), VALUE :: array_size
    END SUBROUTINE f2c_get_contab_sparse_len_f

    SUBROUTINE f2c_get_contab_map_f(contab_pt, array, array_shape, len_shape) &
         & BIND(c, name='f2c_get_contab_map_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: contab_pt
      INTEGER(c_int) :: array(*)
      INTEGER(c_int) :: array_shape(*)
      INTEGER(c_int), VALUE :: len_shape
    END SUBROUTINE f2c_get_contab_map_f

    SUBROUTINE f2c_delete_contab_f(contab_pt) &
         & BIND(c, name='f2c_delete_contab_c')
      USE iso_c_binding, ONLY: c_ptr
      TYPE(c_ptr), VALUE :: contab_pt
    END SUBROUTINE f2c_delete_contab_f

    INTEGER(c_int) FUNCTION f2c_get_mesh_size_f(mesh_pt, loctype) &
         & BIND(c, name='f2c_get_mesh_size_c')
      USE iso_c_binding, ONLY: c_ptr, c_double, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: mesh_pt
      INTEGER(c_int), VALUE :: loctype
    END FUNCTION f2c_get_mesh_size_f

    TYPE(c_ptr) FUNCTION f2c_create_dense_field_f(mesh_pt, loctype, k_size) &
         & BIND(c, name='f2c_create_dense_field_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: mesh_pt
      INTEGER(c_int), VALUE :: loctype
      INTEGER(c_int), VALUE :: k_size
    END FUNCTION f2c_create_dense_field_f

    SUBROUTINE f2c_delete_dense_field_f(field_pt) &
         & BIND(c, name='f2c_delete_dense_field_c')
      USE iso_c_binding, ONLY: c_ptr
      TYPE(c_ptr), VALUE :: field_pt
    END SUBROUTINE f2c_delete_dense_field_f

    SUBROUTINE f2c_get_dense_field_shape_f(field_pt, h_size, k_size) &
         & BIND(c, name='f2c_get_dense_field_shape_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: field_pt
      INTEGER(c_int), INTENT(out) :: h_size, k_size
    END SUBROUTINE f2c_get_dense_field_shape_f

    TYPE(c_ptr) FUNCTION f2c_create_sparse_field_f(contab_pt, k_size) &
         & BIND(c, name='f2c_create_sparse_field_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: contab_pt
      INTEGER(c_int), VALUE :: k_size
    END FUNCTION f2c_create_sparse_field_f

    TYPE(c_ptr) FUNCTION f2c_create_sparse_field_tr_f(contab_pt, k_size) &
         & BIND(c, name='f2c_create_sparse_field_tr_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: contab_pt
      INTEGER(c_int), VALUE :: k_size
    END FUNCTION f2c_create_sparse_field_tr_f

    SUBROUTINE f2c_delete_sparse_field_f(field_pt) &
         & BIND(c, name='f2c_delete_sparse_field_c')
      USE iso_c_binding, ONLY: c_ptr
      TYPE(c_ptr), VALUE :: field_pt
    END SUBROUTINE f2c_delete_sparse_field_f

    TYPE(c_ptr) FUNCTION f2c_create_vertical_field_f(k_size) &
         & BIND(c, name='f2c_create_vertical_field_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      INTEGER(c_int), VALUE :: k_size
    END FUNCTION f2c_create_vertical_field_f

    SUBROUTINE f2c_delete_vertical_field_f(field_pt) &
         & BIND(c, name='f2c_delete_vertical_field_c')
      USE iso_c_binding, ONLY: c_ptr
      TYPE(c_ptr), VALUE :: field_pt
    END SUBROUTINE f2c_delete_vertical_field_f

    SUBROUTINE f2c_get_sparse_field_shape_f(field_pt, dense_size, sparse_size, k_size) &
         & BIND(c, name='f2c_get_sparse_field_shape_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: field_pt
      INTEGER(c_int), INTENT(out) :: dense_size, sparse_size, k_size
    END SUBROUTINE f2c_get_sparse_field_shape_f

    SUBROUTINE f2c_get_vertical_field_size_f(field_pt, k_size) &
         & BIND(c, name='f2c_get_vertical_field_size_c')
      USE iso_c_binding, ONLY: c_ptr, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: field_pt
      INTEGER(c_int), INTENT(out) :: k_size
    END SUBROUTINE f2c_get_vertical_field_size_f

    INTEGER(c_int) FUNCTION f2c_copy_dense_double_f(field_pt, array, meta_data_cpt, range, dir) &
         & BIND(c, name='f2c_copy_dense_double_c')
      USE iso_c_binding, ONLY: c_ptr, c_double, c_int
      IMPORT cdsl_subset_range_type
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: field_pt
      REAL(c_double) :: array(*)
      TYPE(c_ptr), VALUE :: meta_data_cpt
      TYPE(cdsl_subset_range_type), OPTIONAL :: range
      INTEGER(c_int), VALUE :: dir
    END FUNCTION f2c_copy_dense_double_f

    INTEGER(c_int) FUNCTION f2c_copy_sparse_double_f(field_pt, array, meta_data_cpt, range, dir) &
         & BIND(c, name='f2c_copy_sparse_double_c')
      USE iso_c_binding, ONLY: c_ptr, c_double, c_int
      IMPORT cdsl_subset_range_type
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: field_pt
      REAL(c_double) :: array(*)
      TYPE(c_ptr), VALUE :: meta_data_cpt
      TYPE(cdsl_subset_range_type), OPTIONAL :: range
      INTEGER(c_int), VALUE :: dir
    END FUNCTION f2c_copy_sparse_double_f

    INTEGER(c_int) FUNCTION f2c_copy_all_from_dawn_sparse_double_f(field_pt, array, meta_data_cpt) &
         & BIND(c, name='f2c_copy_all_from_dawn_sparse_double_c')
      USE iso_c_binding, ONLY: c_ptr, c_double, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: field_pt
      REAL(c_double) :: array(*)
      TYPE(c_ptr), VALUE :: meta_data_cpt
    END FUNCTION f2c_copy_all_from_dawn_sparse_double_f

    INTEGER(c_int) FUNCTION f2c_copy_vertical_field_double_f(field_pt, array, k_size, dir) &
         & BIND(c, name='f2c_copy_vertical_field_double_c')
      USE iso_c_binding, ONLY: c_ptr, c_double, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: field_pt
      REAL(c_double) :: array(*)
      INTEGER(c_int), VALUE :: k_size
      INTEGER(c_int), VALUE :: dir
    END FUNCTION f2c_copy_vertical_field_double_f

    INTEGER(c_int) FUNCTION f2c_copy_to_dawn_vertical_field_double_f(field_pt, array, k_size) &
         & BIND(c, name='f2c_copy_to_dawn_vertical_field_double_c')
      USE iso_c_binding, ONLY: c_ptr, c_double, c_int
      IMPLICIT NONE
      TYPE(c_ptr), VALUE :: field_pt
      REAL(c_double) :: array(*)
      INTEGER(c_int), VALUE :: k_size
    END FUNCTION f2c_copy_to_dawn_vertical_field_double_f

    TYPE(c_ptr) FUNCTION f2c_create_unstruct_meta_data( &
         & lev_size, lev_step, &
         & dense_blk_size, dense_blk_step, &
         & dense_idx_size, dense_idx_step, &
         & sparse_size, sparse_step &
         & ) BIND(c, name='f2c_create_unstruct_meta_data_c')
      IMPORT c_ptr, c_int
      IMPLICIT NONE
      INTEGER(c_int), VALUE :: &
           & lev_size, lev_step, &
           & dense_blk_size, dense_blk_step, &
           & dense_idx_size, dense_idx_step, &
           & sparse_size, sparse_step
    END FUNCTION f2c_create_unstruct_meta_data

    SUBROUTINE f2c_delete_unstruct_meta_data_f(umd_cpt) &
         & BIND(c, name='f2c_delete_unstruct_meta_data_c')
      IMPORT c_ptr
      TYPE(c_ptr), VALUE :: umd_cpt
    END SUBROUTINE f2c_delete_unstruct_meta_data_f

  END INTERFACE

END MODULE f2c_dawn_unstruct
