MODULE DAWN_STRUCT
  USE iso_c_binding, ONLY : c_null_ptr, C_ASSOCIATED, c_ptr, c_int, c_double, &
       & c_null_char, c_loc, c_char
  USE f2c_dawn_common
  USE F2C_DAWN_STRUCT
  IMPLICIT NONE
  PRIVATE

  PUBLIC :: domain_type
  PUBLIC :: meta_data_2d_type, storage_struct2d_double_type
  PUBLIC :: meta_data_3d_type, storage_struct3d_double_type
  PUBLIC :: meta_data_column_type, storage_column_double_type
  PUBLIC :: get_dawn_initial_halo_size

  TYPE domain_type
    TYPE(c_ptr) :: cpt = c_null_ptr
    INTEGER :: domain_shape(3)
  CONTAINS
    PROCEDURE :: init_from_shape => create_dawn_domain_from_shape
    PROCEDURE :: init_from_dim_sizes => create_dawn_domain
    GENERIC :: init => init_from_shape, init_from_dim_sizes
    PROCEDURE :: get_shape => get_dawn_domain_shape
    PROCEDURE :: get_halos => get_dawn_domain_halos
    PROCEDURE :: set_halos => set_dawn_domain_halos
    PROCEDURE :: is_initialized => domain_is_initialized
    FINAL :: delete_dawn_domain
  END TYPE domain_type

  TYPE meta_data_2d_type
    PRIVATE
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE :: init_from_data_shape => create_dawn_meta_data_2d
    PROCEDURE :: init_from_domain => create_dawn_meta_data_2d_from_dom
    GENERIC :: init => init_from_data_shape, init_from_domain
    PROCEDURE :: is_initialized => meta_data_2d_is_initialized
    FINAL :: delete_dawn_meta_data_2d
  END TYPE meta_data_2d_type

  TYPE meta_data_3d_type
    PRIVATE
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE :: init_from_data_shape => create_dawn_meta_data_3d
    PROCEDURE :: init_from_domain => create_dawn_meta_data_3d_from_dom
    GENERIC :: init => init_from_data_shape, init_from_domain
    PROCEDURE :: is_initialized => meta_data_3d_is_initialized
    FINAL :: delete_dawn_meta_data_3d
  END TYPE meta_data_3d_type

  TYPE meta_data_column_type
    PRIVATE
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE :: init_from_column_size => create_dawn_meta_data_column
    PROCEDURE :: init_from_domain => create_dawn_meta_data_column_from_dom
    GENERIC :: init => init_from_column_size, init_from_domain
    PROCEDURE :: is_initialized => meta_data_column_is_initialized
    FINAL :: delete_dawn_meta_data_column
  END TYPE meta_data_column_type

  TYPE storage_struct2d_double_type
    ! represents Dawn's storage_ij_t type
    TYPE(c_ptr) :: cpt = c_null_ptr
    TYPE(meta_data_2d_type) :: md
  CONTAINS
    PROCEDURE :: init => create_dawn_storage_2d
    PROCEDURE :: set => copy_win_to_dawn_struct2d_double
    PROCEDURE :: get => copy_win_from_dawn_struct2d_double
    PROCEDURE :: is_initialized => storage_struct2d_double_is_initialized
    FINAL :: delete_dawn_storage_2d
  END TYPE storage_struct2d_double_type

  TYPE storage_struct3d_double_type
    ! represents Dawn's storage_ijk_t type
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE :: init => create_dawn_storage_3d
    PROCEDURE :: set => copy_win_to_dawn_struct3d_double
    PROCEDURE :: get => copy_win_from_dawn_struct3d_double
    PROCEDURE :: is_initialized => storage_struct3d_double_is_initialized
    FINAL :: delete_dawn_storage_3d
  END TYPE storage_struct3d_double_type

  TYPE storage_column_double_type
    ! represents Dawn's storage_k_t type
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE :: init => create_dawn_storage_column
    PROCEDURE :: set => copy_win_to_dawn_column_double
    PROCEDURE :: get => copy_win_from_dawn_column_double
    PROCEDURE :: is_initialized => storage_column_double_is_initialized
    FINAL :: delete_dawn_storage_column
  END TYPE storage_column_double_type

  TYPE string_type
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE :: init => create_string
    FINAL :: delete_string
  END TYPE string_type


  CHARACTER(len=*), PARAMETER :: filename = &
       & __FILE__

CONTAINS

  SUBROUTINE create_string(str, text)
    CLASS(string_type) :: str
    CHARACTER(len=*), OPTIONAL, INTENT(in) :: text
    IF (PRESENT(text)) THEN
      str%cpt = f2c_create_string_f(TRIM(text)//c_null_char)
    ENDIF
  END SUBROUTINE create_string

  SUBROUTINE delete_string(str)
    TYPE(string_type) :: str
    IF (C_ASSOCIATED(str%cpt)) THEN
      CALL f2c_delete_string_f(str%cpt)
      str%cpt = c_null_ptr
    ENDIF
  END SUBROUTINE delete_string

  TYPE(c_ptr) FUNCTION double_var_c_ptr(var) RESULT(ptr)
    REAL(c_double), OPTIONAL, TARGET, INTENT(in) :: var
    IF (PRESENT(var)) THEN
      ptr = c_LOC(var)
    ELSE
      ptr = c_null_ptr
    ENDIF
  END FUNCTION double_var_c_ptr

  ! domain:

  SUBROUTINE create_dawn_domain(dom, data_size1, data_size2, data_size3)
    CLASS(domain_type) :: dom
    INTEGER, INTENT(in) :: data_size1, data_size2, data_size3
    dom%domain_shape = [data_size1, data_size2, data_size3]
    dom%cpt = f2c_create_dawn_domain_f(data_size1, data_size2, data_size3)
    CALL assert(C_ASSOCIATED(dom%cpt),'create_dawn_domain failed')
  END SUBROUTINE create_dawn_domain

  SUBROUTINE create_dawn_domain_from_shape(dom, dom_shape)
    CLASS(domain_type) :: dom
    INTEGER, INTENT(in) :: dom_shape(:)
    INTEGER :: test_shape(3)
    IF (SIZE(dom_shape) /= 3) CALL error(filename,__LINE__,'expected a 3d domain')
    dom%domain_shape = dom_shape
    dom%cpt = f2c_create_dawn_domain_f(dom_shape(1), dom_shape(2), dom_shape(3))
    CALL assert(C_ASSOCIATED(dom%cpt),'create_dawn_domain failed')
    test_shape = dom%get_shape()
    CALL assert(ALL(test_shape == dom_shape), 'error in create_dawn_domain')
  END SUBROUTINE create_dawn_domain_from_shape

  SUBROUTINE delete_dawn_domain(dom)
    TYPE(domain_type) :: dom
    CALL f2c_delete_dawn_domain_f(dom%cpt)
    dom%cpt = c_null_ptr
  END SUBROUTINE delete_dawn_domain

  FUNCTION get_dawn_domain_shape(dom) RESULT(dom_shape)
    CLASS(domain_type), INTENT(in) :: dom
    INTEGER :: dom_shape(3)
    CALL check_err(f2c_get_dawn_domain_shape_f(dom%cpt, dom_shape))
  END FUNCTION get_dawn_domain_shape

  FUNCTION domain_is_initialized(dom) RESULT(is_initialized)
    CLASS(domain_type), INTENT(in) :: dom
    LOGICAL :: is_initialized
    is_initialized = C_ASSOCIATED(dom%cpt)
  END FUNCTION domain_is_initialized

  ! halos

  FUNCTION get_dawn_domain_halos(dom) RESULT(dom_halos)
    CLASS(domain_type), INTENT(in) :: dom
    INTEGER :: dom_halos(2,2)
    CALL check_err(f2c_get_dawn_domain_halos_f(dom%cpt, dom_halos))
  END FUNCTION get_dawn_domain_halos

  FUNCTION get_dawn_initial_halo_size() RESULT(ini_halo_size)
    INTEGER :: ini_halo_size
    ini_halo_size = f2c_get_dawn_initial_halo_size_f()
  END FUNCTION get_dawn_initial_halo_size

  SUBROUTINE set_dawn_domain_halos(dom, halos)
    CLASS(domain_type), INTENT(in) :: dom
    INTEGER :: halos(2,2)
    CALL check_err(f2c_set_dawn_domain_halos_f(dom%cpt, halos))
  END SUBROUTINE set_dawn_domain_halos

  ! 2d meta data:

  SUBROUTINE create_dawn_meta_data_2d(meta_data, data_size1, data_size2)
    CLASS(meta_data_2d_type) :: meta_data
    INTEGER, INTENT(in) :: data_size1, data_size2
    meta_data%cpt = f2c_create_dawn_meta_data_2d_f(data_size1, data_size2)
    CALL assert(C_ASSOCIATED(meta_data%cpt),'create_dawn_meta_data_2d failed')
  END SUBROUTINE create_dawn_meta_data_2d

  SUBROUTINE create_dawn_meta_data_2d_from_dom(meta_data, dom)
    CLASS(meta_data_2d_type) :: meta_data
    TYPE(domain_type), INTENT(in) :: dom
    meta_data%cpt = f2c_create_dawn_meta_data_2d_f(dom%domain_shape(1), dom%domain_shape(2))
    CALL assert(C_ASSOCIATED(meta_data%cpt),'create_dawn_meta_data_2d_from_dom failed')
  END SUBROUTINE create_dawn_meta_data_2d_from_dom

  SUBROUTINE delete_dawn_meta_data_2d(meta_data)
    TYPE(meta_data_2d_type) :: meta_data
    CALL f2c_delete_dawn_meta_data_2d_f(meta_data%cpt)
    meta_data%cpt = c_null_ptr
  END SUBROUTINE delete_dawn_meta_data_2d

  FUNCTION meta_data_2d_is_initialized(meta_data) RESULT(is_initialized)
    CLASS(meta_data_2d_type), INTENT(in) :: meta_data
    LOGICAL :: is_initialized
    is_initialized = C_ASSOCIATED(meta_data%cpt)
  END FUNCTION meta_data_2d_is_initialized

  ! 3d meta data:

  SUBROUTINE create_dawn_meta_data_3d(meta_data, data_size1, data_size2, data_size3)
    CLASS(meta_data_3d_type) :: meta_data
    INTEGER, INTENT(in) :: data_size1, data_size2, data_size3
    meta_data%cpt = f2c_create_dawn_meta_data_3d_f(data_size1, data_size2, data_size3)
    CALL assert(C_ASSOCIATED(meta_data%cpt),'create_dawn_meta_data_3d failed')
  END SUBROUTINE create_dawn_meta_data_3d

  SUBROUTINE create_dawn_meta_data_3d_from_dom(meta_data, dom)
    CLASS(meta_data_3d_type) :: meta_data
    TYPE(domain_type), INTENT(in) :: dom
    meta_data%cpt = f2c_create_dawn_meta_data_3d_f(dom%domain_shape(1), dom%domain_shape(2), dom%domain_shape(3))
    CALL assert(C_ASSOCIATED(meta_data%cpt),'create_dawn_meta_data_3d_from_dom failed')
  END SUBROUTINE create_dawn_meta_data_3d_from_dom

  SUBROUTINE delete_dawn_meta_data_3d(meta_data)
    TYPE(meta_data_3d_type) :: meta_data
    CALL f2c_delete_dawn_meta_data_3d_f(meta_data%cpt)
    meta_data%cpt = c_null_ptr
  END SUBROUTINE delete_dawn_meta_data_3d

  FUNCTION meta_data_3d_is_initialized(meta_data) RESULT(is_initialized)
    CLASS(meta_data_3d_type), INTENT(in) :: meta_data
    LOGICAL :: is_initialized
    is_initialized = C_ASSOCIATED(meta_data%cpt)
  END FUNCTION meta_data_3d_is_initialized

  ! column meta data:

  SUBROUTINE create_dawn_meta_data_column(meta_data, data_size)
    CLASS(meta_data_column_type) :: meta_data
    INTEGER, INTENT(in) :: data_size
    meta_data%cpt = f2c_create_dawn_meta_data_column_f(data_size)
    CALL assert(C_ASSOCIATED(meta_data%cpt),'create_dawn_meta_data_column failed')
  END SUBROUTINE create_dawn_meta_data_column

  SUBROUTINE create_dawn_meta_data_column_from_dom(meta_data, dom)
    CLASS(meta_data_column_type) :: meta_data
    TYPE(domain_type), INTENT(in) :: dom
    meta_data%cpt = f2c_create_dawn_meta_data_column_f(dom%domain_shape(3))
    CALL assert(C_ASSOCIATED(meta_data%cpt),'create_dawn_meta_data_column_from_dom failed')
  END SUBROUTINE create_dawn_meta_data_column_from_dom

  SUBROUTINE delete_dawn_meta_data_column(meta_data)
    TYPE(meta_data_column_type) :: meta_data
    CALL f2c_delete_dawn_meta_data_column_f(meta_data%cpt)
    meta_data%cpt = c_null_ptr
  END SUBROUTINE delete_dawn_meta_data_column

  FUNCTION meta_data_column_is_initialized(meta_data) RESULT(is_initialized)
    CLASS(meta_data_column_type), INTENT(in) :: meta_data
    LOGICAL :: is_initialized
    is_initialized = C_ASSOCIATED(meta_data%cpt)
  END FUNCTION meta_data_column_is_initialized

  ! 2d storage:

  SUBROUTINE create_dawn_storage_2d(storage, meta_data, init_value, name, init_array, init_win)
    CLASS(storage_struct2d_double_type) :: storage
    TYPE(meta_data_2d_type), INTENT(in) :: meta_data
    REAL(c_double), INTENT(in), TARGET, OPTIONAL :: init_value
    REAL(c_double), INTENT(in), OPTIONAL :: init_array(:,:)
    INTEGER, INTENT(in), OPTIONAL :: init_win(2,2)
    CHARACTER(len=*), OPTIONAL, INTENT(in) :: name

    TYPE(c_ptr) :: init_value_ptr
    TYPE(string_type) :: name_str
    IF (C_ASSOCIATED(storage%cpt)) &
         & CALL error(filename,__LINE__,'create_dawn_storage_2d: storage is already allocated')

    init_value_ptr = double_var_c_ptr(init_value)
    CALL name_str%init(name)
    storage%cpt = f2c_create_dawn_storage_2d_f(meta_data%cpt, init_value_ptr, name_str%cpt)
    CALL assert(C_ASSOCIATED(storage%cpt),'create_dawn_storage_2d failed')
    IF ( PRESENT(init_array) ) THEN
      CALL copy_win_to_dawn_struct2d_double(storage, init_array, init_win)
    ENDIF
  END SUBROUTINE create_dawn_storage_2d

  SUBROUTINE delete_dawn_storage_2d(storage)
    TYPE(storage_struct2d_double_type) :: storage
    CALL f2c_delete_dawn_storage_2d_f(storage%cpt)
    storage%cpt = c_null_ptr
  END SUBROUTINE delete_dawn_storage_2d

  FUNCTION storage_struct2d_double_is_initialized(storage) RESULT(is_initialized)
    CLASS(storage_struct2d_double_type) :: storage
    LOGICAL :: is_initialized
    is_initialized = C_ASSOCIATED(storage%cpt)
  END FUNCTION storage_struct2d_double_is_initialized

  ! 3d storage:

  SUBROUTINE create_dawn_storage_3d(storage, meta_data, init_value, name, init_array, init_win)
    CLASS(storage_struct3d_double_type) :: storage
    TYPE(meta_data_3d_type), INTENT(in) :: meta_data
    REAL(c_double), INTENT(in), TARGET, OPTIONAL :: init_value
    CHARACTER(len=*), OPTIONAL, INTENT(in) :: name
    REAL(c_double), INTENT(in), OPTIONAL :: init_array(:,:,:)
    INTEGER, INTENT(in), OPTIONAL :: init_win(2,3)

    TYPE(c_ptr) :: init_value_ptr
    TYPE(string_type) :: name_str
    IF (C_ASSOCIATED(storage%cpt)) &
         & CALL error(filename,__LINE__,'create_dawn_storage_3d: storage is already allocated')

    init_value_ptr = double_var_c_ptr(init_value)
    call name_str%init(name)
    storage%cpt = f2c_create_dawn_storage_3d_f(meta_data%cpt, init_value_ptr, name_str%cpt)
    CALL assert(C_ASSOCIATED(storage%cpt),'create_dawn_storage_3d failed')
    IF ( PRESENT(init_array) ) THEN
      CALL copy_win_to_dawn_struct3d_double(storage, init_array, init_win)
    ENDIF
  END SUBROUTINE create_dawn_storage_3d

  SUBROUTINE delete_dawn_storage_3d(storage)
    TYPE(storage_struct3d_double_type) :: storage
    CALL f2c_delete_dawn_storage_3d_f(storage%cpt)
    storage%cpt = c_null_ptr
  END SUBROUTINE delete_dawn_storage_3d

  FUNCTION storage_struct3d_double_is_initialized(storage) RESULT(is_initialized)
    CLASS(storage_struct3d_double_type) :: storage
    LOGICAL :: is_initialized
    is_initialized = C_ASSOCIATED(storage%cpt)
  END FUNCTION storage_struct3d_double_is_initialized

  ! column storage:

  SUBROUTINE create_dawn_storage_column(storage, meta_data, init_value, name, init_array, init_win)
    CLASS(storage_column_double_type) :: storage
    TYPE(meta_data_column_type), INTENT(in) :: meta_data
    REAL(c_double), INTENT(in), TARGET, OPTIONAL :: init_value
    CHARACTER(len=*), OPTIONAL, INTENT(in) :: name
    REAL(c_double), INTENT(in), OPTIONAL :: init_array(:)
    INTEGER, INTENT(in), OPTIONAL :: init_win(2)
    TYPE(c_ptr) :: init_value_ptr
    TYPE(string_type) :: name_str
    IF (C_ASSOCIATED(storage%cpt)) &
         & CALL error(filename,__LINE__,'create_dawn_storage_column: storage is already allocated')

    init_value_ptr = double_var_c_ptr(init_value)
    call name_str%init(name)
    storage%cpt = f2c_create_dawn_storage_column_f(meta_data%cpt, init_value_ptr, name_str%cpt)
    CALL assert(C_ASSOCIATED(storage%cpt),'create_dawn_storage_column failed')
    IF ( PRESENT(init_array) ) THEN
      CALL copy_win_to_dawn_column_double(storage, init_array, init_win)
    ENDIF
  END SUBROUTINE create_dawn_storage_column

  SUBROUTINE delete_dawn_storage_column(storage)
    TYPE(storage_column_double_type) :: storage
    CALL f2c_delete_dawn_storage_column_f(storage%cpt)
    storage%cpt = c_null_ptr
  END SUBROUTINE delete_dawn_storage_column

  FUNCTION storage_column_double_is_initialized(storage) RESULT(is_initialized)
    CLASS(storage_column_double_type) :: storage
    LOGICAL :: is_initialized
    is_initialized = C_ASSOCIATED(storage%cpt)
  END FUNCTION storage_column_double_is_initialized

  ! copy 2d win to Dawm:
  SUBROUTINE copy_win_to_dawn_struct2d_double(storage, array,  win)
    CLASS(storage_struct2d_double_type) :: storage
    REAL(c_double), INTENT(in) :: array(:,:)
    INTEGER, INTENT(in), OPTIONAL :: win(2,2)
    INTEGER :: a_shape(2)
    INTEGER :: default_win(2,2)
    a_shape = SHAPE(array)
    IF (PRESENT(win)) THEN
      CALL check_err(f2c_copy_win_to_dawn_struct2d_double_f(array, a_shape, win, storage%cpt))
    ELSE
      default_win(:,1) = [1, a_shape(1)]
      default_win(:,2) = [1, a_shape(2)]
      CALL check_err(f2c_copy_win_to_dawn_struct2d_double_f(array, a_shape, default_win, storage%cpt))
    ENDIF
  END SUBROUTINE copy_win_to_dawn_struct2d_double

  ! copy 3d win to Dawm:
  SUBROUTINE copy_win_to_dawn_struct3d_double(storage, array, win)
    class(storage_struct3d_double_type) :: storage
    REAL(c_double), INTENT(in) :: array(:,:,:)
    INTEGER, INTENT(in), OPTIONAL :: win(2,3)
    INTEGER :: a_shape(3)
    INTEGER :: default_win(2,3)
    a_shape = SHAPE(array)
    IF (PRESENT(win)) THEN
      CALL check_err(f2c_copy_win_to_dawn_struct3d_double_f(array, a_shape, win, storage%cpt))
    ELSE
      default_win(:,1) = [1, a_shape(1)]
      default_win(:,2) = [1, a_shape(2)]
      default_win(:,3) = [1, a_shape(3)]
      CALL check_err(f2c_copy_win_to_dawn_struct3d_double_f(array, a_shape, default_win, storage%cpt))
    ENDIF
  END SUBROUTINE copy_win_to_dawn_struct3d_double

  ! copy column win to Dawm:
  SUBROUTINE copy_win_to_dawn_column_double(storage, array, win)
    class(storage_column_double_type) :: storage
    REAL(c_double), INTENT(in) :: array(:)
    INTEGER, INTENT(in), OPTIONAL :: win(2)
    INTEGER :: a_size
    INTEGER :: default_win(2)
    a_size = SIZE(array)
    IF (PRESENT(win)) THEN
      CALL check_err(f2c_copy_win_to_dawn_column_double_f(array, a_size, win, storage%cpt))
    ELSE
      default_win = [1, a_size]
      CALL check_err(f2c_copy_win_to_dawn_column_double_f(array, a_size, default_win, storage%cpt))
    ENDIF
  END SUBROUTINE copy_win_to_dawn_column_double

  ! copy 2d win from Dawm:
  SUBROUTINE copy_win_from_dawn_struct2d_double(storage, array, win)
    CLASS(storage_struct2d_double_type) :: storage
    REAL(c_double), INTENT(inout) :: array(:,:)
    INTEGER, INTENT(in), OPTIONAL :: win(2,2)
    INTEGER :: a_shape(2)
    INTEGER :: default_win(2,2)
    a_shape = SHAPE(array)
    IF (PRESENT(win)) THEN
      CALL check_err(f2c_copy_win_from_dawn_struct2d_double_f(array, a_shape, win, storage%cpt))
    ELSE
      default_win(:,1) = [1, a_shape(1)]
      default_win(:,2) = [1, a_shape(2)]
      CALL check_err(f2c_copy_win_from_dawn_struct2d_double_f(array, a_shape, default_win, storage%cpt))
    ENDIF
  END SUBROUTINE copy_win_from_dawn_struct2d_double

  ! copy 3d win from Dawm:
  SUBROUTINE copy_win_from_dawn_struct3d_double(storage, array, win)
    CLASS(storage_struct3d_double_type) :: storage
    REAL(c_double), INTENT(inout) :: array(:,:,:)
    INTEGER, INTENT(in), OPTIONAL :: win(2,3)
    INTEGER :: a_shape(3)
    INTEGER :: default_win(2,3)
    a_shape = SHAPE(array)
    IF (PRESENT(win)) THEN
      CALL check_err(f2c_copy_win_from_dawn_struct3d_double_f(array, a_shape, win, storage%cpt))
    ELSE
      default_win(:,1) = [1, a_shape(1)]
      default_win(:,2) = [1, a_shape(2)]
      default_win(:,3) = [1, a_shape(3)]
      CALL check_err(f2c_copy_win_from_dawn_struct3d_double_f(array, a_shape, default_win, storage%cpt))
    ENDIF
  END SUBROUTINE copy_win_from_dawn_struct3d_double

#if 1
  ! copy column win from Dawm:
  SUBROUTINE copy_win_from_dawn_column_double(storage, array, win)
    CLASS(storage_column_double_type) :: storage
    REAL(c_double), INTENT(inout) :: array(:)
    INTEGER, INTENT(in), OPTIONAL :: win(2)
    INTEGER :: a_size
    INTEGER :: default_win(2)
    a_size = SIZE(array)
    IF (PRESENT(win)) THEN
      CALL check_err(f2c_copy_win_from_dawn_column_double_f(array, a_size, win, storage%cpt))
    ELSE
      default_win = [1, a_size]
      CALL check_err(f2c_copy_win_from_dawn_column_double_f(array, a_size, default_win, storage%cpt))
    ENDIF
  END SUBROUTINE copy_win_from_dawn_column_double
#endif
  ! misc:

  SUBROUTINE assert(cond, msg)
    LOGICAL, INTENT(in) :: cond
    CHARACTER(len=*), INTENT(in) :: msg
    IF (cond) RETURN
    PRINT*,'dawn error:', msg
    STOP 'program stop in module dawn'
  END SUBROUTINE assert

  SUBROUTINE check_err(ierr, msg)
    INTEGER, INTENT(in) :: ierr
    CHARACTER(len=*), OPTIONAL, INTENT(in) :: msg
    IF (ierr == 0) RETURN
    PRINT*,'dawn error: ierr=',ierr
    IF (PRESENT(msg)) PRINT*,msg
    STOP 'program stop in module dawn'
  END SUBROUTINE check_err

  SUBROUTINE error(file,line,reason)
    CHARACTER(len=*),INTENT(in) :: file, reason
    INTEGER,INTENT(in) :: line
    PRINT*,'*** Error in file ',file,' at line ',line
    PRINT*,'***',reason
    STOP
  END SUBROUTINE error

END MODULE DAWN_STRUCT
