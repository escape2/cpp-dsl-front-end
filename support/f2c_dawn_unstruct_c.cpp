#include <atlas/mesh.h>

#include "driver-includes/unstructured_interface.hpp"
#include "interface/atlas_interface.hpp"
#include "f2c_dawn_unstruct.hpp"
#include "AtlasSupport.hpp"
#include <numeric> // for std::lcm
#include <type_traits>

#ifdef CDSL_WITH_CUDA
#include "driver-includes/cuda_utils.hpp"
#endif

// some local decls:
void gen_block_coords_base1(int flat_pos_f, int block_size, int *iblock_f_ptr, int *idx_f_ptr);


using std::string, std::vector, std::size_t;
using dawn::LocationType;

static void die(const string& reason) {
  std::cout << "bad case: (" << __FILE__ << "; " << __LINE__ << "):" << reason << std::endl;
  exit(1);
}

enum BackendDirection { to_backend, from_backend};

int get_dense_size(atlas::Mesh &mesh, LocationType loctype) {
  switch (loctype) {
  case LocationType::Cells:
    return mesh.cells().size();
  case LocationType::Edges:
    return mesh.edges().size();
  case LocationType::Vertices:
    return mesh.nodes().size();
  default:
    return 0;
  }
}

int get_dense_size(atlas::Mesh &mesh, const vector<LocationType> &chain) {
  if ( chain.size() == 0) return 0;
  return get_dense_size(mesh, chain[0]);
}

int get_sparse_size(atlas::Mesh &mesh, const vector<LocationType> &chain) {
  if ( chain.size() == 0) return 0;
  return get_dense_size(mesh, chain[0]);
}

static string chain2str(const vector<LocationType> &chain)  {
  static constexpr std::array<char,3> ilt2char{'C','E','V'};
  int len_chain = chain.size();
  if (len_chain == 0) return "";
  string s(len_chain,'?');
  for (int p = 0; p < len_chain; ++p) {
    int ilt = static_cast<int>(chain[p]);
    s[p] = ilt2char[ilt];
  }
  return s;
}

static vector<LocationType> str2chain(const string &s)  {
  vector<LocationType> chain;
  int len_chain = s.size();
  if (len_chain == 0) return chain;
  chain.resize(len_chain);
  LocationType v;
  for (int p = 0; p < len_chain; ++p) {
    switch (s[p]) {
    case 'C':
      v = LocationType::Cells;
      break;
    case 'E':
      v = LocationType::Edges;
      break;
    case 'V':
      v = LocationType::Vertices;
      break;
    default:
      die("invalid chain:"+s);
    }
    chain[p] = v;
  }
  return chain;
}


int Contab::get_minor_index() {
  constexpr int linkval[3][3] = { {-1,0,1}, {0,-1,1}, {0,-1,1} };
  const int nchain = chain.size();
  if (nchain == 0) return 0;
  int idx = static_cast<int>(chain[0]);
  if (nchain == 1) return idx;
  assert(nchain <= 29);
  for(int chain_pos = 2; chain_pos < nchain; ++chain_pos) {
    const int elm0 = static_cast<int>(chain[chain_pos-1]);
    const int elm1 = static_cast<int>(chain[chain_pos]);
    const int lval = linkval[elm0][elm1];
    assert(lval >= 0);
    idx = idx * 2 + lval;
  }
  return idx;
}



void Contab::set_tab(atlas::Mesh &mesh) {
    sparse_size = 0;
    for (int ie = 0; ie < dense_size; ++ie) {
      tab[ie] = getNeighbors(atlasInterface::atlasTag{}, mesh, chain, ie);
      auto &env = tab[ie];
      int nenv = env.size();
      sparse_len[ie] = nenv;
      sparse_size = std::max(sparse_size, nenv);
    }
    major_idx = chain.size();
    minor_idx = get_minor_index();
  }


Contab::Contab(atlas::Mesh &mesh, const vector<LocationType> &chain) :
  chain(chain),
  name(chain2str(chain)),
  dense_size(get_dense_size(mesh, chain)),
  tab(dense_size, vector<int>()),
  sparse_len(dense_size, 0) { set_tab(mesh); }

Contab::Contab(atlas::Mesh &mesh, const string &chain_str) :
  chain(str2chain(chain_str)),
  name(chain_str),
  dense_size(get_dense_size(mesh, chain)),
  tab(dense_size, vector<int>()),
  sparse_len(dense_size, 0) { set_tab(mesh); }

/*
Contab::Contab(Mesh &cdsl_mesh, const string &chain_str) {
  atlas::Mesh &atlas_mesh = cdsl_mesh.get_atlas_mesh();
  Contab(atlas_mesh, chain_str);
}
*/

const vector<int> &Contab::get_neighbors(int loc) {
  return tab[loc];
}

class DetailedMesh {
  atlas::Mesh mesh;
  vector< std::map<int,Contab*> > contab_maps;

  DetailedMesh(atlas::Mesh &mesh) : mesh(mesh) {};

};




DenseField::DenseField(atlas::Mesh &mesh, LocationType loctype, int k_size) :
  mesh(mesh),
  loctype(loctype),
  dense_size(get_dense_size(mesh,loctype)),
  k_size(k_size),
  field{name, atlas::array::DataType::real64(),
        atlas::array::make_shape(dense_size, k_size)},
  view{atlas::array::make_view<double, 2>(field)} {
#ifdef CDSL_WITH_CUDA
    //std::cout << "***(debug msg): call allocField(cudaStorage, dense_size, k_size);" << std::endl;
    dawn::allocField(&cuda_storage, dense_size, k_size); 
#endif
  };

#define USE_DAWN_ITF
template <typename T>
void check_view_3d(atlasInterface::SparseDimension<T> &v, std::array<int, 3> used_shape) {
  // Note: The used_shape ( == "access bounds") in Dawn may be a permutation of the shape given to Atlas.
  int stride[3];
  std::array<int, 3> dimpos = {0, 1, 2};

  auto shape = used_shape;
  stride[0] = &v(1,0,0) - &v(0,0,0);
  stride[1] = &v(0,1,0) - &v(0,0,0);
  stride[2] = &v(0,0,1) - &v(0,0,0);
  for (int idim=0; idim<3; idim++) {
    for (int jdim=idim+1; jdim<3; jdim++) {
      int m = std::lcm(stride[idim], stride[jdim]); // first collision point
      int idim_maxpos = (used_shape[idim]-1) * stride[idim];
      int jdim_maxpos = (used_shape[jdim]-1) * stride[jdim];
      assert(idim_maxpos < m or jdim_maxpos < m); // if we can reach m in both dims then we have a collision
    }
  }
}

SparseField::SparseField(const Contab &contab, int k_size) :
  dense_size(contab.dense_size),
  sparse_size(contab.sparse_size),
  k_size(k_size),
  field{name, atlas::array::DataType::real64(),
        atlas::array::make_shape(contab.dense_size, k_size, contab.sparse_size)},
  view{atlas::array::make_view<double, 3>(field)} {
    check_view_3d(view, {contab.dense_size, contab.sparse_size, k_size});
#ifdef CDSL_WITH_CUDA
    /*
    std::cout << "(debug SparseField::SparseField): call allocField(cudaStorage, dense_size, sparse_size, k_size);"
              << contab.dense_size << ", "
              << contab.sparse_size << ", "
              << k_size << std::endl;
    */
    dawn::allocField(&cuda_storage, contab.dense_size, contab.sparse_size, k_size); 
#endif
  };

VerticalField::VerticalField(int k_size) :
  k_size(k_size),
  field{name, atlas::array::DataType::real64(),
        atlas::array::make_shape(k_size)},
  view{atlas::array::make_view<double, 1>(field)} {
#ifdef CDSL_WITH_CUDA
    dawn::allocField(&cuda_storage, k_size); 
#endif
  };

#ifdef CDSL_WITH_CUDA
void cdsl_cuda_copy_win(dawn::float_type *dst_field, // dest field pointer
                        dawn::float_type *src_field, // source field pointer            
                        int field_jstep, // flat_element_index step in jdim
                        int win_i0, // idim offset of win
                        int win_j0, // jdim offset of win
                        int win_isize, // win extent in idim
                        int win_jsize, // win extent in jdim
                        cudaMemcpyKind dir) {

  //assumptions:
  // - (i,j) semantics: flat_element_index =  j * field_jstep + i
  // - src and dst fields are of same shape

  int flat_win_offset = win_i0 + win_j0 * field_jstep;
  dawn::float_type *dst = dst_field + flat_win_offset;
  dawn::float_type *src = src_field + flat_win_offset;
  size_t size_of_element = sizeof(dawn::float_type);
  size_t width_bytes = win_isize * size_of_element;
  size_t height = win_jsize;

  if (win_isize == field_jstep) {
    gpuErrchk(cudaMemcpy(dst, src, height * width_bytes, dir));
  } else {
    // printf("multi block copy mode: with_bytes=%d, height=%d\n", width_bytes, height);
    size_t dpitch = field_jstep * size_of_element;
    size_t spitch = dpitch;
    gpuErrchk(cudaMemcpy2D(dst, dpitch, src, spitch, width_bytes, height, dir));
  }
}
#endif

template<int T>
int copy_vertical_field(VerticalField *field_pt, double *array, int k_size) {
  if (!field_pt) return 1;
  VerticalField &field = *field_pt;
  assert(field.k_size == k_size);
#ifdef CDSL_WITH_CUDA
  const bool dawn_float_is_double = std::is_same_v<double,dawn::float_type>;
  assert(dawn_float_is_double);

  if constexpr (T == to_backend) {
    gpuErrchk(cudaMemcpy(field.cuda_storage, array, sizeof(dawn::float_type) * k_size,
                         cudaMemcpyHostToDevice));
  } else if constexpr (T == from_backend) {
    gpuErrchk(cudaMemcpy(array, field.cuda_storage, sizeof(dawn::float_type) * k_size,
                         cudaMemcpyDeviceToHost));
  } else {
    die("f2c_copy_vertical_field (cuda case): invalid template value");
  }
#else
  auto view = field.view;
  for (int k = 0; k < k_size; ++k) {
    if constexpr (T == to_backend) {
      view(k) = array[k]; 
    } else if constexpr (T == from_backend) {
      array[k]  = view(k);
    } else {
      die("f2c_copy_vertical_field: invalid template value");
    }
  }
#endif
  return 0;
}

template<int T>
// T = to_backend: to Dawn
// T = from_backend: from dawn
int f2c_copy_dense(DenseField *field_pt, double *array, const UnstructMetaData *umd_pt, const UnstructSubsetRange *usr_pt) {
  if ( !(field_pt and array and umd_pt) ) return 1;
  DenseField &field = *field_pt;
  const UnstructMetaData &umd = *umd_pt;


  // evaluate subset range:
  const UnstructSubsetRange &usr = *usr_pt;
  assert(usr.blk_size > 0);
  const int start_blk = usr.start_blk_f - 1;
  const int start_idx = usr.start_idx_f - 1;
  const int end_blk = usr.end_blk_f - 1;
  const int end_idx = usr.end_idx_f - 1;
  const int blk_size = usr.blk_size;

  int usr_dense_num = end_blk*blk_size + end_idx + 1;
  int usr_dense_cap = (end_blk+1)*blk_size;

  // make sure that the subset is not larger than allowed
  assert(field.dense_size >= usr_dense_num);
  assert(field.k_size == umd.lev_num);
  assert(umd.sparse_dim_size == 1);

  auto &view = field.view;

#ifdef CDSL_WITH_CUDA
  // allocate host_buffer:
  const size_t host_buffer_size = field.dense_size * umd.lev_num;
  dawn::float_type* host_buffer = new dawn::float_type[host_buffer_size];

  //horizontal section:
  int hsec_start =  start_blk * blk_size + start_idx;
  int hsec_end =  end_blk * blk_size + end_idx;
  int hsec_num = hsec_end - hsec_start + 1;

  if constexpr (T == from_backend) {
    // copy device image to host_buffer:
    cdsl_cuda_copy_win(host_buffer, field.cuda_storage, field.dense_size,
                       hsec_start, 0, // window offsets
                       hsec_num,  umd.lev_num, // window extents
                       cudaMemcpyDeviceToHost);
  }
#endif

  // copy host_buffer to application array:
  
  // levels:
  for (int k = 0; k < umd.lev_num; ++k) {    
    const int k_pos = k * umd.lev_step;

    // horizontal blocks:
    for (int blk = start_blk; blk <= end_blk; ++blk) {
      const int blk_pos = k_pos + blk * umd.blk_dim_step;

      // special bounds for start and end block:
      const int is = (blk == start_blk ? start_idx : 0);
      const int ie = (blk == end_blk ? end_idx : blk_size-1);
      
      // horizontal index within a block:
      for (int idx = is; idx <= ie; ++idx) {
        const int idx_pos = blk_pos + idx * umd.idx_dim_step;
        // d = combined horizontal (dense) index
        const int d = blk * blk_size + idx;

        //Q: do we need CPU and cuda copies in the same program?
        // => consider using E (execution Environment) as another template parameter
#ifdef CDSL_WITH_CUDA

        const int hb_pos =  k * field.dense_size + d;
        if constexpr (T == to_backend) {
          host_buffer[hb_pos] = array[idx_pos];
        } else if constexpr (T == from_backend) {
          array[idx_pos] = host_buffer[hb_pos];
        } else {
          die("f2c_copy_dense (cuda case): invalid template value");
        }
#else

        if constexpr (T == to_backend) {
          view(d,k) = array[idx_pos];
        } else if constexpr (T == from_backend) {
          array[idx_pos] = view(d,k);          
        } else {
          die("f2c_copy_dense: invalid template value");
        }
        
#endif
      } // idx
    } // blk
  } // k

#ifdef CDSL_WITH_CUDA
  if constexpr (T == to_backend) {
    // copy host_buffer to device image:
    cdsl_cuda_copy_win(field.cuda_storage, host_buffer, field.dense_size,
                       hsec_start, 0, // window offsets
                       hsec_num,  umd.lev_num, // window extents
                       cudaMemcpyHostToDevice);
  }
  // free host_buffer:
  delete[] host_buffer;
#endif

  return 0;
}


//#define DAWN_CUDA_SPARSE_ACCESS_KSD // K has largest stride, Dense idx has unit stride
#define DAWN_CUDA_SPARSE_ACCESS_SKD // Sparse index has largest stride, Dense idx has unit stride

template<int T>
// T = to_backend: to Dawn
// T = from_backend: from dawn
int f2c_copy_sparse(SparseField *field_pt, double *array, const UnstructMetaData *umd_pt, const UnstructSubsetRange *usr_pt) {
  if ( !(field_pt and array and umd_pt) ) return 1;
  SparseField &field = *field_pt;
  const UnstructMetaData &umd = *umd_pt;
  const UnstructSubsetRange &usr = *usr_pt;

  assert(usr.blk_size == umd.idx_dim_size);
  
  const int start_blk = usr.start_blk_f - 1;
  const int start_idx = usr.start_idx_f - 1;
  const int end_blk = usr.end_blk_f - 1;
  const int end_idx = usr.end_idx_f - 1;
  const int blk_size = usr.blk_size;
  
  if (umd.blk_dim_size == 1) {
    assert(start_blk == 0 and end_blk == 0 and blk_size == umd.idx_dim_size);
  }

  assert(field.dense_size >= end_blk*blk_size + end_idx);
  assert(field.k_size >= umd.lev_num);

  assert(field.sparse_size == umd.sparse_dim_size);
  assert(umd.lev_num == field.k_size);

  auto &view = field.view;

  // we only support full range transfer:
  assert(start_blk == 0 and start_idx == 0);
  assert(end_blk*blk_size + end_idx + 1 == field.dense_size);

    
#ifdef CDSL_WITH_CUDA
  // allocate host_buffer:
  const size_t host_buffer_size = field.dense_size * field.sparse_size * umd.lev_num;
  dawn::float_type* host_buffer = new dawn::float_type[host_buffer_size];
    
  //horizontal section:
  int hsec_start =  start_blk * blk_size + start_idx;
  int hsec_end =  end_blk * blk_size + end_idx;
  int hsec_num = hsec_end - hsec_start + 1;

  if constexpr (T == from_backend) {
    // copy device image to host_buffer:
    gpuErrchk(cudaMemcpy(host_buffer, field.cuda_storage, sizeof(dawn::float_type) * host_buffer_size,
                         cudaMemcpyDeviceToHost));
  }

#endif

  // levels:
  for (int k = 0; k < umd.lev_num; ++k) {    
    const int k_pos = k * umd.lev_step;

    // horizontal blocks:
    for (int blk = start_blk; blk <= end_blk; ++blk) {
      const int blk_pos = k_pos + blk * umd.blk_dim_step;

      // special bounds for start and end block:
      const int is = blk == start_blk ? start_idx : 0;
      const int ie = blk == end_blk ? end_idx : blk_size-1;
 
      // horizontal index within a block:
      for (int idx = is; idx <= ie; ++idx) {
        const int idx_pos = blk_pos + idx * umd.idx_dim_step;
        // d = combined horizontal (dense) index
        const int d = blk * blk_size + idx;

        // sparse index:
        for (int s = 0; s < umd.sparse_dim_size; ++s) {
          const int s_pos  = idx_pos + s * umd.sparse_dim_step;

          const int a_pos = s_pos;
#ifdef CDSL_WITH_CUDA

#ifdef DAWN_CUDA_SPARSE_ACCESS_SKD
          // (sparse iter: largest mem steps); (dense iter: small mem stpes)
          const int hb_pos =  s * umd.lev_num * field.dense_size + k * field.dense_size + d;
          assert(hb_pos < host_buffer_size);
#elif DAWN_CUDA_SPARSE_ACCESS_KSD
          // (level iter: largest mem steps); (dense iter: smallest mem stpes)
          const int hb_pos =  k * field.dense_size * umd.sparse_dim_size + s * field.dense_size + d;
          assert(hb_pos < host_buffer_size);
#else
#error "bad case"
#endif
          if constexpr (T == to_backend) {
            host_buffer[hb_pos] = array[a_pos];
          } else if constexpr (T == from_backend) {
            array[a_pos] = host_buffer[hb_pos];
          } else {
            die("f2c_copy_spare (cuda case): invalid template value");
          }
#else

          if constexpr (T == to_backend) {
            view(d,s,k) = array[a_pos];
          } else if constexpr (T == from_backend) {
            array[a_pos] = view(d,s,k);
          } else {
            die("f2c_copy_sparse: invalid template value");
          }

#endif

        } //s
      } // idx
    } // blk
  } // k

#ifdef CDSL_WITH_CUDA
  if constexpr (T == to_backend) {
    // copy host_buffer to device image:
    gpuErrchk(cudaMemcpy(field.cuda_storage, host_buffer, sizeof(dawn::float_type) * host_buffer_size,
                         cudaMemcpyHostToDevice));
  }
  // free host_buffer:
  delete[] host_buffer;
#endif

  return 0;
}


cdsl::Mesh::Mesh(atlas::Mesh *atlas_mesh_pt) : atlas_mesh_pt(atlas_mesh_pt) {

#ifdef CDSL_WITH_CUDA
  ggtm_pt =  new dawn::GlobalGpuTriMesh;
  *ggtm_pt = atlasToGlobalGpuTriMesh(*atlas_mesh_pt);
#endif
  is_valid = true;
}

namespace cdsl {
  atlas::Mesh* Mesh::get_atlas_mesh_pt() const {
    assert(is_valid);
    return atlas_mesh_pt;
  }
#ifdef CDSL_WITH_CUDA
  dawn::GlobalGpuTriMesh* Mesh::get_ggtm_pt() const {
    assert(is_valid);
    return ggtm_pt;
  }
#endif
}
  
cdsl::Mesh* create_mesh_explicit(int ncells, int nedges, int nverts,
                                 const double vlat[], const double vlon[],
                                 const MeshMapSpecs &c2v, const MeshMapSpecs &e2v,
                                 const MeshMapSpecs &e2c, const MeshMapSpecs &v2c,
                                 const MeshMapSpecs &v2e, const MeshMapSpecs &c2e,
                                 int input_start_idx) {

  atlas::Mesh* atlas_mesh = CreateAtlasMesh(ncells, nedges, nverts,
                                            vlat, vlon,
                                            c2v, e2v,
                                            e2c, v2c,
                                            v2e, c2e,
                                            input_start_idx);

  cdsl::Mesh *cdsl_mesh = new cdsl::Mesh(atlas_mesh);

  return cdsl_mesh;
}

void gen_block_coords_base1(int flat_pos_f, int block_size, int *iblock_f_ptr, int *idx_f_ptr) {
  // assuming 1-based semantics for flat_pos_f, iblock_f, idx_f
  assert(iblock_f_ptr and idx_f_ptr);
  const int flat_pos_c = flat_pos_f - 1;
  int &iblock_f = *iblock_f_ptr;
  int &idx_f = *idx_f_ptr;    
  
  int iblock_c = flat_pos_c / block_size;
  int idx_c = flat_pos_c - iblock_c * block_size;
  
  iblock_f = iblock_c + 1;
  idx_f = idx_c + 1;
};

void gen_subset_range_base1(UnstructSubsetRange *usr_pt, int num_skip, int num_elements, int block_size) {
  // generate subset_range, using 1-based coords
  int blk_size;
  if (block_size) {
    blk_size = block_size;
  } else {
    blk_size = num_elements;
  }

  UnstructSubsetRange &usr = *usr_pt;
  if (num_elements < 1) {
    usr.start_blk_f = 1;
    usr.start_idx_f = 1;
    usr.end_blk_f = -1;
    usr.end_idx_f = -1;
    usr.blk_size = 1;
  }

  gen_block_coords_base1(num_skip+1, blk_size, &usr.start_blk_f, &usr.start_idx_f);
  gen_block_coords_base1(num_skip+num_elements, blk_size, &usr.end_blk_f, &usr.end_idx_f);
  usr.blk_size = blk_size;
  
}

extern"C" {

  cdsl::Mesh* f2c_create_mesh_explicit_c(int ncells, int nedges, int nverts,
                                         double vlat[], const double vlon[],
                                         int c2v_map[], int c2v_sparse_size, int c2v_sparse_step,
                                         int e2v_map[], int e2v_sparse_size, int e2v_sparse_step,
                                         int e2c_map[], int e2c_sparse_size, int e2c_sparse_step,
                                         int v2c_map[], int v2c_sparse_size, int v2c_sparse_step,
                                         int v2e_map[], int v2e_sparse_size, int v2e_sparse_step,
                                         int c2e_map[], int c2e_sparse_size, int c2e_sparse_step
                                         ) {

    // Note: all map values (including missing values) are shifted by -input_start_idx
    const int input_start_idx = 1;
    const MeshMapSpecs  c2v = {c2v_map, ncells, c2v_sparse_size, c2v_sparse_step};
    const MeshMapSpecs  e2v = {e2v_map, nedges, e2v_sparse_size, e2v_sparse_step};
    const MeshMapSpecs  e2c = {e2c_map, nedges, e2c_sparse_size, e2c_sparse_step};
    const MeshMapSpecs  v2c = {v2c_map, nverts, v2c_sparse_size, v2c_sparse_step};
    const MeshMapSpecs  v2e = {v2e_map, nverts, v2e_sparse_size, v2e_sparse_step};
    const MeshMapSpecs  c2e = {c2e_map, ncells, c2e_sparse_size, c2e_sparse_step};

    return create_mesh_explicit(ncells, nedges, nverts,
                                vlat, vlon,
                                c2v, e2v,
                                e2c, v2c,
                                v2e, c2e,
                                input_start_idx);

  }

  //Contab* f2c_create_contab_from_str_c(atlas::Mesh* mesh, char const *chain_cstr) {
  Contab* f2c_create_contab_from_str_c(cdsl::Mesh* mesh, char const *chain_cstr) {
    atlas::Mesh* atlas_mesh_pt = mesh->get_atlas_mesh_pt();
    string chain_str(chain_cstr);
    auto *contab = new Contab(*atlas_mesh_pt, chain_str);
    return contab;
  }

  void f2c_get_contab_shape_c(Contab* contab, int* dense_size, int *sparse_size) {
    *dense_size = contab->dense_size;
    *sparse_size = contab->sparse_size;
  }

  void f2c_get_contab_sparse_len_c(Contab* contab, int* array, int array_size) {
    assert(array_size >= contab->dense_size);
    auto &sparse_len = contab->sparse_len;
    for(int i = 0; i < contab->dense_size; ++i) {
      array[i] = sparse_len[i];
    }
  }

  void f2c_get_contab_map_c(Contab* contab, int* array, int* shape, int ndims) {
    assert(ndims == 2);
    int fast_size = shape[0];
    int slow_size = shape[1];
    const auto &tab = contab->tab;
    const int dense_size = contab->dense_size;
    int d_step, s_step, sparse_size;

    if ( slow_size == dense_size ) {
      d_step = fast_size;
      s_step = 1;
      sparse_size = fast_size;
    } else if ( fast_size == dense_size ) {
      d_step = 1;
      s_step = fast_size;
      sparse_size = slow_size;
    } else {
      die("f2c_get_contab_data_c: invalid array shape");
    }
    assert(sparse_size >= contab->sparse_size);
    const int caller_start_index = 1;
    const int undef_index = caller_start_index - 1;
    for (int d = 0; d < dense_size; ++d) {
      const int d_pos = d * d_step;
      const auto &vec = tab[d];
      const int vec_len = vec.size();
      for (int s = 0; s < vec_len; ++s) {
        int p = d_pos + s * s_step;
        array[p] = vec[s] + caller_start_index;
      }
      for (int s = vec_len; s < sparse_size; ++s) {
        int p = d_pos + s * s_step;
        array[p] = undef_index;
      }

    }

  }

  void f2c_delete_contab_c(Contab *contab_pt) {
    delete contab_pt;
  }

  //int f2c_get_mesh_size_c(atlas::Mesh* mesh, int loctype_kind) {
  int f2c_get_mesh_size_c(cdsl::Mesh* mesh, int loctype_kind) {
    atlas::Mesh* atlas_mesh_pt = mesh->get_atlas_mesh_pt();
    //atlas::Mesh* atlas_mesh_pt = &mesh->get_atlas_mesh();
    LocationType loctype = static_cast<LocationType>(loctype_kind);
    return get_dense_size(*atlas_mesh_pt, loctype);
  }

  //DenseField *f2c_create_dense_field_c(atlas::Mesh* mesh, int loctype_kind, int k_size) {
  DenseField *f2c_create_dense_field_c(cdsl::Mesh* mesh, int loctype_kind, int k_size) {
    atlas::Mesh* atlas_mesh_pt = mesh->get_atlas_mesh_pt();
    LocationType loctype = static_cast<LocationType>(loctype_kind);
    assert(mesh);
    auto *field = new DenseField(*atlas_mesh_pt, loctype, k_size);
    return field;
  }

  void f2c_delete_dense_field_c(DenseField *field_pt) {
    delete field_pt;
  }

  void f2c_get_dense_field_shape_c(DenseField* field, int* dense_size, int* k_size) {
    assert(field);
    *dense_size = field->dense_size;
    *k_size = field->k_size;
  }

  SparseField *f2c_create_sparse_field_c(Contab* contab, int k_size) {
    auto *field = new SparseField(*contab, k_size);
    return field;
  }

  void f2c_get_sparse_field_shape_c(const SparseField* field, int* dense_size, int* sparse_size, int* k_size) {
    assert(field);
    const SparseField &f = *field;
    *dense_size = f.dense_size;
    *sparse_size = f.sparse_size;
    *k_size = f.k_size;
  }

  void f2c_delete_sparse_field_c(SparseField *field_pt) {
    delete field_pt;
  }

  VerticalField *f2c_create_vertical_field_c(int k_size) {
    auto *field = new VerticalField(k_size);
    return field;
  }

  void f2c_get_vertical_field_size_c(VerticalField* field,  int* k_size) {
    *k_size = field->k_size;
  }

  void f2c_delete_vertical_field_c(VerticalField *field_pt) {
    delete field_pt;
  }

  int f2c_copy_vertical_field_double_c(VerticalField *field, double *array, int k_size, int dir) {
    switch (dir) {
    case to_backend:
      return copy_vertical_field<to_backend>(field, array, k_size);
    case from_backend:
      return copy_vertical_field<from_backend>(field, array, k_size);
    default:
      die("invalid copy direction");
    }
    return 1;
  }

  int f2c_copy_to_dawn_vertical_field_double_c(VerticalField *field, double *array, int k_size) {
    return copy_vertical_field<to_backend>(field, array, k_size);
  }

  int f2c_copy_from_dawn_vertical_field_double_c(VerticalField *field, double *array, int k_size) {
    return copy_vertical_field<from_backend>(field, array, k_size);
  }

  int f2c_copy_dense_double_c(DenseField *field, double *array, UnstructMetaData *umd_pt, UnstructSubsetRange *usr_pt, int dir) {
    UnstructSubsetRange all_usr;
    if (!usr_pt) {
      gen_subset_range_base1(&all_usr, 0, field->dense_size, umd_pt->idx_dim_size);
      usr_pt = &all_usr;
    }
    switch (dir) {
    case to_backend:
      return f2c_copy_dense<to_backend>(field, array, umd_pt, usr_pt);
    case from_backend:
      return f2c_copy_dense<from_backend>(field, array, umd_pt, usr_pt);
    default:
      die("invalid copy direction");
    }
    return 1;
  }

  int f2c_copy_sparse_double_c(SparseField *field, double *array, UnstructMetaData *umd_pt, UnstructSubsetRange *usr_pt, int dir) {
    UnstructSubsetRange all_usr;
    assert(field);
    assert(umd_pt);
    if (!usr_pt) {
      gen_subset_range_base1(&all_usr, 0, field->dense_size, umd_pt->idx_dim_size);
      usr_pt = &all_usr;
    }
    switch (dir) {
    case to_backend:
      return f2c_copy_sparse<to_backend>(field, array, umd_pt, usr_pt);
    case from_backend:
      return f2c_copy_sparse<from_backend>(field, array, umd_pt, usr_pt);
    default:
      die("invalid copy direction");
    }
    return 1;
  }

  // application data layout description:
  // works for all cases: dense, sparse, blocked or nonblocked
  UnstructMetaData *f2c_create_unstruct_meta_data_c(int lev_num, int lev_step,
                                                    int blk_dim_size, int blk_dim_step,
                                                    int idx_dim_size, int idx_dim_step,
                                                    int sparse_dim_size,
                                                    int sparse_dim_step
                                                    //int dense_size
                                                    ) {
    UnstructMetaData *umd = new UnstructMetaData;
    *umd = {
            .lev_num = lev_num,
            .lev_step = lev_step,
            
            .blk_dim_size = blk_dim_size,
            .blk_dim_step = blk_dim_step,

            .idx_dim_size = idx_dim_size,
            .idx_dim_step = idx_dim_step,

            .sparse_dim_size = sparse_dim_size,
            .sparse_dim_step = sparse_dim_step
    };
    //todo: require dense size
    //gen_subset_range_base1(&default_usr, 0, dense_size, umd_pt->idx_dim_size);
    return umd;
  }

  void f2c_delete_unstruct_meta_data_c(UnstructMetaData *umd) {
    delete umd;
  }

} // end of extern "C"
