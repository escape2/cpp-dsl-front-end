#ifndef _CDSL_F2C_DAWN_UNSTRUCT_HPP
#define _CDSL_F2C_DAWN_UNSTRUCT_HPP

//#include "cdsl_config.h"
#include "AtlasSupport.hpp"

#ifdef CDSL_WITH_CUDA
#include "atlasToGlobalGpuTriMesh.h"
#endif

namespace cdsl {

class Mesh {
public:
  bool is_valid=false;
  atlas::Mesh *atlas_mesh_pt;
  Mesh(atlas::Mesh *atlas_mesh);
  atlas::Mesh* get_atlas_mesh_pt() const;
#ifdef CDSL_WITH_CUDA
  dawn::GlobalGpuTriMesh *ggtm_pt;
  dawn::GlobalGpuTriMesh* get_ggtm_pt() const;
#endif
};

} // end of cdsl namespace

class Contab {
public:
  const std::vector<dawn::LocationType> chain;
  const std::string name;
  int dense_size = 0;
  int sparse_size = 0;
  int major_idx = 0;
  int minor_idx = 0;
  std::vector<std::vector<int>> tab;
  std::vector<int> sparse_len;

  int get_minor_index();
  void set_tab(atlas::Mesh &mesh);
  Contab(atlas::Mesh &mesh, const std::vector<dawn::LocationType> &chain);
  Contab(atlas::Mesh &mesh, const std::string &chain_str);
  //Contab(cdsl::Mesh &cdsl_mesh, const std::string &chain_str);
  const std::vector<int> &get_neighbors(int loc);
};

enum CdslDimKind { cdsl_idx_dim_kind = 0,
                   cdsl_blk_dim_kind,
                   cdsl_lev_dim_kind,
                   cdsl_sparse_dim_kind,
                   cdsl_num_dim_kinds
};

// struct to describe unstructured range
struct UnstructSubsetRange {
  // Fortran offset convention (min. possible index == 1)
  // all data relate to the index space
  int start_blk_f = 1;
  int start_idx_f = 1;
  int end_blk_f = 0;
  int end_idx_f = 0;
  int blk_size = 1;
};


// struct to describe unstructured application data layout 
struct UnstructMetaData {
  // dim_size is wrt indices in a dim
  // dim_step is wrt indices in a flat 1d space (that can be mapped to the full multi-dim space)
  // TODO: change to iterable components:
  //   dim_size[cdsl_num_dim_kinds];
  //   dim_step[cdsl_num_dim_kinds]
  int lev_num = 1; // = 1 for horizontal data
  int lev_step = 1; //  = 1 for horizontal data
  int blk_dim_size = 1; // capacity of blocks (not the block size)
  int blk_dim_step = 1; 
  int idx_dim_size = 0; // capacity of indices per block
  int idx_dim_step = 1;
  int sparse_dim_size = 1; // = 1 for dense data;  capacity of indices in sparse dim
  int sparse_dim_step = 1; // = 1 for dense data
  // todo:
  //UnstructSubsetRange default_usr;
};


class DenseField {
public:
  atlas::Mesh &mesh;
  dawn::LocationType loctype;
  const int dense_size; // number of valid elements in horizontal domain 
  const int k_size; // number of valid levels in the vertical domain 
  const std::string name = "noname_DenseField";
  atlas::Field field;
  atlasInterface::Field<double> view;
#ifdef CDSL_WITH_CUDA
  dawn::float_type* cuda_storage = nullptr;
#endif
  DenseField(atlas::Mesh &mesh, dawn::LocationType loctype, int k_size);
};

class SparseField {
public:
  const int dense_size; // number of valid elements in horizontal domain
  const int sparse_size; // max number of sparse values (big enough for all locations)
  const int k_size;
  const std::string name = "noname_SparseField";
  atlas::Field field;
  atlasInterface::SparseDimension<double> view;
#ifdef CDSL_WITH_CUDA
  dawn::float_type* cuda_storage = nullptr;
#endif
  SparseField(const Contab &contab, int k_size);
};

class VerticalField {
public:
  const int k_size;
  const std::string name = "noname_VerticalField";
  atlas::Field field;
  atlasInterface::VerticalField<double> view;
#ifdef CDSL_WITH_CUDA
  dawn::float_type* cuda_storage = nullptr;
#endif
  VerticalField(int k_size);
};

#endif
