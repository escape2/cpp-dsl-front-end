#ifndef _AtlasSupport_hpp
#define _AtlasSupport_hpp


#include <atlas/mesh.h>


//namespace atlas {
//  class Mesh;
//}

struct MeshMapSpecs {
  int *map = nullptr;  // maps dense index to a set of sparse indices using a flat (2d array -> 1d array) data layout
  int dense_size = 0;  // number of dense elements
  int sparse_size = 0; // max. number of sparse elements per dense element 
  int sparse_step = 1; // one of {1, dense_size}; describes the map related index change when iterating the sparse set
  bool is_valid() const;
};

atlas::Mesh* CreateAtlasMesh(int ncells, int nedges, int nverts,
                             const double vlat[], const double vlon[],
                             const MeshMapSpecs &c2v, const MeshMapSpecs &e2v,
                             const MeshMapSpecs &e2c, const MeshMapSpecs &v2c,
                             const MeshMapSpecs &v2e, const MeshMapSpecs &c2e,
                             int start_idx);

#endif
