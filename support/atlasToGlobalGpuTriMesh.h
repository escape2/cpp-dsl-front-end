// source code origintes from git@github.com:dawn-ico/icondusk-e2e.git
// file utils/atlasToGlobalGpuTriMesh.h
// original LICENSE file: see following /* ... */ comment block

/*
MIT License

Copyright (c) 2020 MeteoSwiss-APN

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <atlas/mesh.h>

#include "driver-includes/cuda_utils.hpp"

dawn::GlobalGpuTriMesh atlasToGlobalGpuTriMesh(const atlas::Mesh& mesh);

size_t get_sparse_GlobalGpuTriMesh_size(const std::vector<dawn::LocationType>& chain);
size_t get_dense_GlobalGpuTriMesh_size(const dawn::GlobalGpuTriMesh &ggtm, const std::vector<dawn::LocationType>& chain);
