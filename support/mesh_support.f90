MODULE mesh_support
  USE iso_c_binding, ONLY: c_double, c_ptr, c_null_ptr, c_associated
  USE f2c_dawn_unstruct, ONLY: f2c_create_mesh_explicit_f
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: dp, mesh_map_type, mesh_type
  PUBLIC :: cdsl_loctype_cells, cdsl_loctype_edges, cdsl_loctype_verts
  PUBLIC :: cdsl_loctype0_sym, cdsl_loctype1_sym, cdsl_loctype2_sym
  PUBLIC :: sym2loctype, create_atlas_mesh, count_con, set_meta, check_inversion

  INTEGER, PARAMETER :: dp = c_double
  INTEGER, PARAMETER :: dawn_loctype_cells = 0
  INTEGER, PARAMETER :: dawn_loctype_edges = 1
  INTEGER, PARAMETER :: dawn_loctype_verts = 2
  CHARACTER(len=1), PARAMETER :: dawn_loctype0_sym = 'C'
  CHARACTER(len=1), PARAMETER :: dawn_loctype1_sym = 'E'
  CHARACTER(len=1), PARAMETER :: dawn_loctype2_sym = 'V'

  INTEGER, PARAMETER :: cdsl_loctype_cells = dawn_loctype_cells
  INTEGER, PARAMETER :: cdsl_loctype_edges = dawn_loctype_edges
  INTEGER, PARAMETER :: cdsl_loctype_verts = dawn_loctype_verts
  CHARACTER(len=1), PARAMETER :: cdsl_loctype0_sym = dawn_loctype0_sym
  CHARACTER(len=1), PARAMETER :: cdsl_loctype1_sym = dawn_loctype1_sym
  CHARACTER(len=1), PARAMETER :: cdsl_loctype2_sym = dawn_loctype2_sym

  TYPE mesh_map_type
    LOGICAL :: is_valid = .FALSE.
    INTEGER :: from, to
    CHARACTER(len=1) :: from_sym, to_sym
    INTEGER :: undef_value = 0
    INTEGER :: sparse_size
    INTEGER :: dense_size
    INTEGER :: dense_dim_pos
    INTEGER, ALLOCATABLE :: sparse_len(:)
    INTEGER, ALLOCATABLE :: map(:,:)
  CONTAINS
    PROCEDURE, NON_OVERRIDABLE :: get_sparse_step => mesh_map__get_sparse_step
  END TYPE mesh_map_type

  TYPE mesh_type
    TYPE(c_ptr) :: cpt = c_null_ptr
  CONTAINS
    PROCEDURE, NON_OVERRIDABLE :: is_valid => mesh_map__is_valid
  END TYPE mesh_type


CONTAINS

  SUBROUTINE assert(cond, line)
    LOGICAL, INTENT(in) :: cond
    INTEGER, INTENT(in) :: line
    IF (cond) RETURN
    PRINT*,'*** ASSERT CALL FAILED:'
    PRINT*,'*** file:',__FILE__
    PRINT*,'*** line:',line
    STOP
  END SUBROUTINE assert

  FUNCTION mesh_map__get_sparse_step(map) RESULT(step)
    CLASS(mesh_map_type), INTENT(in) :: map
    INTEGER :: step
    IF (map%dense_dim_pos == 1) THEN
      step = map%dense_size
    ELSEIF (map%dense_dim_pos == 2) THEN
      step = 1
    ELSE
      PRINT*,'invalid map%dense_dim_pos: ',map%dense_dim_pos
      CALL assert(.FALSE., __LINE__)
    ENDIF
  END FUNCTION mesh_map__get_sparse_step

  FUNCTION sym2loctype(sym) RESULT(kind)
    CHARACTER(len=1), INTENT(in) :: sym
    INTEGER :: kind
    SELECT CASE(sym)
    CASE('C')
      kind = cdsl_loctype_cells
    CASE('E')
      kind = cdsl_loctype_edges
    CASE('V')
      kind = cdsl_loctype_verts
    CASE default
      kind = -1
    END SELECT
  END FUNCTION sym2loctype

#if 0
  FUNCTION loctype2sym(loctype) RESULT(sym)
    INTEGER, INTENT(in) :: loctype
    CHARACTER(len=1) :: sym
    CHARACTER(len=1), PARAMETER :: symtab(0:2) = ['C','E','V']
    CALL assert(loctype >= 0 .AND. loctype <= 2, __LINE__)
    sym = symtab(loctype)
  END FUNCTION loctype2sym
#endif

  SUBROUTINE set_meta(x2y, from_sym, to_sym)
    TYPE(mesh_map_type), INTENT(inout) :: x2y
    CHARACTER(len=1) :: from_sym, to_sym
    x2y%from_sym = from_sym
    x2y%to_sym = to_sym
    x2y%from = sym2loctype(from_sym)
    x2y%to = sym2loctype(to_sym)
    CALL assert(x2y%from >= 0 .AND. x2y%to >= 0, __LINE__)
  END SUBROUTINE set_meta

  SUBROUTINE count_con(x2y, expected_sparse_size)
    TYPE(mesh_map_type), INTENT(inout) :: x2y
    INTEGER, INTENT(in) :: expected_sparse_size
    INTEGER :: ix
    ALLOCATE(x2y%sparse_len(SIZE(x2y%map,2)))
    DO ix = 1, SIZE(x2y%map,2)
      x2y%sparse_len(ix) = COUNT(x2y%map(:,ix) > 0)
    ENDDO
    x2y%sparse_size = MAXVAL(x2y%sparse_len)
    CALL assert(x2y%sparse_size == expected_sparse_size, __LINE__)
  END SUBROUTINE count_con

  SUBROUTINE check_inversion(x2y, y2x)
    TYPE(mesh_map_type), INTENT(in) :: x2y, y2x
    INTEGER :: nmatch(x2y%dense_size)
    LOGICAL :: match
    INTEGER :: ix, iiy, iy, jjx, jx, nx, ny
    nx = x2y%dense_size
    ny = y2x%dense_size
    nmatch = 0
    ix_loop: DO ix = 1, nx
      iiy_loop: DO iiy = 1, x2y%sparse_len(ix)
        iy = x2y%map(iiy,ix)
        CALL assert(iy > 0, __LINE__)

        match = .FALSE.
        jjx_loop: DO jjx = 1, y2x%sparse_len(iy)
          jx = y2x%map(jjx,iy)
          CALL assert(jx > 0, __LINE__)
          IF (ix == jx) THEN
            match = .TRUE.
            nmatch(ix) = nmatch(ix) + 1
          ENDIF
        ENDDO jjx_loop
        CALL assert(match, __LINE__)

      ENDDO iiy_loop
    ENDDO ix_loop
    CALL assert(ALL(x2y%sparse_len == nmatch), __LINE__)
  END SUBROUTINE check_inversion

  FUNCTION create_atlas_mesh(ncells, nedges, nverts, vlat, vlon, c2v, e2v, e2c, v2c, v2e, c2e) RESULT(mesh)
    INTEGER, INTENT(in) :: ncells, nedges, nverts
    REAL(dp), INTENT(in) :: vlat(:), vlon(:)

    TYPE(mesh_map_type), INTENT(in) :: c2v, e2v, e2c, v2c, v2e, c2e
    TYPE(mesh_type) :: mesh
    mesh%cpt = f2c_create_mesh_explicit_f(ncells, nedges, nverts, &
         & vlat, vlon, &
         & c2v%map, c2v%sparse_size, c2v%get_sparse_step(), &
         & e2v%map, e2v%sparse_size, e2v%get_sparse_step(), &
         & e2c%map, e2c%sparse_size, e2c%get_sparse_step(), &
         & v2c%map, v2c%sparse_size, v2c%get_sparse_step(), &
         & v2e%map, v2e%sparse_size, v2e%get_sparse_step(), &
         & c2e%map, c2e%sparse_size, c2e%get_sparse_step() )
    CALL assert(mesh%is_valid(), __LINE__)
  END FUNCTION create_atlas_mesh

  LOGICAL FUNCTION mesh_map__is_valid(mesh)
    CLASS(mesh_type), INTENT(in) :: mesh
    mesh_map__is_valid = c_ASSOCIATED(mesh%cpt)
  END FUNCTION mesh_map__is_valid

END MODULE mesh_support
