#ifndef CDSL_SUPPORT_CUDA_HELPERS_HPP
#define CDSL_SUPPORT_CUDA_HELPERS_HPP

//#define DEBUG_CUDA_HELPERS

#ifdef CDSL_WITH_CUDA
template<typename T>
auto unique_cuda_malloc(size_t num_elem) {

  auto my_malloc = [](size_t mySize) {
                     void* dev_ptr;
                     gpuErrchk(cudaMalloc((void**)&dev_ptr, mySize*sizeof(T)));
#ifdef DEBUG_CUDA_HELPERS
                     std::cout << "unique_cuda_malloc: alloc size = " << mySize << std::endl;
#endif
                     return (T*)dev_ptr;
                   };

  auto my_deleter =  [](int* dev_ptr) {
#ifdef DEBUG_CUDA_HELPERS
                       std::cout << "unique_cuda_malloc: free." << std::endl;
#endif

                       cudaFree(dev_ptr);
                     };

  return std::unique_ptr<T, decltype(my_deleter)>(my_malloc(num_elem),my_deleter);
}
#endif

#endif
