#ifndef CDSL_SUPPORT_F2C_DAWN_COMMON_C_HPP
#define CDSL_SUPPORT_F2C_DAWN_COMMON_C_HPP

#include <string>

enum f2c_error_code{
  F2C_E_SUCCESS = 0,
  F2C_E_INVALID_ARGUMENT,
};

struct f2c_error_msg_t {
  int ec;
  char const *msg;
};


extern"C" {

  std::string* f2c_create_string_c(char const *name);
  void f2c_delete_string_c(std::string* str_pt);
}


#endif
