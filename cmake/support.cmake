function(register_generated_property)
  get_property(my_filelist GLOBAL PROPERTY generated_files_register)
  set_property(GLOBAL PROPERTY generated_files_register ${my_filelist} ${ARGV})
endfunction()


function(fix_generated_property)
  # CMAKE doesn't recognise GENERATED property for sources added from different scopes..
  # see: https://gitlab.kitware.com/cmake/cmake/-/issues/18399
  #
  # -> we have to call this function in every scope where we want to use one of our
  # self generated sources
  get_property(my_filelist GLOBAL PROPERTY generated_files_register)
  
  set_property(SOURCE ${my_filelist} PROPERTY GENERATED 1)
endfunction()


function(copy_sources)
  # produces: COPY_TARGET
  set(_bindir ${CMAKE_CURRENT_BINARY_DIR})
  set(_srcdir ${CMAKE_CURRENT_SOURCE_DIR})
  set(_results "")

  foreach (_relpath ${all_SOURCES})
    set(_result_path ${_bindir}/${_relpath})
    if(CDSL_DEBUG_MODE)
      # create link instead of copy so that we can debug inplace:
      get_filename_component(_dest_dir ${_result_path} DIRECTORY)
      if (NOT EXISTS ${_dest_dir})
        file(MAKE_DIRECTORY ${_dest_dir})
      endif()
      file(CREATE_LINK ${_srcdir}/${_relpath} ${_result_path} SYMBOLIC)
    else()
      configure_file(${_relpath} ${_result_path} COPYONLY)
    endif()
    list(APPEND _results ${_result_path})
  endforeach()
  add_custom_target(COPY_TARGET ALL DEPENDS ${_results} )
endfunction()

function(autogen)
  # produces: AUTOGEN_TARGET

  set(my_results "")
  set(my_output
    include/unstruct_dsl.hpp
    include/unstruct_dsl.json
    )
  add_custom_command(OUTPUT
    ${my_output}
    COMMAND ${CDSL_PYTHON_PRG} src/gen_unstruct.py -hpp -c conf/std_dims.conf > include/unstruct_dsl.hpp
    COMMAND ${CDSL_PYTHON_PRG} src/gen_unstruct.py -json -c conf/std_dims.conf > include/unstruct_dsl.json
    DEPENDS conf/std_dims.conf src/gen_unstruct.py src/dim_config.py src/srcgen_common.py
    )
  list(APPEND my_results ${my_output})

  set(my_output
    include/struct_dsl.hpp
    include/struct_dsl.json
    )
  add_custom_command(OUTPUT
    ${my_output}
    COMMAND ${CDSL_PYTHON_PRG} src/gen_struct.py -hpp -c conf/std_dims.conf > include/struct_dsl.hpp
    COMMAND ${CDSL_PYTHON_PRG} src/gen_struct.py -json -c conf/std_dims.conf > include/struct_dsl.json
    DEPENDS conf/std_dims.conf src/gen_struct.py src/dim_config.py src/srcgen_common.py
    )
  list(APPEND my_results ${my_output})

  set(my_output include/fields_dsl.hpp)
  add_custom_command(OUTPUT
    ${my_output}
    COMMAND ${CDSL_PYTHON_PRG} src/gen_fields.py  > include/fields_dsl.hpp
    DEPENDS conf/std_dims.conf src/gen_fields.py src/dim_config.py src/srcgen_common.py
    )
  list(APPEND my_results ${my_output})

  set(my_output
    ${CDSL_ROOT}/support/f2c_dawn_common_c_decls.hpp
    ${CDSL_ROOT}/support/f2c_dawn_common_f.f90
    ${CDSL_ROOT}/support/f2c_dawn_struct_c_decls.hpp
    ${CDSL_ROOT}/support/f2c_dawn_struct_f.f90

    )
  add_custom_command(OUTPUT
    ${my_output}
    COMMAND ${CDSL_PYTHON_PRG} ${CDSL_ROOT}/src/gen_f2c_itf.py -dir=${CDSL_ROOT}/support
    DEPENDS ${CDSL_ROOT}/src/common.py
    )
  list(APPEND my_results ${my_output})
  register_generated_property(${my_results})

  add_custom_target(AUTOGEN_TARGET ALL DEPENDS ${my_results})

  # not strictly required - but separates phases
  add_dependencies(AUTOGEN_TARGET COPY_TARGET)
  
endfunction()

function(configure_special_files  INPUT_DIR  OUTPUT_DIR)
  configure_file(${INPUT_DIR}/conf/cdsl_config.sh.in ${OUTPUT_DIR}/conf/cdsl_config.sh @ONLY)
  configure_file(${INPUT_DIR}/conf/cdsl_config.py.in ${OUTPUT_DIR}/conf/cdsl_config.py @ONLY)
  configure_file(${INPUT_DIR}/conf/cdsl-config.cmake.in ${OUTPUT_DIR}/conf/cdsl-config.cmake @ONLY)
  configure_file(${INPUT_DIR}/conf/cdsl-config-version.cmake ${OUTPUT_DIR}/conf/cdsl-config-version.cmake COPYONLY)
  configure_file(${INPUT_DIR}/conf/cdsl_config.txt.in ${OUTPUT_DIR}/conf/cdsl_config.txt @ONLY)
endfunction()

