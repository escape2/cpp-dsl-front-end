#
# sets CDSL_DAWN4PY_PATH, CDSL_DAWN_INCLUDE_DIR
#
if(NOT CDSL_DAWN4PY_PATH)
  execute_process(COMMAND ${Python3_EXECUTABLE} -B ${CMAKE_SOURCE_DIR}/conf/test_dawn.py
    ERROR_VARIABLE MY_ERROR
    RESULT_VARIABLE MY_STATUS
    OUTPUT_VARIABLE MY_OUTPUT
    )
  if(MY_STATUS AND NOT MY_STATUS EQUAL 0)
    message(FATAL_ERROR "Error: ${MY_ERROR}")
  endif()
  string(STRIP ${MY_OUTPUT} CDSL_DAWN4PY_PATH)
  set(CDSL_DAWN4PY_PATH ${CDSL_DAWN4PY_PATH} CACHE PATH "dawn4py directory within Dawn")
  if(NOT CDSL_DAWN4PY_PATH MATCHES "dawn4py$")
    message(FATAL_ERROR "Unexpected dawn4py module path (${CDSL_DAWN4PY_PATH}).")
  endif()
  message(STATUS "Found dawn4py (${CDSL_DAWN4PY_PATH})")
endif()

if(NOT CDSL_DAWN_INCLUDE_DIR)
  if(NOT DAWN)
    set(DAWN "$ENV{DAWN}" CACHE PATH "Dawn source directory")
  endif()
  if(NOT DAWN)
    message(FATAL_ERROR "Undefined environment variable: DAWN.")
  endif()
  set(CDSL_DAWN_INCLUDE_DIR "${DAWN}/dawn/src")
  set(_file "${CDSL_DAWN_INCLUDE_DIR}/driver-includes/unstructured_interface.hpp")
  if(NOT EXISTS ${_file})
    message(FATAL_ERROR "${_file} does not exist.")
  endif()
endif()
