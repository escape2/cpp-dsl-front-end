
# load LLVM package:

find_package(LLVM REQUIRED)


# set CDSL_LLVM_LIBCLANG_PATH:

find_library(CDSL_LLVM_LIBCLANG_PATH
  NAMES libclang-${LLVM_VERSION_MAJOR}.so libclang.so
  PATHS ${LLVM_LIBRARY_DIRS}
  DOC "path to libclang"
  REQUIRED
  )

if(CDSL_LLVM_LIBCLANG_PATH STREQUAL "CDSL_LLVM_LIBCLANG_PATH-NOTFOUND")
  message(FATAL_ERROR "libclang not found in directory ${LLVM_LIBRARY_DIRS}")
endif()

message(STATUS "Found libclang: ${CDSL_LLVM_LIBCLANG_PATH}")


# link liblang to our lib directory:

file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)

file(CREATE_LINK ${CDSL_LLVM_LIBCLANG_PATH} ${CMAKE_CURRENT_BINARY_DIR}/lib/libclang-${LLVM_VERSION_MAJOR}.so
  RESULT my_status
  SYMBOLIC
  )
if(my_status AND NOT my_status EQUAL 0)
  message(FATAL_ERROR ${my_status})
endif()

# find clang++:
set(my_dir ${LLVM_INSTALL_PREFIX}/${LLVM_TOOLS_INSTALL_DIR})

find_program(CDSL_CLANGPP_PRG clang++ HINTS ${my_dir})
if( ${CDSL_CLANGPP_PRG} STREQUAL "CDSL_CLANGPP_PRG-NOTFOUND")
  message(FATAL_ERROR "clang++ not found.")
endif()

