#
# some files conatain the build version of CDSL_ROOT
# we have to rewrite these files with the final installation path
#
set(CDSL_CMAKE_INSTALL_PHASE TRUE)

include(cmake/support.cmake)

set(CDSL_ROOT ${CMAKE_INSTALL_PREFIX})
configure_special_files(${CMAKE_CURRENT_BINARY_DIR}  ${CMAKE_CURRENT_BINARY_DIR}/_pre_install)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/bin/cdsl.in ${CMAKE_CURRENT_BINARY_DIR}/_pre_install/bin/cdsl @ONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/bin/sir2cpp.in ${CMAKE_CURRENT_BINARY_DIR}/_pre_install/bin/sir2cpp @ONLY)


