#
# sets CDSL_PYTHON_PRG
#

function(get_bindings test_arg bindings lcpath)
  execute_process(COMMAND ${CDSL_PYTHON_PRG} -B ${CMAKE_SOURCE_DIR}/conf/test_clang.py ${test_arg}
    ERROR_VARIABLE MY_ERROR
    RESULT_VARIABLE MY_STATUS
    OUTPUT_VARIABLE MY_OUTPUT
    )
  if(MY_STATUS EQUAL 0)
    list(GET MY_OUTPUT 0 out0)
    list(GET MY_OUTPUT 1 out1)
    list(GET MY_OUTPUT 2 out2)
    string(STRIP ${out2} out2)
    set(${bindings} ${out0} CACHE PATH "Python clang module" FORCE)
    set(${lcpath} ${out1} CACHE FILEPATH "libclang path" FORCE)
    set(preferred_libclang ${out2})
    if(${preferred_libclang} MATCHES "libclang\-")
      if(NOT ${preferred_libclang} STREQUAL "libclang-${LLVM_VERSION_MAJOR}.so")
        message(STATUS "*WARNING*: Version mismatch between LLVM-${LLVM_VERSION_MAJOR} and Python bindings built for ${out2}")
      else()
        message(STATUS "Found matching versions of LLVM-${LLVM_VERSION_MAJOR} and Python bindings built for ${out2}")
      endif()
    else()
      message(STATUS "Found LLVM-${LLVM_VERSION_MAJOR} and Python bindings (${out2})")
    endif()
    set(have_libclang_bindings TRUE PARENT_SCOPE)
  else()
    set(${bindings} "" CACHE PATH "Python clang module" FORCE)
    set(${lcpath} "" CACHE FILEPATH "libclang path" FORCE)
  endif()
endfunction()

function(set_cdsl_python_prg)
  # Dawn requires version Python version 3.4
  # ctypes fallback usage of LD_LIBRARY_PATH seems to require 3.6
  set(REQUIRED_PYTHON3_VERSION "3.6.0" )
  if(Python3_VERSION VERSION_LESS REQUIRED_PYTHON3_VERSION)
    message(FATAL_ERROR "Python (${CDSL_PYTHON_PRG}) version >= ${REQUIRED_PYTHON3_VERSION} required (found ${Python3_VERSION}).")
  endif()
  message(STATUS "Found Python3_VERSION ${Python3_VERSION}  (required: ${REQUIRED_PYTHON3_VERSION}).")
  set(CDSL_PYTHON_PRG ${Python3_EXECUTABLE} CACHE FILEPATH "Python3_EXECUTABLE")
endfunction()

# pick Python:
if(NOT CDSL_PYTHON_PRG)
  # consider ENV{CDSL_PYTHON_PRG}:
  if(NOT "$ENV{CDSL_PYTHON_PRG}" STREQUAL "")
    set(Python3_EXECUTABLE $ENV{CDSL_PYTHON_PRG})
    find_package(Python3)
    if(Python3_FOUND)
      message(STATUS "Python3 path taken from CDSL_PYTHON_PRG environment variable.")
      set_cdsl_python_prg()
    else()
      unset(Python3_EXECUTABLE)
    endif()
  endif()
endif()

if(NOT CDSL_PYTHON_PRG)
  # consider VIRTUAL_ENV:
  if(NOT "$ENV{VIRTUAL_ENV}" STREQUAL "")
    set(Python_FIND_VIRTUALENV ONLY)
    find_package(Python3)
    if(Python3_FOUND)
      message(STATUS "Python3 path taken from virtual env ($ENV{VIRTUAL_ENV}).")
      set_cdsl_python_prg()
    else()
      unset(Python_FIND_VIRTUALENV)
    endif()
  endif()
endif()

if(NOT CDSL_PYTHON_PRG)
  # consider PATH:
  execute_process(COMMAND which "python3" OUTPUT_VARIABLE MY_OUTPUT_VAR)
  string(STRIP ${MY_OUTPUT_VAR} CURRENT_PYTHON)
  set(Python3_EXECUTABLE ${CURRENT_PYTHON})
  find_package(Python3)
  if(Python3_FOUND)
    message(STATUS "Python path taken from PATH environment variable (${Python3_EXECUTABLE}.")
    set_cdsl_python_prg()
  else()
    unset(Python3_EXECUTABLE)
  endif()
endif()

if(NOT CDSL_PYTHON_PRG)
  # fallback:
  find_package(Python3)
  if(Python3_FOUND)
    set(CDSL_PYTHON_PRG ${Python3_EXECUTABLE} CACHE FILEPATH "Python3_EXECUTABLE")
    message(STATUS "Found Python3 via fallback (${Python3_EXECUTABLE}.")
    set_cdsl_python_prg()
  endif()
endif()

# store PYTHONPATH and LD_LIBRARY_PATH for runtime:
# + because currently Dawn might not find the protobuf libs if they have been built outside of Dawn
# - storing the full paths is somewhat overkill, instead we should pick those we need
if("${CDSL_LD_LIBRARY_PATH}" STREQUAL "")
  set(my_candidates $ENV{CDSL_LD_LIBRARY_PATH} $ENV{LD_LIBRARY_PATH})
  list(GET my_candidates 0 my_output)
  if(NOT "${my_output}" STREQUAL "NOTFOUND")
    set(CDSL_LD_LIBRARY_PATH ${my_output} CACHE PATH "LD_LIBRARY_PATH at build generation time")
  endif()
endif()
if("${CDSL_PYTHONPATH}" STREQUAL "")
  set(my_candidates $ENV{CDSL_PYTHONPATH} $ENV{PYTHONPATH})
  list(GET my_candidates 0 my_output)
  if(NOT "${my_output}" STREQUAL "NOTFOUND")
    set(CDSL_PYTHONPATH ${my_output} CACHE PATH "PYTHONPATH at build generation time")
  endif()
endif()

if(NOT CDSL_PYTHON_PRG)
  message(FATAL_ERROR "Python3 not found.")
endif()

if( CDSL_PYTHON_PRG AND
    CDSL_CLANG_MODULE_PATH AND
    CDSL_LIBCLANG_PATH AND
    CDSL_DAWN4PY_PATH
    )
  set(have_libclang_bindings TRUE)
else()
  set(have_libclang_bindings FALSE)
endif()

if(NOT have_libclang_bindings)

  ## establish Python bindings to libclang:
  ## for cases 1..3 we need the dlinfo package

  # 1. try: libclang-<Version>.so
  get_bindings(libclang-${LLVM_VERSION_MAJOR}.so CDSL_CLANG_MODULE_PATH CDSL_LIBCLANG_PATH)

  # 2. try: libclang.so
  if (NOT have_libclang_bindings)
    get_bindings(libclang.so CDSL_CLANG_MODULE_PATH CDSL_LIBCLANG_PATH)
  endif()
  
  # 3. try: offer the libclang-<Version>.so link in our lib directory:
  if (NOT have_libclang_bindings)
    get_bindings(${CDSL_ROOT}/lib CDSL_CLANG_MODULE_PATH CDSL_LIBCLANG_PATH)
  endif()
  
  # 4. fallback: tell clang module explicitly which library to load:
  if (NOT have_libclang_bindings)
    get_bindings(${CDSL_LLVM_LIBCLANG_PATH} CDSL_CLANG_MODULE_PATH CDSL_LIBCLANG_PATH)
  endif()
  
  if (NOT have_libclang_bindings)
    message(FATAL_ERROR "Could not establish working Python bindings to libclang.")
  endif()

  get_filename_component(CDSL_CLANG_MODULE_DIR ${CDSL_CLANG_MODULE_PATH} DIRECTORY CACHE)

endif()

