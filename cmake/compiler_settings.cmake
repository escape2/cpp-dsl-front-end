
# compiler:
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

if (CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
  # gfortran
  set(_common_fcflags "-cpp -fmax-errors=1 -ffree-line-length-0 -march=native -mpc64")
  if(DEFINED ENV{CDSL_EXTRA_FCFLAGS})
    set(_common_fcflags "${_common_fcflags} $ENV{CDSL_EXTRA_FCFLAGS}")
  endif()
  set (CMAKE_Fortran_FLAGS_RELEASE "${_common_fcflags} -O2")
  set (CMAKE_Fortran_FLAGS_DEBUG   "${_common_fcflags} -g -O0")
else()
  message("Note: using untested compiler case:")
  message("Fortran_COMPILER_NAME=${Fortran_COMPILER_NAME}")
  message("CMAKE_Fortran_COMPILER_ID=${CMAKE_Fortran_COMPILER_ID}")
endif()

