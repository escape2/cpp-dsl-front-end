# Netcdf for Fortran
#
# we require that the user makes nf-config visible

if("${CDSL_NF_CONFIG_PRG}" STREQUAL "")
  find_program (CDSL_NF_CONFIG_PRG nf-config REQUIRED)
  if(${CDSL_NF_CONFIG_PRG} STREQUAL "CDSL_NF_CONFIG_PRG-NOTFOUND")
    message(FATAL_ERROR "Cannot find nf-config program.")
  else()
    message(STATUS "Found nf-config (${CDSL_NF_CONFIG_PRG})")
  endif()
endif()

if("${CDSL_NETCDFF_LIB_DIR}" STREQUAL "")
  execute_process(COMMAND ${CDSL_NF_CONFIG_PRG} "--flibs" OUTPUT_VARIABLE MY_OUTPUT_VAR )
  if(${MY_OUTPUT_VAR} MATCHES "-L[ \t\r\n]*([^ \t\r\n]+)")
    set(CDSL_NETCDFF_LIB_DIR ${CMAKE_MATCH_1} CACHE PATH  "nf-config --flibs")
  else()
    message(FATAL_ERROR "Cannot find libdir in '${MY_OUTPUT_VAR}'.")
  endif()
endif()

if("${CDSL_NETCDFF_LIB}" STREQUAL "")
  find_library(CDSL_NETCDFF_LIB netcdff HINTS ${CDSL_NETCDFF_LIB_DIR}  REQUIRED)
  message(STATUS "Found NETCDFF library (${CDSL_NETCDFF_LIB})")
endif()

if("${CDSL_NETCDFF_INCLUDE_DIR}" STREQUAL "")
  execute_process(COMMAND ${CDSL_NF_CONFIG_PRG} "--includedir" OUTPUT_VARIABLE CDSL_NETCDFF_INCLUDE_DIR )
  string(STRIP ${CDSL_NETCDFF_INCLUDE_DIR} CDSL_NETCDFF_INCLUDE_DIR)
  set(CDSL_NETCDFF_INCLUDE_DIR ${CDSL_NETCDFF_INCLUDE_DIR} CACHE PATH "nf-config --includedir")
  message(STATUS "Found NETCDFF include directory (${CDSL_NETCDFF_INCLUDE_DIR})")
endif()

