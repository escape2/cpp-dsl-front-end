#
# sets CDSL_GridTools_INCLUDE_PATH
#

if("${CDSL_GridTools_INCLUDE_PATH}" STREQUAL "")

  set(CDSL_GridTools_INCLUDE_PATH ${GridTools_INCLUDE_PATH})
  if("${CDSL_GridTools_INCLUDE_PATH}" STREQUAL "")
    set(CDSL_GridTools_INCLUDE_PATH $ENV{GridTools_INCLUDE_PATH})
    if("${CDSL_GridTools_INCLUDE_PATH}" STREQUAL "")
      message(FATAL_ERROR "CDSL_GridTools_INCLUDE_PATH not set.")
    endif()
  endif()

  if(NOT EXISTS ${CDSL_GridTools_INCLUDE_PATH}/gridtools/stencil_composition/stencil_composition.hpp)
    message(FATAL_ERROR "Not found: ${CDSL_GridTools_INCLUDE_PATH}/gridtools/stencil_composition/stencil_composition.hpp")
  endif()

  set(CDSL_GridTools_INCLUDE_PATH ${CDSL_GridTools_INCLUDE_PATH} CACHE PATH "must contain gridtools/stencil_composition/stencil_composition.hpp")
  message(STATUS "CDSL_GridTools_INCLUDE_PATH = ${CDSL_GridTools_INCLUDE_PATH}")

endif()



