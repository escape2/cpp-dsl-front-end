#
# general settings
#

# check if build dir != source dir
if("${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}")
    message(FATAL_ERROR "In-source build are not supported.")
endif()


# build type:
if (NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "default build type" FORCE)
endif()
message(STATUS "CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}.")

#debug mode: enable extra debug support
if("$ENV{CDSL_DEBUG_MODE}")
  set(CDSL_DEBUG_MODE TRUE CACHE INTERNAL "mode with extra debug support" )
else()
  set(CDSL_DEBUG_MODE FALSE CACHE INTERNAL "mode without extra debug support" )
endif()

# module path:
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

# download dir for gridfiles:
if(NOT CDSL_DOWNLOAD_DIR)
  set(CDSL_DOWNLOAD_DIR ${PROJECT_BINARY_DIR}/.download CACHE PATH "Download dir for gridfiles.")
  file(MAKE_DIRECTORY ${CDSL_DOWNLOAD_DIR})
endif()

# RPATH handling:
set(CMAKE_SKIP_BUILD_RPATH FALSE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# git sha1:
execute_process(COMMAND  git rev-parse HEAD
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  RESULT_VARIABLE _result
  OUTPUT_VARIABLE _out)
if(NOT "${_result}" STREQUAL "0")
  message(FATAL_ERROR "git command failed")
endif()
string(STRIP ${_out} _out)
set(CDSL_GIT_SHA1 ${_out} CACHE STRING "current git hash")

execute_process(COMMAND  git rev-parse --short HEAD
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  RESULT_VARIABLE _result
  OUTPUT_VARIABLE _out)
if(NOT "${_result}" STREQUAL "0")
  message(FATAL_ERROR "git command failed")
endif()
string(STRIP ${_out} _out)
set(CDSL_GIT_SHA1_SHORT ${_out} CACHE STRING "short version of current git hash")

# Dawn capabilities:
if("$ENV{DAWN_HAS_OFFSET_REDUCTION}")
  set(CDSL_HAS_OFFSET_REDUCTION 1)
else()
  set(CDSL_HAS_OFFSET_REDUCTION 0)
endif()
message(STATUS "assuming CDSL_HAS_OFFSET_REDUCTION=${CDSL_HAS_OFFSET_REDUCTION}")

if("$ENV{DAWN_HAS_FLEX_STRUCT_HALOS}")
  set(CDSL_HAS_FLEX_STRUCT_HALOS 1)
else()
  set(CDSL_HAS_FLEX_STRUCT_HALOS 0)
endif()
message(STATUS "assuming CDSL_HAS_FLEX_STRUCT_HALOS=${CDSL_HAS_FLEX_STRUCT_HALOS}")

# tests:
enable_testing()
