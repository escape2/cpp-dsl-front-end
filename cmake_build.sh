#!/bin/bash
#
# driver script to build/test/install the frontend
#
# the first argument is evaluated as build phase parameter (range: 0..4):
#
# 0: delete the build dir contents without the .download directory
# 1: build generation phase
# 2: build phase
# 3: test phase
# 4: install phase
# all: 0 .. 4

set -e

BUILD_PHASE="$1"
self=$0

if [[ "$BUILD_PHASE" == "all" ]]; then
    $self 0 && $self 1 && $self 2 && $self 3 && $self 4
    exit
elif [[ "$BUILD_PHASE" == "1" ]]; then
    # for now: we do a full cleanup at every config stage
    if [[ -f build/CMakeCache.txt ]]; then
        $self 0
    fi
fi

export CDSL_SOURCE_DIR=$(pwd)

# check if we source the user-defined config file or our system_config
if [[ -f user_config.sh ]]; then
    # this file is not part of the repository but is used if it exists
    CONFIG_FILE=$PWD/user_config.sh
else
    CONFIG_FILE=$PWD/conf/config.sh
fi

mkdir -p build && cd build && touch .is_build_dir


if [[ $BUILD_PHASE == 0 ]]; then

    # cleanup:
    # We try to be careful about where we are:
    if [[ ! -f ".is_build_dir" ]]; then
        echo "Error1: Could not confirm build dir property for cleanup."
        exit 1
    fi
    if [[ -f CMakeLists.txt ]]; then
        echo "Error2: Found unexpected file: CMakeLists.txt."
        exit 1
    fi
    entries=$(echo *)
    if [[ "$entries" == "*" ]]; then
        echo "$0: no cleanup to do"
        exit
    fi
    # We try to be very careful about where we are:
    if [[ ! $entries =~ CMakeCache.txt ]]; then
        echo "Error2: Could not confirm build dir property for cleanup."
        exit 1
    fi
    # delete visible build dir files:
    echo "$0: delete in $PWD/: $entries"
    rm -rf $entries

elif [[ $BUILD_PHASE == 1 ]]; then
    echo "CONFIG_FILE = $CONFIG_FILE"
    # config:
    source $CONFIG_FILE

    # generate configuration templates:
    mkdir -p conf
    $CDSL_SOURCE_DIR/conf/meta_conf.pl

    # generate list of source files:
    $CDSL_SOURCE_DIR/conf/update_filenames.pl

    # cmake:
    MY_CMAKE_ARGS=""
    if [[ ! -z "$ATLAS" ]]; then
        MY_CMAKE_ARGS="$MY_CMAKE_ARGS -Datlas_DIR=$ATLAS/lib/cmake/atlas"
    fi
    if [[ ! -z "$CDSL_INSTALL_DIR" ]]; then
        MY_CMAKE_ARGS="$MY_CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=$CDSL_INSTALL_DIR"
    fi
    echo "$0: cmake $MY_CMAKE_ARGS -S $CDSL_SOURCE_DIR"
    BUILD_TYPE="Release"
    cmake $MY_CMAKE_ARGS -S $CDSL_SOURCE_DIR -DCMAKE_BUILD_TYPE=$BUILD_TYPE

elif [[ $BUILD_PHASE == 2 ]]; then

    # config:
    source $CONFIG_FILE

    export VERBOSE=1
    echo "$0: cmake --build ."
    cmake --build .

elif [[ $BUILD_PHASE == 3 ]]; then

    echo "$0: ctest --verbose"
    ctest --verbose

elif [[ $BUILD_PHASE == 4 ]]; then

    echo "$0: make install"
    make install

else

    echo "Nothing to do? Select building BUILD_PHASE: 0..4"

fi

