#!/usr/bin/env python3
# Distribution: see LICENSE.txt

from enum import Enum
from common import *
import debug_ast as AST

__ALL__ = ["Terminal", "Fcall"]


def reset_sir_globals():
    global g_sir_globals
    g_sir_globals = ""


class Terminal:
    def __init__(self, terminal):
        self.t = terminal

    def str(self):
        return self.t


class Fcall(DispFormGen):
    def __init__(self, fname, *args, **kwargs):
        self.fname = fname
        assert not (args and kwargs)
        self.args = args
        self.kwargs = kwargs

    def get_output_str(self):
        return self.str()

    def str(self):
        reset_sir_globals()
        if self.fname == "sir_utils.make_sir":
            grid_type_fcall = self.kwargs.get("grid_type") or die("bad case")
            assert isinstance(grid_type_fcall,AST.Fcall)
            grid_type = grid_type_fcall.args[0]
            body = "my_sir = (" + dispatch(self) + ")"
            return "\n".join([script_head(), g_sir_globals, body, script_tail(grid_type)])
        else:
            die("bad case")

    def _str(self):
        return dispatch(self)


class OLD_BuiltinType(Enum):
    Boolean = "SIR.BuiltinType.Boolean"
    Double = "SIR.BuiltinType.Double"
    Float = "SIR.BuiltinType.Float"
    Integer = "SIR.BuiltinType.Integer"


class OLD_VerticalRegion(Enum):
    Forward = "SIR.VerticalRegion.Forward"
    Backward = "SIR.VerticalRegion.Backward"


class OLD_Interval(Enum):
    Start = "SIR.Interval.Start"
    End = "SIR.Interval.End"


class OLD_GridType:
    def Value(arg):
        return Fcall("SIR.GridType.Value", arg)


class OLD_LocationType:
    def Value(arg):
        return Fcall("SIR.LocationType.Value", arg)


class GlobalVariableValue:
    def __init__(self):
        pass


class GVMap:
    def __init__(self):
        self._map = {}

    def __getitem__(self, key):
        if self._map.get(key) is None:
            self._map[key] = GlobalVariableValue()
        return self._map[key]

    def __setitem__(self, key, value):
        assert isinstance(value, GlobalVariableValue)
        self._map[key] = value


class GlobalVariableMap:
    def __init__(self):
        self.map = GVMap()


def proc_GlobalVariableMap(f):
    global g_sir_globals
    a = ["sir_globals=sir_utils.GlobalVariableMap()"]

    for k, v in f.map._map.items():
        for p in vars(v):
            v_p = getattr(v, p)
            a.append('sir_globals.map["' + k + '"].' + p + "=" + str(v_p))
    g_sir_globals = "\n".join(a)
    return "sir_globals"


def script_tail(grid_type):
    if grid_type == "Cartesian":
        backend = "dawn4py.CodeGenBackend.CXXNaive"
    else:
        backend = "dawn4py.CodeGenBackend.CXXNaiveIco"
    s = "\n".join(
        [
            "my_code = dawn4py.compile(my_sir, backend=" + backend + ")",
            "print(my_code)",
            "#end",
        ]
    )
    return s


def script_head():
    s = "\n".join(
        [
            "#!/usr/bin/env python",
            "#start",
            "import dawn4py",
            "from dawn4py.serialization import SIR, AST",
            "from dawn4py.serialization import utils as sir_utils",
        ]
    )
    return s

def dispatch(f, keyword=""):
    # print the SIR python calls 
    # no pretty-printer; a readable form reuires post-processing with black
    s0 = keyword
    if isinstance(f, str):
        s = '"' + f + '"'
    elif isinstance(f, dict):
        a = []
        for k,v in f.items():
            a.append(dispatch(v, keyword=k + "=")) 
        s = (",").join(a)
    elif isinstance(f, list):
        a = [dispatch(x) for x in f]
        s = "[" + ",".join(a) + "]"
    elif isinstance(f, tuple):
        a = [dispatch(x) for x in f]
        s = "(" + ",".join(a) + ")"
    elif isinstance(f, Fcall):
        a = [dispatch(x) for x in f.args]
        if f.kwargs:
            a.append(dispatch(f.kwargs))
        b =  ",".join(a)
        s = f.fname + "(" + b + ")"
    elif isinstance(f, AST.Fcall):
        return dispatch(Fcall(f.fname, *f.args, **f.kwargs), keyword=keyword)
    elif isinstance(f, Enum):
        s = f.value
    elif isinstance(f, Terminal):
        s = f.t
    elif isinstance(f, (bool, int, float)):
        s = str(f)
    elif isinstance(f, GlobalVariableMap):
        s = proc_GlobalVariableMap(f)
    elif f is None:
        s = "None"
    else:
        die("bad case: " + str(f))
    return s0 + s
