#!/usr/bin/env python3
# Distribution: see LICENSE.txt

# Description: transformation from SA to HIR

from collections import namedtuple, OrderedDict
from copy import copy
import json
import pprint

import tvs
from tvs import tdef, tqdef, tfun, is_lot, gen_tv, Tv, is_tv, tv_str, tname, tqname

import parser_config
from parser_config import struct_uqt_set, set_type2elm_type, elm_type2set_type
from parser_config import Dimension, g_dsl_vdim
import dsl_parser
from common import *

from clang.cindex import Cursor

from inspect import currentframe, getframeinfo

import HIR
from HIR import BinOp, UnOp

id2binop = {
    "+": BinOp.plusOp,
    "-": BinOp.minusOp,
    "*": BinOp.mulOp,
    "/": BinOp.divOp,
    "**": BinOp.powerOp,
    "&&": BinOp.logicalAnd,
    "||": BinOp.logicalOr,
    "==": BinOp.logicalEqual,
    "!=": BinOp.logicalNotEqual,
    ">": BinOp.logicalGt,
    "<": BinOp.logicalLt,
    ">=": BinOp.logicalGe,
    "<=": BinOp.logicalLe,
}

id2unop = {
    "-": UnOp.unaryMinus,
    "!": UnOp.logNot,
    "++": UnOp.incrementOp,
    "--": UnOp.decrementOp,
}

elem_type2dim_name = {}

set_type2dim_name = {}

used_GridDimension = {}

require_init = True


def reset_fun_decl_space():
    global fun_decl_space
    fun_decl_space = {}


def reset():
    global reg_gdims, reg_FieldDecls, VRegionState, DomCompState
    global g_cur_fun
    reg_gdims = {}
    reg_FieldDecls = {}
    VRegionState = 0
    DomCompState = 0  # domain_computation scope
    g_cur_fun = None

    reset_fun_decl_space()


def add_DomCompState(delta):
    global DomCompState
    DomCompState += delta
    assert DomCompState in (0, 1)


def add_VRegionState(delta):
    global VRegionState
    VRegionState += delta
    assert VRegionState in (0, 1)


def set_fun_decl_space(name, decl):
    global fun_decl_space
    if fun_decl_space.get(name):
        die("Shadowed declarations not suppoorted by current flat declaration space.")
    fun_decl_space[name] = decl


def register_GridDimension(gdim):
    gdim0 = reg_gdims.get(gdim.name)
    if gdim0:
        # we already have the name registered: check if the objects match
        if not gdim._dim.matches(gdim0._dim):
            ICE()
    else:
        reg_gdims[gdim.name] = gdim


def register_FieldDecl(fdecl):
    return  # disabled -> TODO: we need to clarify the HIR intention first
    fdecl0 = reg_FieldDecls.get(fdecl.name)
    if fdecl0:
        assert fdecl0._type == fdecl._type
        # todo: do we want to check more properties?
        return
    reg_FieldDecls[fdecl.name] = fdecl


def init():
    global require_init
    init_dim_names()
    require_init = False


def unused_proposed_str(s):
    # return "proposed_" + s
    return s


def ast_str(s):
    return "AST_" + s


def init_dim_names():
    for s in parser_config.set_type2elm_type:
        e = parser_config.set_type2elm_type[s]
        d = s
        assert not s in set_type2dim_name
        set_type2dim_name[s] = d
        if not e in ("bool", "int", "float", "double"):
            assert not e in elem_type2dim_name
            elem_type2dim_name[e] = d


def check_id_length(s):
    if len(s) > 31:
        die("Identifier too long: " + s)
    return s


def namespaced(s):
    return check_id_length("HIR_" + s)


def uqt2SetType(uqt):
    return (
        elem_type2dim_name.get(uqt)
        or set_type2dim_name.get(uqt)
        or die("unexpected case")
    )


def hir_dim_name(dim):
    return dim.name


def dim2LocationType(dim):
    elem = set_type2elm_type.get(dim)
    if elem == "Vert":
        elem = "Node"
    return elem.lower()


def strip_intrinsic_prefix(s):
    if s.startswith("Intrinsic_"):
        return s[10:]
    return s


def strip_edsl_prefix(s):
    if s.startswith("const "):
        s = s[6:]
    if s.startswith("EDSL::"):
        s = s[6:]
    return s


def strip_const_prefix(s):
    if s.startswith("const "):
        s = s[6:]
    return s


def strip_type_name(ct):
    s = strip_edsl_prefix(ct)
    if s.startswith("Intrinsic_"):
        return s[10:].capitalize()
    return s


def uqt2scalar_hir_type(*, uqt):
    # tentative HIR extension: allow bools:
    if uqt in ("double", "int", "float", "bool"):
        return uqt
    die("unsupported case:")


def get_field_value_type(*, uqt, locus):
    if uqt in ("Field", "Field_dp"):
        return HIR.Type("double", locus)
    if uqt in ("Field_bool", "Field_int"):
        return HIR.Type("int", locus)
    die("bad case")


def grid_dims2hir_Domain(gdims, locus):
    # list of HIR.GridDimension -> HIR.Domain
    nonsparse_hdims = []
    sparse_hdims = []
    vdim = None
    other_dims = []
    num_struct_hdims = 0
    num_unstruct_hdims = 0
    for gdim in gdims:
        d = gdim._dim
        if d.is_horizontal():
            if d.is_struct():
                num_struct_hdims += 1
            else:
                num_unstruct_hdims += 1
            assert num_struct_hdims * num_unstruct_hdims == 0
            if d.is_sparse():
                sparse_hdims.append(gdim)
            else:
                nonsparse_hdims.append(gdim)
        elif d.is_vertical():
            assert vdim == None
            vdim = gdim
        else:
            other_dims.append(gdim)
    if vdim == None:
        vdim = hir_GridDimension(g_dsl_vdim, locus)
    assert num_struct_hdims in (0, 2)
    if not len(nonsparse_hdims) > 0:
        general_error("Horizontal aspect is missing.")
    if num_struct_hdims:
        my_hkinds = [x._dim.iter_set.get_struct_hdim_kind() for x in nonsparse_hdims]
        if my_hkinds[0] > my_hkinds[1]:
            nonsparse_hdims = nonsparse_hdims[::-1]
    DomainParallelDimensions = nonsparse_hdims
    VerticalDimension = vdim
    ParallelDimensions = other_dims
    return HIR.Domain(
        DomainParallelDimensions, VerticalDimension, ParallelDimensions, locus
    )


def match_hir_exprModel(f):

    g = (
        match_hir_unaryOpModel(f)
        or match_hir_binaryOpModel(f)
        or match_hir_TernaryOp(f)
        or match_hir_Literal(f)
        or match_hir_VarAccess(f)
        or match_hir_FctCall(f)
    )

    if DomCompState:
        # HIR extension: allow DotProduct
        g = (
            g
            or match_hir_NeighbourReduce(f)
            or match_hir_DotProduct(f)
            or match_hir_FieldAccess(f)
        )
    return g


def match_hir_stmtModel(f):
    if not isinstance(f, Tv):
        return
    # HIR extension: allow AssignmentStmt
    g = (
        match_hir_VarDecl(f)
        or match_hir_IfStmt(f)
        or match_hir_BlockStmt(f)
        or match_hir_AssignmentStmt(f)
    )
    if g != None:
        return g

    if DomCompState:
        g = match_hir_SparseIfStmt(f) or match_hir_LoopOn(f)
    else:
        g = match_hir_VerticalRegion(f)

    return g


def simplify_stmts(stmts):
    assert isinstance(stmts, (list, tuple))
    a = []
    for f in stmts:
        if f.t == tdef.decl_stmt:
            a.extend(f.var_decls)
        else:
            a.append(f)
    return a


def unused_match_hir_stmtModel_list(flist):
    if not isinstance(flist, (list, tuple)):
        return
    glist = []
    for f in flist:
        g = match_hir_stmtModel(f)
        if g == None:
            return
        glist.append(g)
    return glist


def match_all(matcher, flist, error_msg=None):
    if isinstance(flist, (list, tuple)):
        glist = []
        for f in flist:
            g = matcher(f)
            if g == None:
                if error_msg:
                    error(error_msg)
                return
            glist.append(g)
        return glist
    else:
        g = matcher(flist)
        return g


def match_None(x):
    if x == none:
        return


def listify(x):
    if isinstance(x, (list, tuple)):
        return x
    return [x]


def has_single_chain_property(dims):
    udims = [d for d in dims if d.is_unstruct()]
    if len(udims) < 2:
        return True
    last_iter_sets = []
    for d in udims:
        osets = d.orig_sets
        if len(osets) != len(last_iter_sets):
            ICE()
        for p in range(len(osets)):
            if not osets[p].matches(last_iter_sets[p]):
                return False
        last_iter_sets.append(d.iter_set)
    return True


def dfun2_field_var_decl(f):
    assert f.t == tdef.var_decl and f.tq == tqdef.field
    decl_dims = getattr(f, "_decl_dims", ())
    has_single_chain_property(decl_dims) or die("single chain property required")
    gdims = []
    for d in decl_dims:
        gdim = hir_GridDimension(d, f.locus)
        register_GridDimension(gdim)
        gdims.append(gdim)

    Type = get_field_value_type(uqt=f._ctp.uqt, locus=f.locus)
    h = HIR.FieldDecl(f.id, Type, gdims, f.locus)
    set_fun_decl_space(h.name, h)
    if getattr(f, "is_fun_local", None):
        # to be discussed:: disabled: register_FieldDecl(h)
        return HIR.NOP(f.locus)
    return h


def dfun2_scalar_var_decl(f):
    assert f.t == tdef.var_decl and f.tq == tqdef.scalar
    ct = getattr(f, "ct", "")
    if ct.startswith("EDSL::Intrinsic_"):
        return HIR.NOP(f.locus)  # we don't need to declare intrinsic variables
    id = getattr(f, "id", "")
    ctp = f._ctp
    hir_type = HIR.Type(uqt2scalar_hir_type(uqt=ctp.uqt), f.locus)
    is_const = getattr(ctp, "const", False)
    expr = getattr(f, "expr", None)
    hir_expr = (
        None
        if expr == None
        else match_all(match_hir_exprModel, expr) or error("unsported expression form")
    )
    has_idx = ctp.has_idx
    if has_idx:
        idx = getattr(ctp, "idx", None) or ICE()
        if not ctp.idx_is_int:
            die("invalid vector declaration")
        ndims = idx
    else:
        ndims = 0
        assert isinstance(ndims, int)
    init_list = getattr(f, "init_list", None)
    if init_list:
        expr_list = init_list.init_list_expr
    else:
        expr_list = ()

    hir_expr_list = []
    for x in expr_list:
        y = dispatch(x)
        hir_expr_list.append(y)
    if ndims == 0:
        assert not hir_expr_list
        hir_init = hir_expr
    else:
        assert not hir_expr
        hir_init = hir_expr_list
    return HIR.VarDecl(
        hir_type, id, f.locus, ndims, initialization=hir_init, is_const=is_const
    )


def dfun_var_decl(f):
    assert f.t == tdef.var_decl
    if f.tq == tqdef.field:
        return dfun2_field_var_decl(f)
    elif f.tq == tqdef.scalar:
        return dfun2_scalar_var_decl(f)
    else:
        tstop(getframeinfo(currentframe()))


def match_hir_VarDecl(f):
    if f.t != tdef.var_decl or f.tq != tqdef.scalar:
        return
    return dfun2_scalar_var_decl(f)


def match_hir_FieldDecl(f):
    if f.t != tdef.var_decl or f.tq != tqdef.field:
        return
    return dfun2_field_var_decl(f)


def dfun_translation_unit(f):
    name = f.filename
    dotpos = name.rfind(".")
    if dotpos > 0:
        name = name[:dotpos]
    body = getattr(f, "body", ())
    funs = []
    globs = []
    for x in body:
        if x.t in (tdef.function_decl, tdef.var_decl):
            y = dispatch(x)
        else:
            # we ignore anything else
            continue
        if y is None:
            continue
        if x.t == tdef.var_decl:
            if funs:
                node_error(
                    x, "Global declarations must procede all function declarations."
                )
            globs.append(y)
        else:
            funs.append(y)
    SpOrEks = funs
    gdims = list(reg_gdims.values())
    domain = grid_dims2hir_Domain(gdims, f.locus)
    fdecls = []
    vdecls = globs
    source = f.id
    edsl_extension_src = getattr(f, "edsl_extension_src", None)
    return HIR.Program(
        name,
        source,
        gdims,
        domain,
        fdecls,
        vdecls,
        SpOrEks,
        f.locus,
        edsl_extension_src,
    )


def wrap_function_decl(fun):
    def wrapper(f):
        global g_cur_fun
        g_cur_fun = f
        g = fun(f)
        g_cur_fun = None
        return g

    return wrapper


@wrap_function_decl
def dfun_function_decl(f):
    name = getattr(f, "id", "") or die("missing f.id")
    return_type = getattr(f, "ct", "") or die("missing f.ct")
    if return_type != "void":
        die("non void functions not supported yet")
    parm_decls = dispatch(getattr(f, "parm_decls", ()))
    reset_fun_decl_space()
    stmts = getattr(f, "stmts", None)
    if stmts != None:
        a = filter_NOP(dispatch(stmts))
        block_stmt = HIR.BlockStmt(a, f.locus)
    else:
        block_stmt = None
    field_decls = list(fun_decl_space.values())
    reset_fun_decl_space()
    h = HIR.ScopedProgram(
        name, return_type, parm_decls, field_decls, block_stmt, f.locus
    )
    h._is_struct = f.is_struct
    return h


def filter_NOP(alist):
    if alist == None:
        ICE()
    a = []
    for x in alist:
        if isinstance(x, HIR.NOP):
            continue
        if isinstance(x, (list, tuple)):
            x = filter_NOP(x)
            if x:
                a.expand(x)
        a.append(x)
    return a


def dfun_block_stmt(f):
    assert isinstance(f, Tv)
    stmts = getattr(f, "stmts", ())
    a = dispatch(stmts)
    return HIR.BlockStmt(filter_NOP(a))


def match_hir_BlockStmt(f):
    if f.t != tdef.block_stmt:
        return
    return dfun_block_stmt(f)


def match_hir_Literal(f):
    if not f.t in (
        tdef.integer_literal,
        tdef.bool_literal,
        tdef.bool_literal,
        tdef.floating_literal,
    ):
        return
    return dispatch(f)


def gen_zero_Literal_int(locus):
    return HIR.Literal(HIR.Type("int", locus), "0", locus)


def dfun_var_ref(f):
    # There is a shift in concepts here:
    #   from: The frontend sees scalars and fields as variables (var).
    #   to: The HIR uses the term Var for non-fields.
    #
    assert f.t == tdef.var_ref
    if f.tq == tqdef.field:
        return HIR.FieldAccess(name=f.id, locus=f.locus)
    elif f.tq == tqdef.scalar:
        # Note: A var_ref is always without index, it reference the whole var object.
        h = HIR.VarAccess(
            f.id, f.locus, is_nonlocal_ref=getattr(f, "is_nonlocal_ref", False)
        )
    else:
        die("unexpected case")
    return h


def match_hir_lValueModel(f):
    if DomCompState:
        return (
            match_hir_FieldAccess(f)
            or match_hir_VarAccess(f)
            or match_hir_FieldAccess(f)
        )
    else:
        return match_hir_VarDecl(f) or match_hir_VarAccess(f)


def dfun_var_assign(f):
    return match_hir_AssignmentStmt(f) or error("invalid var_assign")


def match_hir_AssignmentStmt(f):
    if f.t != tdef.var_assign:
        return
    lhs = match_hir_lValueModel(f.lhs)
    rhs = match_hir_exprModel(f.rhs)
    if lhs and rhs:
        return HIR.AssignmentStmt(lhs, rhs, f.id, f.locus)
    return


def dfun_ternop(f):
    ICE("ternop not implemented yet")


def match_hir_TernaryOp(f):
    if f.t != tdef.ternop:
        return
    return dfun_ternop(f)


def dfun_binop(f):
    assert f.t == tdef.binop
    id = getattr(f, "id", "")
    hir_op = id2binop.get(id, None) or ICE("bad op")
    args = getattr(f, "args", ())
    assert len(args) == 2
    (lhs, rhs) = dispatch(args)
    return HIR.BinaryOp(hir_op, lhs, rhs, f.locus)


def match_hir_binaryOpModel(f):
    if f.t != tdef.binop:
        return
    return dfun_binop(f)


def dfun_unop(f):
    assert f.t == tdef.unop
    id = getattr(f, "id", "")
    hir_op = id2unop.get(id, None) or ICE("bad op")
    args = getattr(f, "args", ())
    assert len(args) == 1
    hir_expr = dispatch(args[0])
    return HIR.UnaryOp(hir_op, hir_expr, f.locus)


def match_hir_unaryOpModel(f):
    if f.t != tdef.unop:
        return
    return dfun_unop(f)


def ct2dim_name(ct):
    d = strip_type_name(ct)
    return get_dim_name(d)


def match_RelativeDimAccess_with_offset(f):
    if f.t != tdef.binop:
        return
    ctp = f._ctp
    if not getattr(ctp, "is_intrinsic", False):
        return
    args = getattr(f, "args", ())
    if len(args) != 2:
        return
    id = getattr(f, "id", "")
    if not id in ("+", "-"):
        return
    s = "-" if id == "-" else ""  # sign
    if args[0].t == tdef.var_ref and args[0]._ctp.is_intrinsic:
        (var, oval) = args
    elif args[1].t == tdef.var_ref and args[1]._ctp.is_intrinsic:
        (oval, var) = args
        if s:
            die("invalid offset")
    else:
        return
    if oval.t != tdef.integer_literal:
        return
    related_set = getattr(f, "_related_set", None)
    if related_set is not None:
        dim = Dimension(related_set)
    else:
        die("unexpected case")
    h_GridDimension = hir_GridDimension(dim, f.locus)
    h_distance = s + oval.v
    return HIR.Offset(h_GridDimension, h_distance, f.locus)


def match_RelativeDimAccess_without_offset(f):
    if f.t != tdef.var_ref:
        return
    if not getattr(f, "is_intrinsic", False):
        return
    related_set = getattr(f, "_related_set", None)
    if related_set is not None:
        dim = Dimension(related_set)
    else:
        ICE()
    gdim = hir_GridDimension(dim, f.locus)
    dist = 0
    return HIR.Offset(gdim, dist, f.locus)


def match_hir_AbsoluteFieldAccess(f):
    # TODO: Check if the HIR.AbsoluteFieldAccess corresponds to an SIR element.
    die("We do not have a test for an AbsoluteFieldAccess.")
    var_ref = getattr(f, "var_ref") or die("bad case")
    dkeys = getattr(f, "dim_keys", ())
    name = var_ref.id
    access_list = []
    for k in dkeys:
        # TODO: we are missing the VarAccess here
        hk = match_hir_FieldAccess(k)
        if not hk:
            return
        access_list.append(hk)
    return HIR.AbsoluteFieldAccess(name, access_list, f.locus)


def key_is_set_var(f):
    if f.t == tdef.var_ref and f.tq == tqdef.set:
        return True
    return False


def match_hir_FieldAccess(f):
    """ D2.3: The FieldAccess is a expression that defines an (relative to each grid point) access to a field """
    if f.tq != tqdef.field:
        return
    if f.t == tdef.var_ref:
        # minimal field access without access indices:
        return HIR.FieldAccess(name=f.id, locus=f.locus)
    if f.t != tdef.var_access:
        return
    var_ref = f.var_ref
    dkeys = getattr(f, "dim_keys", ())
    offsets = []
    dof = getattr(f, "_dof", ()) or die("missing _dof attribute")
    is_dense = all([d.is_dense() for d in f._decl_dims])
    indirect_dense_access = is_dense and any([d.is_sparse() for d in f._dof])
    for k in dkeys:
        if key_is_set_var(k):
            continue  # do we need to represent set access information?
        os = (
            match_RelativeDimAccess_with_offset(k)
            or match_RelativeDimAccess_without_offset(k)
            or None
        )
        if not os:
            return
        offsets.append(os)
    return HIR.FieldAccess(var_ref.id, f.locus, offsets, indirect_dense_access)


def dfun_var_access(f):
    assert f.t == tdef.var_access
    if f.tq == tqdef.field:
        return (
            match_hir_FieldAccess(f)
            or match_hir_AbsoluteFieldAccess(f)
            or die("bad case")
        )
    else:
        die("TODO: implement access to scalar variables")


def dfun_edsl_fcall(f):
    # TODO: do we have a use case?
    die("untested extension: edsl_fcall")
    h = Hnode("DSLFunCall")
    h.name = f.id
    h.arguments = dispatch(getattr(f, "args", ()))
    return h


def match_hir_FctCall(f):
    if f.t != tdef.fcall:
        return
    name = f.id
    args = getattr(f, "args", ())
    if not args:
        return
    hir_args = [match_hir_exprModel(expr) for expr in args]
    if not all(hir_args):
        ICE()
    return HIR.FctCall(name, hir_args, f.locus)


def dfun_fcall(f):
    return match_hir_FctCall(f) or error("invalid function call")


def dfun_field_spec_stmt(f):
    return HIR.NOP(f.locus)


def dfun_return_stmt(f):
    return_expr = getattr(f, "attr", None)
    if return_expr:
        return_expr = dispatch(expr)
    return HIR.ReturnStmt(f.locus, return_expr)


def dfun_parameters(f):
    die("untested extension: FunParams")
    h = Hnode("FunParams")
    assert is_lot(f.v)
    h._children = dispatch(f.v)
    return h


def match_hir_Condition(f):
    g = (
        match_hir_unaryOpModel(f)
        or match_hir_binaryOpModel(f)
        or match_hir_TernaryOp(f)
        or match_hir_VarAccess(f)
        or match_hir_Literal(f)
    )

    if DomCompState:
        g = g or match_hir_FieldAccess(f) or match_hir_NeighbourReduce(f)
    return g


def match_hir_SparseIfStmt(f):
    if f.t != tdef.if_stmt:
        return
    cond = f.cond
    is_sparse = cond.t == tdef.EDSL_fcall and getattr(cond, "id", "") == "unlikely"
    if not is_sparse:
        return
    return dfun_if_stmt(f)


def match_hir_IfStmt(f):
    if f.t != tdef.if_stmt:
        return
    cond = f.cond
    is_sparse = cond.t == tdef.EDSL_fcall and getattr(cond, "id", "") == "unlikely"
    if is_sparse:
        return
    return dfun_if_stmt(f)


def dfun_if_stmt(f):
    cond = f.cond
    is_sparse = False
    if cond.t == tdef.EDSL_fcall and getattr(cond, "id", "") == "unlikely":
        # TODO: check domain computation scope
        is_sparse = True
        sparse_args = getattr(cond, "args", ())
        if len(sparse_args) != 1:
            die("internal error")
        cond = sparse_args[0]
    hir_cond = match_hir_Condition(cond) or die("unsupported case")
    hir_then = listify(match_all(match_hir_stmtModel, f.tcase) or die("bad case"))
    fcase = getattr(f, "fcase", None)
    hir_else = (
        listify(match_all(match_hir_stmtModel, fcase) or die("bad case"))
        if fcase
        else None
    )
    if is_sparse:
        h = HIR.SparseIfStmt(f.locus, hir_cond, hir_then, hir_else)
    else:
        h = HIR.IfStmt(f.locus, hir_cond, hir_then, hir_else)
    return h


def Set2LocTypeName(myset):
    elm_type = myset.get_elm_type() or ICE()
    return elm_type.name


def hir_GridDimension(dim, locus):
    # parser_config.Dimension -> HIR.GridDimension
    # side effect: register GridDimension in used_GridDimension
    h_name = hir_dim_name(dim)
    if not dim.is_struct():
        h_LocationType = HIR.LocationType(Set2LocTypeName(dim.iter_set), locus)
        h_dimension_type = "dense" if dim.is_dense() else "sparse"
        usets = dim.orig_sets + [dim.iter_set]
        total_iter_chain = []
        for compound_set in usets:
            assert hasattr(compound_set, "get_chain")
            total_iter_chain.extend(compound_set.get_chain())
        hir_chain = [
            HIR.LocationType(Set2LocTypeName(s), locus) for s in total_iter_chain
        ]
        h_chain = hir_chain
    else:
        h_LocationType = None
        h_dimension_type = None
        h_chain = None

    h = HIR.GridDimension(h_name, locus, h_LocationType, h_dimension_type, h_chain)
    h._dim = dim  # will be removed from the HIR later (see cleanup_hir)
    used_GridDimension[h_name] = h
    return h


def dfun_nreduce(f):
    red_dim = getattr(f, "_red_dim", None) or ICE()
    assert red_dim.is_unstruct()
    gdim = hir_GridDimension(red_dim, f.locus)
    init_type = HIR.Type("double", f.locus)
    literal = HIR.Literal(init_type, "0", f.locus)  # TODO: gather this
    exprModel = dispatch(f.field_expr)
    weights = []
    offsets = []
    w = getattr(f, "weights")
    if w:
        if w.t != tdef.reduction_weights:
            ICE()
        init_list = getattr(w, "init_list", None)
        if init_list:
            ile = getattr(init_list, "init_list_expr", None)
            if not ile:
                error("Expected weights as init_list expression.")
            weights = dispatch(ile)
        offsets = getattr(w, "offsets") or ()

    if offsets:
        if offsets.t != tdef.reduction_offsets:
            ICE()
        init_list = getattr(offsets, "init_list", None)
        if init_list:
            ile = getattr(init_list, "init_list_expr", None)
            if not ile:
                error("Expected offsets as init_list expression.")
            offsets = dispatch(ile)
            
    # TODO: check for other op values
    return HIR.NeighbourReduce(f.locus, gdim, literal, exprModel, weights, offsets, op="add")


def match_hir_NeighbourReduce(f):
    if f.t != tdef.nreduce:
        return
    return dfun_nreduce(f)


def dfun_loop_on(f):
    sparse_dims = getattr(f, "_sparse_dims", ()) or ICE()
    args = getattr(f, "args", ())
    gdims = []
    if len(sparse_dims) > 1:
        error("Currently there is only support for one sparse dimension.")
        for d in sparse_dims:
            gdims.append(hir_GridDimension(d))
    else:
        d = sparse_dims[0]
        gdims.append(hir_GridDimension(d, f.locus))
    stmts = getattr(f, "stmts", ())
    bstmt = dispatch(stmts)
    return HIR.LoopOn(gdims, bstmt, f.locus)


def match_hir_LoopOn(f):
    if f.t != tdef.loop_on:
        return
    return dfun_loop_on(f)


def dfun_range_loop(f):
    die("untested extension: DoLoop")
    # TODO: do we have a use case?
    assert f.t == tdef.range_loop
    h = Hnode("DoLoop")
    var = f.loop_var_decl
    h.VarDecl = hir_VarDecl(var.ct, var.id)
    expr = f.expr
    if expr.t != tdef.var_ref:
        die("unsuported loop range expr")
    h.GridDimension = HIR.GridDimension(ct2dim_name(var.ct))
    stmts = getattr(f, "stmts", ())
    if stmts != None:
        h.BlockStmt = dfun_block_stmt(stmts)
    return h


def match_hir_VarAccess(f):
    if not f.t in (tdef.var_ref, tdef.vector_subscript_expr):
        return
    if f.tq != tqdef.scalar:
        return
    id = f.id
    if f.t == tdef.vector_subscript_expr:
        var_ref = f.var_ref
        id = var_ref.id
        expr = f.expr
        # if you want strict HIR conformance:
        #   if expr.t != tdef.integer_literal: die('expected integer literal')
        #   idx = match_hir_Literal(expr) or die('bad case')
        # we use the most general expression allowed by SIR:
        idx = match_hir_exprModel(expr) or die("bad case")
    else:
        idx = None
    va = HIR.VarAccess(
        id, f.locus, index=idx, is_nonlocal_ref=getattr(f, "is_nonlocal_ref", False)
    )
    return va


class VarOffset:
    def __init__(self, var_access, literal_offset):
        self.var_access = var_access
        self.literal_offset = literal_offset


def match_VarOffset(f):
    if f.t != tdef.binop and f.tq != tqdef.scalar:
        return
    binop = dfun_binop(f)
    op = binop.op
    if not op in (BinOp.minusOp, BinOp.plusOp):
        return
    cm = binop.getcm()
    if isinstance(cm[0], HIR.VarAccess) and isinstance(cm[1], HIR.Literal):
        (va, lit) = (cm[0], cm[1])
    elif isinstance(cm[1], HIR.VarAccess) and isinstance(cm[0], HIR.Literal):
        (va, lit) = (cm[1], cm[0])
    else:
        return
    if op == BinOp.minusOp:
        lit.value = "-" + lit.value
    return VarOffset(va, lit)


def match_hir_DimensionLevel(f):
    va = match_hir_VarAccess(f)
    if va is not None:
        t = HIR.Type("int", f.locus)
        lit = HIR.Literal(t, "0", f.locus)
    else:
        var_offset = match_VarOffset(f)
        if not var_offset:
            return
        (va, lit) = (var_offset.var_access, var_offset.literal_offset)
    return HIR.DimensionLevel(va, lit, f.locus)


def match_interval_bound(f):
    b = match_hir_Literal(f) or match_hir_DimensionLevel(f) or None
    return b


def gen_zero_interval_bound():
    return gen_Literal_zero_int()


def match_hir_DimensionInterval(f1, f2, order=None):
    b1 = match_interval_bound(f1)
    b2 = match_interval_bound(f2)
    if not (b1 and b2):
        return
    return HIR.DimensionInterval(b1, b2, f1.locus, order)


def gen_single_DimensionInterval(locus):
    b1 = gen_zero_Literal_int(locus)
    b2 = gen_zero_Literal_int(locus)
    return HIR.DimensionInterval(b1, b2, locus)


def gen_single_level_VR(hir_comp, locus):
    hir_interval = gen_single_DimensionInterval(locus)
    return HIR.VerticalRegion([hir_interval], [hir_comp], locus)


def merge_two_computations(comp0, comp1):
    if comp0.t != tdef.compute_on or comp1.t != tdef.compute_on:
        return
    dims0 = comp0._dense_dims
    dims1 = comp1._dense_dims
    if len(dims0) != len(dims1):
        return
    for p in range(len(dims0)):
        if not dims0[p].matches(dims1[p]):
            return
    merged = copy(comp0)
    merged.stmts.extend(comp1.stmts)
    return merged


def merge_computations(comps, locus):
    # simple merge of computations to meet HIR requirements
    if len(comps) <= 1:
        return comps
    new_comps = []
    cur_comp = None
    for p in range(len(comps)):
        if cur_comp is None:
            cur_comp = comps[p]
            continue
        m = merge_two_computations(cur_comp, comps[p])
        if m is not None:
            cur_comp = m
        else:
            new_comps.append(cur_comp)
            cur_comp = None
    if cur_comp:
        new_comps.append(cur_comp)
    return new_comps


def dfun_vertical_region(f):
    # TODO: extend beyond the single interval & single computation case

    # - the following merge process is only valid for a single vertical interval:
    stmts = getattr(f, "stmts", ())
    if not all(s.t == tdef.compute_on for s in stmts):
        error("The vertical_region of the HIR must only contain compute_on constructs.")
    if len(stmts) > 1:
        stmts = merge_computations(stmts, f.locus)

    # TODO: if we still have mult. stmts then we need to split the vertical region
    if len(stmts) != 1 or stmts[0].t != tdef.compute_on:
        # give up
        error(
            "The vertical_region of the HIR must only contain a single compute_on construct."
        )

    add_VRegionState(1)
    args = getattr(f, "args", ())
    if len(args) == 3:
        order = args[2].id
    else:
        order = None
    interval = match_hir_DimensionInterval(args[0], args[1], order) or error(
        "invalid interval"
    )

    stmt = dispatch(stmts[0])
    add_VRegionState(-1)
    return HIR.VerticalRegion([interval], [stmt], f.locus)


def dfun_compute_on(f):
    implicit_VR = not VRegionState
    if implicit_VR:
        add_VRegionState(1)
    add_DomCompState(1)
    assert f.t == tdef.compute_on
    dense_dims = getattr(f, "_dense_dims", ())
    gdims = []
    for d in dense_dims:
        gdims.append(hir_GridDimension(d, f.locus))
    stmts = getattr(f, "stmts", ())
    assert isinstance(stmts, list)
    simple_stmts = simplify_stmts(stmts)
    hir_stmts = match_all(
        match_hir_stmtModel,
        simple_stmts,
        error_msg="Invalid statement within compute_on region.",
    )
    if hir_stmts is None:
        error("Cannot match stmts of compute_on block to HIR stmtModel.")
    bstmt = HIR.BlockStmt(filter_NOP(hir_stmts), f.locus)
    hir_comp = HIR.Computation(gdims, bstmt, f.locus)
    if implicit_VR:
        hir_vr = gen_single_level_VR(hir_comp, f.locus)
        add_DomCompState(-1)
        add_VRegionState(-1)
        return hir_vr
    add_DomCompState(-1)
    if implicit_VR:
        add_VRegionState(-1)
    return hir_comp


def match_hir_VerticalRegion(f):
    if f.t == tdef.vertical_region:
        return dfun_vertical_region(f)
    elif f.t == tdef.compute_on and VRegionState == 0:
        return dfun_compute_on(f)  # will create a VR internally
    return


def dfun_decl_stmt(f):
    vdecls = getattr(f, "var_decls", ())
    assert is_lot(vdecls)
    a = []
    for vd in vdecls:
        h = dfun_var_decl(vd)
        a.append(h)
    return a


def dfun_dot_product(f):
    return match_hir_DotProduct(f) or error("invalid dot_product")


def match_hir_DotProduct(f):
    if f.t != tdef.dot_product:
        return
    args = getattr(f, "args", ())
    assert len(args) == 2
    field_access_pair = dispatch(args) or ICE()
    vector_dims = f._vector_dims
    assert len(vector_dims) == 2
    gdim_pair = (
        hir_GridDimension(vector_dims[0], f.locus),
        hir_GridDimension(vector_dims[1], f.locus),
    )
    return HIR.DotProduct(
        gdim_pair[0], field_access_pair[0], gdim_pair[1], field_access_pair[1], f.locus
    )

    return dfun_dot_product(f)


def dfun_floating_literal(f):
    hir_Type = HIR.Type(uqt2scalar_hir_type(uqt=f._ctp.uqt), f.locus)
    str_value = str(f.v)
    return HIR.Literal(hir_Type, str_value, f.locus)


def dfun_integer_literal(f):
    hir_Type = HIR.Type(uqt2scalar_hir_type(uqt=f._ctp.uqt), f.locus)
    str_value = str(f.v)
    return HIR.Literal(hir_Type, str_value, f.locus)


def dfun_bool_literal(f):
    hir_Type = HIR.Type(uqt2scalar_hir_type(uqt=f._ctp.uqt), f.locus)
    v = f.v
    if v == "true":
        value = "1"
    elif v == "false":
        value = "0"
    else:
        die("unexpected case")
    return HIR.Literal(hir_Type, value, f.locus)


def dfun_gridspace(f):
    # this is a SA support node  - there is no HIR representation
    return


def dfun_nil(f):
    return HIR.NOP(f.locus)


def dispatch_list(alist):
    a = []
    for x in alist:
        y = dispatch(x)
        if isinstance(y, (list, tuple)):
            if y:
                a.extend(y)
        else:
            a.append(y)
    return a


def dispatch(f):
    if isinstance(f, Tv):
        if f.t in dispatcher:
            g_locus_stack.push(f.locus)
            h = dispatcher[f.t](f)
            g_locus_stack.pop()
        else:
            print("bad case: f.t=", tname[f.t])
            die("bad case")
        return h
    elif isinstance(f, (list, tuple)):
        a = dispatch_list(f)
        b = filter_NOP(a)
        return b
    elif f is None:
        return
    else:
        dprint("f=", f)
        die("unexpected case")


def hir_str(h, prefix=""):
    return nstr(h, prefix)


def check_hir(h):
    if isinstance(h, str):
        return True
    if not isinstance(h, Hnode):
        dprint("ERROR: not a Hnode:", h)
        return False
    keys = h.__dict__.keys()
    is_okay = True
    for k in keys:
        a = getattr(h, k)
        if isinstance(a, str):
            pass
        elif isinstance(a, Hnode):
            is_okay = is_okay and check_hir(a)
        elif is_lot(a):
            for x in a:
                is_okay = is_okay and check_hir(x)

    return is_okay


def cleanup_hir(h):
    # remove auxiliary attributes from the tree
    assert h is not None
    f = copy(h)
    if isinstance(f, (str, int, float)):
        return f
    elif isinstance(f, (list, tuple)):
        a = []
        for x in f:
            y = cleanup_hir(x)
            if y is not None:
                a.append(y)
        return a
    else:
        to_be_deleted = []
        for (k, v) in vars(f).items():
            if k.startswith("_") and k != "_kind":
                to_be_deleted.append(k)
                continue
            if isinstance(v, (Locus)):
                continue
            if isinstance(v, (list, tuple, str, int, HIR.Hnode)):
                w = cleanup_hir(v)
                setattr(f, k, w)
                continue
            # We leave /None/ values in the tree -but remove them in the json output
            #if v is None:
            #    to_be_deleted.append(k)
            #    continue
        for k in to_be_deleted:
            delattr(f, k)
        return f


def get_hir(f):
    if require_init:
        init()
    reset()
    g = dispatch(f)
    h = cleanup_hir(g)
    return h


def init_dispatcher():
    global dispatcher
    dispatcher = {}
    for k in list(globals().keys()):
        if not k.startswith("dfun_"):
            continue
        ktail = k[5:]
        t = getattr(tdef, ktail, None)
        if t == None:
            continue
        v = globals()[k]
        if callable(v):
            dispatcher[t] = v


# ---

reset()

init_dispatcher()
