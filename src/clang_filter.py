# Distribution: see LICENSE.txt

#
# Description:
# Extracts information from libclang C++ AST that is required
# for subsequent parsing of DSL constructs.
#

import os
import sys
import cdsl_config

from clang.cindex import Index, Config, CursorKind as CK, TypeKind as TK

# Config.set_library_path(cdsl_config.CDSL_LIBCLANG_DIR)
Config.set_library_file(cdsl_config.CDSL_LIBCLANG_PATH)

import tvs
from tvs import tdef, tqdef, Tv, tv_str, CtypeProps, get_tq, Locus

from parser_config import (
    is_field_type,
    is_scalar_type,
    is_set_type,
    is_map_type,
    is_scope_guard_type,
)
from common import *

g_explore_clang_mode = False

__all__ = ["get_clang_filter_tree"]

g_only_default_filter = False
g_only_exposed_results = True
g_src_file = None
g__keep_alive_stuff = []
g_edsl_extension_src = None


def gen_locus(cx):
    cx_loc = cx.location
    cx_ext = cx.extent
    src_file = g_src_file
    start_pos = SrcCodePos(src_file, cx_ext.start.line, cx_ext.start.column)
    end_pos = SrcCodePos(src_file, cx_ext.end.line, cx_ext.end.column)
    extent = SrcCodeExtent(start_pos, end_pos)
    focus_pos = SrcCodePos(src_file, cx_loc.line, cx_loc.column)
    locus = Locus(focus_pos, extent)
    return locus


def keep_alive(x):
    # helps to follow the advice in package clang/cindex.py
    global g__keep_alive_stuff
    g__keep_alive_stuff.append(x)
    if len(g__keep_alive_stuff) > 100:
        die("Unexpected high number of keep-alive objects.")


class SrcFile:
    def __init__(self, filename):
        self.name = filename
        self.cwd = os.getcwd()
        self.lines = read_file(filename)


def reset():
    global g_src_file
    global g_edsl_context, g_tu_extent, g_current_decl_stmt
    g_src_file = None
    g_current_decl_stmt = None
    g_tu_extent = None
    g_edsl_context = None
    g_edsl_extension_src = None


def dTv(t=tdef.nil, tq=tvs.no_tq, ct=tvs.no_ct, id=tvs.no_id, v=tvs.no_v, cx=None):
    # Tv with default attributes
    return set_defaults(Tv(t, tq, ct, id, v, cx))


def get_id(x):
    if hasattr(x, "spelling"):
        return x.spelling
    else:
        return ""


def get_ctype(x):
    if hasattr(x, "type"):
        if x.type.kind == TK.UNEXPOSED:
            if g_only_exposed_results:
                return ""
            return "TK_UNEXPOSED"
        else:
            return x.type.spelling
    return ""


def strip_singleton(a):
    if not is_lot(a):
        return a
    if len(a) > 1:
        return a
    if len(a) == 1:
        return a[0]
    return a


def is_exposed(x):
    k = x.kind
    if (
        k == CK.UNEXPOSED_DECL
        or k == CK.UNEXPOSED_EXPR
        or k == CK.UNEXPOSED_STMT
        or k == CK.UNEXPOSED_ATTR
    ):
        return False
    return True


def is_unexposed(x):
    return not is_exposed(x)


def get_exposed_children(x):
    if not hasattr(x, "get_children"):
        return []
    a = []
    for c in x.get_children():
        if is_exposed(c):
            a.append(c)
        else:
            b = get_exposed_children(c)
            if len(b) == 1:
                a.append(b[0])
            elif len(b) == 0:
                # c is an unexposed leaf node,
                # happens,  e.g., for c.kind == CursorKind.UNEXPOSED_ATTR
                continue
            else:
                # an unexposed node with multiple exposed children ???
                die("*** Found non-trivial unexposed complexity. ***")
    return a


def get_exposed_arguments(x):
    if not hasattr(x, "get_arguments"):
        return []
    a = []
    for arg in x.get_arguments():
        if is_exposed(arg):
            a.append(arg)
        else:
            eclist = get_exposed_children(x)
            if eclist:
                a.append(strip_singleton(eclist))
    return a


def filter_unary_operator(x):
    assert x.kind == CK.UNARY_OPERATOR
    ctype = get_ctype(x)
    tlist = [t.spelling for t in x.get_tokens()]
    n = len(tlist)
    clist = get_exposed_children(x)

    assert len(clist) == 1
    c0 = clist[0]
    tlist0 = [t.spelling for t in c0.get_tokens()]
    n0 = len(tlist0)
    assert n0 < n
    if tlist0[0] == tlist[0]:
        # operand is at the beginning, operator at the end
        op = "".join(tlist[n0:])
    elif tlist0[n0 - 1] == tlist[n - 1]:
        # operand is at the end, operator at the beginning
        op = "".join(tlist[0 : n - n0])
    else:
        die("unexpected case")
    arg_v0 = dispatch(c0)
    f = dTv(t=tdef.unop, tq=get_tq(ctype), ct=ctype, id=op, cx=x)
    f.args = [arg_v0]
    return f


def filter_binary_operator(x):
    assert x.kind == CK.BINARY_OPERATOR
    ctype = x.type.spelling
    clist = get_exposed_children(x)
    slist = [t.spelling for t in x.get_tokens()]
    slist0 = [t.spelling for t in clist[0].get_tokens()]
    slist1 = [t.spelling for t in clist[1].get_tokens()]
    n = len(slist)
    n0 = len(slist0)
    n1 = len(slist1)
    if n0 >= len(slist):
        t = tdef.binop_unresolved
        print("location=", x.location)
        print("binop_unresolved:n,n0,n1", n, n0, n1)
        reject_cx(x)
    op = slist[n0]
    lhs = dispatch(clist[0])
    rhs = dispatch(clist[1])
    id = op
    args_ct = tvs.common_ct([lhs, rhs])
    binop_tv = dTv(t=tdef.binop, tq=get_tq(ctype), ct=ctype, id=id, cx=x)
    binop_tv.args = [lhs, rhs]
    return binop_tv


def filter_namespace(x):
    global g_edsl_context, g_edsl_extension_src
    assert x.kind == CK.NAMESPACE
    id = x.spelling
    name = id
    if name == "edsl":
        if g_edsl_context is not None:
            reject_cx(x, "Invalid nesting of edsl namespaces.")
        g_edsl_context = name
        a = dispatch_children(x)
        g_edsl_context = None
    elif name == "edsl_extension":
        if g_edsl_context is not None:
            reject_cx(x, "Invalid nesting of edsl namespaces.")
        if g_edsl_extension_src is not None:
            reject_cx(x, "Multiple edsl_extension namespaces not supported yet-")
        g_edsl_extension_src = get_cx_src(x)
        return
    else:
        if g_edsl_context is None:
            return
        a = []
        for c in x.get_children():
            if filter_namespace(c) or filter_function_decl(c):
                a.append(tvs.last_tv)
            else:
                reject_cx(c)
    body_tv = dTv(t=tdef.namespace_body, v=a, cx=x)
    return dTv(t=tdef.namespace, id=id, v=body_tv, cx=x)


def get_sempar(x0):
    if hasattr(x0, "referenced"):
        x = x0.referenced
    else:
        x = x0
    a = []
    if not hasattr(x, "semantic_parent"):
        return []
    p = x.semantic_parent
    while p:
        t = p.spelling
        if t != g_src_file.name:
            a.append(t)
        if not hasattr(x, "semantic_parent"):
            a.reverse()
            return "::".join(a)
        p = p.semantic_parent
    a.reverse()
    return "::".join(a)


def filter_call_expr(x):
    assert x.kind == CK.CALL_EXPR
    id = get_id(x)
    ctype = get_ctype(x)
    tq = get_tq(ctype) or get_tq_from_cx(x)
    rlist = []
    arg_list = get_exposed_children(x)
    if arg_list:
        a = []
        for arg in arg_list:
            if arg.kind in (
                CK.CALL_EXPR,
                CK.DECL_REF_EXPR,
                CK.INTEGER_LITERAL,
                CK.FLOATING_LITERAL,
                CK.CXX_BOOL_LITERAL_EXPR,
                CK.BINARY_OPERATOR,
                CK.MEMBER_REF_EXPR,
                CK.PAREN_EXPR,
                CK.UNARY_OPERATOR,
                CK.CXX_FUNCTIONAL_CAST_EXPR,
                CK.TYPE_REF,
                CK.INIT_LIST_EXPR,
            ):
                y = dispatch(arg)
                if y:
                    a.append(y)
            else:
                reject_cx(x)

        result = dTv(tdef.call_expr, tq, ct=ctype, id=id, v=rlist, cx=x)
        result.args = a
        return result

    # we only go here if there are no args:
    a = []
    clist = get_exposed_children(x)
    if clist:
        die("untested case: no args but children")
    for ec in clist:
        if ec.kind in [CK.DECL_REF_EXPR, CK.MEMBER_REF_EXPR]:
            arg = dispatch(ec)
            a.append(arg)
        else:
            print("unsupported ec.kind=", ec.kind)
            die("need to extent kind selection for argument parser")
    if a:
        a_ct = tvs.common_ct(a)
        other_tv = dTv(t=tdef.other, ct=a_ct, v=strip_singleton(a), cx=x)
        rlist.extend(other_tv)
    f = dTv(tdef.call_expr, tq, ct=ctype, id=id, v=rlist, cx=x)
    f.args = a
    return f


def filter_if_stmt(x):
    assert x.kind == CK.IF_STMT
    tlist = [t.spelling for t in x.get_tokens()]
    clist = get_exposed_children(x)
    a = []
    for c in clist:
        pc = dispatch(c)
        if pc:
            a.append(pc)
        else:
            reject_cx(c)
    return dTv(t=tdef.if_stmt, v=a, cx=x)


def filter_compound_stmt(x):
    assert x.kind == CK.COMPOUND_STMT
    a = []
    accept = 0
    clist = get_exposed_children(x)
    for ec in clist:
        fec = dispatch(ec)
        if fec:
            a.append(fec)
        else:
            reject_cx(ec)
    return dTv(t=tdef.block_stmt, v=a, cx=x)


def filter_function_decl(x):
    assert x.kind == CK.FUNCTION_DECL
    id = get_id(x)
    # we define the type of the function as the libclang result_type
    # the signature (libclang type) is already in the parameters
    rtype = get_result_type(x) or ""
    rlist = []
    a = []
    arg_list = get_exposed_arguments(x)
    for arg in arg_list:
        if arg.kind != CK.PARM_DECL:
            reject_cx(arg)
        parm_decl = dispatch(arg)
        a.append(parm_decl)
    rlist.append(dTv(t=tdef.parameters, v=a, cx=x))
    comp = None
    clist = get_exposed_children(x)
    for c in clist:
        if c.kind == CK.COMPOUND_STMT:
            assert comp == None
            comp = dispatch(c)
        elif c.kind in [CK.TYPE_REF, CK.PARM_DECL]:
            continue
        else:
            reject_cx(c)
    if comp != None:
        rlist.append(comp)
    f = dTv(tdef.function_decl, tq=get_tq(rtype), ct=rtype, id=id, v=rlist, cx=x)
    return f


def get_tq_from_cx(x):
    xtype = getattr(x, "type", None)
    if xtype is None:
        return tqdef.no_tq
    s = xtype.spelling
    if s == "void":
        return tqdef.no_tq
    if s.startswith("EDSL::Field"):
        return tqdef.field
    return tqdef.no_tq


def filter_decl_ref_expr(x):
    assert x.kind == CK.DECL_REF_EXPR
    if x.spelling.startswith("__"):
        return None
    id = get_id(x)
    ct = get_ctype(x)
    # we skip function signatures
    if is_invalid_type(ct):
        return None
    type_decl = dispatch(x.type.get_declaration())
    tq = get_tq(ct) or type_decl.tq
    g = dTv(tdef.decl_ref_expr, tq, ct=ct, id=id, cx=x)
    g.type_decl = type_decl
    return g


def filter_member_ref_expr(x):
    assert x.kind == CK.MEMBER_REF_EXPR
    id = get_id(x)
    ct = get_ctype(x)
    a = get_head_tv_list(x)
    clist = get_exposed_children(x)
    if len(clist) > 1:
        reject_cx(x)
    for c in clist:
        if not c.kind in (
            CK.DECL_REF_EXPR,
            CK.CALL_EXPR,
            CK.PAREN_EXPR,
            CK.CXX_FUNCTIONAL_CAST_EXPR,
            CK.MEMBER_REF_EXPR,
        ):
            reject_cx(x, "Unexpected case in filter_member_ref_expr.")
        y = dispatch(c)
        if y:
            a.append(y)

    return dTv(tdef.member_ref_expr, get_tq(ct), ct=ct, id=id, v=a, cx=x)


def filter_cxx_for_range_stmt(x):
    assert x.kind == CK.CXX_FOR_RANGE_STMT
    clist = get_exposed_children(x)
    a = []
    for c in clist:
        if not c.kind in [
            CK.VAR_DECL,
            CK.DECL_REF_EXPR,
            CK.COMPOUND_STMT,
            CK.MEMBER_REF_EXPR,
            CK.CALL_EXPR,
        ]:
            reject_cx(c)
        y = dispatch(c)
        if y:
            a.append(y)
    f = dTv(t=tdef.range_loop, v=a, cx=x)
    return f


def filter_parm_decl(x):
    assert x.kind == CK.PARM_DECL
    x_ct = get_ctype(x)
    x_id = get_id(x)
    x_type_decl = dispatch(x.type.get_declaration())
    g = dTv(tdef.parm_decl, tq=get_tq(x_ct), ct=x_ct, id=x_id, cx=x)
    g.type_decl = x_type_decl
    return g


def filter_init_list_expr(x):
    assert x.kind == CK.INIT_LIST_EXPR
    ctype = get_ctype(x)
    clist = get_exposed_children(x)
    a = []
    for c in clist:
        y = dispatch(c)
        if y:
            a.append(y)
    f = dTv(tdef.init_list, tq=get_tq(ctype), ct=ctype, v=a, cx=x)
    return f


def filter_array_subscript_expr(x):
    assert x.kind == CK.ARRAY_SUBSCRIPT_EXPR
    ctype = get_ctype(x)
    clist = get_exposed_children(x)
    a = []
    for c in clist:
        y = dispatch(c)
        if y != None:
            a.append(y)
    return dTv(tdef.array_subscript_expr, tq=get_tq(ctype), ct=ctype, v=a, cx=x)


def filter_var_decl(x):
    assert x.kind == CK.VAR_DECL
    id = get_id(x)
    ctype = get_ctype(x)
    clist = get_exposed_children(x)
    have_array_type = False
    if isinstance(ctype, str) and ctype.find("[") >= 0:
        have_array_type = True
    have_init_list = False
    a = []
    type_ref = None
    for c in clist:
        if c.kind == CK.TYPE_REF:
            # we prefer to store this info as attribute
            assert type_ref is None
            type_ref = dispatch(c)
            continue
        if c.kind in (
            CK.CALL_EXPR,
            CK.INTEGER_LITERAL,
            CK.CXX_BOOL_LITERAL_EXPR,
            CK.FLOATING_LITERAL,
            CK.CXX_FUNCTIONAL_CAST_EXPR,
            CK.CSTYLE_CAST_EXPR,
            CK.BINARY_OPERATOR,
            CK.INIT_LIST_EXPR,
            CK.DECL_REF_EXPR,
            CK.ARRAY_SUBSCRIPT_EXPR,
            CK.TYPE_REF,
        ):
            y = dispatch(c)
            if y:
                if y.t == tdef.init_list:
                    have_init_list = True
                a.append(y)
        else:
            reject_cx(c)

    # We always need the init_list for array initialization or something is wrong.
    # Maybe we did not check if the input code compiles.
    if have_array_type and not have_init_list:
        # location of var_decl is not good, we use the location of current_decl_stmt instead
        if g_current_decl_stmt is not None:
            reject_cx(g_current_decl_stmt, "Uninitialized array declaration statement.")
        else:
            reject_cx(x, "Uninitialized array declaration.")
    g = dTv(tdef.var_decl, tq=get_tq(ctype), ct=ctype, id=id, v=a, cx=x)
    if type_ref is not None:
        g.type_ref = type_ref
    return g


def filter_paren_expr(x):
    assert x.kind == CK.PAREN_EXPR
    ct = get_ctype(x)
    tq = get_tq(ct)
    clist = get_exposed_children(x)
    a = []
    for c in clist:
        a.append(dispatch(c))
    if len(a) == 1:
        # peel off paren to simplify parsing stage
        return a[0]

    # remaining cases:
    f = dTv(t=tdef.paren_expr, tq=tq, ct=ct, v=a, cx=x)
    return f


def filter_decl_stmt(x):
    global g_current_decl_stmt
    assert x.kind == CK.DECL_STMT
    if g_current_decl_stmt != None:
        ICE()
    g_current_decl_stmt = x
    clist = get_exposed_children(x)
    stmt = []

    for c in clist:
        if c.kind == CK.VAR_DECL:
            y = dispatch(c)
            stmt.append(dispatch(c))
        elif c.kind == CK.FUNCTION_DECL:
            reject_cx(c, "Expected variable declaration - found function declaration")
        else:
            reject_cx(c)
    g_current_decl_stmt = None
    if not stmt:
        return dTv(t=tdef.unexpected, v=get_cx_src(x), cx=x)
    return dTv(t=tdef.decl_stmt, v=stmt, cx=x)


def filter_return_stmt(x):
    assert x.kind == CK.RETURN_STMT
    eca = get_exposed_children(x)
    a = []
    for c in eca:
        y = dispatch(c)
        if y:
            a.append(y)
    return dTv(tdef.return_stmt, v=a, cx=x)


def filter_bool_literal(x):
    assert x.kind == CK.CXX_BOOL_LITERAL_EXPR
    ct = get_ctype(x)
    tlist = [t.spelling for t in x.get_tokens()]
    # see comment in filter_integer_literal
    # if len(tlist) != 1: reject_cx(x)
    val = tlist[0]
    tq = tqdef.scalar
    y = dTv(tdef.bool_literal, tq=tq, ct=ct, v=val, cx=x)
    return y


def filter_integer_literal(x):
    assert x.kind == CK.INTEGER_LITERAL
    ct = get_ctype(x)
    tlist = [t.spelling for t in x.get_tokens()]
    # It seems that there can be many more tokens if the RHS is the
    # result of a cpp expansion. We only need the first token.
    # assert(len(tlist) == 1)
    val = tlist[0]
    tq = get_tq(ct)
    y = dTv(tdef.integer_literal, tq=tq, ct=ct, v=val, cx=x)
    return y


def filter_floating_literal(x):
    assert x.kind == CK.FLOATING_LITERAL
    ct = get_ctype(x)
    tlist = [t.spelling for t in x.get_tokens()]
    # see comment in filter_integer_literal
    # assert(len(tlist) == 1)
    val = tlist[0]
    tq = get_tq(ct)
    y = dTv(tdef.floating_literal, tq=tq, ct=ct, v=val, cx=x)
    return y


def filter_translation_unit(x):
    global g_tu_extent
    assert x.kind == CK.TRANSLATION_UNIT
    g_tu_extent = x.extent
    v = dispatch_children(x)
    f = dTv(tdef.translation_unit, id=get_id(x), v=v, cx=x)
    f.parse_cwd = g_src_file.cwd
    f.parse_filename = g_src_file.name
    f.edsl_extension_src = g_edsl_extension_src
    return f


def is_invalid_type(v):
    if ("(" in v) or (")" in v):
        return True
    return False


def filter_common_cast_expr(x):
    assert x.kind in (CK.CXX_FUNCTIONAL_CAST_EXPR, CK.CSTYLE_CAST_EXPR)
    ct = get_ctype(x)
    if not ct:
        reject_cx(x)
    clist = get_exposed_children(x)
    expr = None
    for c in clist:
        if c.kind == CK.TYPE_REF:
            continue
        if c.kind == CK.CALL_EXPR:
            if expr:
                reject_cx(c)
            else:
                expr = dispatch(c)
        else:
            reject_cx(c)
    if not is_field_type(ct=ct):
        reject_cx(x, "filter_common_cast_expr: expected field type")
    tq = get_tq(ct)
    expr = dTv(t=tdef.field_cast_expr, tq=tq, ct=ct, v=[expr], cx=x)
    return expr


def filter_cxx_functional_cast_expr(x):
    assert x.kind == CK.CXX_FUNCTIONAL_CAST_EXPR
    return filter_common_cast_expr(x)


def filter_cstyle_cast_expr(x):
    assert x.kind == CK.CSTYLE_CAST_EXPR
    return filter_common_cast_expr(x)


def filter_template_ref(x):
    x_id = get_id(x)
    return dTv(t=tdef.template_ref, id=x_id, cx=x)


def filter_typedef_decl(x):
    ct = get_ctype(x)
    id = get_id(x)
    clist = get_exposed_children(x)
    v = [dispatch(c) for c in clist]
    return dTv(tdef.typedef_decl, get_tq(ct), ct=ct, id=id, v=v, cx=x)


def filter_type_ref(x):
    ct = get_ctype(x)
    id = get_id(x)
    return dTv(tdef.type_ref, get_tq(ct), ct=ct, id=id, cx=x)


def filter_class_decl(x):
    x_ct = get_ctype(x)
    x_id = get_id(x)
    return dTv(t=tdef.class_decl, ct=x_ct, id=x_id, cx=x)


def filter_struct_decl(x):
    x_ct = get_ctype(x)
    x_id = get_id(x)
    return dTv(t=tdef.struct_decl, ct=x_ct, id=x_id, cx=x)


def filter_type_alias_decl(x):
    assert x.kind == CK.TYPE_ALIAS_DECL
    x_ct = get_ctype(x)
    x_id = get_id(x)
    clist = get_exposed_children(x)
    v = [dispatch(c) for c in clist]
    return dTv(tdef.type_alias_decl, tqdef.other, ct=x_ct, id=x_id, v=v, cx=x)


def dispatch_children(x):
    a = []
    for c in x.get_children():
        y = dispatch(c)
        if y:
            a.append(y)
    return a


def get_result_type(x):
    y = getattr(x, "result_type", None)
    if not y:
        return
    z = getattr(y, "spelling", None)
    if not z:
        return
    return z


def get_head_tv_list(x):
    a = []

    if hasattr(x, "result_type"):
        y = x.result_type
        if y:
            if hasattr(y, "spelling"):
                z = y.spelling
                if z:
                    a.append(Tv(tdef.result_type, v=z))
    return a


def set_defaults(f):
    ct = getattr(f, "ct", "")
    if ct:
        f._ctp = CtypeProps(ct)
        if not g_explore_clang_mode:
            if getattr(f._ctp, "is_invalid", False):
                reject_cx(getattr(f, "cx", None), "invalid type")
        if f.t in (tdef.bool_literal, tdef.integer_literal, tdef.floating_literal):
            f._ctp.is_const = True
        if f.tq == tqdef.no_tq:
            f.tq = get_tq(ct)
    if f.id and f.ct:
        if "EDSL::" + f.id == f.ct:
            f.is_EDSL_ctor = True

    if not hasattr(f, "cx"):
        return f
    x = f.cx
    f.locus = gen_locus(x)
    if hasattr(x, "referenced"):
        fqnp = get_sempar(x)
        if fqnp:
            f.fqnp = fqnp
            if fqnp == "EDSL" or fqnp.startswith("EDSL::"):
                f.fqnp_sw_EDSL = True
        return f

    if hasattr(x, "get_arguments"):
        a = []
        for arg in x.get_arguments():
            y = dispatch(arg)
            if y:
                a.append(y)
        if a:
            f.cx_args = a
    type_ref = getattr(x, "type_ref", None)
    if f.tq == tqdef.no_tq and type_ref is not None:
        f.tq = type_ref.tq


def default_filter(x):
    if is_unexposed(x) and g_only_exposed_results:
        # we only look at children:
        a = dispatch_children(x)
        if len(a) == 1:
            return a[0]
        if len(a) == 0:
            return []
        die("unexplored case")  # we expect single link casting only

    v = []
    xlocus = gen_locus(x)
    if hasattr(x, "result_type"):
        y = x.result_type
        if y:
            z = y.kind
            if z != TK.INVALID:
                v.append(Tv(tdef.result_type, v=z, locus=xlocus))

    if hasattr(x, "get_tokens"):
        tlist = [t.spelling for t in x.get_tokens()]
        if tlist:
            v.append(Tv(tdef.tokens, v=tlist, locus=xlocus))

    if hasattr(x, "get_arguments"):
        a = []
        for arg in x.get_arguments():
            y = dispatch(arg)
            if y:
                a.append(y)
        if a:
            v.append(Tv(tdef.from_get_arguments, v=a, locus=xlocus))

    if hasattr(x, "get_children"):
        a = dispatch_children(x)
        if a:
            v.append(a)
    ct = get_ctype(x)
    id = get_id(x)
    return Tv(tdef.CKV, get_tq(ct), ct=ct, id=id, v=[x.kind, v], locus=xlocus)


def reject_cx(x, msg="", cx_src=None):
    p = "***"
    if x:
        print(
            p,
            "Source code rejected (line=%s, col=%s)."
            % (x.location.line, x.location.column),
            file=sys.stderr,
        )
        if cx_src == None:
            cx_src = get_cx_src(x)
        if msg and cx_src:
            print(p, msg, ":", '"' + cx_src + '"', file=sys.stderr)
        elif msg:
            print(p, msg, file=sys.stderr)
        elif cx_src:
            print(p, '"' + cx_src + '"', file=sys.stderr)
        error_exit(1)
    else:
        die("unexpected case")


def dispatch(x):
    if g_edsl_context is None:
        if x.kind == CK.TRANSLATION_UNIT:
            return filter_translation_unit(x)
        if x.kind == CK.NAMESPACE:
            return filter_namespace(x)
        return
    if x.kind in filter and not g_only_default_filter:
        return filter[x.kind](x)
    return default_filter(x)


def read_file(filename):
    with open(filename, mode="r") as f:
        src = f.readlines()
    return src


def get_src_line(locus):
    y = locus.line - 1
    line = g_src_file.lines[y]
    return line


def get_cx_src(x):
    e = x.extent
    if str(e.start.file) != str(g_tu_extent.start.file):
        print("tu_extent.start.file=", "[" + str(g_tu_extent.start.file) + "]")
        print("        e.start.file=", "[" + str(e.start.file) + "]")
        return "(out of file source)"
    y0 = e.start.line - 1
    x0 = e.start.column - 1
    y1 = e.end.line - 1
    x1 = e.end.column - 1
    if y0 == y1:
        s = g_src_file.lines[y0][x0:x1]
        return s
    a = [g_src_file.lines[y0][x0:]]
    for i in range(y0 + 1, y1):
        a.append(g_src_file.lines[i])
    a.append(g_src_file.lines[y1][:x1])
    s = "".join(a)
    return s


def get_src(locus):
    e = locus.extent
    if str(e.start.file) != str(g_tu_extent.start.file):
        print("tu_extent.start.file=", "[" + str(g_tu_extent.start.file) + "]")
        print("        e.start.file=", "[" + str(e.start.file) + "]")
        die("get_cx_src failed")
    y0 = e.start.line - 1
    x0 = e.start.column - 1
    y1 = e.end.line - 1
    x1 = e.end.column - 1
    if y0 == y1:
        s = g_src_file.lines[y0][x0:x1]
        return s
    a = [g_src_file.lines[y0][x0:]]
    for i in range(y0 + 1, y1):
        a.append(g_src_file.lines[i])
    a.append(g_src_file.lines[y1][:x1])
    s = "".join(a)
    return s


def get_tree(filename, include_dirs, macro_defs=None, nopath=None):
    global g_src_file
    reset()
    if not macro_defs:
        macro_defs = []
    assert filename.endswith(".ast")
    index = Index.create()
    tu = index.read(filename)
    tuc = tu.cursor
    g_src_file = SrcFile(tuc.spelling)
    keep_alive(index)
    e2root = dispatch(tuc)
    if nopath:
        e2root.id = os.path.basename(e2root.id)
        e2root.parse_cwd = "."
        e2root.parse_filename = os.path.basename(e2root.parse_filename)
    return e2root


def get_clang_filter_tree(filename, include_dirs, macro_defs=None):
    return get_tree(filename, include_dirs, macro_defs=None)


filter = {
    CK.ARRAY_SUBSCRIPT_EXPR: filter_array_subscript_expr,
    CK.BINARY_OPERATOR: filter_binary_operator,
    CK.CALL_EXPR: filter_call_expr,
    CK.COMPOUND_STMT: filter_compound_stmt,
    CK.CXX_FOR_RANGE_STMT: filter_cxx_for_range_stmt,
    CK.CXX_FUNCTIONAL_CAST_EXPR: filter_cxx_functional_cast_expr,
    CK.CLASS_DECL: filter_class_decl,
    CK.CSTYLE_CAST_EXPR: filter_cstyle_cast_expr,
    CK.DECL_REF_EXPR: filter_decl_ref_expr,
    CK.DECL_STMT: filter_decl_stmt,
    CK.IF_STMT: filter_if_stmt,
    CK.INIT_LIST_EXPR: filter_init_list_expr,
    CK.FUNCTION_DECL: filter_function_decl,
    CK.MEMBER_REF_EXPR: filter_member_ref_expr,
    CK.NAMESPACE: filter_namespace,
    CK.PAREN_EXPR: filter_paren_expr,
    CK.PARM_DECL: filter_parm_decl,
    CK.RETURN_STMT: filter_return_stmt,
    CK.STRUCT_DECL: filter_struct_decl,
    CK.TRANSLATION_UNIT: filter_translation_unit,
    CK.TEMPLATE_REF: filter_template_ref,
    CK.TYPE_ALIAS_DECL: filter_type_alias_decl,
    CK.TYPEDEF_DECL: filter_typedef_decl,
    CK.TYPE_REF: filter_type_ref,
    CK.UNARY_OPERATOR: filter_unary_operator,
    CK.VAR_DECL: filter_var_decl,
    # literals:
    CK.CXX_BOOL_LITERAL_EXPR: filter_bool_literal,
    CK.INTEGER_LITERAL: filter_integer_literal,
    CK.FLOATING_LITERAL: filter_floating_literal,
}
