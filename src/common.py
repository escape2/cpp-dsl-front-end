# Distribution: see LICENSE.txt

import sys
import os
from inspect import currentframe, getframeinfo
from collections import OrderedDict
import json
from decimal import Decimal
import os

__all__ = [
    "CDSL_DEBUG_DEV",
    "die",
    "DispFormGen",
    "Display_form",
    "disp",
    "disp_str",
    "error",
    "expect",
    "g_cdsl_dir",
    "general_error",
    "get_cdsl_dir",
    "g_locus_stack",
    "ICE",
    "is_invalid",
    "is_lot",
    "Locus",
    "node_error",
    "reject",
    "SrcCodeExtent",
    "SrcCodePos",
    "str2float",
    "str2int",
    "user_error",
    "warn",
]


CDSL_DEBUG_DEV = not (os.environ.get("CDSL_DEBUG_DEV") or "").lower() in (
    "",
    "0",
    "false",
    "disabled",
)

if CDSL_DEBUG_DEV:
    from pdb import set_trace

g_cdsl_dir = None


def get_cdsl_dir():
    global g_cdsl_dir
    if g_cdsl_dir is not None:
        return g_cdsl_dir
    d = os.environ.get("CDSL_ROOT") or os.environ.get("DSL_FRONTEND_DIR")
    if not d:
        d = os.path.dirname(os.path.realpath(__file__ + "/.."))
    cdsl_path = d + "/bin/cdsl"
    os.path.isfile(cdsl_path) or die(cdsl_path + " does not exist.")
    g_cdsl_dir = d
    return d


class LocusStack:
    def __init__(self):
        self.reset()

    def push(self, locus):
        self.stack.append(locus)

    def pop(self):
        assert self.stack
        self.stack.pop()

    def top(self):
        if not self.stack:
            return None
        return self.stack[-1]

    def reset(self):
        self.stack = []


class SrcCodePos:
    def __init__(self, src_file, line, column):
        self.file = src_file
        self.line = line
        self.column = column

    def to_dict(self):
        return OrderedDict(
            [
                ("_encoded_class_", self.__class__.__name__),
                # ('file', self.file.name), file entry is too redundant
                ("line", self.line),
                ("column", self.column),
            ]
        )


class SrcCodeExtent:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def to_dict(self):
        return OrderedDict(
            [
                ("_encoded_class_", self.__class__.__name__),
                ("start", self.start.to_dict()),
                ("end", self.end.to_dict()),
            ]
        )


class Locus:
    def __init__(self, pos, extent):
        self.pos = pos
        self.extent = extent

    def get_src_coords(self):
        pos = self.pos
        return (pos.file.name, pos.line, pos.column)

    def to_dict(self):
        return OrderedDict(
            [
                ("_encoded_class_", "Locus"),
                ("pos", self.pos.to_dict()),
                ("extend", self.extent.to_dict()),
            ]
        )


def reject(msg=None, locus=None):
    error(msg, locus, reject=True)
    # if not locus: locus = g_locus_stack.top()
    # if not locus:
    #     general_error(locus, 'expected: if not locus: locus = g_locus_stack.top()'
    # pos = locus.pos
    # if pos:
    #     p = '***'
    #     print(p,"Error in line=%s, col=%s." % (pos.line,pos.column), file=sys.stderr)
    #     if msg: print(p,msg,file=sys.stderr)
    #     print(p,'Rejected source code:', file=sys.stderr)
    #     print(get_src(locus), file=sys.stderr)
    # error_exit(1)


def expect(cond, error_msg=None):
    if cond:
        return
    error("Expected: " + error_msg)


def error_exit(errn):
    if CDSL_DEBUG_DEV:
        set_trace()
    exit(errn)


def die(reason):
    if CDSL_DEBUG_DEV:
        set_trace()
    raise Exception(reason)

def user_error(*reasons):
    general_error(*reasons)

def warn(reason):
    print("Warning:", reason, file=sys.stderr)


def get_src(locus):
    e = locus.extent
    start = e.start
    end = e.end
    src_file = start.file
    y0 = start.line - 1
    x0 = start.column - 1
    y1 = end.line - 1
    x1 = end.column - 1
    if y0 == y1:
        s = src_file.lines[y0][x0:x1]
        return s
    a = [src_file.lines[y0][x0:]]
    for i in range(y0 + 1, y1):
        a.append(src_file.lines[i])
    a.append(src_file.lines[y1][:x1])
    s = "".join(a)
    return s


def error(reason_list_or_str, locus=None, reject=False):
    if is_lot(reason_list_or_str):
        reason = "\n".join(reason_list_or_str)
    else:
        reason = reason_list_or_str
    if not locus:
        locus = g_locus_stack.top()
    p = "***"
    if locus:
        try:
            (filename, line, column) = locus.get_src_coords()
        except:
            if CDSL_DEBUG_DEV:
                set_trace()
            else:
                ICE()
        print(
            p,
            "Error in file %s, line %s, column %s):" % (filename, line, column),
            file=sys.stderr,
        )
        if reject:
            print(p, "Rejected source code:", file=sys.stderr)
        print(get_src(locus), file=sys.stderr)
    else:
        print("(unspecified source code location)")
    if reason:
        print(p, reason, file=sys.stderr)
    error_exit(1)


def node_error(f, msg):
    locus = getattr(f, "locus", None)
    if not locus:
        die("f has not location data")
    reject(msg)


def general_error(*reasons):
    print("Error:", *reasons, file=sys.stderr)
    error_exit(1)


def tstop(frameinfo):
    print("tstop in file", frameinfo.filename, "at line", frameinfo.lineno)
    error_exit(1)


def is_lot(x):
    if isinstance(x, (list, tuple)):
        return True
    return False


def dprint(*args):
    print("dprint:", *args)


def str2int(s):
    if not isinstance(s, str):
        die("bad case")
    try:
        i = int(s)
        return i
    except ValueError:
        return


def str2float(s):
    if not isinstance(s, str):
        die("bad case")
    try:
        f = float(Decimal(s))
        return f
    except:
        return


def ICE(msg=None):
    if msg:
        die("Internal error: " + msg)
    else:
        die("Internal error")


def proposed_str(s):
    return "proposed_" + s


def debug():
    set_trace()


def invalidate(obj):
    obj.is_invalid = True


def is_invalid(obj):
    return getattr(obj, "is_invalid", False)


class DispFormGen:
    def gen_display_form(self):
        return Display_form(self)


class Display_form:
    obj_alias = {}
    type_counter = {}

    def __init__(self, obj, first=None, drop=None, only=None):
        drop = self.aux_arg2set(drop)
        only = self.aux_arg2set(only)
        (obj_is_already_known, obj_alias) = self.gen_obj_alias(obj)
        data = OrderedDict()
        if obj_is_already_known:
            data["#"] = "$" + obj_alias
            self.data = data
            return
        data["#"] = obj_alias
        if first is None:
            first = ()
        for (k, v) in first:
            if k in drop:
                continue
            if only and not (k in only):
                continue
            data[k] = v
        for k, v in vars(obj).items():
            if k in data or k in drop or v is None:
                continue
            if only and not (k in only):
                continue
            val = self.fetch_display_form(v)
            data[k] = val
        self.data = data

    @classmethod
    def aux_arg2set(cls, arg):
        if arg is None:
            return ()
        elif isinstance(arg, (list, tuple, set)):
            return set(arg)
        else:
            return set([arg])

    def str(self):
        simple_form = self.gen_simple_form(self)
        s = self.str2(simple_form)
        return s

    @classmethod
    def gen_obj_alias(cls, obj):
        """ returns (object_is_already_known, object_alias) """
        alias = cls.obj_alias.get(obj)
        if alias is not None:
            return (True, alias)
        type_name = type(obj).__name__
        type_count = cls.type_counter.get(type_name)
        if type_count is None:
            type_count = 0
        else:
            type_count += 1
        cls.type_counter[type_name] = type_count
        alias = type_name + "_" + str(type_count)
        cls.obj_alias[obj] = alias
        return (False, alias)

    @classmethod
    def fetch_display_form(cls, v):
        if isinstance(v, DispFormGen):
            alias = cls.obj_alias.get(v)
            if alias is not None:
                return "$" + alias
            return v.gen_display_form()
        elif isinstance(v, list):
            fetch = cls.fetch_display_form
            return [fetch(x) for x in v]
        elif isinstance(v, str):
            return v
        else:
            return v

    @classmethod
    def str2(cls, v, prefix=""):
        s = ""
        pfi = "    "
        cls_str2 = cls.str2
        if isinstance(v, Display_form):
            return cls_str2(v.data)
        elif isinstance(v, dict):
            if len(v) == 0:
                return "{}"
            elif len(v) == 1:
                (k, x) = list(v.items())[0]
                y = cls_str2(x, prefix + pfi)
                return str(k) + ": " + y
            else:
                s = ""
                a = []
                for (k, x) in v.items():
                    y = cls_str2(x, prefix + "|" + pfi)
                    if y and y[-1] == "\n":
                        y = y[:-1]
                    a.append(str(k) + ": " + y)
                s = "\n" + prefix + "|" + ("\n" + prefix + "|").join(a)
            return s
        elif isinstance(v, (list, tuple)):
            if len(v) == 0:
                return "[]"
            elif len(v) == 1:
                return cls_str2(v[0], prefix + " ")
            else:
                s = ""
                a = []
                for x in v:
                    y = cls_str2(x, prefix + pfi)
                    if y[0] != "\n":
                        y = "\n" + prefix + y
                    if y[-1] == "\n":
                        y = y[:-1]
                    a.append(y)
                s += "".join(a)
                return s
        else:
            return str(v)

    @classmethod
    def gen_simple_form(cls, obj):
        if isinstance(obj, Display_form):
            return cls.gen_simple_form(obj.data)
        elif isinstance(obj, dict):
            result = OrderedDict()
            for k, v in obj.items():
                # disable next condition if you want see False values
                if isinstance(v, (bool, str, list, tuple)) and not v:
                    continue
                result[k] = cls.gen_simple_form(v)
            return result
        elif isinstance(obj, list):
            return [cls.gen_simple_form(x) for x in obj]
        elif isinstance(obj, tuple):
            return tuple(cls.gen_simple_form(list(obj)))
        else:
            return obj

    @classmethod
    def reset(cls):
        cls.obj_alias = {}
        cls.type_counter = {}


def disp_str(obj, reset=True):
    if reset:
        Display_form.reset()
    if isinstance(obj, DispFormGen):
        dform = obj.gen_display_form()
        return dform.str()
    elif isinstance(obj, (list, tuple)):
        return "\n".join([disp_str(x, reset=False) for x in obj])
    else:
        return str(obj)


def disp(obj, reset=True):
    s = disp_str(obj, reset)
    print(s)


def json_str(f):
    return json.dumps(f, indent=4)


def json_print(f):
    print(json.dumps(f, indent=4))


def loc_err(msg):
    locus = g_locus_stack.top()
    if not locus:
        die("unexpected empty locus_stack")
    reject(locus, msg)


# global variables:

g_locus_stack = LocusStack()
