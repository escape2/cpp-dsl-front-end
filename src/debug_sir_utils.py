#!/usr/bin/env python3
# Distribution: see LICENSE.txt

import inspect
from debug_sir import Terminal, Fcall, GlobalVariableMap

def sir_utils_call(fun_name,  **kwargs):
    return Fcall("sir_utils." + fun_name, **kwargs)

def filter_defaults(d, **kwargs):
    fd = {}
    for k,v in d:
        if k in kwargs and kwargs[k] == v:
            continue
        fd[k] = v
    return fd

def make_sir(**kwargs):
    functions = kwargs.get("functions")
    if functions is None:
        functions = []
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_field_dimensions_cartesian(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_field(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_interval(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_field_access_expr(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_literal_access_expr(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_assignment_stmt(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_ast(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_block_stmt(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_expr_stmt(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_if_stmt(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_var_access_expr(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_vertical_region_decl_stmt(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_stencil(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_field_dimensions_unstructured(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_field_dimensions_vertical(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_vertical_field(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_binary_operator(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_reduction_over_neighbor_expr(**kwargs):
    #todo: check # kwargs = filter_defaults(kwargs, weights=None, include_center=False, offsets=None)
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_type(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_var_decl_stmt(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_unary_operator(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_loop_stmt(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_unstructured_field_access_expr(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_unstructured_offset(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

def make_fun_call_expr(**kwargs):
    return sir_utils_call(inspect.currentframe().f_code.co_name, **kwargs)

