#!/usr/bin/env python3
# Distribution: see LICENSE.txt

import sys
import re
import os
from common import *
import argparse

g_identifier_re = re.compile(r"^([\w:]+)$")

g_gridtools_dawn_halo_extent = None
g_c_dawn_storage_model = None
g_c_name_mangling_suffix = None

g_f2c_prefix = "f2c_"
g_f2c_c_suffix = "_c"
g_f2c_f_suffix = "_f"


def update_backend_specs(storage_model, halo_size):
    global g_gridtools_dawn_halo_extent, g_c_dawn_storage_model, g_c_name_mangling_suffix
    g_c_dawn_storage_model = storage_model.upper()
    g_gridtools_dawn_halo_extent = halo_size
    g_c_name_mangling_suffix = (
        "_" + g_c_dawn_storage_model + str(g_gridtools_dawn_halo_extent)
    )

# default specs:
update_backend_specs("GTMC", 3)


def mangle_name(func_name):
    return "CPP_NAME_MANGLING(" + func_name + ")"

def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-dir",
        default=".",
        help="new output file scheme",
        action="store",
    )

    parser.add_argument(
        "-halo_size",
        help="max halo size",
        default="3",
        action="store",
    )

    parser.add_argument(
        "-dawn_backend",
        help="one fo {mc, cuda}",
        default="mc",
        action="store",
    )
    return parser.parse_args()


def reset_iso_syms():
    global g_iso_syms
    g_iso_syms = {}


def register_iso_sym(sym):
    global g_iso_syms
    g_iso_syms[sym] = 1


def match_identifier(f):
    return bool(g_identifier_re.match(f))


class TypeSpec(DispFormGen):
    def __init__(
        self, name, is_ptr=False, array_spec=None, is_const=False, is_optional=False
    ):
        assert match_identifier(name)
        self.name = name
        self.has_f_type = name in ("char", "int", "double")
        assert is_ptr == bool(is_ptr)
        self.is_ptr = is_ptr
        self.array_spec = array_spec or ()
        assert is_const == bool(is_const)
        self.is_const = is_const
        assert is_optional == bool(is_optional)
        self.is_optional = is_optional
        if self.is_optional:
            assert self.is_ptr

    def c_str(self):
        const_q = " const" if self.is_const else ""
        return self.name + const_q + ("*" if self.is_ptr else "")

    def f_type(self):
        self.is_iso_c_ptr_type = False
        if self.is_ptr:
            if self.is_optional:
                register_iso_sym("c_ptr")
                self.is_iso_c_ptr_type = True
                return "TYPE(c_ptr)"
            if self.name == "char":
                register_iso_sym("c_char")
                return "CHARACTER(len=1,kind=c_char), DIMENSION(*)"
            elif self.name == "double":
                register_iso_sym("c_double")
                return "REAL(c_double), DIMENSION(*)"
            elif self.name == "int":
                register_iso_sym("c_int")
                return "INTEGER(c_int), DIMENSION(*)"
            else:
                assert not self.has_f_type
                register_iso_sym("c_ptr")
                self.is_iso_c_ptr_type = True
                return "TYPE(c_ptr)"
        else:
            if self.name == "int":
                register_iso_sym("c_int")
                return "INTEGER(c_int)"
            if self.name == "void":
                return ""
            else:
                die("todo")


class BFLine(DispFormGen):
    max_line_len = 90 - 2

    def __init__(self, line=""):
        self.line = line
        self.cur_len = len(line)
        assert self.cur_len <= self.max_line_len
        if line.find("&") >= 0:
            self.nobreak = True
        else:
            self.nobreak = False
    def add(self, s):
        if s.find("&") >= 0:
            self.nobreak = True
        slen = len(s)
        if (self.cur_len + slen <= self.max_line_len) or self.nobreak:
            self.line += s
            self.cur_len += slen
        else:
            t = " & " + s
            tlen = len(t)
            assert tlen <= self.max_line_len
            self.line += " &\n" + t
            self.cur_len = tlen
        return self

    def madd(self, ms):
        for s in ms:
            self.add(s)
        return self

    def get_line(self):
        return self.line


class FLine(DispFormGen):
    def __init__(self, elms):
        self.elms = []
        self.add(elms)

    def add(self, a):
        if isinstance(a, str):
            self.elms.append(a)
        elif isinstance(a, list):
            assert all(isinstance(x, str) for x in a)
            self.elms.extend(a)
        else:
            die("bad case")

    def str(self):
        return "".join(self.elms)


class FunArg(DispFormGen):
    def __init__(self, type_spec, name):
        self.type_spec = type_spec
        self.name = name

    def c_str(self):
        aspec = "".join("[" + str(x) + "]" for x in self.type_spec.array_spec)
        return self.type_spec.c_str() + " " + self.name + aspec

    def f_spec(self):
        ts = self.type_spec
        fspec = FLine(ts.f_type())
        aspec = ",".join(str(x) for x in self.type_spec.array_spec[::-1])
        aspec = "(" + aspec + ")" if aspec else ""
        intent_in = False
        value = False
        if ts.is_iso_c_ptr_type:
            value = True
        elif ts.has_f_type:
            if ts.is_ptr or ts.array_spec:
                intent_in = True
            else:
                value = True
        elif ts.is_ptr:
            value = True
        elif ts.is_const:
            value = True
        if value:
            intent_in = False
        if intent_in:
            fspec.add(", INTENT(in)")
        if value:
            fspec.add(", VALUE")
        fspec.add([" :: ", self.name + aspec])
        return fspec


class FunDecl(DispFormGen):
    def __init__(self, return_type, name, args, mangle=False):
        assert isinstance(return_type, TypeSpec)
        self.return_type = return_type
        assert isinstance(name, str)
        self.name = name
        self.f2c_c_name = g_f2c_prefix + name + g_f2c_c_suffix
        self.f2c_f_name = g_f2c_prefix + name + g_f2c_f_suffix
        assert isinstance(args, list)
        assert all(isinstance(x, FunArg) for x in args)
        self.args = args
        self.mangle = mangle
        # self.cooked_c_name = mangle_name(self.f2c_c_name) if mangle else self.f2c_c_name

    def cooked_c_name(self):
        return mangle_name(self.f2c_c_name) if self.mangle else self.f2c_c_name

    def c_str(self):
        args_str = ", ".join([arg.c_str() for arg in self.args])
        return (
            self.return_type.c_str()
            + " "
            + self.cooked_c_name()
            + "("
            + args_str
            + ");"
        )


def serialize(a, level=0):
    if isinstance(a, list):
        b = []
        for x in a:
            y = serialize(x, level + 1)
            if isinstance(y, list):
                b.extend(y)
            else:
                b.append(y)
        return b
    elif isinstance(a, FLine):
        if not a.elms:
            return []
        prefix = "  " * (level - 1)
        return BFLine().madd([prefix] + a.elms).get_line()
    else:
        die("bad case")


def gen_c_decl(fun_decl):
    assert isinstance(fun_decl, FunDecl)
    s = fun_decl.c_str()
    return s


def gen_f_itf(fun_decl):
    assert isinstance(fun_decl, FunDecl)
    reset_iso_syms()
    c_rtype = fun_decl.return_type
    f2c_c_name = fun_decl.f2c_c_name
    f2c_f_name = fun_decl.f2c_f_name
    cooked_c_name = fun_decl.cooked_c_name()
    f_rtype = c_rtype.f_type()
    args = ", ##".join(x.name for x in fun_decl.args).split("##")
    args_specs = [x.f_spec() for x in fun_decl.args]
    iso_syms = g_iso_syms.keys()
    if iso_syms:
        iso_list = ", ##".join(x for x in iso_syms).split("##")
        iso_use = FLine(["USE iso_c_binding, ONLY: "] + iso_list)
    else:
        iso_use = ""
    bind_list = ["&\n&BIND(c,name=\"&\n", "&"+cooked_c_name+"\"", ")"]

    if f_rtype:
        itf = [
            FLine([f_rtype, " FUNCTION ", f2c_f_name, "("] + args + [") "] + bind_list),
            [
                iso_use if iso_use else [],
                FLine("IMPLICIT NONE"),
                *args_specs,
            ],
            FLine(["END FUNCTION ", f2c_f_name]),
        ]
    else:
        itf = [
            FLine(["SUBROUTINE ", f2c_f_name, "("] + args + [") "] + bind_list),
            [
                iso_use if iso_use else [],
                FLine("IMPLICIT NONE"),
                *args_specs,
            ],
            FLine(["END SUBROUTINE ", f2c_f_name]),
        ]

    s = "\n".join(serialize(itf))
    return s


def proc_C(decls):
    for decl in decls:
        c_decl = gen_c_decl(decl)
        print(c_decl, file=g_ofile)


def gen_mo_head(mo_name):
    print(
        "\n".join(
            [
                "MODULE " + mo_name,
                "USE iso_c_binding, ONLY : c_int, c_ptr",
                "IMPLICIT NONE",
                "PUBLIC",
                "INTERFACE",
                "",
            ]
        ),
        file=g_ofile,
    )


def gen_mo_tail(mo_name):
    print(
        "\n".join(
            [
                "END INTERFACE",
                "END MODULE " + mo_name,
            ]
        ),
        file=g_ofile,
    )


def proc_F(decls):
    for decl in decls:
        f_itf = gen_f_itf(decl)
        print(f_itf, file=g_ofile)
        print(file=g_ofile)

def proc_decls(odir, decls):
    global g_ofile
    assert os.path.isdir(odir)
    ofile_name = "f2c_dawn_struct_general.hpp"
    ofile_path = "/".join([odir, "f2c_dawn_struct_general.hpp"])
    with open(ofile_path, "w") as g_ofile:
        proc_C(unmangled_decls)
    ofile_name = "f2c_dawn_struct.hpp"
    ofile_path = "/".join([odir, ofile_name])
    with open(ofile_path, "w") as g_ofile:
        proc_C(mangled_decls)
    update_backend_specs("GTCU", 3)
    ofile_name = "f2c_dawn_struct.hpp"
    ofile_path = "/".join([odir, ofile_name])
    with open(ofile_path, "w") as g_ofile:
        proc_C(mangled_decls)


def proc_unmangled_decls(odir):
    global g_ofile
    assert os.path.isdir(odir)
    ofile_name = "f2c_dawn_common_c_decls.hpp"
    ofile_path = "/".join([odir, ofile_name])
    with open(ofile_path, "w") as g_ofile:
        proc_C(unmangled_decls)
    ofile_name = "f2c_dawn_common_f.f90"
    ofile_path = "/".join([odir, ofile_name])
    mo_name = "f2c_dawn_common"
    with open(ofile_path, "w") as g_ofile:
        gen_mo_head(mo_name)
        proc_F(unmangled_decls)
        gen_mo_tail(mo_name)


def proc_mangled_decls(odir):
    global g_ofile
    assert os.path.isdir(odir)

    # C decls:
    ofile_name = "f2c_dawn_struct_c_decls.hpp"
    ofile_path = "/".join([odir, ofile_name])
    print("write output to ", ofile_path)
    with open(ofile_path, "w") as g_ofile:
        proc_C(mangled_decls)

    # f90:
    ofile_name = "f2c_dawn_struct_f.f90"
    ofile_path = "/".join([odir, ofile_name])
    mo_name = "F2C_DAWN_STRUCT"
    print("write output to ", ofile_path)
    with open(ofile_path, "w") as g_ofile:
        g_ofile.write("#include \"name_mangling.h\"\n")
        gen_mo_head(mo_name)
        proc_F(mangled_decls)
        gen_mo_tail(mo_name)



def eval_args():
    odir = g_args.dir or "."
    proc_unmangled_decls(odir)

    halo_size = str2int(g_args.halo_size)
    assert halo_size is not None
    assert halo_size >= 0
    dawn_backend = g_args.dawn_backend.lower()
    if dawn_backend == "mc":
        sm = "GTMC"
    elif dawn_backend == "cuda":
        sm = "GTCU"
    else:
        die("invalid dawn_backend:", dawn_backend)
    update_backend_specs(storage_model=sm, halo_size=halo_size)
    proc_mangled_decls(odir=odir)


# --- global vars from here on ---

dom_type = TypeSpec("gtd::domain", is_ptr=True)
int_type = TypeSpec("int")
dpt_type = TypeSpec("double", is_ptr=True)
opt_dpt_type = TypeSpec("double", is_ptr=True, is_optional=True)
ipt_type = TypeSpec("int", is_ptr=True)
md_type = TypeSpec("gtd::meta_data_t", is_ptr=True)
mdij_type = TypeSpec("gtd::meta_data_ij_t", is_ptr=True)
mdk_type = TypeSpec("gtd::meta_data_k_t", is_ptr=True)
st_type = TypeSpec("gtd::storage_t", is_ptr=True)
stij_type = TypeSpec("gtd::storage_ij_t", is_ptr=True)
stk_type = TypeSpec("gtd::storage_k_t", is_ptr=True)
c_str_type = TypeSpec("char", is_ptr=True, is_const=True)
void_type = TypeSpec("void")
string_type = TypeSpec("std::string", is_ptr=True)
str_pt_arg = FunArg(TypeSpec("std::string", is_ptr=True), "name")

dom_arg = FunArg(dom_type, "dom_pt")
dom_shape_arg = FunArg(TypeSpec("int", array_spec=[3]), "dom_shape")
halo_arg = FunArg(TypeSpec("int", array_spec=[2, 2]), "dom_halos")
init_value_arg = FunArg(opt_dpt_type, "init_value")
mdij_arg = FunArg(mdij_type, "md_pt")
md_arg = FunArg(md_type, "md_pt")
mdk_arg = FunArg(mdk_type, "md_pt")
c_str_arg = FunArg(c_str_type, "name")
st_arg = FunArg(st_type, "s_pt")
stij_arg = FunArg(stij_type, "s_pt")
stk_arg = FunArg(stk_type, "s_pt")
tni_arg = FunArg(int_type, "total_ni")
tnj_arg = FunArg(int_type, "total_nj")
tnk_arg = FunArg(int_type, "total_nk")
win1_arg = FunArg(TypeSpec("int", array_spec=[2]), "f_win")
win2_arg = FunArg(TypeSpec("int", array_spec=[2, 2]), "f_win")
win3_arg = FunArg(TypeSpec("int", array_spec=[3, 2]), "f_win")

unmangled_decls = [
    # support:
    FunDecl(TypeSpec("std::string", is_ptr=True), "create_string", [c_str_arg]),
    FunDecl(
        void_type,
        "delete_string",
        [FunArg(TypeSpec("std::string", is_ptr=True), "str_pt")],
    ),
]

mangled_decls = [
    # domain:
    FunDecl(dom_type, "create_dawn_domain", [tni_arg, tnj_arg, tnk_arg], mangle=True),
    FunDecl(void_type, "delete_dawn_domain", [dom_arg], mangle=True),
    FunDecl(int_type, "get_dawn_domain_shape", [dom_arg, dom_shape_arg], mangle=True),
    # halos:
    FunDecl(int_type, "get_dawn_domain_halos", [dom_arg, halo_arg], mangle=True),
    FunDecl(int_type, "set_dawn_domain_halos", [dom_arg, halo_arg], mangle=True),
    FunDecl(int_type, "get_dawn_initial_halo_size", [], mangle=True),
    # meta data
    FunDecl(mdij_type, "create_dawn_meta_data_2d", [tni_arg, tnj_arg], mangle=True),
    FunDecl(void_type, "delete_dawn_meta_data_2d", [mdij_arg], mangle=True),
    FunDecl(
        md_type,
        "create_dawn_meta_data_3d",
        [tni_arg, tnj_arg, tnk_arg],
        mangle=True,
    ),
    FunDecl(void_type, "delete_dawn_meta_data_3d", [md_arg], mangle=True),
    FunDecl(mdk_type, "create_dawn_meta_data_column", [tnk_arg], mangle=True),
    FunDecl(void_type, "delete_dawn_meta_data_column", [mdk_arg], mangle=True),
    # storage:
    FunDecl(
        stij_type,
        "create_dawn_storage_2d",
        [mdij_arg, init_value_arg, str_pt_arg],
        mangle=True,
    ),
    FunDecl(void_type, "delete_dawn_storage_2d", [stij_arg], mangle=True),
    FunDecl(
        st_type,
        "create_dawn_storage_3d",
        [md_arg, init_value_arg, str_pt_arg],
        mangle=True,
    ),
    FunDecl(void_type, "delete_dawn_storage_3d", [st_arg], mangle=True),
    FunDecl(
        stk_type,
        "create_dawn_storage_column",
        [mdk_arg, init_value_arg, str_pt_arg],
        mangle=True,
    ),
    FunDecl(void_type, "delete_dawn_storage_column", [stk_arg], mangle=True),
    # data transfer:
    FunDecl(
        int_type,
        "copy_win_to_dawn_struct2d_double",
        [FunArg(dpt_type, "f"), FunArg(ipt_type, "f_shape"), win2_arg, stij_arg],
        mangle=True,
    ),
    FunDecl(
        int_type,
        "copy_win_from_dawn_struct2d_double",
        [FunArg(dpt_type, "f"), FunArg(ipt_type, "f_shape"), win2_arg, stij_arg],
        mangle=True,
    ),
    FunDecl(
        int_type,
        "copy_win_to_dawn_struct3d_double",
        [FunArg(dpt_type, "f"), FunArg(ipt_type, "f_shape"), win3_arg, st_arg],
        mangle=True,
    ),
    FunDecl(
        int_type,
        "copy_win_from_dawn_struct3d_double",
        [FunArg(dpt_type, "f"), FunArg(ipt_type, "f_shape"), win3_arg, st_arg],
        mangle=True,
    ),
    FunDecl(
        int_type,
        "copy_win_to_dawn_column_double",
        [FunArg(dpt_type, "f"), FunArg(int_type, "f_size"), win1_arg, stk_arg],
        mangle=True,
    ),
    FunDecl(
        int_type,
        "copy_win_from_dawn_column_double",
        [FunArg(dpt_type, "f"), FunArg(int_type, "f_size"), win1_arg, stk_arg],
        mangle=True,
    ),
]

all_decls = unmangled_decls + mangled_decls

if __name__ == "__main__":
    print("gen_f2c_itf.py: cwd=",os.getcwd())
    g_args = get_args()
    g_ofile_name = "main_out.tmp"
    with open(g_ofile_name, "w") as g_ofile:
        eval_args()

