from os.path import basename

import itfir2c
from itfir2c import c_method_name, cdsl_name, serialize

class Gstate:
    def __init__(self, itfir):
        # consts:
        self.enable_name_mangling = itfir.enable_name_mangling
        self.have_cdsl_support = itfir.have_cdsl_support
        self._itfir_id = itfir._id
        # variables:
        self.interfaces = {}
        if self.have_cdsl_support:
            self.iso_syms = {"c_ptr": 1, "c_null_ptr": 1, "c_associated": 1}
        else:
            self.iso_syms = {}
            
    def register_iso_sym(self, isosym):
        self.iso_syms[isosym] = 1

    def register_interface(self, iface_name, iface):
        key = iface_name.lower()
        assert not key in self.interfaces
        self.interfaces[key] = iface


class Itf_f:
    def __init__(self, code):
        self.code = code


def remove_trailing_empty(a):
    if a and not a[-1]:
        a.pop()


def gen_sir_f_interface(itfir):
    global gstate
    gstate = Gstate(itfir)

    # make sure that C and Fortran interfaces are consistent:
    assert gstate._itfir_id == itfir2c.gstate._itfir_id

    procs = []
    stencil_types = []
    publics = []
    for stencil in itfir.stencils:
        stencil_procs = gen_stencil_f_interface(stencil)
        if gstate.have_cdsl_support:
            stype = gen_stencil_f_type(stencil)
            publics.append("PUBLIC :: " + stencil.name + "_type")
            stencil_types.extend(stype)
            stencil_types.append("")
            procs.extend(stencil_procs)
            procs.append("")
    remove_trailing_empty(procs)
    remove_trailing_empty(stencil_types)

    module_name = gen_f_module_name(itfir.filename)

    # global USE statements:

    iso_syms = gstate.iso_syms.keys()
    if iso_syms:
        use_iso_syms = "USE iso_c_binding, ONLY: " + ", ".join(iso_syms)
    else:
        use_iso_syms = None

    if gstate.have_cdsl_support:
        use_dawn = (
            "USE dawn_struct" if itfir.grid_type_str == "Cartesian" else "USE dawn_unstruct"
        )
    else:
        use_dawn = None

    # name mangling:

    if gstate.enable_name_mangling:
        inc = '#include "name_mangling.h"'
    else:
        inc = None

    # public Fortran interfaces:

    iface = []
    for iname, ilines in gstate.interfaces.items():
        publics.append("PUBLIC :: " + iname)
        iface.extend(ilines)
        iface.append("")

    if iface:
        remove_trailing_empty(iface)
        iface = ["INTERFACE", iface, "END INTERFACE"]

    # combine all findings into one Fortran module:
    if procs:
        body = [
            "CONTAINS\n",
            [
                *procs,
            ],
        ]
    else:
        body = []


    module = [
        inc,
        "MODULE " + module_name,
        "! provides access to Dawn-generated stencil methods defined in",
        "! " + itfir.filename or "UNSPECIFIED_SIR2CPP_FILENAME",
        [
            use_iso_syms,
            use_dawn,
            "IMPLICIT NONE",
            "PRIVATE",
            "",
            *publics,
            "" if publics else None,
            *stencil_types,
            "" if stencil_types else None,
            *iface,
            "" if iface else None,
        ],
        *body,
        "END MODULE " + module_name,
    ]
    module_str = "\n".join(serialize(module))
    return Itf_f(module_str)


def f_method_name(stencil_name, method_name):
    if stencil_name.startswith("c_") or stencil_name.startswith("fc_"):
        return "f_" + stencil_name + "__" + method_name
    else:
        return stencil_name + "__" + method_name


def f2c_method_name(stencil_name, method_name):
    return "f2c_" + stencil_name + "_" + method_name


def stencil_type_name(stencil_name):
    return stencil_name + "_type"


def ctype2ftype(c_type):
    c_kind = "c_" + c_type
    gstate.register_iso_sym(c_kind)
    if c_type == "int":
        family = "INTEGER"
    elif c_type in ("float", "double"):
        family = "REAL"
    elif c_type == "bool":
        family = "LOGICAL"
    else:
        die("unexpected case")
    return family + "(" + c_kind + ")"


def gen_f_module_name(file_name):
    if not file_name:
        return "dawn_stencils"
    b = basename(file_name)
    if b.endswith(".cpp"):
        head = b[:-4]
    else:
        head = b
    f = "dawn_" + head
    return f


def flines(args, start=None, end=None):
    if not args:
        return []
    max_len = 60
    n = 0
    s = ""
    a = []
    s = start or ""
    for x in args:
        if len(s) + len(x) <= max_len:
            s += x
        else:
            a.append(s + " &")
            s = x
    if end:
        s += end
    a.append(s)
    return a


def gen_stencil_f_type(f):
    f_name = f.name
    gv_access_binding = []
    for gdecl in f.gdecls:
        var_name = gdecl.var_name
        for action in ("get_", "set_"):
            long_name = f_method_name(f_name, action + var_name)
            short_name = action + var_name
            binding = "PROCEDURE :: " + short_name + " => " + long_name
            gv_access_binding.append(binding)
    ftype = [
        "TYPE " + f_name + "_type",
        [
            "PRIVATE",
            "TYPE(c_ptr) :: cpt = c_null_ptr",
        ],
        "CONTAINS",
        [
            "PROCEDURE :: init => " + f_method_name(f_name, "init"),
            "FINAL :: " + f_method_name(f_name, "finalize"),
            "PROCEDURE :: run => " + f_method_name(f_name, "run"),
            *gv_access_binding,
        ],
        "END TYPE " + f_name + "_type",
    ]
    return ftype


def gen_stencil_f_interface(f):
    if f.is_struct:
        con = gen_stencil_f_init(f)
        run = gen_stencil_f_run(f)
    else:
        con = gen_unstruct_stencil_f_init(f)
        run = gen_unstruct_stencil_f_run(f)
    gvars = gen_stencil_f_gvar_access(f)
    procs = [
        con,
        "",
        run,
        "",
        gvars,
    ]

    return procs


def gen_unstruct_stencil_f_init(f):
    f2c_name = f2c_method_name(f.name, "init")
    c_name = c_method_name(f.name, "init")
    head_params = ["mesh_pt", "k_size"]
    field_pt_params = [p.name + "_pt" for p in f.parm_decls]
    sep_field_pt_parms = ",# ".join(field_pt_params).split("#")
    stencil = cdsl_name("stencil")
    con_itf = [
        "TYPE(c_ptr) FUNCTION " + f2c_name + "(mesh_pt, k_size, &",
        *flines(sep_field_pt_parms, start="& ", end=") &"),
        "& BIND(c, name='" + c_name + "')",
        [
            "USE iso_c_binding, ONLY : c_ptr, c_int",
            "TYPE(c_ptr), VALUE :: mesh_pt",
            "INTEGER(c_int), VALUE :: k_size",
            *["TYPE(c_ptr), VALUE :: " + x for x in field_pt_params],
        ],
        "END FUNCTION " + f2c_name,
    ]
    gstate.register_interface(f2c_name, con_itf)

    f_init_name = f_method_name(f.name, "init")
    field_params = [p.name for p in f.parm_decls]
    sep_field_parms = ",# ".join(field_params).split("#")
    f2c_init_name = f2c_method_name(f.name, "init")
    field_cpt_params = [p.name + "%cpt" for p in f.parm_decls]
    sep_field_cpt_parms = ",# ".join(field_cpt_params).split("#")
    specs = [
        "TYPE(mesh_type) :: mesh",
        "INTEGER, INTENT(IN) :: k_size",
    ]
    for p in f.parm_decls:
        if p.is_vertical:
            ftype = "TYPE(vertical_field_type)"
        elif p.is_dense:
            ftype = "TYPE(dense_field_type)"
        else:
            ftype = "TYPE(sparse_field_type)"
        specs.append(ftype + " :: " + p.name)

    con = [
        "SUBROUTINE " + f_init_name + "(" + stencil + ", mesh, k_size, &",
        *flines(sep_field_parms, start="& ", end=")"),
        [
            "CLASS(" + stencil_type_name(f.name) + ") :: " + stencil,
            *specs,
            stencil + "%cpt = " + f2c_init_name + "(mesh%cpt, k_size, &",
            *flines(sep_field_cpt_parms, start="& ", end=")"),
        ],
        "END SUBROUTINE " + f_init_name,
        "",
    ]

    my_f2c_final_name = f2c_method_name(f.name, "delete")
    my_c_final_name = c_method_name(f.name, "delete")
    my_f_final_name = f_method_name(f.name, "finalize")

    decon_itf = [
        "SUBROUTINE " + my_f2c_final_name + "(stencil_cpt) &",
        [
            "& BIND(c, name='" + my_c_final_name + "')",
            "USE iso_c_binding, ONLY : c_ptr",
            "TYPE(c_ptr), VALUE :: stencil_cpt",
        ],
        "END SUBROUTINE " + my_f2c_final_name,
    ]
    gstate.register_interface(my_f2c_final_name, decon_itf)

    decon = [
        "SUBROUTINE " + my_f_final_name + "(" + stencil + ")",
        [
            "TYPE(" + stencil_type_name(f.name) + ") :: " + stencil,
            "CALL " + my_f2c_final_name + "(" + stencil + "%cpt)",
        ],
        "END SUBROUTINE " + my_f_final_name,
    ]

    return con + decon


def gen_unstruct_stencil_f_run(f):
    my_f2c_run_name = f2c_method_name(f.name, "run")
    my_f_run_name = f_method_name(f.name, "run")
    my_c_run_name = c_method_name(f.name, "run")
    stencil = cdsl_name("stencil")

    bind = [
        "SUBROUTINE " + my_f2c_run_name + "(" + stencil + "_cpt) &",
        "& BIND(c, name='" + my_c_run_name + "')",
        [
            "USE iso_c_binding, ONLY : c_ptr",
            "TYPE(c_ptr), VALUE :: " + stencil + "_cpt",
        ],
        "END SUBROUTINE " + my_f2c_run_name,
    ]
    gstate.register_interface(my_f2c_run_name, bind)

    run = [
        "SUBROUTINE " + my_f_run_name + "(" + stencil + ")",
        [
            "CLASS(" + stencil_type_name(f.name) + ") :: " + stencil,
            # *bind,
            "CALL " + my_f2c_run_name + "(" + stencil + "%cpt)",
        ],
        "END SUBROUTINE " + my_f_run_name,
    ]

    return run


def gen_stencil_f_gvar_access(f):
    my_f2c_run_name = f2c_method_name(f.name, "run")
    wrappers = []
    f_name = f.name
    for gdecl in f.gdecls:
        var_name = gdecl.var_name
        var_ctype = gdecl.type_name
        f2c_type = ctype2ftype(var_ctype)
        if var_ctype == "bool":
            f_type = "LOGICAL"
        elif var_ctype == "int":
            f_type = "INTEGER"
        else:
            f_type = f2c_type
        c_get_name = c_method_name(f_name, "get_" + var_name)
        f2c_get_name = f2c_method_name(f_name, "get_" + var_name)
        f_get_name = f_method_name(f_name, "get_" + var_name)
        c_set_name = c_method_name(f_name, "set_" + var_name)
        f2c_set_name = f2c_method_name(f_name, "set_" + var_name)
        iso_sym = "c_" + var_ctype
        f_set_name = f_method_name(f_name, "set_" + var_name)
        arg_name = var_ctype + "_var"
        get_itf = [
            f2c_type + " FUNCTION " + f2c_get_name + "(stencil_cpt) &",
            [
                "& BIND(c, name='&",
                "&" + c_get_name + "')",
                "USE iso_c_binding, ONLY: c_ptr, " + iso_sym,
                "IMPLICIT NONE",
                "TYPE(c_ptr), VALUE :: stencil_cpt",
            ],
            "END FUNCTION " + f2c_get_name,
        ]
        gstate.register_interface(f2c_get_name, get_itf)

        get_wrap = [
            f_type + " FUNCTION " + f_get_name + "(stencil)",
            [
                "CLASS(" + stencil_type_name(f.name) + ") :: stencil",
                "IF (.NOT. c_associated(stencil%cpt)) STOP 'ERROR("
                + f_get_name
                + "): missing initialization'",
                f_get_name + " = " + f2c_get_name + "(stencil%cpt)",
            ],
            "END FUNCTION " + f_get_name,
            "",
        ]

        set_itf = [
            "SUBROUTINE " + f2c_set_name + "(stencil_cpt, " + arg_name + ") &",
            [
                "& BIND(c, name='&",
                "&" + c_set_name + "')",
                "USE iso_c_binding, ONLY: c_ptr, " + iso_sym,
                "IMPLICIT NONE",
                "TYPE(c_ptr), VALUE :: stencil_cpt",
                f2c_type + ", VALUE :: " + arg_name,
            ],
            "END SUBROUTINE " + f2c_set_name,
        ]
        gstate.register_interface(f2c_set_name, set_itf)
        set_wrap = [
            "SUBROUTINE " + f_set_name + "(stencil, " + arg_name + ")",
            [
                "CLASS(" + stencil_type_name(f.name) + ") :: stencil",
                f_type + ", INTENT(IN) :: " + arg_name,
                f2c_type + ":: tmp",
                "IF (.NOT. c_associated(stencil%cpt)) STOP 'ERROR("
                + f_set_name
                + "): missing initialization'",
                "tmp = " + arg_name,
                "CALL " + f2c_set_name + "(stencil%cpt, " + "tmp" + ")",
            ],
            "END SUBROUTINE " + f_set_name,
        ]

        wrappers.extend(get_wrap + set_wrap)
        wrappers.append("")

    return wrappers[:-1]


def gen_stencil_f_init(f):
    my_f2c_init_name = f2c_method_name(f.name, "init")
    my_c_init_name = c_method_name(f.name, "init")
    my_f_init_name = f_method_name(f.name, "init")

    my_f2c_final_name = f2c_method_name(f.name, "delete")
    my_c_final_name = c_method_name(f.name, "delete")
    my_f_final_name = f_method_name(f.name, "finalize")

    pfi = "   "
    con_itf = [
        "TYPE(c_ptr) FUNCTION " + my_f2c_init_name + "(dom_cpt) &",
        [
            "& BIND(c, name='&",
            "&" + my_c_init_name + "')",
            "USE iso_c_binding, ONLY : c_ptr",
            "TYPE(c_ptr), VALUE :: dom_cpt",
        ],
        "END FUNCTION " + my_f2c_init_name,
    ]
    gstate.register_interface(my_f2c_init_name, con_itf)

    con = [
        "SUBROUTINE " + my_f_init_name + "(stencil, dom)",
        [
            "CLASS(" + stencil_type_name(f.name) + ") :: stencil",
            "TYPE(domain_type) :: dom",
            "IF (c_associated(stencil%cpt)) STOP 'ERROR("
            + my_f_init_name
            + "): multiple initialization'",
            "stencil%cpt = " + my_f2c_init_name + "(dom%cpt)",
            "IF (.not. c_associated(stencil%cpt)) STOP 'ERROR("
            + my_f_init_name
            + "): failed'",
        ],
        "END SUBROUTINE " + my_f_init_name,
        "",
    ]

    decon_itf = [
        "SUBROUTINE " + my_f2c_final_name + "(stencil_cpt) &",
        [
            "& BIND(c, name='&",
            "&" + my_c_final_name + "')",
            "USE iso_c_binding, ONLY : c_ptr",
            "TYPE(c_ptr), VALUE :: stencil_cpt",
        ],
        "END SUBROUTINE " + my_f2c_final_name,
    ]
    gstate.register_interface(my_f2c_final_name, decon_itf)

    decon = [
        "SUBROUTINE " + my_f_final_name + "(stencil)",
        [
            "TYPE(" + stencil_type_name(f.name) + ") :: stencil",
            "CALL " + my_f2c_final_name + "(stencil%cpt)",
        ],
        "END SUBROUTINE " + my_f_final_name,
    ]

    return con + decon


def gen_stencil_f_run(f):
    my_f2c_run_name = f2c_method_name(f.name, "run")
    my_f_run_name = f_method_name(f.name, "run")
    my_c_run_name = c_method_name(f.name, "run")
    pfi = "   "
    sub_args = []
    f2c_args = ["stencil_cpt"]
    call_args = ["stencil%cpt"]
    specs = []
    for p in f.parm_decls:
        sub_args.append(p.name)
        f2c_args.append(p.name + "_cpt")
        call_args.append(p.name + "%cpt")
        if p.Type == "storage_ij_t":
            storage_type = "storage_struct2d_double_type"
        elif p.Type == "storage_ijk_t":
            storage_type = "storage_struct3d_double_type"
        elif p.Type == "storage_k_t":
            storage_type = "storage_column_double_type"
        else:
            die("unsupported storage type: " + p.Type)
        specs.append("TYPE(" + storage_type + ") :: " + p.name)
    f2c_arg_list = ", ".join(f2c_args)
    bind = [
        "SUBROUTINE " + my_f2c_run_name + "(" + f2c_arg_list + ") &",
        [
            "& BIND(c, name='&",
            "&" + my_c_run_name + "')",
            "USE iso_c_binding, ONLY : c_ptr",
            "TYPE(c_ptr), VALUE :: " + f2c_arg_list,
        ],
        "END SUBROUTINE " + my_f2c_run_name,
    ]
    gstate.register_interface(my_f2c_run_name, bind)

    sub_arg_list = ", ".join(sub_args)
    call_arg_list = ", ".join(call_args)
    run = [
        "SUBROUTINE " + my_f_run_name + "(stencil, " + sub_arg_list + ")",
        [
            "CLASS(" + stencil_type_name(f.name) + ") :: stencil",
            *specs,
            "IF (.NOT. c_associated(stencil%cpt)) STOP 'ERROR("
            + my_f_run_name
            + "): missing initialization'",
            "CALL " + my_f2c_run_name + "(" + call_arg_list + ")",
        ],
        "END SUBROUTINE " + my_f_run_name,
    ]

    return run
