# Distribution: see LICENSE.txt

import tvs
from tvs import (
    Tv,
    tdef,
    tqdef,
    tfun,
    is_lot,
    gen_tv,
    is_tv,
    tv_str,
    tname,
    tqname,
    die,
    CtypeProps,
)

from common import die

enable_type_reduction = True
enable_simple_cut = 1
pfi = " "


def reduce_type(f):
    t = getattr(f, "t", 0)
    if not t in type_reduction_table:
        return
    tq = getattr(f, "tq", 0)
    (t2, tq2) = type_reduction_table[t]
    if tq and tq != tq2:
        die("internal error in type reduction")
    g = f
    g.t = t2
    g.tq = tq
    return g


def simple_cut(k, f):
    t = getattr(f, "t", 0)
    attr = simple_cut_table.get(t)
    if attr == None:
        return
    attr_val = getattr(f, attr, None)
    if is_lot(attr_val):
        die("bad case: tried to move a list")
    if attr_val == None:
        return
    if attr_val:
        return attr_val
    return


def gen_singleton(f):  # prob. not needed
    if not isinstance(f, Tv):
        return
    g = f
    for k in f.__dict__.keys():
        if not k in ("lhs", "rhs", "expr", "var_decl"):
            continue
        v = getattr(f, k, None)
        if not v:
            continue
        if not is_lot(v):
            continue
        if len(v) != 1:
            continue
        setattr(g, k, v[0])
    return g


def attr2list(a0, f):
    if not a0 in attr2list_table:
        return
    a1 = attr2list_table[a0]
    a1v = getattr(f, a1, None)
    if not a1v:
        return
    if not isinstance(a1v, (list, tuple)):
        return
    return a1v


def transform_var_decl(f):
    expr = getattr(f, "expr", None)
    if not expr:
        return
    if expr.t != tdef.var_ctor:
        return
    if getattr(expr, "args", None) or getattr(expr, "expr", None):
        return
    # we drop the default ctor
    g = f
    delattr(g, "expr")
    return g


def post_process(k, f):
    if not isinstance(f, Tv):
        return f
    g = f
    if enable_type_reduction:
        g = reduce_type(g) or g
    if enable_simple_cut:
        g = simple_cut(k, g) or g
    # special cases:
    if getattr(g, "t", 0) == tdef.var_decl:
        g = transform_var_decl(g) or g
    # add ctype properties:
    if not getattr(g, "_ctp", None):
        ct = getattr(g, "ct", "")
        if ct:
            g._ctp = CtypeProps(ct)
    return g


def traverse_tree(f, level=0):
    if is_lot(f):
        g = []
        for x in f:
            y = traverse_tree(x, level + 1)
            if y:
                g.append(y)
    elif is_tv(f):
        g = f
        for k in f.__dict__.keys():
            ax = getattr(f, k)
            if isinstance(ax, (list, tuple)):
                ay = traverse_tree(ax, level + 1)
                setattr(g, k, ay)
            elif isinstance(ax, Tv):
                ay = traverse_tree(ax, level + 1)
                az = post_process(k, ay) or ay
                setattr(g, k, az)
        if isinstance(g, Tv):
            g = post_process(k, g)
    return g


def get_ast(f):
    return traverse_tree(f)


type_reduction_table = {
    # format: lhs_type : (rhs_type, type_qualifier)
    # a node N of type lhs_type changes its (type, qualifier) attributes as given on the rhs:
    tdef.field_expr: (tdef.expr, tqdef.field),
    tdef.set_expr: (tdef.expr, tqdef.set),
    tdef.scalar_expr: (tdef.expr, tqdef.scalar),
    tdef.field_decl: (tdef.var_decl, tqdef.field),
    tdef.scalar_decl: (tdef.var_decl, tqdef.scalar),
    tdef.field_ref: (tdef.var_ref, tqdef.field),
    tdef.set_ref: (tdef.var_ref, tqdef.set),
    tdef.scalar_ref: (tdef.var_ref, tqdef.scalar),
    tdef.field_assign: (tdef.var_assign, tqdef.field),
    tdef.scalar_assign: (tdef.var_assign, tqdef.scalar),
    tdef.field_access: (tdef.var_access, tqdef.field),
    tdef.scalar_ctor: (tdef.var_ctor, tqdef.scalar),
    tdef.field_ctor: (tdef.var_ctor, tqdef.field),
    tdef.scalar_binop: (tdef.binop, tqdef.scalar),
}

simple_cut_table = {
    # format: LHS_TYPE : ATTR
    # a node of type LHS_TYPE gets replaced by the value of its attribute ATTR
    #
    tdef.dim_spec: "expr",
    tdef.dim_key: "expr",
    tdef.field2scalar_conv: "expr",
    tdef.field_cast_expr: "expr",
    tdef.op_set_expr: "expr",
    tdef.expr: "expr",
    tdef.paren_expr: "expr",
}


attr2list_table = {
    # format: A : B
    # N.A.B becomes N.A if type(N.A.B) == list and len(N.A.B)>0
    "field_spec": "args",
}
