from google.protobuf import json_format
from google.protobuf.json_format import MessageToJson

from common import *

class ExtSIR:
    def __init__(self, sir, edsl_extension_src):
        self.sir = sir
        self.edsl_extension_src = edsl_extension_src

    def get_output_str(self):
        sir = self.sir
        if hasattr(sir, "get_output_str"):
            sir_str = sir.get_output_str()
        else:
            sir_str = str(sir)
        if self.edsl_extension_src is not None:
            return sir_str + "\n# edsl_extension_src:\n" + self.edsl_extension_src
        else:
            return sir_str

    def get_sir(self):
        return self.sir

    def get_edsl_extension_src(self):
        return self.edsl_extension_src

    def json_str(self):
        if self.edsl_extension_src:
            warn("EDSL extension code is not included in SIR json format")
        return MessageToJson(self.sir)

    def write_edsl_extension_src(self, base_filename):
        if self.edsl_extension_src:
            ext_filename = base_filename + "_edsl_ext.cpp"
            with open(ext_filename, "w") as f:
                f.write(self.edsl_extension_src)

    def write_json(self, filename):
        if not filename:
            user_error("Undefined output filename for SIR output.")
        with open(filename, "w") as f:
            f.write(MessageToJson(self.sir))
        self.write_edsl_extension_src(filename)

    def write_str(self, filename):
        if not filename:
            user_error("Undefined output filename for SIR output.")
        with open(filename, "w") as f:
            f.write(str(self.sir))
        self.write_edsl_extension_src(filename)
