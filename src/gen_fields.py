#!/usr/bin/env python3
# Distribution: see LICENSE.txt

import sys
import json
from collections import OrderedDict
import argparse
import configparser
import dim_config

import srcgen_common as sgc


def gen_field(type, field_suffix):
    cname = "Field" + field_suffix
    members = []
    nonmembers = []
    # constructors, specification:
    members.append(cname + "()")  # we need this for the GridspaceFieldTemplate
    members.append(cname + "(Gridspace &gs)")
    members.append("void spec(Gridspace &gs)")
    for kmax in range(1, 6):
        args = ["const Dim_spec& ds" + str(k) for k in range(kmax)]
        members.append(cname + "(" + ",".join(args) + ")")
        members.append("void spec(" + ",".join(args) + ")")

    # access:
    for kmax in range(1, 6):
        args = ["const Dim_key& dk" + str(k) for k in range(kmax)]
        members.append(cname + " operator () (" + ",".join(args) + ")")

    # initialization:
    members.append(cname + " operator= (const " + cname + " f)")
    members.append(cname + " operator= (const " + type + " val)")
    for kmax in range(2, 4):
        args = []
        for k in range(1, kmax):
            args.append("const " + type + " val" + str(k))
        # print(args)
        members.append(cname + " init(" + ",".join(args) + ")")

    # set numerical types:
    num_types = ("int", "float", "double")

    # relational operators:
    rtype = "Field_bool"
    for op in ("==", "!=", "<", ">", "<=", ">="):
        args = [cname + " f", cname + " g"]
        nonmembers.append(rtype + " operator" + op + " (" + ",".join(args) + ")")
        if type in num_types:
            for t in num_types:
                args = [cname + " f", t + " x"]
                args_r = args[::-1]
                nonmembers.append(
                    rtype + " operator" + op + " (" + ",".join(args) + ")"
                )
                nonmembers.append(
                    rtype + " operator" + op + " (" + ",".join(args_r) + ")"
                )

    # algebraic ops:
    nonmembers.append(cname + " operator- (" + cname + ")")
    for op in ("+", "-", "*", "/"):
        args = [cname + " x", cname + " y"]
        nonmembers.append(cname + " operator" + op + " (" + ",".join(args) + ")")
        for t in num_types:
            args = [cname + " x", t + " y"]
            args_r = args[::-1]
            nonmembers.append(cname + " operator" + op + " (" + ",".join(args) + ")")
            nonmembers.append(cname + " operator" + op + " (" + ",".join(args_r) + ")")

    # compound assignment operators:
    for op in ("+=", "-=", "*=", "/="):
        members.append(cname + " operator" + op + " (" + cname + " f)")
        members.append(cname + " operator" + op + " (" + type + " val)")

    # logic operators:
    nonmembers.append(cname + " operator! (" + cname + " f)")
    for op in ("&&", "||"):
        args = [cname + " f", cname + " g"]
        nonmembers.append(cname + " operator" + op + " (" + ",".join(args) + ")")

    # neighbor_reduce:
    if cname == "Field":
        weights_variants = [[], ["const ReductionWeights w"], ["const ReductionWeights w", "const ReductionOffsets offsets"]]
    else:
        # weights_variants = [ [], ["const " + cname + "& w"], ["const double w[]"], ["const std::initializer_list<double> w"] ]
        weights_variants = [[]]

    details_variants = [[], ["op::Reduce_op_kind op_kind", type + " initial_value"]]
    always = ["const Set_expr& se0"]
    farg = ["const " + cname + "& f"]
    for weights in weights_variants:
        for details in details_variants:
            args = always + details + weights
            members.append(cname + " nreduce(" + ", ".join(args) + ")")
            nonmembers.append(cname + " nreduce(" + ", ".join(args + farg) + ")")
            # for k in range(kmax):
            # key_args.append('const Set_expr& se' + str(k))
            # farg = ['const ' + cname + '& f']
            # args = key_args + weights + ['op::Reduce_op_kind op_kind', type + ' initial_value']
            # members.append(cname + ' nreduce(' + ','.join(args) + ')')
            # nonmembers.append(cname + ' nreduce(' + ','.join(args+farg) + ')')
            # args = key_args + weights + [type + ' initial_value']
            # members.append(cname + ' nreduce(' + ','.join(args) + ')')
            # nonmembers.append(cname + ' nreduce(' + ','.join(args+farg) + ')')
            # args = key_args + weights
            # members.append(cname + ' nreduce(' + ','.join(args) + ')')
            # nonmembers.append(cname + ' nreduce(' + ','.join(args+farg) + ')')

    # conversion to element type - requires front-end test if we actually have a scalar expression
    members.append("operator " + type + "() const")

    # field_dot_product:
    other = "const " + cname + "& other"
    members.append(cname + " dot_product(" + other + ")")
    members.append(cname + " dot_product(const Set_expr set, " + other + ")")
    nonmembers.append(
        cname + " dot_product(const " + cname + "& x, const " + cname + "& y)"
    )
    nonmembers.append(
        cname
        + " dot_product(const Set_expr set, const "
        + cname
        + "& x, const "
        + cname
        + "& y)"
    )

    # gen class:
    c = {
        "type": "class",
        "name": cname,
        "members": members,
        "nonmembers": nonmembers,
    }
    return c


def gen_fields():
    src = []
    src.append(gen_field("bool", "_bool"))
    src.append(gen_field("int", "_int"))
    src.append(gen_field("float", "_sp"))
    src.append(gen_field("double", "_dp"))
    src.append(gen_field("double", ""))  # default field type is double
    return src


src = gen_fields()
for c in src:
    sgc.print_class(c)
