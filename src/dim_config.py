# Distribution: see LICENSE.txt

from collections import OrderedDict
import configparser

global structured_dim_conf, unstructured_dim_conf


def proc_config(conf):
    global structured_dim_conf
    global unstructured_dim_conf

    switch_keys = (
        "is_structured",
        "is_vertical_dim",
        "is_decomposed_dim",
        "is_extra_dim",
        "is_horizontal_dim",
    )

    nonswitch_keys = ("set_type", "element_type", "domain_set_var", "size", "mesh_type")
    dim_keys = switch_keys + nonswitch_keys
    structured_dim_conf = OrderedDict()
    unstructured_dim_conf = OrderedDict()
    hdim_count = 0
    for s in conf.sections():
        dim = OrderedDict()
        for k in conf[s]:
            if not k in dim_keys:
                die(
                    "configuration file contains the unexpeced key '"
                    + k
                    + "' in section '"
                    + s
                    + "'"
                )
            v = conf[s][k]
            dim[k] = v
        for k in dim_keys:
            if not k in dim:
                die(
                    "configuration file does not contain the required key '"
                    + k
                    + "' in section '"
                    + s
                    + "'"
                )
            if k in switch_keys:
                try:
                    is_true = conf[s].getboolean(k)
                except ValueError:
                    die(
                        "bad value in configuration file, section: "
                        + s
                        + ", key: '"
                        + k
                        + "'"
                    )
                dim[k] = is_true
            if isinstance(dim[k], str):
                if dim[k].lower().startswith("intrinsic"):
                    die("invalid " + k + " name: " + dim[k] + " in section " + s)

        # convert size string:
        size = dim["size"].lower()
        if size in ("none", "undef", "undefined"):
            dim["size"] = 0
        else:
            try:
                dim["size"] = int(dim["size"])
            except ValueError:
                die("bad value in configuration file, section: " + s + ", key: 'size'")

        if dim["is_structured"]:
            structured_dim_conf[s] = dim
        else:
            unstructured_dim_conf[s] = dim

        if dim.get("is_structured", False) and dim.get("is_horizontal_dim", False):
            hdim_count += 1
            if hdim_count > 2:
                die("too many horizontal dimensions")
            dim["struct_hdim_kind"] = hdim_count - 1
