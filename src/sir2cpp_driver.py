#!/usr/bin/env python3
import os

from dawn4py.serialization import SIR
from google.protobuf import json_format, text_format
import sir2cpp
from common import *
import argparse


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-o",
        metavar="file",
        help="Dawn output filename",
        action="store",
        required=False,
    )

    parser.add_argument(
        "-b",
        metavar="backend",
        help="one of Dawns backends (CXXNaive, CXXNaiveIco, CUDA or CUDAIco)",
        action="store",
        required=False,
    )

    # Not supported yet: interfaces without CDSL support infrastructure.
    #
    # parser.add_argument(
    #     "-cdsl",
    #     help="assume CDSL support is available",
    #     action="store_true",
    # )

    parser.add_argument("file", help="SIR file")

    return parser.parse_args()


def get_sir_from_file(sir_fname):
    try:
        with open(sir_fname, "r") as f:
            sir_json = f.read()
    except:
        general_error("Cannot open file", sir_fname)

    sir_msg = None
    try:
        sir_msg = json_format.Parse(sir_json, SIR.SIR_pb2.SIR())
    except:
        pass
    try:
        sir_msg = text_format.Parse(sir_json, SIR.SIR_pb2.SIR())
    except:
        pass

    if sir_msg is None:
        general_error("Cannot parse SIR file", sir_fname)

    return sir_msg


def output_str(ostring, ofname=None):
    if ofname:
        with open(ofname, "w") as f:
            f.write(ostring)
    else:
        print(ostring)


def output_code(code, ofile_name):
    if not ofile_name:
        return output_str(code.serialize())
    b = os.path.basename(ofile_name)
    d = os.path.dirname(ofile_name)
    if b.endswith(".cpp"):
        head = b[:-4]
    elif b.endswith(".cu"):
        head = b[:-3]
    else:
        head = b
    if code.c_itf_is_cuda:
        c_itf_suffix = ".cu"
    else:
        c_itf_suffix = ".cpp"
    f_itf_name = (d + "/" if d else "") + "f_itf_" + head + ".f90"
    c_itf_name = (d + "/" if d else "") + "c_itf_" + head + c_itf_suffix
    cpp_code_lines = code.inject_extension_code()

    output_str("\n".join(cpp_code_lines), ofile_name)
    if code.c_itf_code:
        output_str(code.c_itf_code, c_itf_name)
    if code.f_itf_code:
        output_str(code.f_itf_code, f_itf_name)


def proc_args(args):
    if args.file:
        sir = get_sir_from_file(args.file)
    else:
        user_error("Missing file argument.")

    if args.b:
        backend_name = args.b.lower()
    else:
        user_error("Missing backend arument.")

    ofile_name = args.o or None

    have_cdsl_support = True # not supported yet: args.cdsl

    if args.file:
        ifile_name = args.file
    else:
        user_error("File argument missing.")

    cpp = sir2cpp.get_sir_cpp(
        sir,
        backend_name=backend_name,
        ofile_name=ofile_name,
        have_cdsl_support=have_cdsl_support,
    )

    output_code(cpp, ofile_name)


if __name__ == "__main__":
    proc_args(get_args())
