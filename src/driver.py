#!/usr/bin/env python3
# Distribution: see LICENSE.txt

# Description: main execution entry for DSL frontend
# Production pipeline is organized in the following stages:
# (clang_filter -> dsl_parser -> dsl_ast -> sa -> sa2hir -> hir2sir -> sir2cpp
# The first step uses libclang + Python bindings.
# The last two steps use Dawn.
# Update:
#  The processing can also start with a given SIR file. This only executes the sir2cpp step.

import sys
import os
import atexit
import tempfile
import argparse
import dawn4py

# check where we are and import cdsl_config:
ENV_CDSL_ROOT = os.environ.get("CDSL_ROOT")
CDSL_CONF_PATH = "/".join([os.environ.get("CDSL_ROOT"), "conf"])
if not CDSL_CONF_PATH in sys.path:
    sys.path.append(CDSL_CONF_PATH)
import cdsl_config

assert cdsl_config.CDSL_ROOT == ENV_CDSL_ROOT

# add path for clang bindings:
if not cdsl_config.CDSL_CLANG_MODULE_DIR in sys.path:
    sys.path.append(cdsl_config.CDSL_CLANG_MODULE_DIR)

import tvs
import clang_filter
import dsl_parser
import dsl_ast
import sa
import sa2hir
import hir2sir
import ext_sir

if not hir2sir.CDSL_DEBUG_SIR_STAGE:
    import sir2cpp

    have_sir2cpp = True
else:
    have_sir2cpp = False
from common import *

check_parse_tree = 0
g_tmp_files = []
g_cdsl_file = None
g_sir_file = None
ofile_name = None

def get_frontend_dir():
    dsl_dir = get_cdsl_dir()
    if not dsl_dir:
        die("Missing environment variable DSL_FRONTEND_DIR")
    if not os.path.isdir(dsl_dir):
        die("Expecting directory at DSL_FRONTEND_DIR =" + dsl_dir)
    inc_dir = dsl_dir + "/include"
    if not os.path.isdir(inc_dir):
        die("Expecting directory at DSL_FRONTEND_DIR/include =" + inc_dir)
    ifiles = (
        "dsl.hpp",
        "fields_dsl.hpp",
        "struct_dsl.hpp",
        "struct_dsl.json",
        "unstruct_dsl.hpp",
        "unstruct_dsl.json",
    )
    for fname in ifiles:
        fpath = inc_dir + "/" + fname
        if not os.path.isfile(fpath):
            die("Expecting file at " + fpath)
    return dsl_dir


def get_tmp_filename():
    global g_tmp_files
    (handle, tname) = tempfile.mkstemp(suffix=".ast", prefix="_cdsl_tmp_")
    os.close(handle)
    if not os.path.exists(tname):
        general_error("Cannot generate tempfile.")
    g_tmp_files.append(tname)
    return tname


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-I", metavar="dir", help="include directory", action="append", required=False
    )
    parser.add_argument(
        "-o", metavar="file", help="output filename", action="store", required=False
    )
    parser.add_argument(
        "-s",
        metavar="stage",
        # default="CPP",
        help="max processing stage: one of clang_filter, parser, AST, SA, HIR, SIR, CPP",
        action="store",
        required=False,
    )
    parser.add_argument("-json", help="json output", action="store_true")
    parser.add_argument(
        "-D",
        metavar="macro",
        help="cpp macro definition",
        action="append",
        required=False,
    )
    parser.add_argument(
        "-nopath", help="do not encode the full filepath", action="store_true"
    )

    parser.add_argument(
        "-b",
        metavar="backend",
        help="one of Dawns backends (CXXNaive, CXXNaiveIco, CUDA or CUDAIco)",
        action="store",
        required=False,
    )

    parser.add_argument("file", help="DSL source code file")

    return parser.parse_args()


def output_str(ostring, ofname=None):
    if ofname:
        with open(ofname, "w") as f:
            f.write(ostring)
    else:
        print(ostring)


def emit_clang_ast(filename):
    macros = ["-D" + m for m in args.D] if args.D else []
    macros.append("-DEDSL_EXTENSION_FUN='/*EDSL_EXTENSION_FUN*/'")
    warnings = [
        "-Wall",
        "-Wno-unused-variable",
        "-Wno-missing-braces",
        "-Wno-sign-compare",
    ]
    includes = ["-I" + d for d in include_dirs]

    clangpp = cdsl_config.CDSL_CLANGPP_PRG
    ast_file = get_tmp_filename()
    cmd_args = (
        [clangpp, "-emit-ast", "-std=c++17", "-O0", "-g"]
        + warnings
        + includes
        + macros
        + ["-c", "-o", ast_file, args.file]
    )
    cmd = " ".join(cmd_args)
    retval = os.system(cmd)
    if retval != 0:
        if os.path.exists(ast_file):
            os.remove(ast_file)
        general_error("Initial AST generation failed.")
    return ast_file


def stage_clang_filter():
    if g_cdsl_file:
        ast_file = emit_clang_ast(args.file)
    else:
        general_error("Cannot enter clang_filter stage without DSL source file.")
    y = clang_filter.get_tree(ast_file, include_dirs, args.D, args.nopath)
    return y


def stage_parser(x=None):
    if x == None:
        x = stage_clang_filter()
    y = dsl_parser.get_parse_tree(x)
    if check_parse_tree:
        if not tvs.is_high_level_tree(y):
            print("error: parser result contains low level nodes", file=sys.stderr)
            exit(1)
        else:
            print("# type-level check passed", file=sys.stderr)
    return y


def stage_ast(x=None):
    if x == None:
        x = stage_parser()
    y = dsl_ast.get_ast(x)
    return y


def stage_sa(x=None):
    if x == None:
        x = stage_ast()
    y = sa.get_tree(x)
    return y


def stage_hir(x=None):
    if x == None:
        x = stage_sa()
    y = sa2hir.get_hir(x)
    return y


def stage_sir(x=None):
    if x == None:
        x = stage_hir()
    y = hir2sir.get_ext_sir(x)
    return y


def stage_cpp(x=None):
    if x == None:
        if g_sir_file is not None:
            x = get_sir_from_file(args.file)
        else:
            x = stage_sir()
    y = sir2cpp.get_sir_cpp(x, backend_name=backend_name, ofile_name=ofile_name)
    return y


def init_dispatcher():
    global dispatcher
    dispatcher = {}
    for k in list(globals().keys()):
        v = globals()[k]
        if callable(v) and k.startswith("stage_"):
            t = k[6:]
            dispatcher[t] = v


def dispatch(stage):
    stage_fun = dispatcher.get(stage)
    assert stage_fun != None
    y = stage_fun()
    return y


def output_code(code):
    if not ofile_name:
        return output_str(code.serialize())
    b = os.path.basename(ofile_name)
    d = os.path.dirname(ofile_name)
    if b.endswith(".cpp"):
        head = b[:-4]
    elif b.endswith(".cu"):
        head = b[:-3]
    else:
        head = b
    if code.c_itf_is_cuda:
        c_itf_suffix = ".cu"
    else:
        c_itf_suffix = ".cpp"
    f_itf_name = (d + "/" if d else "") + "f_itf_" + head + ".f90"
    c_itf_name = (d + "/" if d else "") + "c_itf_" + head + c_itf_suffix
    cpp_code_lines = code.inject_extension_code()

    output_str("\n".join(cpp_code_lines), ofile_name)
    if code.c_itf_code:
        output_str(code.c_itf_code, c_itf_name)
    if code.f_itf_code:
        output_str(code.f_itf_code, f_itf_name)


def output_tree(x):
    if enable_json:
        if ofile_name and hasattr(x, "write_json") and callable(x.write_json):
            x.write_json(ofile_name)
        elif hasattr(x, "json_str") and callable(x.json_str):
            output_str(x.json_str(), ofile_name)
        else:
            print("json output not supported for stage", max_stage, file=sys.stderr)
            exit(1)
    elif ofile_name and hasattr(x, "write_str") and callable(x.write_str):
         x.write_str(ofile_name)
    elif have_sir2cpp and isinstance(x, sir2cpp.Code):
        output_code(x)
    elif hasattr(x, "get_output_str"):
        output_str(x.get_output_str(), ofile_name)
    elif isinstance(x, DispFormGen):
        output_str(disp_str(x), ofile_name)
    elif isinstance(x, str):
        output_str(x, ofile_name)
    elif hasattr(x, "str") and callable(x.str):
        output_str(x.str(), ofile_name)
    elif hasattr(x, "__str__"):
        output_str(str(x), ofile_name)
    else:
        assert False, "bad case"


def check_installation():
    cdsl_dir = get_cdsl_dir()
    for dir_var in ("CLANG_LIBRARY_PATH",):
        d = os.environ.get(dir_var) or general_error(dir_var + " not set.")
        if not os.path.isdir(d):
            general_error("Directory", d, "not found.")


@atexit.register
def cleanup_atexit():
    for f in g_tmp_files:
        if os.path.exists(f):
            os.remove(f)


def get_min_stage(args):
    f = args.file
    if f.endswith(".cpp"):
        return "clang_filter"
    elif f.endswith(".sir"):
        return "cpp"
    # give up:
    user_error("Cannot guess min_stage from arguments.")


def get_max_stage(args):
    if args.s:
        return args.s.lower()
    if args.o:
        ofile = args.o.lower()
        if ofile.endswith(".cpp") or ofile.endswith(".cu"):
            return "cpp"
        else:
            user_error("Cannot guess stage from outputfile:", ofile)
    # give up:
    user_error("Cannot guess max_stage from arguments.")


def get_sir_from_file(sir_fname):
    from dawn4py.serialization import SIR
    from google.protobuf import json_format, text_format

    with open(sir_fname, "r") as f:
        sir_json = f.read()
    sir_msg = None
    try:
        sir_msg = json_format.Parse(sir_json, SIR.SIR_pb2.SIR())
    except:
        pass
    try:
        sir_msg = text_format.Parse(sir_json, SIR.SIR_pb2.SIR())
    except:
        pass

    if sir_msg is None:
        general_error("Cannot parse SIR file", sir_fname)

    return sir_msg


if __name__ == "__main__":
    args = get_args()

    ofile_name = args.o or ""
    frontend_dir = get_frontend_dir()
    include_dirs = [frontend_dir + "/include"]
    if args.I:
        include_dirs.extend(args.I)
    enable_json = args.json
    init_dispatcher()
    stages = [*dispatcher.keys()]
    min_stage = get_min_stage(args)
    max_stage = get_max_stage(args)
    if not min_stage in stages:
        user_error("Invalid start stage:", min_stage, "must be one of:", stages)
    if not max_stage in stages:
        user_error("Invalid end stage:", max_stage, "must be one of:", stages)

    if min_stage == "clang_filter":
        g_cdsl_file = args.file
    elif min_stage == "cpp":
        g_sir_file = args.file
        if not max_stage == "cpp":
            user_error("Invalid end stage:", max_stage)

    if max_stage == "cpp":
        # todo: maybe this check should be moved to the respective stage:
        if hir2sir.CDSL_DEBUG_SIR_STAGE:
            user_error(
                "Cannot enter cpp or cu stage with enabled environment variable CDSL_DEBUG_SIR_STAGE."
            )
            if enable_json:
                user_error(
                    "json output format is incompatible with environment variable CDSL_DEBUG_SIR_STAGE."
                )

    # backend selection:
    if args.b is not None:
        backend_name = args.b
    else:
        backend_name = os.environ.get("CDSL_BACKEND") or None
    if backend_name is not None:
        backend_name = backend_name.lower()
    if max_stage == "cpp" and backend_name is None:
        user_error("Unspecified backend")
        if not backend_name in ("cxxnaive", "cxxnaiveico", "cuda", "cudaico"):
            user_error("Error: Invalid backend name for cpp stage: ", backend_name)
        if backend_name in ("cuda", "cudaico"):
            if ofile_name and not ofile_name.endswith(".cu"):
                # todo: we should rather use nvcc -x ...
                user_error("Given output filename of cuda stage does end with '.cu'")
        elif backend_name in ("cxxnaive", "cxxnaiveico"):
            if ofile_name and not ofile_name.endswith(".cpp"):
                user_error("Given output filename of cuda stage does end with '.cpp'")

    output_tree(dispatch(max_stage))
