# Distribution: see LICENSE.txt
from os.path import dirname, basename
import dawn4py
from common import *
from collections.abc import Iterable
import re
from sir2itfir import Itfir
import itfir2c
import itfir2f


# global consts:

g_xinc_loc_re = re.compile(r"^\s*(namespace\s+dawn_generated|\#\s*include)")
g_cdecl_loc_re = re.compile(r"^\s*(using\s+namespace\s+gridtools::dawn;)")


backend_name2backend = {
    "cxxnaive": dawn4py.CodeGenBackend.CXXNaive,
    "cuda": dawn4py.CodeGenBackend.CUDA,
    "cxxnaiveico": dawn4py.CodeGenBackend.CXXNaiveIco,
    "cudaico": dawn4py.CodeGenBackend.CUDAIco,
}

# result type


class Code:
    def __init__(
        self,
        cpp,
        c_itf_code=None,
        f_itf_code=None,
        edsl_extension_src=None,
        extra_includes=None,
        extra_c_decls=None,
        is_cuda=None,
        c_itf_is_cuda=None,
    ):
        self._cpp = cpp
        self.c_itf_code = c_itf_code
        self.f_itf_code = f_itf_code
        self.edsl_extension_src = edsl_extension_src
        self.extra_includes = extra_includes
        self.extra_c_decls = extra_c_decls
        self.is_cuda = is_cuda or False
        self.c_itf_is_cuda = c_itf_is_cuda or False
        self.lines = cpp.split("\n")

    def get_extension_code(self):
        if self.edsl_extension_src is None:
            return ""
        a = (
            (
                [
                    "// --- start(added cdsl extension code) ---",
                    self.edsl_extension_src,
                    "using namespace edsl_extension;",
                    "// --- end(added cdsl extension code) ---",
                ]
            )
            if self.edsl_extension_src
            else []
        )
        s = "\n".join(a)
        return s

    def get_extra_c_decls_code(self):
        if self.extra_c_decls is None:
            return ""
        a = (
            (
                [
                    "// --- start cdsl added code (extra_c_decls) ---",
                    *self.extra_c_decls,
                    "// --- end cdsl added code (extra_c_decls) ---",
                ]
            )
            if self.extra_c_decls
            else []
        )
        s = "\n".join(a)
        return s

    def get_include_code(self):
        if self.extra_includes is None:
            return
        a = [
            "// --- start(added cdsl include directives) ---",
            *["#include " + x for x in self.extra_includes],
            "// --- end(added cdsl include directives) ---",
        ]
        return "\n".join(a)

    def inject_extension_code(self):
        ext_code = self.get_extension_code()
        include_code = self.get_include_code()
        c_decls_code = self.get_extra_c_decls_code()
        if not (ext_code or include_code or c_decls_code):
            return self.cpp
        xinc = []
        if include_code:
            xinc.append(include_code)
        if ext_code:
            xinc.append(ext_code)
        a = self.lines
        xinc_pos = None
        cdecl_pos = None
        b = []
        for p in range(len(a)):
            line = a[p]
            match = g_xinc_loc_re.match(line)
            if match:
                if include_code:
                    b.append(include_code)
                    include_code = None
            match = g_cdecl_loc_re.match(line)
            if match:
                if c_decls_code:
                    b.append(c_decls_code)
                    c_decls_code = None
                if ext_code:
                    b.append(ext_code)
                    ext_code = None
            b.append(line)
        self.lines = b
        return b

    def inject_extra_includes(self):
        if self.extra_includes is None:
            return self.cpp
        include_code = "\n#include ".join(self.extra_includes)

    def serialize(self):
        c_itf_part = [
            "#ifdef REQUIRE_C_INTERFACE",
            self.c_itf_code or "",
            "#endif",
        ]
        f_itf_part = [
            # this is just for inspection
            "#ifdef 0",
            "// Fortran interface:",
            self.f_itf_code or "",
            "#endif",
        ]
        a = self.lines
        if self.c_itf_code:
            a.extend(c_itf_part)
        if self.f_itf_code:
            a.extend(f_itf_part)
        s = "\n".join(a)
        return s


# main entry:


def get_sir_cpp(sir_or_ext_sir, backend_name, ofile_name=None, have_cdsl_support=True):

    if backend_name is None:
        die("bad case")

    bname = backend_name.lower()

    if hasattr(sir_or_ext_sir, "get_sir"):
        sir = sir_or_ext_sir.get_sir()
    else:
        sir = sir_or_ext_sir

    itfir = Itfir.fromSIR(sir, bname, ofile_name, have_cdsl_support)
    c_itf = itfir2c.gen_sir_c_interface(itfir)
    f_itf = itfir2f.gen_sir_f_interface(itfir)

    is_cuda_backend = itfir.is_cuda_backend
    backend = backend_name2backend[bname]

    if hasattr(sir_or_ext_sir, "get_edsl_extension_src"):
        edsl_extension_src = sir_or_ext_sir.get_edsl_extension_src()
    else:
        edsl_extension_src = None

    cpp_code = dawn4py.compile(sir, backend=backend)
    assert cpp_code
    extra_c_decls = None
    extra_includes = ["<cmath>"]
    if edsl_extension_src:
        if is_cuda_backend:
            edsl_extension_src = (
                "#define EDSL_EXTENSION_FUN __device__\n" + edsl_extension_src
            )
        else:
            edsl_extension_src = (
                "#define EDSL_EXTENSION_FUN /**/\n" + edsl_extension_src
            )

    LibTag = c_itf.LibTag
    dawn_c_decl_list = c_itf.dawn_c_decl_list

    if itfir.grid_type_str == "Cartesian":
        if is_cuda_backend:
            extra_includes.extend(
                [
                    "<gridtools/storage/storage_traits_cuda.hpp>",
                    '"driver-includes/timer_cuda.hpp"',
                ]
            )
        code = Code(
            cpp_code,
            c_itf.code,
            f_itf.code,
            edsl_extension_src=edsl_extension_src,
            extra_includes=extra_includes,
            is_cuda=is_cuda_backend,
            c_itf_is_cuda=is_cuda_backend,
        )
    elif itfir.grid_type_str == "Unstructured":
        if LibTag == "atlasInterface::atlasTag":
            extra_includes.append('"interface/atlas_interface.hpp"')
        elif LibTag is not None:
            die("unknown g_LibTag: " + LibTag)
        if dawn_c_decl_list:
            extra_c_decls = itfir2c.serialize(
                [
                    'extern "C" {',
                    dawn_c_decl_list,
                    "}",
                ]
            )
        code = Code(
            cpp_code,
            c_itf.code,
            f_itf.code,
            edsl_extension_src=edsl_extension_src,
            extra_includes=extra_includes,
            extra_c_decls=extra_c_decls,
            is_cuda=is_cuda_backend,
        )
    else:
        die("Unknown grid type:", sir.gridType)

    return code
