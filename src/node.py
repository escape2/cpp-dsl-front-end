#!/usr/bin/env python3
# Distribution: see LICENSE.txt

from enum import Enum
from collections import OrderedDict
import json
from common import *

show_internal_attr = 1
pfi = "  "


class Node(DispFormGen):
    """ Base class for HIR and SIR stages """

    def __init__(self, node_type):
        self._node_type = node_type

    def addc(self, f):
        f_is_lot = is_lot(f)
        if hasattr(self, "_children"):
            if f_is_lot:
                self._children.expand(f)
            else:
                self._children.append(f)
        else:
            if f_is_lot:
                self._children = f
            else:
                self._children = [f]

    def getc(self):
        return getattr(self, "_children", [])

    def assert_node_type(self, node_type):
        assert self._node_type == node_type

    def str(self):
        return Node__str(self)

    def nt_is(self, nodetype):
        return self._node_type == nodetype

    def to_dict(self):
        return Node__to_dict(self)

    def json_str(self):
        d = self.to_dict()
        return json.dumps(d, indent=1)


def Node__to_dict(f):
    """ implementation of Node.to_dict """
    if is_lot(f):
        a = []
        for x in f:
            g = Node__to_dict(x)
            a.append(g)
        return a

    if isinstance(f, str):
        return f

    if isinstance(f, (int, float)):
        die("unexpected case: f = " + str(f))

    # handle Node objects:
    assert isinstance(f, Node)
    keys = f.__dict__.keys()
    d = OrderedDict()

    # order matters: first we gather simple attributes
    rest_kv = []
    for k in sorted(keys):
        v = getattr(f, k)
        if isinstance(v, (str, float, int, bool)):
            d[k] = v
        elif isinstance(v, Enum):
            d[k] = str(v)
        elif v is None:
            # We filter out attributes with None value.
            pass
        else:
            rest_kv.append((k, v))

    # complex entries follow:
    for (k, v) in rest_kv:
        if is_lot(v):
            d[k] = Node__to_dict(v)
        elif isinstance(v, Locus):
            # ignore irrelevant entries
            pass
        elif hasattr(v, "to_dict"):
            tt1 = v.to_dict()
            d[k] = v.to_dict()
        else:
            die("unexpeced case")
    return d


def Node__str(f, prefix=""):
    """ implementation of Node.str """
    if isinstance(f, (str, float, int, bool)):
        return prefix + str(f)
    s = ""
    if is_lot(f):
        if not f:
            return prefix + "[]"
        a = []
        for x in f:
            a.append(Node__str(x, prefix + pfi))
        s = "\n".join(a)
        return s
    if f == None:
        return "BAD VALUE: None"
    if not isinstance(f, Node):
        return prefix + str(f)
    nt = f._node_type
    keys = f.__dict__.keys()
    alist = []
    rlist = []
    for k in keys:
        if k.startswith("_") and not show_internal_attr:
            continue
        a = getattr(f, k)
        if k == "_node_type":
            continue
        elif isinstance(a, (str, float, int, bool)):
            alist.append(k + ": " + str(a))
        else:
            rlist.append((k, a))
    s += prefix + nt + ": {"
    prefix2 = prefix + pfi
    sep = ", "
    if alist:
        s += "\n" + prefix2 + sep.join(alist)
    if rlist:
        for r in rlist:
            (k, a) = r
            if isinstance(a, Node) and k == a._node_type:
                s += "\n" + Node__str(a, prefix2)  # avoid double keys
            elif k == "_children":
                s += "\n" + Node__str(a, prefix)
            elif is_lot(a):
                if not a:
                    s += "\n" + prefix2 + k + ": " + str(a)
                else:
                    s += "\n" + prefix2 + k + ": ["
                    s += "\n" + Node__str(a, prefix2) + " ]"
            else:
                s += "\n" + prefix2 + k + ":"
                s += "\n" + Node__str(a, prefix2 + pfi)
    s += " }"
    return s
