#!/usr/bin/env python3
# Distribution: see LICENSE.txt

import sys
import json
from collections import OrderedDict
import argparse
import configparser
import dim_config
import srcgen_common as sgc

forward_decl = {}
declared = {}

abuse_comma_operator = False

subset_method_tail = " subset(const bool cond) const"


def add_forward_decl(c):
    global forward_decl
    if not c in declared:
        forward_decl[c] = 1


def declare_class(c):
    global declared
    declared["class " + c] = 1


def iterator_methods(iter_name, element_name):
    return [
        "bool operator != (const " + iter_name + "& other)",
        "const " + iter_name + "& operator++ ()",
        iter_name + " begin () const",
        iter_name + " end () const",
        "const " + element_name + " operator* ()",
    ]


def gen_elem_class(elem_name):
    cname = elem_name
    members = [cname + "()"]
    members.extend(connected_sets(elem_name))
    c = {
        "type": "class",
        "name": cname,
        "members": members,
        "is_set": False,
        "is_direct": True,
        "inherits": "Direct_element_base",
    }
    declare_class(cname)
    return c


def connected_sets(origin):
    members = []
    for i in range(elem_num):
        s = sets[i]
        t = s + "_of_" + origin.lower()
        members.append("const " + t + "& " + s.lower())
        add_forward_decl("class " + t)
    return members


def gen_elem_of_elem_class(i0, i1):
    e0 = elements[i0]
    e1 = elements[i1]
    cname = e1 + "_of_" + e0.lower()
    members = [cname + "()"]
    members.append("operator " + e1 + "()")
    add_forward_decl("class " + e1)
    members.extend(connected_sets(e0))
    c = {
        "type": "class",
        "name": cname,
        "members": members,
        "is_set": False,
        "is_direct": False,
        "inherits": "Indirect_element_base",
        "link_type0": e0,
        "link_type1": e1,
    }
    declare_class(cname)
    return c


def gen_set_class(i0):
    e0 = elements[i0]
    s0 = sets[i0]
    cname = s0
    members = [cname + "()"]
    members.extend(iterator_methods(cname, e0))
    members.extend(connected_sets(s0))
    members.append(cname + " where(const Field_bool mask) const")
    members.append("Sparse_" + cname.lower() + " via(const Set_expr s0) const")
    members.append(
        "Sparse_" + cname.lower() + " via(const Set_expr s0, const Set_expr s1) const"
    )
    members.append(
        "Sparse_"
        + cname.lower()
        + " via(const Set_expr s0, const Set_expr s1, const Set_expr s2) const"
    )
    if abuse_comma_operator:
        members.append("Field_key operator , (const Field_key& k0)")
    c = {
        "type": "class",
        "name": s0,
        "members": members,
        "is_set": True,
        "is_dense": True,
        "inherits": "Dense_set_base",
        "element_type": e0,
        "domain_set_var": s0.lower(),
    }
    declare_class(cname)
    return c


def gen_sets_of_elem_class(i0, i1):
    e0 = elements[i0]
    s1 = sets[i1]
    cname = s1 + "_of_" + e0.lower()
    iter_elem = elements[i1] + "_of_" + elements[i0].lower()
    members = [cname + "()"]
    members.extend(iterator_methods(cname, iter_elem))
    add_forward_decl("class " + iter_elem)
    members.extend(connected_sets(s1))
    c = {
        "type": "class",
        "name": cname,
        "members": members,
        "is_set": True,
        "is_dense": False,
        "inherits": "Sparse_" + s1.lower(),
        "element_type": iter_elem,
        "domain_set_var": cname.lower(),
        "link_type0": e0,
        "link_type1": s1,
    }
    declare_class(cname)
    return c


def gen_sets_of_any_elem_class(i1):
    s1 = sets[i1]
    e1 = elements[i1]

    cname = "Sparse_" + s1.lower()
    members = [cname + "()"]
    c = {
        "type": "class",
        "name": cname,
        "members": members,
        "is_set": True,
        "is_dense": False,
        "inherits": "Sparse_set_base",
        "element_type": "Sparse_" + e1.lower(),
        "domain_set_var": cname.lower(),
        "link_type0": "?",
        "link_type1": s1,
    }
    declare_class(cname)
    return c


def gen_sets_of_set_class(i0, i1):
    s0 = sets[i0]
    s1 = sets[i1]
    cname = s1 + "_of_" + s0.lower()
    members = [cname + "()"]
    members.append(cname + " via(const Set_expr s0) const")
    members.append(cname + " via(const Set_expr s0, const Set_expr s1) const")
    members.append(
        cname + " via(const Set_expr s0, const Set_expr s1, const Set_expr s2) const"
    )
    members.extend(connected_sets(s1))
    c = {"type": "class", "name": cname, "members": members, "inherits": "Dim_chain"}
    declare_class(cname)
    return c


def gen_chain_class_old(link_start, link_end):
    cname = link_end + "_of_" + link_start.lower()
    c = {"type": "class", "name": cname, "inherits": "Field_key"}
    declare("class " + cname)
    return c


def declare_domain_sets():
    for s in sets:
        c = s
        print("extern", c, c.lower() + ";")
        c = "Sparse_" + s.lower()
        print("extern", c, c.lower() + ";")
        for e in elements:
            c = s + "_of_" + e.lower()
            print("extern", c, c.lower() + ";")


def gen_classes():
    src = []
    for k in range(elem_num):
        src.append(gen_elem_class(elements[k]))
        src.append(gen_sets_of_any_elem_class(k))
        src.append(gen_set_class(k))

    for k0 in range(elem_num):
        for k1 in range(elem_num):
            src.append(gen_elem_of_elem_class(k0, k1))
            src.append(gen_sets_of_elem_class(k0, k1))
            src.append(gen_sets_of_set_class(k0, k1))
    return src


def print_empty_class(c, prefix=""):
    inh = ""
    if "inherits" in c:
        inh = " : public " + c["inherits"]
    print(prefix + "class", c["name"] + inh, "{};")


def old_print_class(c, prefix=""):
    if not "members" in c:
        return print_empty_class(c, prefix)
    inh = ""
    if "inherits" in c:
        inh = " : public " + c["inherits"]
    print(prefix + "class", c["name"] + inh, "{")
    print(prefix + "public:")
    pref = prefix + sgc.pfi
    for d in c["members"]:
        print(pref, d + ";")
    print(prefix + "};")
    print()


def gen_hpp_output():
    print("// Generated automatically by " + sys.argv[0] + " - do not edit.")
    print()
    for d in forward_decl:
        print(d + ";")
    print()
    for c in classes:
        if not "members" in c:
            sgc.print_empty_class(c)
    print()
    for c in classes:
        if "members" in c:
            sgc.print_class(c)
    declare_domain_sets()


def gen_json_output():
    info = classes
    for d in dim_conf:
        section = {"type": "dim_config", "name": d}
        conf = {}
        for k in dim_conf[d]:
            conf[k] = dim_conf[d][k]
        section["conf"] = conf
        info.append(section)
    section = {
        "type": "common",
        "conf": {"cdsl_itf_version": "no_version.cdsl_itf_version"},
    }
    info.append(section)
    print(json.dumps(info, indent=4))


def die(reason):
    print("error:", reason)
    exit(1)


def set_global_state():
    global sets, elements, elem_num, dim_conf
    global set_var, set_type, set_var2type, set_type2var
    dim_conf = dim_config.unstructured_dim_conf
    sets = []
    elements = []
    set_var = []
    set_var2type = {}
    set_type2var = {}
    for s in dim_conf:
        dim = dim_conf[s]
        sets.append(dim["set_type"])
        set_var.append(dim["domain_set_var"])
        elements.append(dim["element_type"])
    elem_num = len(elements)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-hpp", help="generates cpp header file", action="store_true")
    parser.add_argument("-json", help="generates json file", action="store_true")
    parser.add_argument(
        "-c", metavar="file", help="configuration file", action="store", required=True
    )
    args = parser.parse_args()
    config = configparser.ConfigParser()
    if not config.read(args.c):
        print("error: Cannot read file", args.c)
        parser.print_help()
        exit(1)
    dim_config.proc_config(config)
    set_global_state()
    classes = gen_classes()
    if args.hpp:
        gen_hpp_output()
    elif args.json:
        gen_json_output()
