#!/usr/bin/env python3
# Distribution: see LICENSE.txt

import os
import re
from inspect import currentframe, getframeinfo
import json
from enum import Enum, IntEnum
from copy import copy
import cdsl_config
from common import *

sparse_composition_pattern_str = r"^(\w+)_of_(\w+)$"
sparse_composition_pattern = re.compile(sparse_composition_pattern_str)

g_dsl_types = {}
g_dsl_vars = {}
g_dsl_vset = None
g_dsl_vdim = None
g_basic_DSL_vars = None
g_basic_dsl_entities = None


class Space(Enum):
    # orthogonal decomposition of global iteration space
    extra = 0  # not part of 3d space, e.g., tracer, or other cartesian dimensions
    horizontal = 1
    vertical = 2


class MeshType(Enum):
    structured = 0
    unstructured = 1


class BasicDSLEntities:
    have_inst = False

    def __init__(self):
        assert not self.have_inst
        self.var_counter = 0
        self.name_space = {}
        self.have_inst = True

    def gen_bid(self, name):
        assert self.name_space.get(name) is None
        self.var_counter += 1
        self.name_space[name] = self.var_counter
        return self.var_counter


class ElmType(DispFormGen):
    # Eelement DSL Type, e.g.: Cell, Edge
    def __init__(self, elm_type_name):
        self.name = elm_type_name
        self.set_type = None  # to be set later

    def matches(self, other):
        return isinstance(other, ElmType) and self.name == other.name

    def gen_display_form(self):
        return Display_form(self, only="name")

    @classmethod
    def gen_any_type(cls):
        return cls("?")


class SetType(DispFormGen):
    # set DSL Type
    def __init__(self, type_name, mesh_type, space, is_basic=False):
        self.name = type_name
        assert isinstance(mesh_type, MeshType)
        self.mesh_type = mesh_type
        assert isinstance(space, Space)
        self.space = space
        self.elm_type = None  # iteration element type
        self.related_set_var = None
        self.bid = g_basic_dsl_entities.gen_bid(type_name) if is_basic else None

    def matches(self, other):
        if not isinstance(other, SetType):
            return False
        return self is other or self.name == "*" or other.name == "*" or self == other

    def gen_display_form(self):
        return Display_form(self, only="name")

    def is_struct(self):
        return self.mesh_type == MeshType.structured

    def is_unstruct(self):
        return self.mesh_type == MeshType.unstructured

    def get_elm_type(self):
        return self.elm_type

    def get_name(self):
        return self.name

    def get_struct_hdim_kind(self):
        return struct_hdim_kind.get(self.name)  # None is ok

    def is_any_SetType(self):
        return self.type_name == "*"

    def gen_var(self, name, is_basic=False):
        svar = SetVar(name, self, is_basic=is_basic)
        assert self.related_set_var is None
        self.related_set_var = svar
        return svar

    @classmethod
    def gen_struct_type(cls, type_name, space):
        return cls(
            type_name=type_name,
            mesh_type=MeshType.structured,
            space=space,
            is_basic=True,
        )

    @classmethod
    def gen_unstruct_type(cls, type_name, is_basic=False):
        return cls(
            type_name=type_name,
            mesh_type=MeshType.unstructured,
            space=Space.horizontal,
            is_basic=is_basic,
        )

    @classmethod
    def gen_any_type(cls):
        return cls(
            type_name="*",
            mesh_type=MeshType.unstructured,
            space=Space.horizontal,
            is_basic=False,
        )


class ElmLinkedSetType(SetType):
    # e.g.: Cells_of_Edge
    # This DSL set type is attached to a certain DSL element type
    # We store the related set type of that element type and use the
    # element type only in the name.
    def __init__(self, type_name, orig_elm_type, iter_set_type):
        super().__init__(
            type_name, mesh_type=MeshType.unstructured, space=Space.horizontal
        )
        if orig_elm_type != None:
            assert isinstance(orig_elm_type, ElmType)
        self.orig_set_type = orig_elm_type.set_type or die(
            "undefined elm_type -> set_type relation"
        )
        self.iter_set_type = iter_set_type
        # The type_name argument is redundant and currently only required to confirm the naming scheme:
        orig_elm_type = self.orig_set_type.elm_type or die(
            "undefined set_type -> elm_type relation"
        )
        if orig_elm_type.name == "?":
            name = "Sparse_" + self.iter_set_type.name.lower()
        else:
            name = self.iter_set_type.name + "_of_" + orig_elm_type.name.lower()
        if type_name != name:
            die("inconsistent naming scheme")

    def get_elm_type(self):
        return self.iter_set_type.get_elm_type()

    def matches(self, other):
        if not isinstance(other, SElmLinkedSetType):
            return False
        return self.iter_set_type.matches(
            other.iter_set_type
        ) and self.orig_set_type.matches(other.orig_set_type)

    def get_name(self):
        return self.name

    def gen_var(self, name, is_basic=False):
        assert not is_basic
        return ElmLinkedSetVar(name, self)


class Expr(DispFormGen):
    pass


class ElmExpr(Expr):
    pass


class SetExpr(Expr):
    pass


class FieldExpr(Expr):
    pass


class Var(Expr):
    def __init__(self, var_name, var_type):
        self.name = var_name
        self.t = var_type

    def str(self):
        return self.t.name + " " + self.name

    def get_display_form(self):
        return Display_form(self)

    def get_mesh_type(self):
        return self.t.mesh_type


class ElmVar(Var, ElmExpr):
    def __init__(self, var_name, var_type):
        assert isinstance(var_type, ElmType)
        super().__init__(var_name, var_type)
        self.related_set = None  # to be defined later

    def is_struct(self):
        return self.t.is_struct()

    def is_unstruct(self):
        return self.t.is_unstruct()

    def get_name(self):
        return self.name


class SetVar(Var, SetExpr):
    def __init__(self, var_name, var_type, is_basic=False):
        assert isinstance(var_type, SetType)
        super().__init__(var_name, var_type)
        self.related_elm = None  # to be defined later
        self.bid = g_basic_dsl_entities.gen_bid(var_name) if is_basic else None

    def is_struct(self):
        return self.t.is_struct()

    def is_unstruct(self):
        return self.t.is_unstruct()

    def is_horizontal(self):
        return self.t.space == Space.horizontal

    def is_vertical(self):
        return self.t.space == Space.vertical

    def get_name(self):
        return self.name

    def get_elm_type(self):
        return self.t.get_elm_type()

    def get_iter_set(self):
        return self

    def is_simple(self):
        return not bool(getattr(self, "orig", ()))

    def get_chain(self):
        return [self]

    def matches(self, other):
        if isinstance(other, SetVar):
            if self.bid is not None:
                return self.bid == other.bid
            ICE()
        elif isinstance(other, ExtendedSetExpr):
            if len(other.chain) > 1:
                return False
            return self.matches(other.iter_chain[0])
        elif isinstance(other, UnifiedSetExpr):
            return other.matches(self)
        else:
            die("unsupported case")

    def get_struct_hdim_kind(self):
        return self.t.get_struct_hdim_kind()


def gen_unstruct_SetVar(var_name, var_type):
    assert var_type.is_unstruct()
    return SetVar(var_name, var_type)


def match_set_lists(ref, tst, start_is_dense):
    if start_is_dense:
        # all test entries must match
        n = len(tst)
        if n > len(ref):
            return False
    else:
        # maximal possible match is okay
        n = min(len(tst), len(ref))
    for p in range(n):
        if not tst[-1 - p].matches(ref[-1 - p]):
            return False
    return True


class LinkedSetExpr(SetExpr):
    # represents a list of (optionally) linked sets
    # split into two parts: orig and iter
    # a structured set can be described as a linked set without origin
    def __init__(self, set_or_set_seq, start_is_dense=False):
        if isinstance(set_or_set_seq, list):
            assert len(set_or_set_seq)
            set_seq = list(set_or_set_seq)
        else:
            set_seq = [set_or_set_seq]

        if set_seq[-1].is_struct():
            self.iter_set = set_seq[-1]
            self.orig_sets = ()
            self.name = self.iter_set.name
            return

        set_seq = [s for s in set_seq if s.is_unstruct()]
        set_expr = set_seq[0]
        start_orig_sets = getattr(set_expr, "orig_sets", [])
        if start_is_dense and start_orig_sets:
            invalidate(self)
            return

        ref_seq = start_orig_sets + [set_expr.get_iter_set()]
        for p in range(1, len(set_seq)):
            set_expr = set_seq[p]
            test_seq = getattr(set_expr, "orig_sets", ())
            if test_seq:
                m = match_set_lists(ref_seq, test_seq, start_is_dense=start_is_dense)
                if not m:
                    # die("test stop")
                    invalidate(self)
                    return
                if len(test_seq) > len(ref_seq):
                    if start_is_dense:
                        invalidate(self)
                        return
                    ref_seq = copy(test_seq)
            set_expr_iter_set = set_expr.get_iter_set()
            assert set_expr_iter_set.is_unstruct()
            ref_seq.append(set_expr_iter_set)
        self.iter_set = set_seq[-1].get_iter_set()
        self.orig_sets = ref_seq[:-1]
        if self.orig_sets:
            name_parts = [s.name for s in self.orig_sets + [self.iter_set]]
            self.name = "/".join(name_parts)
        else:
            self.name = self.iter_set.name

    def is_simple(self):
        return self.iter_set.is_simple() and not self.orig_set_seq

    def get_iter_set(self):
        return self.iter_set

    def is_struct(self):
        return self.iter_set.is_struct()

    def is_unstruct(self):
        return self.iter_set.is_unstruct()

    def is_uhd(self):
        return self.iter_set.is_unstruct() and not self.orig_sets

    def matches(self, other):
        if not self.iter_set.matches(other.iter_set):
            return False
        if self.is_unstruct():
            if len(self.orig_sets) != len(other.orig_sets):
                return False
            for p in range(len(self.orig_sets)):
                if not self.orig_sets[p].matches(other.orig_sets[p]):
                    return False
        return True

    def is_dense(self):
        return self.iter_set.is_struct() or self.is_uhd()

    def is_horizontal(self):
        return self.iter_set.is_horizontal()

    def is_sparse(self):
        return not self.is_dense()

    def is_vertical(self):
        return self.iter_set.is_vertical()

    def get_name(self):
        return self.name


class ElmLinkedSetVar(SetVar):
    def __init__(self, var_name, var_type):
        assert isinstance(var_type, ElmLinkedSetType)
        super().__init__(var_name, var_type)

    def is_simple(self):
        return False

    def get_iter_set(self):
        return self.t.iter_set_type.related_set_var

    def gen_linked_set_expr(self):
        iter_set = self.get_iter_set()
        orig_set = self.t.orig_set_type.related_set_var
        return LinkedSetExpr([orig_set, iter_set])


class ExtendedSetExpr(SetExpr):
    # represents a chain of sets that is aggregated to one "extended set"
    # each contributing set has lost its independend meaning
    # there is no orig part
    def __init__(self, set_chain):
        assert isinstance(set_chain, (list, tuple))
        assert len(set_chain) >= 1
        assert all(isinstance(x, SetVar) for x in set_chain)
        assert all(getattr(x, "bid", None) is not None for x in set_chain)
        self.chain = list(set_chain)
        self.name = ".".join([s.name for s in set_chain])

    def get_mesh_type(self):
        return MeshType.unstructured

    def is_struct(self):
        return self.chain[0].is_struct()

    def is_unstruct(self):
        return self.chain[0].is_unstruct()

    def get_name(self):
        return self.name

    def is_simple(self):
        return False

    def is_horizontal(self):
        return self.chain[0].is_horizontal()

    def is_vertical(self):
        return self.chain[0].is_vertical()

    def get_struct_hdim_kind(self):
        return self.chain[0].get_struct_hdim_kind()

    def get_elm_type(self):
        return self.chain[-1].get_elm_type()

    def get_chain(self):
        return self.chain

    def get_iter_set(self):
        return self

    def matches(self, other):
        if isinstance(other, SetVar):
            if len(self.chain) > 1:
                return False
            return self.chain[0].matches(other)
        elif isinstance(other, ExtendedSetExpr):
            sic = self.chain
            oic = other.chain
            if len(sic) != len(oic):
                return False
            for p in range(len(sic)):
                if not sic[p].matches(oic[p]):
                    return False
            return True
        else:
            ICE()


def tail_matching_set_lists(slist, tail):
    nt = len(tail)
    if nt > len(slist):
        return False
    for p in range(nt):
        if not slist[-1 - p].matches(tail[-1 - p]):
            return False
    return True


def gen_set_from_set_chain(chain):
    assert isinstance(chain, (list, tuple)) and chain
    if len(chain) == 1:
        return chain[0]
    return ExtendedSetExpr(chain)


class Dimension(LinkedSetExpr):
    def __init__(self, set_or_set_seq):
        super().__init__(set_or_set_seq, start_is_dense=True)

    def matches(self, other):
        assert isinstance(other, Dimension)
        return super().matches(other)

    def get_size(self):
        iter_set = self.get_iter_set()
        orig_sets = getattr(self, "orig_sets", ())
        if len(orig_sets) != 1:
            return None
        # TODO: replace names with properties
        if orig_sets[0].name != "edges":
            return None
        if iter_set.name in ("verts", "cells"):
            return 2
        return None

    #def peel_off_sparse_part(self):
    #    if self.is_dense(): return self
    #    new_dim = Dimension(self.orig_sets)
    #    return new_dim

def is_scalar_type(*, ct=None, uqt=None):
    return is_elem_type(ct=ct, uqt=uqt) or is_literal_type(ct=ct, uqt=uqt)


def is_literal_type(*, ct=None, uqt=None):
    if ct:
        return bool(literal_types_re.match(ct))
    elif uqt:
        return bool(uqt_literal_types_re.match(uqt))
    ICE()


def is_elem_type(*, ct=None, uqt=None):
    if ct:
        return bool(elem_types_re.match(ct))
    elif uqt:
        return bool(uqt_elem_types_re.match(uqt))
    ICE()


def is_compound_set_type(*, uqt=None):
    parts = uqt.split("_of_")
    if len(parts) < 2:
        return False
    caps = parts[0:1] + [p.capitalize() for p in parts[1:]]
    tst = [is_dense_set_type(uqt=p) for p in caps]
    return all(tst)


def is_set_type(*, ct=None, uqt=None):
    if ct:
        return bool(set_types_re.match(ct))
    elif uqt:
        return bool(uqt_set_types_re.match(uqt)) or is_compound_set_type(uqt=uqt)
    ICE()


def is_dense_set_type(*, uqt=None):
    if uqt:
        return bool(uqt_dense_set_types_re.match(uqt))
    ICE()


def is_map_type(*, ct=None, uqt=None):
    if ct:
        return bool(map_types_re.match(ct))
    elif uqt:
        return bool(uqt_map_types_re.match(uqt))
    die("internal error")


def is_scope_guard_type(*, ct=None, uqt=None):
    if ct:
        return bool(scope_guard_types_re.match(ct))
    elif uqt:
        return bool(uqt_scope_guard_types_re.match(uqt))
    return False


def is_invalid_type(v):
    if ("(" in v) or (")" in v):
        return True
    return False


def is_field_type(*, ct=None, uqt=None):
    if ct:
        return bool(field_types_re.match(ct))
    elif uqt:
        return bool(unq_field_types_re.match(uqt))
    die("internal error")


def is_assign_op(s):
    if assign_re.match(s):
        return True
    return False


def is_binop(s):
    if binop_re.match(s):
        return True
    return is_assign_op(s)


def is_unop(s):
    return s == "-"


def is_cmpop(s):
    if cmpop_re.match(s):
        return True
    return False


def is_subset_method(s):
    if subset_methods_re.match(s):
        return True
    return False


def eval_dim_config(dim_conf):
    global set_type2conf, set_type2mesh_type, elem_type2mesh_type
    assert isinstance(dim_conf, dict)
    set_type = dim_conf.get("set_type") or die("missing set_type in dim-config")
    assert not set_type in set_type2conf
    set_type2conf[set_type] = dim_conf
    mesh_type = dim_conf.get("mesh_type")
    set_type2mesh_type[set_type] = mesh_type
    elem_type = dim_conf.get("element_type")
    elem_type2mesh_type[elem_type] = mesh_type


def load_data():
    global set_types, dense_set_types, sparse_set_types
    global elem_types, direct_elem_types, indirect_elem_types, intrinsic_elem_types
    global literal_types, scalar_types, struct_types, unstruct_types
    global struct_hdim_conf, vdim_conf, unstruct_dim_conf, struct_dim_conf
    global sparse_connected_set_types, sparse_family_set_types
    global set_type2elm_type, elm_type2set_type
    global set_type2set_var, set_var2set_type, set_type2size
    global map_to_sparse_family
    global dim_type2dim_set_map
    global root_dir, set_type2link_types, elem_type2link_types
    global unstruct_dense_set_types
    global set_type2conf, set_type2mesh_type, elem_type2mesh_type
    global set_bounds

    literal_types = ["bool", "int", "float", "double"]
    src = [None, None]
    root_dir = cdsl_config.CDSL_ROOT
    fpath = root_dir + "/include/struct_dsl.json"
    if not os.path.exists(fpath):
        general_error("Cannot open " + fpath + " (autogenerated at build time).")
    with open(fpath, "r") as file:
        src[0] = json.load(file)
    fpath = root_dir + "/include/unstruct_dsl.json"
    if not os.path.exists(fpath):
        general_error("Cannot open " + fpath + " (autogenerated at build time).")
    with open(fpath, "r") as file:
        src[1] = json.load(file)

    set_type2conf = {}
    set_type2mesh_type = {}
    elem_type2mesh_type = {}
    unstruct_types = []
    struct_types = []
    dense_set_types = []
    sparse_set_types = []
    direct_elem_types = []
    indirect_elem_types = []
    unstruct_dim_conf = []
    struct_dim_conf = []
    struct_hdim_conf = []
    vdim_conf = None
    set_type2elm_type = {}
    elm_type2set_type = {}
    sparse_family_set_types = []
    sparse_connected_set_types = []
    map_to_sparse_family = {}
    set_type2set_var = {}
    set_var2set_type = {}
    unstruct_dense_set_types = []
    set_type2link_types = {}
    elem_type2link_types = {}
    set_type2size = {}
    set_bounds = {}
    for js in range(2):
        if js == 0:
            is_struct = True
        else:
            is_struct = False
        is_unstruct = not is_struct
        for c in src[js]:

            if c.get("type") == "common":
                d = c.get("conf")
                continue

            if c.get("type") == "dim_config":
                eval_dim_config(c.get("conf"))
                d = c.get("conf")
                if is_struct:
                    struct_dim_conf.append(d)
                    if d.get("is_horizontal_dim"):
                        struct_hdim_conf.append(d)
                    elif d.get("is_vertical_dim"):
                        vdim_conf = d
                else:
                    unstruct_dim_conf.append(d)
                continue

            if not "is_set" in c:
                continue

            cname = c["name"]
            if is_struct:
                struct_types.append(cname)
            else:
                unstruct_types.append(cname)
            if c["is_set"]:
                elem_type = c.get("element_type")
                assert elem_type
                bounds = c.get("bounds")
                if bounds:
                    set_bounds[cname] = bounds
                set_type2elm_type[cname] = elem_type
                elm_type2set_type[elem_type] = cname
                set_var = c.get("domain_set_var")
                assert set_var
                set_type2set_var[cname] = set_var
                set_var2set_type[set_var] = cname
                (lt0, lt1) = (c.get("link_type0"), c.get("link_type1"))
                if lt0 and lt1:
                    set_type2link_types[cname] = (lt0, lt1)
                else:
                    set_type2link_types[cname] = (None, cname)
                if c["is_dense"]:
                    dense_set_types.append(cname)
                    if is_unstruct:
                        unstruct_dense_set_types.append(cname)
                else:
                    if cname.startswith("Sparse_"):
                        sparse_family_set_types.append(cname)
                    else:
                        sparse_connected_set_types.append(cname)
                        inh = c.get("inherits", None)
                        if inh.startswith("Sparse_"):
                            map_to_sparse_family[cname] = inh
            else:
                assert c.get("is_set") == False
                (lt0, lt1) = (c.get("link_type0"), c.get("link_type1"))
                if lt0 and lt1:
                    elem_type2link_types[cname] = (lt0, lt1)
                if not "is_direct" in c:
                    continue
                if c["is_direct"]:
                    direct_elem_types.append(cname)
                else:
                    indirect_elem_types.append(cname)

    for c in struct_dim_conf:
        if c.get("is_horizontal_dim") or c.get("is_decomposed_dim"):
            continue
        dim = c.get("set_type")
        s = c.get("size")
        if not s:
            s = "#" + dim
        set_type2size[dim] = s

    elm_type2set_type["?"] = "*"
    set_type2elm_type["*"] = "?"

    for (st, lt) in set_type2link_types.items():
        (orig_dim, iter_dim) = lt
        mt = (elem_type2mesh_type.get(orig_dim), set_type2mesh_type.get(iter_dim))
        if mt[0] == "Edge":
            if mt[1] in ("Cell", "Vert"):
                set_type2size[st] = 2

    for d in set_type2size:
        s = set_type2size[d]

    sparse_set_types = sparse_family_set_types + sparse_connected_set_types
    elem_types = direct_elem_types + indirect_elem_types
    scalar_types = literal_types + elem_types
    set_types = dense_set_types + sparse_set_types


def gen_edsl_pattern(types):
    assert isinstance(types, (list, tuple)) and types
    if len(types) > 1:
        return r"^EDSL::(" + "|".join(types) + r")$"
    return r"^EDSL::" + types[0] + r"$"


def gen_maybeconst_edsl_pattern(types):
    return r"^(const )?EDSL::(" + "|".join(types) + r")$"


def gen_simple_pattern(types):
    return r"^(" + "|".join(types) + r")$"


def gen_maybeconst_simple_pattern(types):
    return r"^(const )?(" + "|".join(types) + r")$"


def init():
    global set_types_re, elem_types_re, literal_types_re, map_types_re, scope_guard_types_re
    global binop_re, assign_re, cmpop_re, field_types_re, subset_methods_re
    global unq_field_types_re, uqt_set_types_re, uqt_elem_types_re, uqt_literal_types_re
    global uqt_dense_set_types_re, uqt_sparse_set_types_re
    global uqt_map_types_re, uqt_scope_guard_types_re
    global struct_types_re, unstruct_types_re
    global unstruct_dense_set_types, unstruct_dense_set_types_re
    global struct_uqt_set, unstruct_uqt_set
    global dense_horizontal_dim_set
    global struct_hdim_kind

    load_data()

    struct_hdim_kind = {}
    tlist = []
    assert len(struct_hdim_conf) == 2
    for pos in range(2):
        conf = struct_hdim_conf[pos]
        assert conf.get("is_horizontal_dim")
        dim = conf.get("set_type")
        kind = conf.get("struct_hdim_kind")
        assert kind == pos
        struct_hdim_kind[dim] = kind
        tlist.append(dim)
    dense_horizontal_dim_set = set(tlist + unstruct_dense_set_types)

    tmp = []
    for ct in struct_types:
        if ct.startswith("Intrinsic_"):
            continue
        tmp.append(ct)
    struct_uqt_set = set(tmp)
    unstruct_uqt_set = set(unstruct_types)

    struct_types_re = re.compile(gen_simple_pattern(struct_types))
    unstruct_types_re = re.compile(gen_simple_pattern(unstruct_types))
    unstruct_dense_set_types_re = re.compile(
        gen_simple_pattern(unstruct_dense_set_types)
    )

    uqt_elem_types_re = re.compile(gen_simple_pattern(elem_types))
    elem_types_re = re.compile(gen_maybeconst_edsl_pattern(elem_types))

    uqt_set_types_re = re.compile(gen_simple_pattern(set_types))
    set_types_re = re.compile(gen_maybeconst_edsl_pattern(set_types))

    uqt_dense_set_types_re = re.compile(gen_simple_pattern(dense_set_types))
    uqt_sparse_set_types_re = re.compile(gen_simple_pattern(sparse_set_types))

    uqt_literal_types_re = re.compile(gen_simple_pattern(literal_types))
    literal_types_re = re.compile(gen_maybeconst_simple_pattern(literal_types))

    binop_list = (
        r"\+",
        r"\-",
        r"\*",
        r"\/",
        r"\=\=",
        r"\!\=",
        r"\>",
        r"\<",
        r"\>\=",
        r"\<\=",
    )
    binop_re = re.compile(r"^(" + "|".join(binop_list) + r")$")

    assign_list = (r"\+\=", r"\-\=", r"\*\=", r"\/\=", r"\=")
    assign_re = re.compile(r"^(" + "|".join(assign_list) + r")$")

    cmpop_list = (r"\=\=", r"\!\=", r"\>", r"\<", r"\>\=", r"\<\=")
    cmpop_re = re.compile(gen_simple_pattern(cmpop_list))

    # for simplicity we also classiy 'reversed' as subset method
    subset_methods = ("lt", "gt", "le", "ge", "interval", "where", "reversed")
    subset_methods_re = re.compile(gen_simple_pattern(subset_methods))

    field_types = (
        "Field",
        "Field_dp",
        "Field_sp",
        "Field_int",
        "Field_bool",
        "Field_lvl",
    )
    unq_field_types_re = re.compile(
        gen_simple_pattern(field_types)
    )  # unqualified field types
    field_types_re = re.compile(gen_edsl_pattern(field_types))

    map_types = ("Vertical_map",)
    uqt_map_types_re = re.compile(gen_simple_pattern(map_types))
    map_types_re = re.compile(gen_edsl_pattern(map_types))

    scope_guard_types = (
        "Intrinsics_scope_guard_type",
        "Vertical_region_scope_guard_type",
    )
    uqt_scope_guard_types_re = re.compile(gen_simple_pattern(scope_guard_types))
    scope_guard_types_re = re.compile(gen_edsl_pattern(scope_guard_types))


def init2():
    global g_dsl_types, g_dsl_vars, g_dsl_vset, g_dsl_vdim, g_basic_dsl_entities
    # we reorganize global information
    # migration is prepared here
    assert g_basic_dsl_entities is None
    g_basic_dsl_entities = BasicDSLEntities()

    # SimpleSetType & assiciated element types:
    sparse_types = []
    for (set_name, set_type_name) in set_var2set_type.items():
        if set_name == "*":
            continue
        is_simple = set_type_name in dense_set_types
        if is_simple:
            is_struct = set_type_name in struct_uqt_set
            if is_struct:
                if struct_hdim_kind.get(set_type_name) != None:
                    space = Space.horizontal
                elif vdim_conf["set_type"] == set_type_name:
                    space = Space.vertical
                else:
                    space = Space.extra
                my_set_type = SetType.gen_struct_type(set_type_name, space=space)
            else:
                my_set_type = SetType.gen_unstruct_type(set_type_name, is_basic=True)
            elm_type_name = set_type2elm_type.get(set_type_name) or ICE()
            my_elm_type = ElmType(elm_type_name)
            my_set_type.elm_type = my_elm_type
            my_elm_type.set_type = my_set_type
            assert not my_set_type.name in g_dsl_types
            g_dsl_types[set_type_name] = my_set_type
            assert not elm_type_name in g_dsl_types
            g_dsl_types[elm_type_name] = my_elm_type
        else:
            sparse_types.append((set_name, set_type_name))

    # support for unspecific cases:
    any_elm_type = ElmType.gen_any_type()
    any_set_type = SetType.gen_any_type()
    any_elm_type.set_type = any_set_type
    any_set_type.elm_type = any_elm_type
    g_dsl_types["?"] = any_elm_type
    g_dsl_types["*"] = any_set_type

    # ElmLinkedSetType:
    for (set_name, set_type_name) in sparse_types:
        if set_name == "*":
            continue  # AnySetType is not used to declare DSL variables
        (orig_elm_type_name, iter_set_type_name) = set_type2link_types.get(
            set_type_name
        )
        orig_set_type_name = elm_type2set_type[orig_elm_type_name]
        orig_elm_type = g_dsl_types.get(orig_elm_type_name) or ICE()
        iter_set_type = g_dsl_types.get(iter_set_type_name) or ICE()
        my_set_type = ElmLinkedSetType(set_type_name, orig_elm_type, iter_set_type)
        assert not my_set_type.name in g_dsl_types
        g_dsl_types[my_set_type.name] = my_set_type

    # dsl vars:
    for (type_name, my_type) in g_dsl_types.items():
        if isinstance(my_type, SetType):
            if type_name == "*":
                continue
            svar_name = set_type2set_var.get(type_name) or ICE()
            svar = my_type.gen_var(svar_name, is_basic=getattr(my_type, "bid", False))
            assert not svar_name in g_dsl_vars
            g_dsl_vars[svar_name] = svar
            if my_type.space == Space.vertical:
                g_dsl_vset = svar
    g_dsl_vdim = Dimension(g_dsl_vset)

    # dsl set bounds:
    for (set_type_name, bounds) in set_bounds.items():
        elm_type_name = set_type2elm_type[set_type_name]
        elm_type = g_dsl_types.get(elm_type_name) or ICE()
        for var_name in bounds:
            var = Var(var_name, elm_type)
            assert not var_name in g_dsl_vars
            g_dsl_vars[var_name] = var


def str2file(filename, s):
    f = open(filename, "w")
    f.write(s)
    f.close()


init()  # this needs work
init2()  # migration place
