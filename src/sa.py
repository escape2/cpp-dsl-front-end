#!/usr/bin/env python3
# Distribution: see LICENSE.txt

# Description: semantic analysis
#  - resolve field access abstraction
#  - check consistency of field declaration and field access
#  - generate missing iterations if required (compute_on, loop_on)


from inspect import currentframe, getframeinfo
import itertools
from enum import Enum
from copy import copy
from tvs import Tv, tdef, tqdef, tname, tv_str, nil_tv
from common import *
from clang_filter import reject
import parser_config
from parser_config import set_type2size, Dimension
from parser_config import (
    SetType,
    ElmType,
    g_dsl_vars,
    g_dsl_types,
    g_dsl_vset,
    SetVar,
    SetExpr,
    ExtendedSetExpr,
    LinkedSetExpr,
    gen_set_from_set_chain,
)


class FunPhase(Enum):
    no_fun_phase = 0
    specification_fun_phase = 1
    execution_fun_phase = 2


class Scope:
    def __init__(self, s_kind, s_id=None):
        self.space = {}
        self.s_id = s_id
        self.is_fun = False
        if s_id:
            self.name = s_kind + ":" + s_id
            if s_kind == "function":
                self.is_fun = True
        else:
            self.name = s_kind

    def set(self, k, v, *, required_update=False, possible_update=False):
        if required_update:
            if not k in self.space:
                die("internal error")
        elif not possible_update:
            if k in self.space:
                die(
                    "Shadowed declarations not suppoorted by current flat declaration space. Symbol:"
                    + str(k)
                )
        self.space[k] = v

    def get(self, k):
        return self.space.get(k, None)


class ExprContext:
    def __init__(self, is_lhs=False, is_rhs=False, dof=None):
        assert bool(is_lhs) != bool(is_rhs)
        self._is_rhs = bool(is_rhs)
        self._dof = dof or ()

    def is_rhs(self):
        return self._is_rhs

    def is_lhs(self):
        return not self._is_rhs

    def get_dof(self):
        return self._dof


class ExprContextStack:
    def __init__(self):
        global expr_context
        self.stack = []
        expr_context = None

    def push(self, context):
        global expr_context
        self.stack.append(context)
        expr_context = context

    def pop(self):
        global expr_context
        assert self.stack
        self.stack.pop()
        expr_context = self.get_cur_context()
        return expr_context

    def get_cur_context(self):
        if self.stack:
            return self.stack[-1]
        else:
            return None

    def is_active(self):
        return bool(self.stack)


def reset():
    global cur_scope, scopes, cur_fun
    global error_stack, fun_phase, expr_context_stack, expr_context
    cur_scope = Scope("start_scope")
    scopes = [cur_scope]
    cur_fun = None
    error_stack = []
    fun_phase = FunPhase.no_fun_phase
    expr_context_stack = ExprContextStack()
    expr_context = None


def get_var(k, default=None):
    for s in reversed(scopes):
        v = s.get(k)
        if v is not None:
            return v
    v = g_dsl_vars.get(k)
    if v:
        return v
    return default


def set_var(k, v, *, required_update=False, possible_update=False):
    global cur_scope
    if k in g_dsl_vars:
        die("Overriding pre-configured DSL variables is not supported yet")
    return cur_scope.set(
        k, v, required_update=required_update, possible_update=possible_update
    )


def get_impl_iter_sets_ref():
    ref = get_var("$impl_iter_sets")
    if ref is None:
        set_var("$impl_iter_dims", [])
        ref = get_var("$impl_iter_sets")
        assert ref is not None
    return ref


def get_impl_iter_sets_copy():
    ref = get_var("$impl_iter_sets")
    if ref:
        return copy(ref)
    else:
        return []


def set_impl_iter_sets(set_list):
    dim_list = []
    for p in range(len(set_list)):
        dim = Dimension(set_list[: p + 1])
        if is_invalid(dim):
            loc_err(
                "Invalid sequence of sets: " + ", ".join([s.name for s in set_list])
            )
        dim_list.append(dim)
    set_var("$impl_iter_dims", dim_list, possible_update=True)
    return set_var("$impl_iter_sets", set_list, possible_update=True)


def push_scope(s_kind, s_id=None):
    global cur_scope, cur_fun
    cur_scope = Scope(s_kind, s_id)
    scopes.append(cur_scope)
    if cur_scope.is_fun:
        cur_fun = s_id
    return cur_scope


def pop_scope():
    global cur_scope, cur_fun
    if len(scopes) < 2:
        die("internal error")
    last_scope = scopes.pop()
    cur_scope = scopes[-1]
    if last_scope.is_fun:
        cur_fun = None
    return last_scope


def resolve_gridspace_args(raw_args):
    args = []
    for x in raw_args:
        if x.t == tdef.gridspace_ref:
            y = dispatch(x)
            gs_var = y.res_gridspace_decl
            gs_specs = gs_var.dim_specs
            args.extend(gs_specs)
        else:
            args.append(x)
    return args


def resolve_retyped_field_refs(raw_refs):
    refs = []
    for f in raw_refs:
        if f.t == tdef.retyped_field_ref:
            g = dfun_retyped_field_ref(f, redispatch=False)
            refs.append(g)
        else:
            refs.append(f)
    return refs


def dfun_field_spec(f):
    assert f.t == tdef.field_spec
    raw_args = getattr(f, "args", ())
    args = resolve_gridspace_args(raw_args)
    if not args:
        ICE()
    set_expr_list = []
    for arg_pos in range(len(args)):
        dkey = args[arg_pos]
        if dkey.tq != tqdef.set:  # or dkey.t != tdef.var_ref:
            error("bad declaration argument")
        set_expr = match_embedded_set(dkey) or ICE()
        set_expr_list.append(set_expr)
    # check if we have a homogeneous pseudo-field declaration
    g = copy(f)
    if len(set_expr_list) == 1:
        orig_sets = getattr(set_expr_list[0], "orig_sets", ())
        if len(orig_sets):
            # this makes it a complete homogeneous pseudo field
            set_expr_list = orig_sets + set_expr_list
            g.is_hfield = True
    elif len(set_expr_list) > 1:
        orig_sets = getattr(set_expr_list[0], "orig_sets", ())
        if len(orig_sets):
            error("invald field declaration")
    dims = []
    for p in range(len(set_expr_list)):
        d = Dimension(set_expr_list[: p + 1])
        if is_invalid(d):
            error("Invalid set expression in declaration: " + args[p].id)
        dims.append(d)
    g._decl_dims = dims
    return g


def gen_field_spec(args, locus):
    f = Tv(tdef.field_spec, locus=locus)
    f.args = args
    return f


def dfun_field_spec_stmt(f):
    field = getattr(f, "field", None) or die("internal error")
    if field.t == tdef.retyped_field_ref:
        error(
            "User-typed field "
            + field.id
            + " has to be specified using its related type definition."
        )
    if field.t != tdef.var_ref or field.tq != tqdef.field:
        die("internal error")
    f_id = getattr(field, "id", None) or die("internal error")
    if getattr(f, "field_spec", None):
        error("field is already specified")
    args = getattr(f, "dim_specs", ()) or error("invalid field_spec")
    field_spec = dfun_field_spec(gen_field_spec(args, f.locus))
    decl = get_var(f_id) or die("internal error: name lookup failed")
    if getattr(decl, "expr", None) is not None:
        error("Unexpected initialization expression.")
    decl.field_spec = field_spec
    # attach new attributes to the already known decl object:
    decl._decl_dims = field_spec._decl_dims
    # return value probably not really required
    g = copy(f)
    g.res_decl = decl
    return g


def dfun_set_method(f):
    assert f.t == tdef.set_method
    expr = getattr(f, "expr", None) or error("missing set expression")
    if expr.tq != tqdef.set:
        error("unsupported expression")
    ctp = getattr(expr, "_ctp") or die("internal error")
    dof = getattr(ctp, "uqt", None) or die("internal error")
    f._dof = [dof]
    return f


def match_set_chain(f):
    if f.tq != tqdef.set:
        return []
    svar = g_dsl_vars.get(f.id) or ICE()
    if isinstance(svar, parser_config.ElmLinkedSetVar):
        # We want to get rid of ElmLinkedSetVar and use LinkedSetExpr instead
        svar = svar.gen_linked_set_expr()
    chain = [svar]
    orig = getattr(f, "orig", None)
    if orig:
        orig_chain = match_set_chain(orig) or ICE()
        chain = orig_chain + chain
    return chain


def match_embedded_set(f):
    if f.tq != tqdef.set:
        return []
    if f.t == tdef.var_ref:
        chain = match_set_chain(f)
        chain_set = set_chain2set_expr(chain)
        return chain_set
    expr = getattr(f, "expr", None) or ICE()
    chain = match_set_chain(expr)
    chain_set = set_chain2set_expr(chain)
    icons = getattr(f, "iter_context", None) or ICE()
    assert isinstance(icons, list)
    icon_sets = [match_embedded_set(x) for x in icons]
    emb_set = LinkedSetExpr(icon_sets + [chain_set])
    return emb_set


def dfun_embedded_set(f):
    assert f.t == tdef.embedded_set and f.tq == tqdef.set
    return match_embedded_set(f)


def set_chain2set_expr(set_chain):
    assert isinstance(set_chain, (list, tuple))
    assert set_chain
    if len(set_chain) == 1:
        return set_chain[0]
    else:
        return ExtendedSetExpr(set_chain)


def filter_CDSL_var_decl(fun):
    def wrapper(arg):
        g = fun(arg)
        if g.id.startswith("CDSL__var2type_binding_of_"):
            return nil_tv
        return g

    return wrapper


@filter_CDSL_var_decl
def dfun_var_decl(f):
    assert f.t == tdef.var_decl
    f_id = getattr(f, "id", "") or die("internal error")
    g = copy(f)
    if fun_phase == FunPhase.specification_fun_phase:
        g.is_fun_parm = True
    elif fun_phase == FunPhase.execution_fun_phase:
        g.is_fun_local = True
    elif fun_phase == FunPhase.no_fun_phase:
        g.is_global = True
    else:
        die("unexepected case")
    field_spec = getattr(f, "field_spec", None)
    fs = dfun_field_spec(field_spec) if field_spec else None
    fs_decl_dims = fs._decl_dims if fs else None
    expr = getattr(f, "expr", None)
    if expr:
        expr_context_stack.push(ExprContext(is_rhs=True, dof=fs_decl_dims))
        expr = promote_field_ref_to_var_access(expr)
        expr = dispatch(expr)
        expr_context_stack.pop()
        g.expr = expr
        if f.tq == tqdef.field:
            g._decl_dims = fs_decl_dims or expr._dof
        set_var(f_id, g)
        return g
    if fs:
        g._decl_dims = fs_decl_dims
        set_var(f_id, g)
        return g
    else:
        set_var(f_id, g)  # used to be f
        return g  # early exit, e.g., for a declaration without specification (function parameter)


def gen_field_var_decl(id, expr=None, spec=None, ct=None, locus=None):
    f = Tv(tdef.var_decl, tqdef.field, id=id, ct=ct, locus=locus)
    f.expr = expr
    f.field_spec = spec
    return f


def is_strictly_sorted(a):
    return all(a[i] < a[i + 1] for i in range(len(a) - 1))


def flat_pos_lists_ref(superlist, level=0):
    plist = []
    for p in itertools.product(*superlist):
        if is_strictly_sorted(p):
            plist.append(p)
    return plist


def flat_pos_lists(superlist, level=0):
    # creates the cartesian product of input lists
    # like itertools.product but rejects unwanted results early
    if not superlist:
        return []
    if not level:
        assert all([is_strictly_sorted(x) for x in superlist])

    if len(superlist) == 1:
        return [[x] for x in superlist[0]]
    head = superlist[0]
    tail = superlist[1:]
    product = []
    for h in head:
        if any([h >= tlist[-1] for tlist in tail]):
            continue  # reject early
        sublists = flat_pos_lists(tail, level + 1)
        if sublists:
            for flist in sublists:
                is_ordered = h < flist[0]
                if is_ordered:
                    product.append([h] + flist)
        else:
            product.append([h])
    return product


def proc_dim_key(key):
    assert key.t == tdef.var_ref or key.tq == tqdef.scalar
    assert key.tq in (tqdef.set, tqdef.scalar)
    is_scalar = key.tq == tqdef.scalar
    expr = getattr(key, "_expr", None)
    is_set = expr is not None and isinstance(expr, SetVar)
    key.is_set = is_set
    assert is_scalar != is_set
    if is_scalar:
        uqt = key._ctp.uqt
        elm_type = g_dsl_types.get(uqt) or ICE()
        set_type = elm_type.set_type
        key._related_set = set_type.related_set_var
        key.is_unstruct = elm_type.set_type.is_unstruct()
        if key.is_unstruct:
            error("Unexpected unstructured scalar dim key.")
    elif is_set:
        key.is_unstruct = expr.is_unstruct()
        key._related_set = expr
        assert hasattr(expr, "t")
        elm_type = expr.t
    else:
        ICE()

    key.is_intrinsic = key._ctp.is_intrinsic
    key.is_dof = key.is_set or key.is_intrinsic  # dof => "is iterated"
    assert key._related_set
    return key


def apply_context_dof_struct_aspect(dof, context_dof):
    dof_sdims = [d for d in dof if d.is_struct()]
    con_sdims = [d for d in context_dof if d.is_struct()]
    if len(dof_sdims) > len(con_sdims):
        # we might have a dot_product between us and the LHS
        # nothing to do here
        return dof
    used = [False for d in con_sdims]
    for d in dof_sdims:
        for p in range(len(con_sdims)):
            if used[p]:
                continue
            if d.matches(con_sdims[p]):
                used[p] = True
                break
    rest = [con_sdims[p] for p in range(len(con_sdims)) if not used[p]]
    if len(dof_sdims) == len(con_sdims):
        assert not rest
    dof = copy(dof) + rest
    return dof


def apply_context_dof_unstruct_aspect(dof, context_dof):
    dof_udims_pos = [p for p in range(len(dof)) if dof[p].is_unstruct()]
    con_udims_pos = [p for p in range(len(context_dof)) if context_dof[p].is_unstruct()]
    if len(dof_udims_pos) > len(con_udims_pos):
        error(
            "Cannot find enough iterations for unstruct. dim chain within expression context."
        )
    elif len(dof_udims_pos) == len(con_udims_pos):
        # check only
        for p in range(len(dof_udims_pos)):
            i = dof_udims_pos[p]
            j = con_udims_pos[p]
            if not dof[i].matches(context_dof[j]):
                error("Field access does not match expression context.")
                return
        return dof
    elif len(dof_udims_pos) == 1 and len(con_udims_pos) > 1:
        # 1. try to match the context tail first:
        i = dof_udims_pos[0]
        dof_udim = dof[i]
        dof_elm_type = dof[0].iter_set.get_elm_type()
        j = con_udims_pos[-1]
        last_con_udim = context_dof[j]
        con_elm_type = last_con_udim.iter_set.get_elm_type()
        if dof_elm_type.matches(con_elm_type):
            con_udims = [context_dof[p] for p in con_udims_pos]
            new_dof = dof[:i] + con_udims + dof[i + 1 :]
            return new_dof
        # 2. try to match context head:
        j = con_udims_pos[0]
        first_con_udim = context_dof[j]
        con_elm_type = first_con_udim.iter_set.get_elm_type()
        if dof_elm_type.matches(con_elm_type):
            con_udims = [context_dof[p] for p in con_udims_pos]
            new_dof = dof[:i] + con_udims + dof[i + 1 :]
            return new_dof
    elif len(dof_udims_pos) == 0:
        # we can just use all unstruct dims from context:
        con_udims = [context_dof[p] for p in con_udims_pos]
        new_dof = con_udims + dof
        return new_dof
    # at this point we have no rule to adapt to the expression context: fail
    return None


def is_unique_list(alist, ignore_list=None):
    if ignore_list is None:
        tst = alist
    else:
        tst = [x for x in alist if not x in ignore_list]
    return len(set(tst)) == len(tst)


def dfun_var_access(f):
    assert f.t == tdef.var_access
    if f.tq != tqdef.field:
        return f

    var = f.var_ref
    id = getattr(var, "id", "") or die("internal error")
    decl = get_var(id) or die("Internal error: name lookup failed")
    decl_dims = getattr(decl, "_decl_dims", None) or error(
        "Missing field specification for " + var.id + "."
    )
    impl_iter_sets = get_impl_iter_sets_copy()
    impl_usets = [s for s in impl_iter_sets if s.is_unstruct()]
    dim_keys = dispatch(getattr(f, "dim_keys", ()))
    dim_keys_range = range(len(dim_keys))
    decl_dims_range = range(len(decl_dims))

    for p in range(len(dim_keys)):
        dim_keys[p] = proc_dim_key(dim_keys[p])

    # find related declaration slots for keys using set types
    # 1. find candidates:
    cand_pos_list = []
    for kpos in dim_keys_range:
        key = dim_keys[kpos]
        key_set = key._related_set
        cand_pos = []
        for dpos in decl_dims_range:
            decl_dim = decl_dims[dpos]
            decl_iter_set = decl_dim.iter_set
            if key_set.matches(decl_iter_set):
                cand_pos.append(dpos)
        if not cand_pos:
            error("Cannot find matching declaration dimension for " + key.id + ".")
        cand_pos_list.append(cand_pos)

    # 2. pick candidate:
    if dim_keys:
        cand_keypos2declpos = flat_pos_lists(cand_pos_list)
        if not cand_keypos2declpos:
            error("Cannot find matching declaration dimensions for field access.")
        if len(cand_keypos2declpos) != 1:
            error(
                "Cannot find unique matching declaration dimensions for field access."
            )
        keypos2declpos = cand_keypos2declpos[0]
    else:
        keypos2declpos = None

    # add decl_pos info to key
    for kpos in dim_keys_range:
        key = dim_keys[kpos]
        dpos = keypos2declpos[kpos]
        key._decl_pos = dpos

    # find dof in decl dims:
    decl_pos_is_dof = [True for d in decl_dims_range]
    for key in dim_keys:
        if not key.is_dof:
            decl_pos_is_dof[key._decl_pos] = False
    dof_pos2decl_pos = [p for p in decl_dims_range if decl_pos_is_dof[p]]
    dof = [decl_dims[p] for p in dof_pos2decl_pos]

    # find access scheme for RHS within expression context:
    if expr_context and expr_context.is_rhs():
        context_dof = expr_context.get_dof() or error(
            "Expected nonzero context DOF for RHS expression."
        )
        new_dof = apply_context_dof_unstruct_aspect(dof, context_dof)
        if new_dof is None:
            error("Cannot convert Field expression to expression context.")
        dof = new_dof
        dof = apply_context_dof_struct_aspect(dof, context_dof)
        if dof is None:
            ICE()

    # connect dof to iteration space:
    dof_range = range(len(dof))
    iter_dims = get_var("$impl_iter_dims", ())
    iter_dims_range = range(len(iter_dims))
    unbound_dof_pos = []
    dof_pos2iter_pos = [None for p in dof_range]
    for dof_pos in dof_range:
        dof_dim = dof[dof_pos]
        found_pos = None
        for iter_pos in iter_dims_range:
            if dof_dim.matches(iter_dims[iter_pos]):
                if found_pos is not None:
                    error("Cannot find unique iteration for " + dof_dim.iter_set.name)
                found_pos = iter_pos
        if found_pos is not None:
            dof_pos2iter_pos[dof_pos] = found_pos
        else:
            unbound_dof_pos.append(dof_pos)

    # check if we have a chance to bind unbound dofs later by
    # generating additional iterations (via unbound dofs of LHS):
    if expr_context and expr_context.is_lhs() and unbound_dof_pos:
        unstruct_unbound_dof = [dof[p] for p in unbound_dof_pos if dof[p].is_unstruct()]
        if unstruct_unbound_dof:
            iter_udims = [x for x in iter_dims if x.is_unstruct()]
            test_iter_sets = [x.iter_set for x in iter_udims]
            for d in unstruct_unbound_dof:
                test_iter_sets.append(d.iter_set)
                test_iter_dim = Dimension(test_iter_sets)
                if is_invalid(test_iter_dim):
                    ICE()
                if not d.matches(test_iter_dim):
                    error(
                        "Iteration space is fundamentally incompatible with "
                        + d.name
                        + " degree of freedom."
                    )

    if not is_unique_list(dof_pos2iter_pos, [None]):
        error("multiple DOF match the same iteration dim")
    g = copy(f)
    g.dim_keys = dim_keys
    g._decl_dims = decl_dims
    g._iter_dims = iter_dims
    g._dof = dof
    g._dof_pos2iter_pos = dof_pos2iter_pos
    g._dof_pos2decl_pos = dof_pos2decl_pos
    g._unbound_dof_pos = unbound_dof_pos
    return g


def gen_var_access(vref):
    assert vref.t == tdef.var_ref
    if vref.tq != tqdef.field:
        return vref  # we only transform field refs
    va = Tv(tdef.var_access, tq=vref.tq, ct=vref.ct, id=vref.id, locus=vref.locus)
    va.var_ref = vref
    return va


def dfun_gridspace_ref(f):
    assert f.t == tdef.gridspace_ref
    var = get_var(f.id) or ICE()
    g = copy(f)
    assert var.t == tdef.gridspace_decl
    g.res_gridspace_decl = var
    return g


def dfun2_scalar_var_ref(f):
    assert f.t == tdef.var_ref and f.tq == tqdef.scalar
    assert getattr(f, "_expr", None) is None
    var = get_var(f.id) or ICE()
    g = copy(f)
    g.res_var = var
    if getattr(var, "is_global", False):
        g.is_nonlocal_ref = True  # extension
    return g


def dfun2_set_var_ref(f):
    assert f.t == tdef.var_ref and f.tq == tqdef.set
    assert getattr(f, "_expr", None) is None
    chain = match_set_chain(f) or ICE()
    g = copy(f)
    g._expr = gen_set_from_set_chain(chain)
    return g


def dfun2_field_var_ref(f):
    assert f.t == tdef.var_ref and f.tq == tqdef.field
    fid = getattr(f, "id", "") or ICE()
    decl = get_var(fid) or ICE("name lookup failed")
    decl_dims = getattr(decl, "_decl_dims", None)
    if decl_dims == None:
        error("Missing field declaration/specification.")
    g = copy(f)
    g._decl_dims = decl_dims
    g._dof = decl_dims
    return g


def dfun_retyped_field_ref(f, redispatch=True):
    field_ref = f.field_ref
    if not redispatch:
        return field_ref
    g = dispatch(field_ref)
    return g


def dfun_var_ref(f):
    assert f.t == tdef.var_ref
    if f.tq == tqdef.field:
        return dfun2_field_var_ref(f)
    elif f.tq == tqdef.set:
        return dfun2_set_var_ref(f)
    elif f.tq == tqdef.scalar:
        return dfun2_scalar_var_ref(f)
    else:
        ICE()


def dfun_return_stmt(f):
    assert f.t == tdef.return_stmt
    g = f
    expr = getattr(f, "expr", None)
    if expr:
        expr = dispatch(f.expr)
        return_dof = getattr(expr, "_dof", [])
    else:
        return_dof = []
    fun = get_var("$function") or die("function lookup failed")
    last_return_dof = getattr(fun, "_return_dof", None)

    if last_return_dof != None:
        if return_dof != last_return_dof:
            error(
                [
                    "Function contains inconsistent return stmts.",
                    str(last_return_dof) + " != " + str(return_dof),
                ]
            )
    fun._return_dof = getattr(expr, "_dof", [])
    return g


def dfun_function_decl(f):
    global fun_phase
    assert f.t == tdef.function_decl
    fun_phase = FunPhase.specification_fun_phase
    id = getattr(f, "id", "") or die("internal error")
    ct = getattr(f, "ct", "") or die("internal error")
    parm_decls = getattr(f, "parm_decls", ())
    g = copy(f)
    g.is_struct = None  # to be defined by compute_on elements
    set_var(id, g)  # add function id to the global scope
    stmts = getattr(g, "stmts", None)
    if stmts == None:
        return g
    push_scope("function", id)
    set_var("$function", g)  # add 'current function' to local scope
    if parm_decls:
        g.parm_decls = dispatch(parm_decls)
    fun_phase = FunPhase.execution_fun_phase
    g.stmts = dispatch(stmts)
    if ct != "void":
        return_dof = getattr(g, "_return_dof", None)
        if return_dof == None:
            error("Missing return statement.")
    pop_scope()
    fun_phase = FunPhase.no_fun_phase
    return g


def dfun_edsl_fcall(f):
    #TODO: Do we have a use case?
    assert f.t == tdef.edsl_fcall
    id = getattr(f, "id", "") or die("internal error")
    fun = get_var(id) or die("internal error: function name lookup failed")
    fun_return_dof = getattr(fun, "_return_dof", ())
    fun_parm_decls = getattr(fun, "parm_decls", ())

    g = f
    g._dof = fun_return_dof
    args = dispatch(getattr(f, "args", ()))
    g.args = args
    if len(fun_parm_decls) != len(args):
        die("Internal error: number of call arguments do not match fun decl.")
    for i in range(len(args)):
        a = args[i]
        p = fun_parm_decls[i]
        if a.tq != p.tq:
            error(
                "argument type at pos "
                + str(i)
                + " does not match function declaration."
            )
        if a.tq == tqdef.field:
            a_dof = getattr(a, "_dof", None) or error(
                "Using unspecified field in function call"
            )
            p_dof = getattr(p, "_decl_dims", None) or error(
                "Call to function that has an unspecified field parameter"
            )
            if a_dof != p_dof:
                error(
                    [
                        "field argument at pos "
                        + str(i)
                        + " does not match function declaration.",
                        str(a_dof) + " != " + str(p_dof),
                    ]
                )
    return g


def dfun_fcall(f):
    assert f.t == tdef.fcall
    f_args = getattr(f, "args", ())
    args = dispatch(f_args)

    # todo: be more precise about elemental attribute
    is_elemental = False
    if f.tq == tqdef.scalar:
        is_elemental = True

    # treat anonymous function created by Clang:
    # is this still relevant?
    is_special_case = False
    ct = getattr(f, "ct", "")
    id = getattr(f, "id", "")
    if ct == "EDSL::Field" and len(args) == 1 and not id:
        is_special_case = True

    g = copy(f)
    g.args = dispatch(f_args)
    g._is_elemental = is_elemental
    if is_elemental or is_special_case:
        g._dof = getattr(args[0], "_dof", ())
    return g


def gen_vertical_region(stmt):
    lo = stmt.locus
    f = Tv(tdef.vertical_region, locus=lo)
    start = Tv(
        tdef.var_ref, tqdef.scalar, ct="const EDSL::Level", id="start_level", locus=lo
    )
    end = Tv(
        tdef.var_ref, tqdef.scalar, ct="const EDSL::Level", id="end_level", locus=lo
    )
    order = Tv(
        tdef.order, tqdef.scalar, ct="EDSL::order::Order_kind", id="forward", locus=lo
    )
    f.args = [start, end, order]
    stmts = []

    if stmt.t == tdef.compute_on:
        # check if we can peel off the compute_on node:
        # todo: why don't we support an empty compute_on?
        if not getattr(stmt, "args", []):
            stmts = stmt.stmts
    if not stmts:
        stmts = [stmt]
    f.stmts = stmts
    return f


def gen_compute_on(dims, stmt):
    f = Tv(tdef.compute_on, locus=stmt.locus)
    args = []
    for d in dims:
        name = d.iter_set.get_name() or ICE()
        my_set = get_var(name) or ICE()
        type_name = my_set.t.get_name()
        v = Tv(
            tdef.var_ref, tqdef.set, ct="EDSL::" + type_name, id=name, locus=stmt.locus
        )
        args.append(v)
    f.args = args
    f.stmts = [stmt]
    return f


def gen_loop_on(dims, stmt):
    f = Tv(tdef.loop_on, locus=stmt.locus)
    args = []
    for d in dims:
        name = d.iter_set.get_name() or ICE()
        my_set = get_var(name) or ICE()
        type_name = my_set.t.get_name()
        v = Tv(
            tdef.var_ref, tqdef.set, ct="EDSL::" + type_name, id=name, locus=stmt.locus
        )
        args.append(v)
    f.args = args
    f.stmts = [stmt]
    return f


def dfun_loop_on(f):
    assert f.t == tdef.loop_on
    stmts = getattr(f, "stmts", ())
    if not stmts:
        return
    args = getattr(f, "args", ())
    if not args:
        error("Invalid loop_on arguments.")
    args = dispatch(args)
    cur_iter_sets = get_var("$impl_iter_sets", [])
    cur_iter_dims = get_var("$impl_iter_dims", [])
    assert len(cur_iter_sets) == len(cur_iter_dims)
    if not cur_iter_dims:
        error("Dangling loop iteration.")
    loop_iter_sets = []
    for x in args:
        assert x._ctp
        assert x.tq == tqdef.set
        svar = x._expr
        loop_iter_sets.append(svar)
    new_iter_sets = cur_iter_sets + loop_iter_sets
    push_scope("loop_on")
    set_impl_iter_sets(new_iter_sets)
    new_iter_dims = get_var("$impl_iter_dims", [])
    assert len(new_iter_sets) == len(new_iter_dims)
    sparse_dims = new_iter_dims[len(cur_iter_dims) :]
    for d in sparse_dims:
        assert d.is_sparse()
    g = copy(f)
    g._sparse_dims = sparse_dims
    g.stmts = dispatch(stmts)
    pop_scope()
    return g


def dfun_vertical_region(f):
    assert f.t == tdef.vertical_region
    args = getattr(f, "args", ())
    stmts = getattr(f, "stmts", ())
    args = dispatch(args)
    impl_iter_sets = get_impl_iter_sets_copy()
    assert g_dsl_vset
    if g_dsl_vset in impl_iter_sets:
        error("List of all implicit dense dimensions must be unique.")
    impl_iter_sets.append(g_dsl_vset)
    push_scope("vertical_region")
    set_impl_iter_sets(impl_iter_sets)
    g = f
    g._dense_dim = Dimension(g_dsl_vset)
    if is_invalid(g._dense_dim):
        ICE()
    g.stmts = dispatch(stmts)
    pop_scope()
    return g


def dfun_compute_on(f0):
    assert f0.t == tdef.compute_on
    f = copy(f0)
    raw_args = getattr(f, "args", ())
    args = resolve_gridspace_args(raw_args)
    stmts = getattr(f, "stmts", ())
    if not stmts:
        return
    compon_sets = []
    vset = None
    vset_arg_pos = None
    for p in range(len(args)):
        x = args[p]
        assert x._ctp
        assert x.tq == tqdef.set
        d = x._ctp.uqt
        s = g_dsl_vars.get(x.id) or die("undefined set: " + x.id)
        if s is g_dsl_vset:
            if vset is not None:
                error("Found more than one vertical dim in implicit iteration.")
            vset = s
            vset_arg_pos = p
        else:
            compon_sets.append(s)
    if vset is not None:
        new_args = copy(args)
        del new_args[vset_arg_pos]
        f.args = new_args
        vr = gen_vertical_region(f)
        vr = dispatch(vr)
        return vr
    if g_dsl_vset in compon_sets:
        ICE()
    impl_sets = get_impl_iter_sets_copy()
    impl_usets = [s for s in impl_sets if s.is_unstruct()]
    compon_usets = [s for s in compon_sets if s.is_unstruct()]
    if len(compon_usets) > 1:
        error("Only one dense horizontal unstruct. dimension allowed.")
    new_impl_sets = impl_sets + compon_sets
    # check if we used any dense dim twice:
    struct_sets = [s for s in new_impl_sets if s.t.is_struct()]
    if len(set(struct_sets)) != len(struct_sets):
        error("List of all implicit strcut dimensions must be unique.")
    push_scope("compute_on")
    set_impl_iter_sets(new_impl_sets)
    g = f
    g._dense_dims = [Dimension(s) for s in compon_sets]
    if is_invalid(g._dense_dims):
        ICE()
    # check if compute_on is struct or unstruct in horizontal space
    is_struct = None
    for d in g._dense_dims:
        if d.is_horizontal():
            if d.is_struct():
                assert is_struct or is_struct is None
                is_struct = True
            else:
                assert not is_struct or is_struct is None
                is_struct = False
    g.is_struct = is_struct
    # copy is_struct property to function
    my_fun = get_var("$function")
    if my_fun.is_struct is None:
        my_fun.is_struct = is_struct
    else:
        assert my_fun.is_struct == is_struct
    g.stmts = dispatch(stmts)
    pop_scope()
    h = fix_wrong_nesting(g)
    return h


def fix_wrong_nesting(f):
    # wrong nesting might be caused by the auto-generation of iterations
    assert f.t == tdef.compute_on
    have_bad_nesting = False
    problems = []
    current_problem = []
    for stmt in f.stmts:
        if stmt.t == tdef.vertical_region:
            have_bad_nesting = True
            if current_problem:
                problems.append(current_problem)
                current_problem = []
            problems.append([stmt])
        else:
            current_problem.append(stmt)
    if current_problem:
        problems.append(current_problem)
        current_problem = []
    if not have_bad_nesting:
        # nothing to do
        assert len(problems) == 1
        return f
    compon_head = copy(f)
    del compon_head.stmts
    solutions = []
    for p in problems:
        if (len(p) > 1) or (len(p) == 1 and p[0].t != tdef.vertical_region):
            # no VR inside p
            compon = copy(compon_head)
            compon.stmts = p
            solutions.append(compon)
        else:
            # have VR inside p
            assert len(p) == 1
            vr = p[0]
            assert vr.t == tdef.vertical_region
            compon = copy(compon_head)
            compon.stmts = vr.stmts
            vr.stmts = [compon]
            solutions.append(vr)
    if len(solutions) == 1:
        return solutions[0]
    else:
        return solutions


def get_dof_map(y, x):
    y_dof = getattr(y, "_dof", ())  # LHS
    x_dof = getattr(x, "_dof", ())  # RHS

    if len(x_dof) != len(y_dof):
        return error(
            ["Number of free dims do not match:", "%s != %s" % (len(y_dof), len(x_dof))]
        )
    y_used = [False for d in y_dof]
    x2y_map = [None for d in x_dof]
    for xpos in range(len(x_dof)):
        found = None
        for ypos in range(len(y_dof)):
            if y_used[ypos]:
                continue
            if x_dof[xpos].matches(y_dof[ypos]):
                found = ypos
                x2y_map[xpos] = ypos
                y_used[ypos] = True
                break
        if found == None:
            return error("Cannot map all RHS dimensions to LHS dimensions.")
    return x2y_map


def copy_attr(f, g, atts):
    for a in atts:
        v = getattr(f, a, None)
        if v != None:
            setattr(g, a, v)
    return g


def dfun_unop(f):
    assert f.t == tdef.unop
    if f.tq != tqdef.field:
        return f
    args = getattr(f, "args", ())
    if len(args) != 1:
        die("internal error")
    expr = dispatch(args[0])
    g = f
    g.args = [expr]
    copy_attr(f, g, ["_dof", "_uhsd"])
    g._dof = expr._dof
    return g


def gen_binop(op, args, locus):
    f = Tv(tdef.binop, tqdef.field, ct="EDSL::Field", id=op, locus=locus)
    f.args = copy(args)
    return f


def promote_field_ref_to_var_access(f):
    if f.t == tdef.var_ref and f.tq == tqdef.field:
        return gen_var_access(f)
    else:
        return f


def extrude_first_arg(dof0, dof1):
    dof0_has_unstruct_dim = any([d.is_unstruct() for d in dof0])
    used1 = [False for d in dof1]
    for p0 in range(len(dof0)):
        d0 = dof0[p0]
        for p1 in range(len(dof1)):
            if used1[p1]:
                continue
            d1 = dof1[p1]
            if d0.matches(d1):
                used1[p1] = True
                break
    extrude0 = [dof1[p] for p in range(len(dof1)) if not used1[p]]
    extrude0_has_unstruct_dim = any([d.is_unstruct() for d in extrude0])
    if dof0_has_unstruct_dim and extrude0_has_unstruct_dim:
        error("Cannot extrude unstruct dims.")
    if extrude0:
        ext_dof0 = dof0 + extrude0
    else:
        ext_dof0 = dof0
    return ext_dof0


def extrude(dof0, dof1):
    ext_dof0 = extrude_first_arg(dof0, dof1)
    ext_dof1 = extrude_first_arg(dof1, dof0)
    return (ext_dof0, ext_dof1)


def dfun2_field_binop(f):
    assert f.t == tdef.binop and f.tq == tqdef.field

    f_args = getattr(f, "args", [])
    f_args = resolve_retyped_field_refs(f_args)
    f_args = [promote_field_ref_to_var_access(x) for x in f_args]
    args = dispatch(f_args)
    dof0 = list(getattr(args[0], "_dof", []))
    dof1 = list(getattr(args[1], "_dof", []))
    (ext_dof0, ext_dof1) = extrude(dof0, dof1)
    ndim_diff0 = len(ext_dof0) - len(dof0)
    ndim_diff1 = len(ext_dof1) - len(dof1)
    dof0 = ext_dof0
    dof1 = ext_dof1

    assert sum([d.is_struct() for d in dof0]) == sum([d.is_struct() for d in dof1])
    # do minimal extrusion:
    if ndim_diff0 < ndim_diff1:
        dof = dof0
    else:
        dof = dof1

    g = copy(f)
    g.args = args
    g._dof = dof
    return g


def dfun2_scalar_binop(f):
    assert f.t == tdef.binop and f.tq == tqdef.scalar
    args = dispatch(f.args)
    g = copy(f)
    g.args = args
    g._dof = []
    return g


def dfun_binop(f):
    assert f.t == tdef.binop
    if f.tq == tqdef.field:
        return dfun2_field_binop(f)
    elif f.tq == tqdef.scalar:
        return dfun2_scalar_binop(f)
    else:
        ICE()


def aux_var_assign(f):
    # dispatch lhs and rhs var_access
    assert f.t == tdef.var_assign
    if f.tq != tqdef.field:
        return f
    g = copy(f)
    args = getattr(f, "args", None)
    if args:
        (lhs, rhs) = args
        del g.args
    else:
        (lhs, rhs) = (
            getattr(f, "lhs", None) or die("ICE"),
            getattr(f, "rhs", None) or die("ICE"),
        )

    (lhs, rhs) = resolve_retyped_field_refs((lhs, rhs))
    if lhs.t == tdef.var_ref and lhs.tq == tqdef.field:
        lhs = gen_var_access(lhs)
    if rhs.t == tdef.var_ref and rhs.tq == tqdef.field:
        rhs = gen_var_access(rhs)
    expr_context_stack.push(ExprContext(is_lhs=True))
    lhs = dispatch(lhs)
    expr_context_stack.pop()
    dof = lhs._dof
    expr_context_stack.push(ExprContext(is_rhs=True, dof=dof))
    rhs = dispatch(rhs)
    expr_context_stack.pop()
    if rhs.tq == tqdef.scalar:
        rhs._extrude = dof
        rhs._dof = dof
    dof_map = get_dof_map(lhs, rhs)
    g = copy(f)
    g._lhs2rhs_map = dof_map
    g.lhs = lhs
    g.rhs = rhs
    g._dof = dof
    g._unbound_dof_pos = lhs._unbound_dof_pos
    return g


def get_list_index(e, alist, mask=None):
    if mask != None:
        for p in range(len(alist)):
            if not mask[p]:
                continue
            if e == alist[p]:
                mask[p] = False
                return p
        return None
    try:
        p = alist.index(e)
    except ValueError:
        p = None
    return p


def dfun_var_assign(f):
    assert f.t == tdef.var_assign
    lhs = f.lhs
    rhs = f.rhs

    if not lhs.tq in (tqdef.scalar, tqdef.field):
        error("Invalid assignment - LHS type not supported.")
    elif (
        lhs.tq == tqdef.scalar
        and rhs.tq != tqdef.scalar
        or lhs.tq == tqdef.field
        and not rhs.tq in (tqdef.scalar, tqdef.field)
    ):
        error("Invalid assignment betwwen incompatible types.")
    if f.tq != tqdef.field:
        return f
    g = aux_var_assign(copy(f))
    if expr_context:
        return g  # we are not at the absolute LHS side yet, we come back later

    # check if there are any DOFs left that need an explicit iteration construct:
    if not g._unbound_dof_pos:
        return g

    # We have one or more degrees of freedom left but no more implicit iteration.
    # The goal here is to generate missing iterations automatically.

    # separate DOFs into dense and sparse parts:
    internal_dense_dof = []
    internal_sparse_dof = []
    for p in g._unbound_dof_pos:
        d = g._dof[p]
        if d.is_dense():
            internal_dense_dof.append(d)
        else:
            internal_sparse_dof.append(d)
    # note: we are re-embedding the original node f, not the evaluated node g
    # we will still evaluate deeper nodes twice, but that should not matter
    h = copy(f)

    # generate sparse iteration:
    if internal_sparse_dof:
        h1 = gen_loop_on(internal_sparse_dof, h)
    else:
        h1 = h

    h2 = h1
    if internal_dense_dof:
        h3 = gen_compute_on(internal_dense_dof, h2)
    else:
        h3 = h2

    # evaluate the new tree:
    g3 = dispatch(h3)
    return g3


def dfun_field_init(f):
    assert f.t == tdef.field_init
    if f.tq != tqdef.field:
        return f
    g = copy(f)
    fs = getattr(f, "field_spec", None)
    if fs:
        fs = dispatch(fs)
        dof = fs._decl_dims
        g.field_spec = fs
    else:
        fr = getattr(f, "field_ref", None)
        assert fr is not None
        fr = dispatch(fr)
        dof = fr._decl_dims
        g.field_ref = fr
    args = dispatch(getattr(f, "args", ()))
    g.args = args
    all_args_are_scalar = all(x.tq == tqdef.scalar for x in args)
    if len(args) == 1:
        if not all_args_are_scalar:
            error("Expected scalar expression in field initialization.")
    elif len(args) == 2:
        if not all_args_are_scalar:
            error("Expected scalar expressions in field initialization.")
        init_dof_pos = None
        for p in reversed(range(len(dof))):
            dim = dof[p]
            if dim.is_struct():
                continue
            dim_size = dim.get_size()
            if dim_size != 2:
                error("Invalid field initialization")
            init_dof_pos = p
            break
        g._init_dof_pos = init_dof_pos
    else:
        error("Invalid field initialization.")
    g._dof = dof
    return g


def dfun_dot_product(f):
    args = getattr(f, "args", ())
    if not args and len(args) == 2:
        error("Invalid dot_product arguments.")
    args = copy(args)
    binop = gen_binop("*", args, f.locus)
    binop = dispatch(binop)
    impl_dims = get_var("$impl_iter_dims", [])
    dof = [None, None]
    dof[0] = getattr(binop.args[0], "_dof", []) or error(
        "No DOF found for dot_product in first argument."
    )
    dof[1] = getattr(binop.args[1], "_dof", []) or error(
        "No DOF found for dot_product in second argument."
    )
    args = binop.args
    vector_dims = [None, None]
    red_dof_pos = [None, None]
    for ap in range(len(args)):
        dp = args[ap]._unbound_dof_pos
        if len(dp) > 1:
            # we could generalize this:
            error("Ambiguous vector dimension in dot_product.")
        elif len(dp) == 1:
            args[ap].red_dof_pos = dp[0]
            vector_dims[ap] = dof[ap][dp[0]]
        else:
            error(
                "Missing vector dimension in " + str(ap) + ". argument of dot_product."
            )

    if not vector_dims[0].matches(vector_dims[1]):
        error("Vector dimensions of dot_product do not match.")
    red_dim = vector_dims[0]
    g = copy(f)
    dof = binop._dof
    red_dof_pos = None
    for p in range(len(dof)):
        if red_dim.matches(dof[p]):
            assert red_dof_pos is None
            red_dof_pos = p
    assert red_dof_pos is not None
    rest_dof = dof[:red_dof_pos] + dof[red_dof_pos + 1 :]
    g._dof = rest_dof
    g._vector_dims = vector_dims
    g.args = binop.args
    return g

def dfun_reduction_weights(f):
    init_list = f.init_list
    raw_ile = getattr(init_list, "init_list_expr", ())
    wlist = dispatch(raw_ile)
    offsets = f.offsets
    if offsets:
        off_init_list = offsets.init_list
        off_ile = getattr(off_init_list, "init_list_expr", ())
        olist = dispatch(off_ile)
    else:
        olist = ()
    dof = []
    pos = -1
    for y in wlist:
        pos += 1
        y_dof = getattr(y, "_dof", None)
        if y_dof is None: continue

        if pos < len(olist):
            # an offset consumes a sparse dof:
            y_dof = [d for d in y_dof if d.is_dense()]
        if any(d.is_sparse() for d in y_dof):
            error("Found unexpected sparse dimension in " + str(pos+1) + " weight.")
        if dof:
            (dof, y_dof) = extrude(dof, y_dof)
        else:
            dof = y_dof
    g = copy(f)
    if wlist:
        g.init_list.init_list_expr = wlist
    g._dof = dof
    return g


def dfun_nreduce(f):
    my_set_expr = getattr(f, "set_expr", None) or error("Missing set_expr in nreduce.")
    if my_set_expr.tq != tqdef.set:
        error("Invalid set_expr in nreduce.")
    my_set_expr = dispatch(my_set_expr)
    if not isinstance(my_set_expr, SetExpr):
        my_set_expr = my_set_expr._expr
        assert isinstance(my_set_expr, SetExpr)
    red_set = my_set_expr
    cur_iter_sets = get_impl_iter_sets_copy()
    if expr_context:
        assert expr_context.is_rhs()
        cur_expr_con_dof = expr_context.get_dof()
        cur_expr_con_sets = [d.iter_set for d in cur_expr_con_dof]
        red_dim = Dimension(cur_expr_con_sets + [red_set])
        if is_invalid(red_dim):
            error("Invalid reduction dimension: " + red_set.get_name())
    else:
        cur_expr_con_dof = []
        red_dim = None
    weights = getattr(f, "weights", None)
    if weights:
        weights = dispatch(weights)
    push_scope("nreduce")
    new_iter_sets = cur_iter_sets + [red_set]
    set_impl_iter_sets(new_iter_sets)
    new_iter_dims = get_var("$impl_iter_dims", [])
    if red_dim is None:
        red_dim = new_iter_dims[-1]
    if red_dim.is_struct():
        error("Expected unstructured reduction dimension.")
    if not red_dim.is_sparse():
        error("Expected sparse reduction dimension.")
    if weights and weights._dof:
        # check dof
        red_orig = Dimension(red_dim.orig_sets)
        (ext_weights_dof, ext_red_orig) = extrude(weights._dof, [red_orig])
        (ext_expr_con_dof, ext2_weights) = extrude(cur_expr_con_dof, weights._dof)
    field_expr = getattr(f, "field_expr", None) or error(
        "Missing field_expr in nreduce."
    )
    expr_context_stack.push(ExprContext(is_rhs=True, dof=cur_expr_con_dof + [red_dim]))
    field_expr = dispatch(field_expr)
    expr_context_stack.pop()
    fe_dof = getattr(field_expr, "_dof", ()) or ICE()
    # locate red_dim in field expression:
    found = None
    for p in reversed(range(len(fe_dof))):
        if fe_dof[p].matches(red_dim):
            found = p
            break
    if found is None:
        error("Cannot connect reduction dimension to field expression.")

    # reduced dof:
    dof = fe_dof[:found] + fe_dof[found + 1 :]
    g = copy(f)
    g.field_expr = field_expr
    g._red_dim = red_dim
    g._dof = dof
    g.weights = weights
    pop_scope()
    return g


def proc_block_stmt(f):
    push_scope("block")
    stmts = getattr(f, "stmts", ())
    g = copy(f)
    g.stmts = dispatch(stmts)
    pop_scope()
    return g


def dfun_if_stmt(f):
    g = f
    g.cond = dispatch(f.cond)
    g.tcase = dispatch(f.tcase)
    g.fcase = dispatch(getattr(f, "fcase", ()))
    return g


def dfun_decl_stmt(f):
    var_decls = f.var_decls
    sa_var_decls = dispatch(var_decls)
    g = copy(f)
    g.var_decls = sa_var_decls
    return g


def dfun_gridspace_decl(f):
    g = copy(f)
    g.dim_specs = dispatch(f.v)
    del g.v
    set_var(g.id, g)
    return g


def dfun_typedef_decl(f):
    g = copy(f)
    set_var(g.id, g)
    return g


def dfun_retyped_field_decl(f):

    typedef_decl = getattr(f, "type_decl", None)
    if typedef_decl:
        assert typedef_decl.t == tdef.typedef_decl
        orig_type_ref = typedef_decl.type_ref
        type_ref_id = typedef_decl.id

    else:
        type_ref = getattr(f, "type_ref", None)
        assert type_ref is not None
        type_ref_id = type_ref._ctp.uqt
        typedef_decl = get_var(type_ref_id)
        orig_type_ref = typedef_decl.type_ref

    key = "CDSL__var2type_binding_of_" + type_ref_id
    binding = get_var(key)
    if binding is None:
        error("Missing type information for " + type_ref_id)
    assert binding.id == key
    assert binding.t == tdef.var_decl
    b_expr = binding.expr
    assert b_expr.id == "CDSL__gen_var2type_binding_id"
    args = b_expr.args
    dim_specs = []
    for x in args:
        if x.t == tdef.gridspace_ref:
            dim_specs.extend(x.res_gridspace_decl.dim_specs)
        else:
            ICE()
    f_spec = gen_field_spec(dim_specs, locus=f.locus)
    field_decl = getattr(f, "field_decl", None)
    expr = getattr(field_decl, "expr", None) if field_decl else None
    g = gen_field_var_decl(
        id=f.id, expr=expr, spec=f_spec, ct=orig_type_ref.ct, locus=f.locus
    )
    h = dispatch(g)
    return h


def dispatch_children(f):
    assert isinstance(f, Tv)
    for k, v in f.__dict__.items():
        if isinstance(v, (Tv, list, tuple)):
            setattr(f, k, dispatch(v))
    return f


def dispatch(f):
    if is_lot(f):
        # we will never return a list that contains a list
        a = []
        for x in f:
            y = dispatch(x)
            if is_lot(y):
                a.extend(y)
            else:
                a.append(y)
        return a
    if isinstance(f, Tv):
        fun = dispatcher.get(f.t, None)
        if fun:
            g_locus_stack.push(f.locus)
            g = fun(f) or die("bad case")
            g_locus_stack.pop()
            return g
        elif f.t in sa_terminals:
            return f
        elif f.t in simple_dispatch_targets:
            return dispatch_children(f)
        else:
            die("unsupported case: f.t = " + tname[f.t])
    else:
        return f


dispatcher = {}

simple_dispatch_targets = (tdef.translation_unit, tdef.EDSL_fcall)

sa_terminals = (
    tdef.order,
    tdef.integer_literal,
    tdef.bool_literal,
    tdef.floating_literal,
    tdef.vector_subscript_expr,
)


def init_dispatcher():
    global dispatcher
    for k in list(globals().keys()):
        if not k.startswith("dfun_"):
            continue
        ktail = k[5:]
        t = getattr(tdef, ktail, None)
        if t == None:
            continue
        v = globals()[k]
        if callable(v):
            dispatcher[t] = v


init_dispatcher()


def get_tree(f):
    reset()
    g = dispatch(f)
    return g
