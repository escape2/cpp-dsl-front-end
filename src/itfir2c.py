from os.path import basename
from common import die


class Gstate:
    def __init__(self, itfir):
        # consts:
        self.backend_name = itfir.backend_name
        self.LibTag = itfir.LibTag
        self.enable_name_mangling = itfir.enable_name_mangling
        self._itfir_id = itfir._id
        # variables:
        self.cpp_namespace = None
        self.dawn_c_decl_list = []


class Itf_c:
    def __init__(self, code, LibTag, dawn_c_decl_list):
        self.code = code
        self.LibTag = LibTag
        self.dawn_c_decl_list = dawn_c_decl_list


def gen_sir_c_interface(itfir):
    global gstate
    gstate = Gstate(itfir)

    if itfir.ofile_name:
        dawn_include_file = itfir.ofile_name
    elif itfir.filename:
        dawn_include_file = "dawn_" + basename(itfir.filename)
    else:
        dawn_include_file = None

    ofile = itfir.ofile_name or "stdout"
    if gstate.backend_name == "cudaico":
        inc = [
            "// Additional C-interface for the Dawn generated cuda code ("
            + ofile
            + ").",
            "// The cuda code has to be compiled separately from this file.",
            "#include " + '"' + "interface/atlas_interface.hpp" + '"',
        ]
    elif dawn_include_file:
        inc = ["#include " + '"' + dawn_include_file + '"']
    else:
        inc = ["// see " + ofile + " for definitions of stencil code"]

    if gstate.enable_name_mangling:
        inc.append('#include "name_mangling.h"')

    if itfir.grid_type_str == "Unstructured":
        inc.append('#include "f2c_dawn_unstruct.hpp"')

    gstate.cpp_namespace = "dawn_generated::" + gstate.backend_name

    stencil_codes = []
    for stencil in itfir.stencils:
        stencil_codes.extend(gen_stencil_c_interface(stencil))
    code_lines = [*inc, "", 'extern "C" {', "", *stencil_codes, "}"]
    code_str = serialize2str(code_lines)
    return Itf_c(code_str, itfir.LibTag, gstate.dawn_c_decl_list)
    # return code_str


def serialize(a, level=0):
    if isinstance(a, list):
        b = []
        for x in a:
            y = serialize(x, level + 1)
            if y is None:
                continue
            if isinstance(y, list):
                b.extend(y)
            else:
                b.append(y)
        return b
    elif isinstance(a, str):
        if a == "":
            return a
        assert level > 0
        return "  " * (level - 1) + a
    elif a is None:
        return a
    else:
        die("bad case")


def serialize2str(a):
    return "\n".join(serialize(a))


def cdsl_name(name):
    return "CDSL__" + name


def c_method_name(stencil_name, method_name):
    name = "_".join(["c", "dawn", gstate.backend_name, stencil_name, method_name])
    if gstate.enable_name_mangling:
        return mangle_name(name)
    else:
        return name


def mangle_name(func_name):
    return "CPP_NAME_MANGLING(" + func_name + ")"


def get_init_struct_name(c_name):
    return "InterfaceData_" + c_name


def gen_stencil_c_interface(f):
    if f.is_struct:
        if gstate.backend_name == "cuda":
            con = gen_stencil_c_init_cuda(f)
            run = gen_stencil_c_run_cuda(f)
        else:
            con = gen_stencil_c_init(f)
            run = gen_stencil_c_run(f)
        gvars = gen_stencil_c_gvar_access(f)
    else:
        if gstate.backend_name == "cudaico":
            con = gen_unstruct_stencil_c_init_cudaico(f)
            run = gen_unstruct_stencil_c_run_cudaico(f)
            gvars = gen_stencil_c_gvar_access_cudaico(f)
        else:
            con = gen_unstruct_stencil_c_init(f)
            run = gen_unstruct_stencil_c_run(f)
            gvars = gen_stencil_c_gvar_access(f)
    stencil = [con, "", run, "", gvars, ""]
    return stencil


def gen_stencil_c_init(f):
    # constructor:
    cpp_class = gstate.cpp_namespace + "::" + f.name
    stencil = cdsl_name("stencil")
    con = [
        cpp_class + "* " + c_method_name(f.name, "init") + "(domain *dom) {",
        [
            cpp_class + " *" + stencil + " = new " + cpp_class + "(*dom);",
            "return " + stencil + ";",
        ],
        "}",
        "",
    ]
    decon = [
        "void "
        + c_method_name(f.name, "delete")
        + "("
        + cpp_class
        + " *"
        + stencil
        + ") {",
        [
            "delete " + stencil + ";",
        ],
        "}",
    ]

    return con + decon


def gen_stencil_c_run(f):
    # executor:
    cpp_class = gstate.cpp_namespace + "::" + f.name
    stencil = cdsl_name("stencil")
    parm_decls = [cpp_class + " *" + stencil]
    call_args = []
    for p in f.parm_decls:
        parm_decls.append(p.Type + " *" + p.name)
        call_args.append("*" + p.name)
    parm_decls = ",\n".join(parm_decls).split("\n")
    run = [
        "void " + c_method_name(f.name, "run") + "(",
        [
            parm_decls,
            ") {",
            stencil + "->run(" + ", ".join(call_args) + ");",
        ],
        "}",
    ]
    return run


def gen_stencil_c_gvar_access(f):
    # global variables:
    f_name = f.name
    template_parm = "<" + gstate.LibTag + ">" if gstate.LibTag else ""
    cpp_class = gstate.cpp_namespace + "::" + f_name + template_parm
    stencil = cdsl_name("stencil")
    stencil_parm = cpp_class + " *" + stencil

    wrappers = []
    for gdecl in f.gdecls:
        var_name = gdecl.var_name
        var_type = gdecl.type_name
        c_get = c_method_name(f_name, "get_" + var_name)
        cpp_get = cpp_class + "_get_" + var_name
        c_set = c_method_name(f_name, "set_" + var_name)
        cpp_set = cpp_class + "_set_" + var_name
        arg_name = "var"
        wraps = [
            var_type + " " + c_get + "(" + stencil_parm + ") {",
            ["return " + stencil + "->get_" + var_name + "();"],
            "}",
            "",
            "void "
            + c_set
            + "("
            + stencil_parm
            + ", "
            + var_type
            + " "
            + arg_name
            + ") {",
            ["return " + stencil + "->set_" + var_name + "(" + arg_name + ");"],
            "}",
            "",
        ]
        wrappers.extend(wraps)
    return wrappers[:-1]


def gen_stencil_c_init_cuda(f):
    # todo: is this really just copy & paste of the non-cuda implementation?
    # constructor:
    cpp_class = gstate.cpp_namespace + "::" + f.name
    stencil = cdsl_name("stencil")
    con = [
        cpp_class + "* " + c_method_name(f.name, "init") + "(domain *dom) {",
        [
            cpp_class + " *" + stencil + " = new " + cpp_class + "(*dom);",
            "return " + stencil + ";",
        ],
        "}",
        "",
    ]
    decon = [
        "void "
        + c_method_name(f.name, "delete")
        + "("
        + cpp_class
        + " *"
        + stencil
        + ") {",
        [
            "delete " + stencil + ";",
        ],
        "}",
    ]

    return con + decon


def gen_stencil_c_run_cuda(f):
    # todo: is this really just copy & paste of the non-cuda implementation?
    # executor:
    cpp_class = gstate.cpp_namespace + "::" + f.name
    stencil = cdsl_name("stencil")
    parm_decls = [cpp_class + " *" + stencil]
    call_args = []
    for p in f.parm_decls:
        parm_decls.append(p.Type + " *" + p.name)
        call_args.append("*" + p.name)
    parm_decls = ",\n".join(parm_decls).split("\n")
    run = [
        "void " + c_method_name(f.name, "run") + "(",
        [
            parm_decls,
            ") {",
            stencil + "->run(" + ", ".join(call_args) + ");",
        ],
        "}",
    ]
    return run


def gen_unstruct_stencil_c_init_cudaico(f):
    global gstate
    # The cudaico backend has a different C interface than cxxnaiveico.
    # For now we rebuild the cxxnaiveico interface style which consumes
    # all data in the init call. Support is given by init_struct below.

    cdsl_mesh_t = "cdsl::Mesh"
    head_params = [cdsl_mesh_t + " *cdsl_mesh_pt", "int k_size"]
    field_pt_params = []
    call_args = ["*cdsl_mesh_pt->get_atlas_mesh_pt()", "k_size"]
    gvar_call_args = [str(d.init_value) for d in f.gdecls]
    direct_call_args = gvar_call_args + ["cdsl_mesh_pt", "k_size"]
    dawn_run_params = []
    for p in f.parm_decls:
        if p.is_vertical:
            ptype = "VerticalField *"
        elif p.is_dense:
            ptype = "DenseField *"
        else:
            ptype = "SparseField *"
        field_pt_params.append(ptype + p.name)
        arg = p.name + "->view"
        call_args.append(arg)
        direct_call_args.append(p.name)
        dawn_run_params.append("::dawn::float_type *" + p.name)
    c_name = c_method_name(f.name, "init")
    stencil = cdsl_name("stencil")
    init_args = head_params + field_pt_params
    gvar_parms = [d.type_name + " " + d.var_name for d in f.gdecls]

    # declare Dawn's C interfaces:
    decl = [
        "void setup_"
        + f.name
        + "(dawn::GlobalGpuTriMesh *mesh, int k_size, cudaStream_t stream);",
        "void free_" + f.name + "();",
        "void run_" + f.name + "(" + ", ".join(gvar_parms + dawn_run_params) + ");",
        "",
    ]
    gstate.dawn_c_decl_list.extend(decl)
    # init_struct:
    init_struct_name = get_init_struct_name(f.name)
    gdecls = [
        d.type_name + " " + d.var_name + " = " + str(d.init_value) for d in f.gdecls
    ]
    init_struct = [
        "struct " + init_struct_name + " {",
        [
            "// support for cxxnaive-interfaces for cudaico",
            *[a + ";" for a in gdecls],
            *[a + ";" for a in init_args],
        ],
        "};",
        "",
    ]

    # constructor:
    con = [
        init_struct_name + "* " + c_name + "(",
        [
            ",#".join(init_args).split("#"),
            ") {",
            [
                "return new " + init_struct_name + " {",
                ",#".join(direct_call_args).split("#"),
                "};",
            ],
            "}",
            "",
        ],
    ]
    # deconstructor:
    decon = [
        "void "
        + c_method_name(f.name, "delete")
        + "("
        + init_struct_name
        + " *interface_data"
        + ") {",
        [
            "free_" + f.name + "();",
            "delete interface_data;",
        ],
        "}",
    ]

    return decl + init_struct + con + decon


def gen_unstruct_stencil_c_run_cudaico(f):
    # executor:
    lib_tag = gstate.LibTag or "None"
    template1_spec = "<" + lib_tag + ">"
    cpp_class = gstate.cpp_namespace + "::" + f.name + template1_spec
    c_name = c_method_name(f.name, "run")
    stencil = cdsl_name("stencil")
    init_struct_name = get_init_struct_name(f.name)
    setup_name = "setup_" + f.name
    run_name = "run_" + f.name
    gvar_call_args = ["interface_data->" + d.var_name for d in f.gdecls]
    run_args = gvar_call_args

    for p in f.parm_decls:
        arg = "interface_data->" + p.name + "->cuda_storage"
        run_args.append(arg)

    run = [
        "void " + c_name + "(" + init_struct_name + " *interface_data" + ") {",
        [
            setup_name
            + "(interface_data->cdsl_mesh_pt->get_ggtm_pt(), interface_data->k_size, 0);",
            run_name + "(",
            ",#".join(run_args).split("#"),
            ");",
        ],
        "}",
    ]
    return run


def gen_stencil_c_gvar_access_cudaico(f):
    # global variables:
    f_name = f.name
    init_struct_name = get_init_struct_name(f.name)
    parm = init_struct_name + " *interface_data"
    wrappers = []
    for gdecl in f.gdecls:
        var_name = gdecl.var_name
        var_type = gdecl.type_name
        c_get = c_method_name(f_name, "get_" + var_name)
        c_set = c_method_name(f_name, "set_" + var_name)
        arg_name = "var"
        wraps = [
            var_type + " " + c_get + "(" + parm + ") {",
            ["return interface_data->" + var_name + ";"],
            "}",
            "",
            "void " + c_set + "(" + parm + ", " + var_type + " " + arg_name + ") {",
            ["interface_data->" + var_name + " = " + arg_name + ";"],
            "}",
            "",
        ]
        wrappers.extend(wraps)
    return wrappers[:-1]


def gen_unstruct_stencil_c_init(f):
    # constructor:
    template1_spec = "<" + gstate.LibTag + ">"
    template2_spec = "<" + gstate.LibTag + ", double>"
    cpp_class = gstate.cpp_namespace + "::" + f.name + template1_spec
    cdsl_mesh_t = "cdsl::Mesh"
    dawn_mesh_t = "dawn::mesh_t" + template1_spec
    head_params = [cdsl_mesh_t + " const *cdsl_mesh_pt", "int k_size"]
    return_type = cpp_class + "* "
    field_pt_params = []
    call_args = ["*cdsl_mesh_pt->get_atlas_mesh_pt()", "k_size"]
    for p in f.parm_decls:
        if p.is_vertical:
            ptype = "VerticalField *"
        elif p.is_dense:
            ptype = "DenseField *"
        else:
            ptype = "SparseField *"
        field_pt_params.append(ptype + p.name)
        arg = p.name + "->view"
        call_args.append(arg)
    c_name = c_method_name(f.name, "init")
    stencil = cdsl_name("stencil")
    con = [
        return_type + c_name + "(",
        [
            ",#".join(head_params + field_pt_params).split("#"),
            ") {",
            [
                "return new " + cpp_class + "(",
                ",#".join(call_args).split("#"),
                ");",
            ],
            "}",
            "",
        ],
    ]
    decon = [
        "void "
        + c_method_name(f.name, "delete")
        + "("
        + cpp_class
        + " *"
        + stencil
        + ") {",
        [
            "delete " + stencil + ";",
        ],
        "}",
    ]

    return con + decon


def gen_unstruct_stencil_c_run(f):
    # executor:
    template1_spec = "<" + gstate.LibTag + ">"
    cpp_class = gstate.cpp_namespace + "::" + f.name + template1_spec
    c_name = c_method_name(f.name, "run")
    stencil = cdsl_name("stencil")

    run = [
        "void " + c_name + "(" + cpp_class + "* " + stencil + ") {",
        [
            stencil + "->run();",
        ],
        "}",
    ]
    return run
