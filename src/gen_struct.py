#!/usr/bin/env python3
# Distribution: see LICENSE.txt

import sys
import json
import argparse
import configparser
import dim_config
import srcgen_common as sgc
from common import die

declared = {}


def reverse(a):
    return a[::-1]


def declare_class(c):
    global declared
    declared["class " + c] = 1


def gen_elem_class(elem_name):
    global known_element_types
    cname = elem_name
    if cname in known_element_types:
        return None
    known_element_types[cname] = True

    members = [cname + "()"]
    members.append(cname + "(int k)")
    nonmembers = []

    # elements of structured sets are ordered and can be compared:
    args = [cname + " x", "int i"]
    args_r = reverse(args)
    for op in ("==", "!=", "<", ">", "<=", ">="):
        nonmembers.append("bool operator" + op + " (" + ",".join(args) + ")")
        nonmembers.append("bool operator" + op + " (" + ",".join(args_r) + ")")

    if is_algebraic[elem_name]:
        nonmembers.append(cname + " operator- (" + cname + " x)")
        for op in ("+", "-", "*", "/"):
            nonmembers.append(
                cname
                + " operator"
                + op
                + " ("
                + ",".join(["int i", cname + " x"])
                + ")"
            )
            nonmembers.append(
                cname
                + " operator"
                + op
                + " ("
                + ",".join([cname + " x", "int i"])
                + ")"
            )
        for op in ("+=", "-=", "*=", "/="):
            members.append(cname + " operator" + op + " (" + cname + " x)")

    members.append(cname + "& operator() (" + cname + " x)")
    c = {
        "type": "class",
        "name": cname,
        "members": members,
        "is_set": False,
        "is_direct": True,
        "inherits": "Direct_element_base",
        "nonmembers": nonmembers,
    }
    declare_class(cname)
    return c


def gen_intrinsic_elem_class(elem_name):
    global known_element_types
    if elem_name in cpp_element_types:
        return None
    cname = "Intrinsic_" + elem_name.lower()
    if cname in known_element_types:
        return None
    known_element_types[cname] = True

    members = []
    nonmembers = []

    for op in ("+", "-"):
        nonmembers.append(
            cname + " operator" + op + " (" + ",".join(["int i", cname + " x"]) + ")"
        )
        nonmembers.append(
            cname + " operator" + op + " (" + ",".join([cname + " x", "int i"]) + ")"
        )

    for op in ("=", "+=", "-=", "*=", "/="):
        m = cname + " operator" + op + " (" + cname + " x) = delete"
        members.append(m)
    c = {
        "type": "class",
        "name": cname,
        "members": members,
        "is_set": False,
        "is_direct": True,
        "inherits": elem_name,
        "nonmembers": nonmembers,
    }
    declare_class(cname)
    return c


def iterator_methods(iter_name, element_name):
    return [
        "bool operator != (const " + iter_name + "& other)",
        "const " + iter_name + "& operator++ ()",
        iter_name + " begin () const",
        iter_name + " end () const",
        "const " + element_name + " operator* ()",
    ]


def gen_set_class(i0):
    global known_element_types
    e0 = elements[i0]
    s0 = sets[i0]
    cname = s0
    if cname in known_element_types:
        return None
    known_element_types[cname] = True

    members = [cname + "()"]
    members.extend(iterator_methods(cname, e0))
    nonmembers = []

    # subsets:
    for subset_op in ("lt", "gt", "le", "ge"):
        members.append(cname + " " + subset_op + " (" + e0 + " x) const")
    members.append(
        cname + " interval(const " + e0 + " start, const " + e0 + " end) const"
    )
    members.append(cname + " where(const Field_bool f) const")

    # set-transform:
    members.append(cname + " reversed() const")

    # identity field generator
    members.append("Field_int" + " id() const")
    members.append("Field field() const")
    for t in ["bool", "int", "sp", "dp"]:
        members.append("Field_" + t + " field_" + t + "() const")

    bounds = (
        ["start_" + e0.lower(), "end_" + e0.lower()] if not is_horizontal[s0] else []
    )
    c = {
        "type": "class",
        "name": s0,
        "members": members,
        "nonmembers": nonmembers,
        "is_set": True,
        "is_dense": True,
        "inherits": "Dense_set_base",
        "element_type": e0,
        "domain_set_var": s0.lower(),
        "bounds": bounds,
    }
    declare_class(cname)
    return c


def declare_domain_sets():
    for s in sets:
        c = s
        print("extern const", c, c.lower() + ";")


def old_declare_domain_bounds():
    for e in elements:
        c = e
        v = c.lower()
        print("extern const", c, "start_" + v + ", " + "end_" + v + ";")


def declare_domain_bounds(classes):
    for c in classes:
        bounds = c.get("bounds")
        if not bounds:
            continue
        et = c["element_type"]
        print("extern const", et, ", ".join(bounds) + ";")


def gen_json_output(classes):
    info = classes

    for d in dim_conf:
        section = {"type": "dim_config", "name": d}
        conf = {}
        for k in dim_conf[d]:
            conf[k] = dim_conf[d][k]
        section["conf"] = conf
        info.append(section)
    section = {
        "type": "common",
        "conf": {"cdsl_itf_version": "no_version.cdsl_itf_version"},
    }
    info.append(section)
    print(json.dumps(info, indent=4))


def gen_classes():
    src = []
    for k in range(elem_num):
        src.append(gen_elem_class(elements[k]))
        src.append(gen_intrinsic_elem_class(elements[k]))
        src.append(gen_set_class(k))
    result = []
    for c in src:
        if c:
            result.append(c)
    return result


def set_global_state():
    global sets, elements, elem_num, dim_conf, is_algebraic
    global known_element_types, cpp_element_types, is_horizontal

    dim_conf = dim_config.structured_dim_conf
    sets = []
    elements = []
    sizes = []
    is_algebraic = {}
    is_horizontal = {}
    for section in dim_conf:
        my_conf = dim_conf[section]
        dim = dim_conf[section]
        sets.append(dim["set_type"])
        elements.append(dim["element_type"])
        sizes.append(dim["size"])
        is_extra = my_conf["is_extra_dim"]
        elem = my_conf["element_type"]
        my_set = my_conf["set_type"]
        is_algebraic[elem] = not is_extra
        is_algebraic[my_set] = not is_extra
        is_horizontal[my_set] = bool(my_conf.get("is_horizontal_dim"))
    elem_num = len(elements)

    cpp_element_types = ("bool", "int", "float", "double")
    known_element_types = {}
    for k in cpp_element_types:
        known_element_types[k] = True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-hpp", help="generates cpp header file", action="store_true")
    parser.add_argument("-json", help="generates json file", action="store_true")
    parser.add_argument(
        "-c", metavar="file", help="configuration file", action="store", required=True
    )
    args = parser.parse_args()
    config = configparser.ConfigParser()
    if not config.read(args.c):
        print("error: Cannot read file", args.c)
        parser.print_help()
        exit(1)
    dim_config.proc_config(config)
    set_global_state()
    classes = gen_classes()
    if args.hpp:
        sgc.gen_hpp_output(classes)
        declare_domain_sets()
        declare_domain_bounds(classes)
    elif args.json:
        gen_json_output(classes)
