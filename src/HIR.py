#!/usr/bin/env python3
# Distribution: see LICENSE.txt

from enum import Enum
from collections import namedtuple, OrderedDict
import json
from common import *
from node import Node
from common import *

HirKindNames = [
    "AbsoluteFieldAccess",
    "AssignmentStmt",
    "BinaryOp",
    "BlockStmt",
    "Computation",
    "DimensionInterval",
    "DimensionLevel",
    "Domain",
    "DotProduct",
    "FctCall",
    "FieldAccess",
    "FieldDecl",
    "GridDimension",
    "IfStmt",
    "Literal",
    "LocationType",
    "LoopOn",
    "NeighbourReduce",
    "NOP",  # preliminaryextension
    "Offset",
    "Program",
    "ReturnStmt",  # preliminary extension
    "ScopedProgram",
    "SparseIfStmt",
    "Type",
    "UnaryOp",
    "VarAccess",
    "VarDecl",
    "VerticalRegion",
]
__all__ = HirKindNames
assert len(HirKindNames) == len(set(HirKindNames))
HirKind = Enum("HirKind", " ".join(HirKindNames))


BinOpNames = [
    "plusOp",
    "minusOp",
    "mulOp",
    "divOp",
    "powerOp",
    "logicalAnd",
    "logicalOr",
    "logicalEqual",
    "logicalNotEqual",
    "logicalGt",
    "logicalLt",
    "logicalGe",
    "logicalLe",
]
assert len(BinOpNames) == len(set(BinOpNames))
BinOp = Enum("BinOp", " ".join(BinOpNames))

UnOpNames = [
    "unaryMinus",
    "logNot",
    "incrementOp",
    "decrementOp",
]
assert len(UnOpNames) == len(set(UnOpNames))
UnOp = Enum("UnOp", " ".join(UnOpNames))


class Hnode(Node):
    """ Base HIR Node """

    def __init__(self, hir_kind, locus):
        assert isinstance(hir_kind, HirKind)
        self._kind = hir_kind
        assert isinstance(locus, Locus)
        self.locus = locus
        self.ContentsModel = None

    def setcm(self, h):
        if h is None:
            self.ContentsModel = []
        elif is_lot(h):
            self.ContentsModel = h
        else:
            self.ContentsModel = [h]

    def addcm(self, h):
        if getattr(self, "ContentsModel", None) is not None:
            if is_lot(h):
                self.ContentsModel.extend(h)
            else:
                self.ContentsModel.append(h)
        else:
            self.setcm(h)

    def cm_append(self, h):
        self.ContentsModel.append(h)

    def cm_extend(self, h):
        self.ContentsModel.extend(h)

    def getcm(self):
        return getattr(self, "ContentsModel", [])


# ---
# classes in the next section below implement the HIR elements
# defined in the ESCAPE-2 deliverable D2.3.
# The 'extensions' entry lists attributes not specified in D2.3
# ---


class Type(Hnode):
    def __init__(self, name, locus):
        super().__init__(HirKind.Type, locus)
        # "bool" is an extension that might be mapped to "int: in the SIR
        assert name in ("double", "int", "float", "bool")
        self.name = name


class Literal(Hnode):
    def __init__(self, type_, value, locus):
        super().__init__(HirKind.Literal, locus)
        self.setcm(type_)
        assert isinstance(value, str)
        self.value = value

    def get_Type(self):
        return self.ContentsModel[0]


class LocationType(Hnode):
    # 'node' location changed to 'Vertex'
    # todo: try to conform to (node,cell,edge)
    def __init__(self, location, locus):
        super().__init__(HirKind.LocationType, locus)
        if location == "Vert":
            location = "Vertex"
        assert location in ("Cell", "Edge", "Vertex")
        self.location = location


class Offset(Hnode):
    def __init__(self, grid_dim, dist, locus):
        super().__init__(HirKind.Offset, locus)
        self.setcm(grid_dim)
        self.distance = str(dist)

    def get_GridDimension(self):
        return self.ContentsModel[0]


class FieldAccess(Hnode):
    def __init__(self, name, locus, offsets=None, indirect_dense_access=False):
        super().__init__(HirKind.FieldAccess, locus)
        self.name = name
        self.setcm(offsets)
        self.indirect_dense_access = indirect_dense_access


class GridDimension(Hnode):
    extensions = ("chain",)

    def __init__(self, name, locus, loc_type=None, dim_type=None, chain=None):
        super().__init__(HirKind.GridDimension, locus)
        self.name = name
        self.setcm(loc_type)
        self.dimension_type = dim_type
        self.chain = chain

    def get_LocationType(self):
        return self.ContentsModel[0]


class AssignmentStmt(Hnode):
    extensions = ("op",)

    def __init__(self, lValueModel, exprModel, op, locus):
        super().__init__(HirKind.AssignmentStmt, locus)
        self.setcm([lValueModel, exprModel])
        self.op = op


class BlockStmt(Hnode):
    def __init__(self, stmtModel_list, locus):
        super().__init__(HirKind.BlockStmt, locus)
        self.setcm(stmtModel_list)


class Computation(Hnode):
    def __init__(self, grid_dim_list, block_stmt, locus):
        super().__init__(HirKind.Computation, locus)
        self.setcm([grid_dim_list, block_stmt])


class DimensionInterval(Hnode):
    def __init__(self, bound1, bound2, locus, LoopOrder=None):
        super().__init__(HirKind.DimensionInterval, locus)
        self.setcm([bound1, bound2])
        if LoopOrder:
            # optional attribute:
            self.LoopOrder = LoopOrder


class Domain(Hnode):
    def __init__(
        self, DomainParallelDimensions, VerticalDimension, ParallelDimensions, locus
    ):
        super().__init__(HirKind.Domain, locus)
        self.setcm([DomainParallelDimensions, VerticalDimension, ParallelDimensions])


class FieldDecl(Hnode):
    def __init__(self, name, Type, gdims, locus):
        super().__init__(HirKind.FieldDecl, locus)
        self.setcm([Type] + gdims)
        self.name = name


class LoopOn(Hnode):
    def __init__(self, grid_dims, block_stmt, locus):
        super().__init__(HirKind.LoopOn, locus)
        self.setcm([grid_dims, block_stmt])


class Program(Hnode):
    extensions = ("name", "source", "edsl_extension_src")

    def __init__(
        self,
        name,
        source,
        grid_dims,
        domain,
        field_decls,
        var_decls,
        SpOrEks,
        locus,
        edsl_extension_src=None,
    ):
        # Note: SpOrEks = list of (ScopedProgram or ExternalKernel)
        super().__init__(HirKind.Program, locus)
        self.setcm([grid_dims, domain, field_decls, var_decls, SpOrEks])
        # HIR extensions:
        self.name = name
        self.source = source
        self.edsl_extension_src = edsl_extension_src


class ScopedProgram(Hnode):
    extensions = ("name", "return_type", "parm_decls", "field_decls")

    def __init__(self, name, return_type, parm_decls, field_decls, BlockStmt, locus):
        super().__init__(HirKind.ScopedProgram, locus)
        self.name = name
        self.return_type = return_type
        self.parm_decls = parm_decls
        self.field_decls = field_decls
        self.BlockStmt = BlockStmt


class VerticalRegion(Hnode):
    def __init__(self, dim_intervals, computations, locus):
        super().__init__(HirKind.VerticalRegion, locus)
        self.setcm([dim_intervals, computations])


class BinaryOp(Hnode):
    # We implement D2.3, section 5.2 "Binary operations element" as a single class
    def __init__(self, binop, lhs_exprModel, rhs_exprModel, locus):
        super().__init__(HirKind.BinaryOp, locus)
        self.setcm([lhs_exprModel, rhs_exprModel])
        assert isinstance(binop, BinOp)
        self.op = binop


class UnaryOp(Hnode):
    # We implement D2.3, section 5.3 "Unary operations element" as a single class
    def __init__(self, unop, exprModel, locus):
        super().__init__(HirKind.UnaryOp, locus)
        self.setcm([exprModel])
        assert isinstance(unop, UnOp)
        self.op = unop


class VarDecl(Hnode):
    extensions = ("is_const",)

    def __init__(
        self,
        type_,
        name,
        locus,
        ndims=0,
        isarg=False,
        initialization=None,
        is_const=False,
    ):
        super().__init__(HirKind.VarDecl, locus)
        self.setcm([type_])
        self.name = name
        self.ndims = ndims
        self.isarg = isarg
        self.initialization = initialization
        self.is_const = is_const


class VarAccess(Hnode):
    extensions = ("is_nonlocal_ref",)

    def __init__(self, name, locus, index=None, is_nonlocal_ref=False):
        super().__init__(HirKind.VarAccess, locus)
        self.setcm([index] if index is not None else [])
        self.name = name
        self.is_nonlocal_ref = is_nonlocal_ref


class DotProduct(Hnode):
    def __init__(self, grid_dim1, field_access1, grid_dim2, field_access2, locus):
        super().__init__(HirKind.DotProduct, locus)
        self.setcm([grid_dim1, field_access1, grid_dim2, field_access2])


class FctCall(Hnode):
    std_fcts = set(
        ["abs", "sqrt", "sin", "cos", "tan", "asin", "acos", "atan", "exp", "log"]
    )
    extension = ("is_std_fct",)

    def __init__(self, name, exprModel_list, locus):
        super().__init__(HirKind.FctCall, locus)
        self.setcm(exprModel_list)
        self.name = name
        self.is_std_fct = name in self.std_fcts


class IfStmt(Hnode):
    def __init__(self, locus, cond, then_case, else_case=None):
        super().__init__(HirKind.IfStmt, locus)
        cm = [cond, then_case]
        if else_case is not None:
            cm.append(else_case)
        self.setcm(cm)


class SparseIfStmt(Hnode):
    def __init__(self, locus, cond, then_case, else_case=None):
        super().__init__(HirKind.SparseIfStmt, locus)
        cm = [cond, then_case]
        if else_case is not None:
            cm.append(else_case)
        self.setcm(cm)


class NeighbourReduce(Hnode):
    # Note: Currently the backend only supports one sparse dimension
    # Therefore grid_dim is currently a single dimension.
    def __init__(self, locus, grid_dim, init_value, expr, weights, offsets, op="add"):
        super().__init__(HirKind.NeighbourReduce, locus)
        # HIR extension: weights, offsets
        self.setcm([grid_dim, init_value, expr, weights, offsets])
        self.op = op


class DimensionLevel(Hnode):
    def __init__(self, var_offset, literal_offset, locus):
        super().__init__(HirKind.DimensionLevel, locus)
        self.setcm([var_offset, literal_offset])


class AbsoluteFieldAccess(Hnode):
    # Note: untested, we have no use case for this element yet
    def __init__(self, name, access_list, locus):
        super().__init__(HirKind.AbsoluteFieldAccess, locus)
        self.name = name
        self.setcm(access_list)


# ---
# classes in the next section below are preliminary new HIR elements (to be discussed)
# ---


class NOP(Hnode):
    """ An empty 'do nothing' statement - can be used to satisfy formal requirements in concepts. """

    # /NOP/ is only used as a temporary Node that gets filtered out before the final tree
    # is returned.
    # TODO: check if we can get rid of the NOP node

    def __init__(self, locus):
        super().__init__(HirKind.NOP, locus)


class ReturnStmt(Hnode):
    """ Describes the return expression of a ScopedProgram.  """

    def __init__(self, locus, return_expr=None):
        super().__init__(HirKind.ReturnStmt, locus)
        self.return_expr = return_expr
