#!/usr/bin/env python3
# Distribution: see LICENSE.txt

# Description: DSL parser using the filtered libclang C++ tree.


from collections import namedtuple, defaultdict
import sys
import types
import time
import re
import json
import argparse
from io import StringIO
from copy import copy
from tvs import tdef, tv_str, tqdef, Tv, get_tq, nil_tv
import clang_filter
from clang_filter import reject
from parser_config import (
    is_field_type,
    is_scalar_type,
    is_set_type,
    is_map_type,
    is_scope_guard_type,
    is_assign_op,
    is_subset_method,
    is_binop,
    is_elem_type,
)
from common import *


def myerr(f, msg):
    error(msg, f.locus)


def is_unop(s):
    return s == "-"


def is_cmpop(s):
    if cmpop_re.match(s):
        return True
    return False


def get_op_sym(s):
    if not s.startswith("operator"):
        return ""
    op = s[8:]
    return op


def get_src_from_clang_filter(x):
    e = x.extent
    src = clang_filter.src_lines
    tue = clang_filter.tu_extent

    if str(e.start.file) != str(tue.start.file):
        print("tu_extent.start.file=", "[" + str(tue.start.file) + "]")
        print("        e.start.file=", "[" + str(e.start.file) + "]")
        die("get_src failed")
    y0 = e.start.line - 1
    x0 = e.start.column - 1
    y1 = e.end.line - 1
    x1 = e.end.column - 1
    if y0 == y1:
        s = src[y0][x0:x1]
        return s
    a = [src[y0][x0:]]
    for i in range(y0 + 1, y1):
        a.append(src[i])
    a.append(src[y1][:x1])
    s = "".join(a)
    return s


g_debug = 0


def set_debug(state):
    global g_debug
    g_debug = state


def handle_unresolved(f, error_msg):
    # no error_msg => no error
    if error_msg is None:
        return
    myerr(f, error_msg)


# --- matcher:


def match_field_ctor(f):
    if f.t != tdef.call_expr:
        return
    tq = f.tq
    if tq != tqdef.field:
        return
    if not getattr(f, "is_EDSL_ctor", False):
        return
    fid = getattr(f, "id", "")
    if not fid:
        return
    ct = getattr(f, "ct", "")
    args = getattr(f, "args", ())
    g = Tv(tdef.field_ctor, tq, ct=ct, id=fid, locus=f.locus)
    if not args:
        return g
    a = []
    for x in args:
        # todo: make this last match more precise
        y = (
            match_dim_spec(x)
            or match_gridspace_ref(x)
            or match_retyped_field_ref(x)
            or match_expr(x, tqdef.field)
        )
        if not y:
            return
        a.append(y)
    g.args = a
    return g


def match_scalar_ctor(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    if not getattr(f, "is_EDSL_ctor", False):
        return
    id = getattr(f, "id", "")
    if not f.id:
        return
    ct = getattr(f, "ct", "")
    args = getattr(f, "args", ())
    if not args:
        g = Tv(tdef.scalar_ctor, tq, ct=ct, id=id, locus=f.locus)
        return g
    if len(args) == 1:
        a0 = args[0]
        y = match_expr(a0, tq=tqdef.scalar)
        if not y:
            return
        g = Tv(tdef.scalar_ctor, tq, ct=ct, id=id, locus=f.locus)
        g.args = [y]
        return g
    die("unexpected case")


def match_init_list(f):
    if f.t != tdef.init_list:
        return
    a = []
    for x in f.v:
        y = match_expr(x)
        a.append(y)
    g = copy(f)
    del g.v
    g.init_list_expr = a
    return g


def match_reduction_weights(f):
    if f.t != tdef.call_expr:
        return
    if not getattr(f, "is_EDSL_ctor", None):
        return
    if f.id != "ReductionWeights":
        return
    ctp = f._ctp
    if ctp.uqt != "ReductionWeights":
        return
    args = getattr(f, "args", ())
    if len(args) > 1:
        return
    init_list = None
    if args:
        init_list = match_init_list(args[0])
        if not init_list:
            return
    g = Tv(tdef.reduction_weights, f.tq, ct=f.ct, id=f.id, locus=f.locus)
    g.init_list = init_list
    return g

def match_reduction_offsets(f):
    if f.t != tdef.call_expr:
        return
    if not getattr(f, "is_EDSL_ctor", None):
        return
    if f.id != "ReductionOffsets":
        return
    ctp = f._ctp
    if ctp.uqt != "ReductionOffsets":
        return
    args = getattr(f, "args", ())
    if len(args) > 1:
        return
    init_list = None
    if args:
        init_list = match_init_list(args[0])
        if not init_list:
            return
    g = Tv(tdef.reduction_offsets, f.tq, ct=f.ct, id=f.id, locus=f.locus)
    g.init_list = init_list
    return g


def match_scalar_decl(f):
    if f.t != tdef.var_decl:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    id = getattr(f, "id", "")
    if not id:
        return
    ctp = f._ctp
    v = list(getattr(f, "v", ()))
    f_has_idx = ctp.has_idx
    f_init_list = None
    f_idx_expr = None
    f_expr = None
    if len(v) == 2:
        assert f_has_idx
        f_idx_expr = v[0]
        f_init_list = v[1]
    elif len(v) == 1:
        if f_has_idx:
            f_init_list = v[0]
        else:
            f_expr = v[0]
    elif len(v) == 0:
        assert not f_has_idx
    else:
        ICE()

    expr = (match_expr(f_expr) or ICE()) if f_expr else None

    # we actually don't need the idx_expr if we know the const expr result already
    idx_expr = (match_expr(f_idx_expr, tqdef.scalar) or ICE()) if f_idx_expr else None
    init_list = (match_init_list(f_init_list) or ICE()) if f_init_list else None

    ct = getattr(f, "ct", "")
    g = Tv(tdef.scalar_decl, tq, ct=ct, id=id, locus=f.locus)
    if expr:
        g.expr = expr
    if idx_expr:
        g.idx_expr = idx_expr
    if init_list:
        g.init_list = init_list
    return g


def match_map_ref(f):
    if f.t != tdef.decl_ref_expr:
        return False
    tq = getattr(f, "tq", 0)
    if tq != tqdef.map:
        return
    id = getattr(f, "id", "")
    if not id:
        return
    g = Tv(tdef.var_ref, tq, ct=getattr(f, "ct", ""), id=id)
    return g


def match_set_ref(f):
    if f.t != tdef.decl_ref_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.set:
        return
    id = getattr(f, "id", "")
    if not id:
        return
    g = Tv(tdef.set_ref, tq, ct=getattr(f, "ct", ""), id=id, locus=f.locus)
    return g


def match_set_ref_chain(f):
    set_ref = match_set_ref(f)
    if set_ref:
        return set_ref
    if f.t != tdef.member_ref_expr:
        return
    if not f.fqnp_sw_EDSL:
        return
    v = f.v
    if len(v) != 1:
        return
    orig = match_set_ref_chain(f.v[0])
    if not orig:
        return
    set_ref = Tv(tdef.set_ref, tqdef.set, f.ct, id=f.id, locus=f.locus)
    set_ref.orig = orig
    return set_ref


def match_set_expr_chain(f):
    if f.t != tdef.member_ref_expr:
        return
    if not f.fqnp_sw_EDSL:
        return
    if f.ct.find("_of_") < 0:
        return
    v = f.v
    if len(v) != 1:
        return
    orig = match_set_expr(f.v[0])
    if not orig:
        return
    set_ref = Tv(tdef.set_ref, tqdef.set, f.ct, id=f.id, locus=f.locus)
    set_ref.orig = orig
    return set_ref


def match_dim_spec_from_op(f):
    if f.t != tdef.member_ref_expr:
        return
    id = getattr(f, "id", "")
    if id != "operator Dim_spec":
        return
    fqnp = getattr(f, "fqnp", "")
    if fqnp != "EDSL::Set_ref":
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    sref = match_set_expr(v[0])
    if not sref:
        return
    g = Tv(tdef.dim_spec, sref.tq, ct=sref.ct, locus=f.locus)
    g.expr = sref
    return g


def match_dim_spec(f, error_msg=None):
    if f.t != tdef.call_expr:
        return
    id = getattr(f, "id", "")
    ct = getattr(f, "ct", "")
    if not (id, ct) == ("operator Dim_spec", "EDSL::Dim_spec"):
        return
    args = getattr(f, "args", ())
    if len(args) != 1:
        return False
    a0 = args[0]
    y = match_dim_spec_from_op(a0)
    if not y and error_msg:
        myerr(f, error_msg)
    return y


def match_field_cast_expr(f):
    if f.t != tdef.field_cast_expr:
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    expr = match_field_call_expr(v[0])
    if not expr:
        return
    g = Tv(tdef.field_cast_expr, expr.tq, ct=expr.ct, locus=f.locus)
    g.expr = expr
    return g


def match_field_spec(f):
    # 1. variant:
    fce = match_field_cast_expr(f)
    fspec = strip_field_cast_expr(fce) if fce else None
    if fspec and fspec.t == tdef.field_spec:
        return fspec
    # 2. variant:
    ctor = match_field_ctor(f)
    if not ctor:
        return
    ctor_args = getattr(ctor, "args", ())
    if not ctor_args:
        return
    for x in ctor_args:
        if not x.t in (tdef.dim_spec, tdef.gridspace_ref):
            return
    g = ctor
    g.t = tdef.field_spec
    return g


def match_field_spec_from_mre(f):
    if f.t != tdef.member_ref_expr:
        return
    fqnp = getattr(f, "fqnp", "")
    if not fqnp.startswith("EDSL::Field"):
        return
    id = getattr(f, "id", "")
    if id != "spec":
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    field = match_field_ref(v[0])
    return field


def match_field_spec_stmt(f):
    if f.t != tdef.call_expr:
        return
    fqnp = getattr(f, "fqnp", "")
    if not fqnp.startswith("EDSL::Field"):
        return
    args = getattr(f, "args", ())
    if not args:
        return
    field = match_field_spec_from_mre(args[0])
    specs = []
    for ds in args[1:]:
        y = match_dim_spec(ds) or match_gridspace_ref(ds)
        if not y:
            return
        specs.append(y)
    g = Tv(tdef.field_spec_stmt, locus=f.locus)
    g.field = field
    g.dim_specs = specs
    return g


def match_field_decl(f):
    if f.t != tdef.var_decl:
        return
    if f.tq != tqdef.field:
        return
    g = Tv(tdef.field_decl, f.tq, ct=f.ct, id=f.id, locus=f.locus)
    v = list(getattr(f, "v", ()))
    if not v:
        return g
    if len(v) != 1:
        return
    y = match_field_spec(v[0])
    if y:
        g.field_spec = y
        return g
    y = match_expr(v[0], tqdef.field)
    if y:
        assert y.tq == tqdef.field
        g.expr = y
        return g
    return y


def match_vmap_access(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    id = getattr(f, "id", "")
    if id != "operator()":
        return
    fqnp = getattr(f, "fqnp", "")
    if fqnp != "EDSL::Vertical_map":
        return
    ct = getattr(f, "ct", "")
    if ct != "EDSL::Level":
        return
    args = getattr(f, "args", ())
    if len(args) < 2:
        return
    vmap = match_var_ref(args[0])
    if vmap.tq != tqdef.map:
        die("internal error")
    a = []
    for x in args[1:]:
        y = match_expr(x)
        if not y:
            return
        a.append(y)
    g = Tv(tdef.vmap_access, tq, ct=ct)
    g.args = a
    return g


def match_vmap_spec(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.map:
        return
    if not getattr(f, "is_EDSL_ctor", False):
        return
    (ct, id) = (getattr(f, "ct", ""), getattr(f, "id", ""))
    if not (ct, id) == ("EDSL::Vertical_map", "Vertical_map"):
        return
    args = getattr(f, "args", ())
    specs = []
    for x in args:
        y = match_dim_spec(x)
        if not y:
            return
        specs.append(y)
    g = Tv(tdef.vmap_spec, tq, ct=ct)
    g.args = specs
    return g


def match_vmap_decl(f):
    if f.t != tdef.var_decl:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.map:
        return
    v = getattr(f, "v", "")
    if len(v) != 1:
        return
    vms = match_vmap_spec(v[0])
    g = Tv(tdef.vmap_decl, tq, ct=getattr(f, "ct", ""), id=getattr(f, "id", ""))
    g.vmap_spec = vms
    return g


def match_gridspace_ref(f):
    if f.t != tdef.decl_ref_expr or f.tq != tqdef.other:
        return
    ctp = f._ctp
    if ctp.uqt != "Gridspace" or ctp.fqnp != "EDSL":
        return
    g = Tv(tdef.gridspace_ref, f.tq, ct=f.ct, id=f.id, locus=f.locus)
    return g


def match_gridspace_decl(f):
    if f.t != tdef.var_decl or f.tq != tqdef.other:
        return
    ctp = f._ctp
    if ctp.fqnp != "EDSL" or ctp.uqt != "Gridspace":
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    call_expr = v[0]
    if call_expr.t != tdef.call_expr:
        return
    if not getattr(call_expr, "is_EDSL_ctor", None):
        return
    args = getattr(call_expr, "args", ())
    # now we are committed: match or fail
    if len(args) == 0:
        myerr(f, "Invalid zero Gridspace.")
    dim_specs = [match_dim_spec(x, "Expected Dim specification.") for x in args]
    g = Tv(tdef.gridspace_decl, tqdef.other, id=f.id, v=dim_specs, locus=f.locus)
    return g


def match_gridspace_expr(f):
    return match_gridspace_ref(f)


def match_typedef_decl(f):
    if f.t != tdef.typedef_decl:
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    type_ref = v[0]
    if type_ref.t != tdef.type_ref:
        return
    g = copy(f)
    g.type_ref = type_ref
    del g.v
    return g


def match_retyped_field_decl(f):
    if f.t != tdef.var_decl:
        return
    v = f.v
    if len(v) == 0:
        type_decl = match_typedef_decl(getattr(f, "type_decl", nil_tv))
        if type_decl is None:
            return
        if type_decl.type_ref.tq != tqdef.field:
            return
        g = copy(f)
        g.t = tdef.retyped_field_decl
        g.type_decl = type_decl
        return g
    if len(v) != 1:
        return
    v0 = v[0]
    if v0.t != tdef.call_expr:
        return
    orig_field_type_id = v0.id
    if not orig_field_type_id.startswith("Field"):
        return
    # rewrite field_decl:
    w0 = copy(v0)
    w0.tq = tqdef.field
    w0.ct = "EDSL::Field"
    clang_filter.set_defaults(w0)
    g = copy(f)
    g.v = [w0]
    g.tq = tqdef.field
    g.ct = "EDSL::" + orig_field_type_id
    clang_filter.set_defaults(g)
    field_decl = match_field_decl(g)
    assert field_decl is not None
    h = copy(f)
    h.t = tdef.retyped_field_decl
    h.tq = tqdef.field
    del h.v
    h.field_decl = field_decl
    return h


def match_var_decl(f):
    if f.t != tdef.var_decl:
        return
    return (
        match_field_decl(f)
        or match_retyped_field_decl(f)
        or match_scalar_decl(f)
        or match_gridspace_decl(f)
        or match_vmap_decl(f)
    )


def match_simple_typedef_decl(f):
    if f.t != tdef.typedef_decl:
        return
    v = f.v
    if len(v) != 1:
        return
    type_ref = v[0]
    if type_ref.t != tdef.type_ref:
        return
    g = copy(f)
    del g.v
    g.type_ref = type_ref
    return g


def match_type_decl(f):
    return match_simple_typedef_decl(f)


def match_decl_stmt(f):
    if f.t != tdef.decl_stmt:
        return
    v = getattr(f, "v", ())
    if not v:
        return
    var_decls = []
    for x in v:
        y = match_var_decl(x)
        if not y:
            return
        var_decls.append(y)
    g = Tv(tdef.decl_stmt, locus=f.locus)
    g.var_decls = var_decls
    return g


def match_scope_guard_deref(f):
    if f.t != tdef.call_expr:
        return False
    if f.id != "operator*":
        return False
    fqnp = getattr(f, "fqnp", "")
    if f.ct == "const EDSL::Intrinsics_scope_guard_type":
        if fqnp != "EDSL::Intrinsics":
            return False
    elif f.ct == "const EDSL::Declaration_region_scope_guard_type":
        if fqnp != "EDSL::Declaration_region":
            return False
    elif f.ct == "const EDSL::Vertical_region_scope_guard_type":
        if fqnp != "EDSL::Vertical_region":
            return False
    else:
        return False
    g = Tv(tdef.scope_guard_deref, get_tq(f.ct), ct=f.ct, locus=f.locus)
    g.fqnp = fqnp
    return g


def match_scope_guard_decl(f):
    if f.t != tdef.var_decl:
        return
    if f.tq != tqdef.scope_guard:
        return
    v = f.v
    if not len(v) == 1:
        return
    deref = match_scope_guard_deref(v[0])
    if not deref:
        return
    if not f.id in (
        "EDSL__compute_on_guard",
        "EDSL__loop_on_guard",
        "EDSL__declaration_region_guard",
        "EDSL__vertical_region_guard",
    ):
        return
    if not f.ct in (
        "EDSL::Intrinsics_scope_guard_type",
        "EDSL::Declaration_region_scope_guard_type",
        "EDSL::Vertical_region_scope_guard_type",
    ):
        return
    g = Tv(tdef.scope_guard_decl, f.tq, ct=f.ct, id=f.id, locus=f.locus)
    g.scope_guard_deref = deref
    return g


def match_mre_op_set_expr(f):
    if f.t != tdef.member_ref_expr:
        return
    (id, fqnp) = (getattr(f, "id", ""), getattr(f, "fqnp", ""))
    if id != "operator Set_expr":
        return
    if fqnp != "EDSL::Set_ref":
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    # y = match_set_expr_chain(v[0]) or match_set_expr(v[0])
    y = match_set_expr(v[0])
    if not y:
        return
    g = Tv(tdef.mre_op_set_expr, get_tq(y.ct), ct=y.ct, locus=f.locus)
    g.expr = y
    return g


def match_op_set_expr(f):
    if f.t != tdef.call_expr:
        return
    (ct, id) = (getattr(f, "ct", ""), getattr(f, "id", ""))
    if (ct, id) != ("EDSL::Set_expr", "operator Set_expr"):
        return
    args = getattr(f, "args", ())
    if len(args) != 1:
        return
    a0 = args[0]
    y = match_mre_op_set_expr(a0)
    if not y:
        return
    y.t = tdef.op_set_expr
    return y


def match_mre_subset_m_expr(f):
    if f.t != tdef.member_ref_expr:
        return
    id = getattr(f, "id", "")
    if not is_subset_method(id):
        return
    if not f.fqnp_sw_EDSL:
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    se = match_set_expr(v[0])
    return se


def match_subset_m_expr(f):
    if f.t != tdef.call_expr:
        return False
    tq = getattr(f, "tq", 0)
    if tq != tqdef.set:
        return
    id = getattr(f, "id", "")
    if not is_subset_method(id):
        return
    args = getattr(f, "args", ())
    if not args:
        return
    mre = match_mre_subset_m_expr(args[0])
    a = []
    for x in args[1:]:
        y = match_expr(x)
        if not y:
            return
        a.append(y)
    g = Tv(tdef.subset_m_expr, tq, ct=getattr(f, "ct", ""), id=id, locus=f.locus)
    assert mre.tq == tqdef.set
    g.expr = mre
    g.args = a
    return g


def match_mre_op_subset_expr(f):
    if f.t != tdef.member_ref_expr:
        return
    id = getattr(f, "id", "")
    if id != "operator Set_expr":
        return
    if not f.fqnp_sw_EDSL:
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    e = match_set_expr(v[0])
    return e


def match_subset_expr(f):
    if f.t != tdef.call_expr:
        return False
    ct = getattr(f, "ct", "")
    if ct != "EDSL::Set_expr":
        return
    args = getattr(f, "args", ())
    if len(args) != 1:
        return
    e = match_mre_op_subset_expr(args[0])
    return e


def match_map_expr(f):
    return match_map_ref(f)


def match_set_via_set(f):
    if f.t != tdef.call_expr or f.tq != tqdef.set:
        return
    if not f.fqnp_sw_EDSL or f.id != "via":
        return
    args = f.args
    if len(args) < 2:
        return
    a0 = args[0]
    if a0.t != tdef.member_ref_expr or a0.id != "via":
        return
    a0_v = a0.v
    if len(a0_v) != 1:
        return
    sexpr = match_set_expr(a0_v[0])
    via_args = []
    for x in args[1:]:
        y = match_op_set_expr(x)
        if y is None:
            return
        via_args.append(y)
    g = Tv(
        tdef.embedded_set, tqdef.set, ct=getattr(f, "ct", ""), id=f.id, locus=f.locus
    )
    g.expr = sexpr
    g.iter_context = via_args
    return g


def match_set_expr(f):
    return (
        match_set_expr_chain(f)
        or match_set_via_set(f)
        or match_set_ref(f)
        or match_op_set_expr(f)
        or match_subset_expr(f)
        or match_subset_m_expr(f)
    )


def match_set_intrinsics(f):
    if f.t != tdef.call_expr:
        return False
    (ct, id) = (getattr(f, "ct", ""), getattr(f, "id", ""))
    if (ct, id) != ("EDSL::Intrinsics", "set_intrinsics"):
        return False
    args = getattr(f, "args", ())
    a = []
    for x in args:
        y = match_set_expr(x) or match_gridspace_ref(x)
        if not y:
            return
        a.append(y)
    if not a:
        return
    g = Tv(tdef.intrinsics, locus=f.locus)
    g.args = a
    return g


def match_iterate_on(f):
    if f.t != tdef.range_loop:
        return False
    v = getattr(f, "v", ())
    if len(v) != 3:
        return
    (v_guard, v_intrin, v_block) = v
    guard = match_scope_guard_decl(v_guard)
    if not guard:
        return
    if guard.ct != "EDSL::Intrinsics_scope_guard_type":
        return
    intrin = match_set_intrinsics(v_intrin)
    if not intrin:
        return
    block = match_block_stmt(v_block)
    if block == None:
        return
    if guard.id == "EDSL__compute_on_guard":
        g = Tv(tdef.compute_on, locus=f.locus)
    elif guard.id == "EDSL__loop_on_guard":
        g = Tv(tdef.loop_on, locus=f.locus)
    else:
        return
    g.args = intrin.args
    g.stmts = block.stmts
    return g


def match_set_iter_deref(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    id = getattr(f, "id", "")
    if not id == "operator*":
        return
    fqnp = getattr(f, "fqnp", "")
    if not is_set_type(ct=fqnp):
        return
    ct = getattr(f, "ct", "")
    if not is_elem_type(ct=ct):
        return
    g = Tv(tdef.set_iter_deref, tq, ct=ct, locus=f.locus)
    g.fqnp = fqnp
    return g


def match_loop_var_decl(f):
    if f.t != tdef.var_decl:
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    sid = match_set_iter_deref(v[0])
    if not sid:
        return
    g = Tv(tdef.loop_var_decl, f.tq, ct=f.ct, id=f.id, locus=f.locus)
    g.set_iter_deref = sid
    return g


def match_range_loop(f):
    if f.t != tdef.range_loop:
        return
    v = f.v
    if len(v) != 3:
        return
    (v_var, v_set, v_block) = v
    var = match_loop_var_decl(v_var)
    if not var:
        return
    set_expr = match_set_expr(v_set)
    if not set_expr:
        return
    block = match_block_stmt(v_block)
    if not block:
        return
    g = Tv(tdef.range_loop, var.tq, ct=var.ct, locus=f.locus)
    g.loop_var_decl = var
    assert set_expr.tq == tqdef.set
    g.expr = set_expr
    g.stmts = block.stmts
    return g


def match_order_kind(f):
    if f.t != tdef.decl_ref_expr:
        return
    ct = getattr(f, "ct", "")
    if ct != "EDSL::order::Order_kind":
        return
    g = Tv(tdef.order, tqdef.scalar, ct=ct, id=f.id, locus=f.locus)
    return g


def match_set_vertical_region(f):
    if f.t != tdef.call_expr:
        return
    (ct, id) = (getattr(f, "ct", ""), getattr(f, "id", ""))
    if (ct, id) != ("EDSL::Vertical_region", "set_vertical_region"):
        return False
    args = getattr(f, "args", ())
    if len(args) != 2 and len(args) != 3:
        return
    y0 = match_expr(args[0], tqdef.scalar)
    if not y0:
        return
    y1 = match_expr(args[1], tqdef.scalar)
    if not y1:
        return
    if len(args) == 3:
        y2 = match_order_kind(args[2])
        if not y2:
            return
    else:
        y2 = Tv(
            tdef.order,
            tqdef.scalar,
            ct="EDSL::order::Order_kind",
            id="forward",
            locus=f.locus,
        )
    g = Tv(tdef.set_vertical_region, locus=f.locus)
    g.args = [y0, y1, y2]
    return g


def match_vertical_region(f):
    if f.t != tdef.range_loop:
        return
    v = getattr(f, "v", ())
    if len(v) != 3:
        return
    (v_guard, v_set_vr, v_block) = v
    guard = match_scope_guard_decl(v_guard)
    if not guard:
        return
    if (guard.id, guard.ct) != (
        "EDSL__vertical_region_guard",
        "EDSL::Vertical_region_scope_guard_type",
    ):
        return
    set_vr = match_set_vertical_region(v_set_vr)
    if not set_vr:
        return
    block = match_block_stmt(v_block)
    g = Tv(tdef.vertical_region, locus=f.locus)
    g.args = set_vr.args
    g.stmts = block.stmts
    return g


def match_iter_stmt(f):
    return match_vertical_region(f) or match_iterate_on(f) or match_range_loop(f)


def match_retyped_field_ref(f):
    if f.t != tdef.decl_ref_expr:
        return
    type_decl = getattr(f, "type_decl", None)
    if type_decl is None:
        return
    typedef_decl = match_simple_typedef_decl(type_decl)
    if typedef_decl is None:
        return
    type_ref_ct = typedef_decl.type_ref.ct
    g = Tv(tdef.retyped_field_ref, tqdef.field, ct=f.ct, id=f.id, locus=f.locus)
    g.typedef_decl = typedef_decl
    field_ref = Tv(tdef.var_ref, tqdef.field, ct=type_ref_ct, id=f.id, locus=f.locus)
    clang_filter.set_defaults(field_ref)
    g.field_ref = field_ref
    return g


def match_simple_field_ref(f):
    if f.t != tdef.decl_ref_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.field:
        return
    g = Tv(tdef.var_ref, tq, ct=f.ct, id=f.id, locus=f.locus)
    return g


def match_field_ref(f):
    if f.t != tdef.decl_ref_expr:
        return
    return match_retyped_field_ref(f) or match_simple_field_ref(f)


def match_scalar_ref(f):
    if f.t != tdef.decl_ref_expr:
        return
    (tq, ct, id) = (getattr(f, "tq", 0), getattr(f, "ct", ""), getattr(f, "id", ""))
    if tq != tqdef.scalar or not (id and ct):
        return
    return Tv(tdef.scalar_ref, tq, ct=ct, id=id, locus=f.locus)


def match_vector_subscript_expr(f):
    if f.t != tdef.array_subscript_expr:
        return
    v = f.v
    if len(v) != 2:
        return
    var_ref = match_scalar_ref(v[0])
    if not var_ref:
        return
    expr = match_expr(v[1])
    if not expr:
        return
    g = Tv(tdef.vector_subscript_expr, tq=f.tq, ct=f.ct, locus=f.locus)
    g.var_ref = var_ref
    g.expr = expr
    return g


def match_vmap_ref(f):
    if f.t != tdef.decl_ref_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.map:
        return
    id = getattr(f, "id", "")
    if not id:
        return
    ct = getattr(f, "ct", "")
    if ct != "EDSL::Vertical_map":
        return
    if getattr(f, "v", ()) or getattr(f, "args", ()):
        return
    g = Tv(tdef.vmap_ref, tq, ct=ct, id=id)
    return g


def match_var_ref(f):
    return (
        match_scalar_ref(f)
        or match_field_ref(f)
        or match_set_ref(f)
        or match_vmap_ref(f)
        or match_gridspace_ref(f)
    )


def match_mre_dim_key(f):
    if f.t != tdef.member_ref_expr:
        return
    (id, v) = (getattr(f, "id", ""), getattr(f, "v", ()))
    if id != "operator Dim_key":
        return
    if len(v) != 1:
        return
    v0 = v[0]
    y = match_expr(v0)
    if not y:
        return
    g = Tv(tdef.dim_key, get_tq(y.ct), ct=y.ct, locus=f.locus)
    g.expr = y
    return g


def match_dim_key(f):
    if f.t != tdef.call_expr:
        return False
    (ct, id, args) = (getattr(f, "ct", 0), getattr(f, "id", ""), getattr(f, "args", ()))
    if (ct, id) != ("EDSL::Dim_key", "operator Dim_key"):
        return
    if len(args) != 1:
        return
    dkey = match_mre_dim_key(args[0])
    if not dkey:
        return
    return dkey


def match_field_access(f):
    if f.t != tdef.call_expr:
        return
    (tq, ct, id, args) = (
        getattr(f, "tq", 0),
        getattr(f, "ct", ""),
        getattr(f, "id", ""),
        getattr(f, "args", ()),
    )
    if not ct.startswith("EDSL::Field"):
        return
    if (tq, id) != (tqdef.field, "operator()"):
        return
    if not args:
        return
    fref = match_field_ref(args[0])
    if not fref:
        return
    g = Tv(tdef.field_access, tq, ct=fref.ct, id=fref.id, locus=f.locus)
    g.var_ref = fref
    dim_keys = []
    for k in args[1:]:
        dkey = match_dim_key(k)
        if not dkey:
            return
        dim_keys.append(dkey)
    g.dim_keys = dim_keys
    return g


def match_var_access(f):
    return match_field_access(f) or match_vmap_access(f)


def match_literal(f):
    # TODO: Do we need a string literal?
    if f.t in (tdef.bool_literal, tdef.integer_literal, tdef.floating_literal):
        g = f
        g.is_literal = True
        return g
    return


# def match_decl_ref_expr(f):
#     if f.t != tdef.decl_ref_expr: return
#     # this a very weak match, we leave the actual inspection to higher nodes with more context
#     return f


def match_fcall(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if not tq:
        return
    (ct, id) = (getattr(f, "ct", ""), getattr(f, "id", ""))
    if not ct:
        return  # we do not require an id here
    args = getattr(f, "args", ())
    a = []
    for x in args:
        y = match_expr(x)
        if not y:
            return
        a.append(y)
    fqnp = getattr(f, "fqnp", "")
    if fqnp == "edsl":
        g = Tv(tdef.edsl_fcall, tq, ct=ct, id=id, locus=f.locus)
    elif getattr(f, "fqnp_sw_EDSL", False):  # fqnp == 'EDSL':
        g = Tv(tdef.EDSL_fcall, tq, ct=ct, id=id, locus=f.locus)
    else:
        g = Tv(tdef.fcall, tq, ct=f.ct, id=f.id, locus=f.locus)
        if fqnp:
            g.fqnp = fqnp
    g.args = a
    return g


def match_field_reducton_mre(f):
    if f.t != tdef.member_ref_expr:
        return
    id = getattr(f, "id", "")
    if not id in ("nreduce", "neighbour_reduce", "neighbour_reduce"):
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    field_expr = match_expr(v[0], tq=tqdef.field)
    tq = getattr(field_expr, "tq", 0)
    ct = getattr(field_expr, "ct", "")
    g = Tv(tdef.nreduce_mre, tq=tq, ct=ct, id=f.id, locus=f.locus)
    g.expr = field_expr
    if not field_expr:
        return
    return g


def match_field_reduction(f):
    if f.t != tdef.call_expr:
        return
    fcall = match_fcall(f)
    if not fcall or fcall.t != tdef.EDSL_fcall:
        return
    if not fcall.id in ("nreduce", "neighbour_reduce", "neighbour_reduce"):
        return
    if fcall.tq != tqdef.field:
        return
    args = getattr(fcall, "args", ())
    field_expr = None
    set_expr = None
    weights = None
    offsets = None
    for x in args:
        if x.t == tdef.reduction_weights:
            weights = x
            continue
        if x.t == tdef.reduction_offsets:
            offsets = x
            continue
        if x.t == tdef.nreduce_mre and field_expr == None:
            field_expr = x.expr
            continue
        if x.tq == tqdef.set and set_expr == None:
            set_expr = x
            continue
        if x.tq == tqdef.field and field_expr == None:
            field_expr = x
            continue
        myerr(f, "unexpected argument in field reduction")

    if field_expr == None:
        myerr(f, "Missing field expression in reduction.")
    if set_expr == None:
        myerr(f, "Missing set expression in reduction.")
    g = Tv(tdef.nreduce, tqdef.field, ct=fcall.ct, locus=f.locus)
    g.field_expr = field_expr
    g.set_expr = set_expr
    if weights is not None:
        weights.offsets = offsets
    elif offsets:
        myerr(f,"found unexpected offsets (without related weights)")
    g.weights = weights
    return g


def match_scalar_fcall(f):
    if f.t != tdef.call_expr:
        return False
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    (ct, id) = (getattr(f, "ct", ""), getattr(f, "id", ""))
    if not (id and ct):
        return
    args = getattr(f, "args", ())
    a = []
    for x in args:
        y = match_expr(x, tqdef.scalar)
        if not y:
            return
        a.append(y)
    fqnp = getattr(f, "fqnp", "")
    if fqnp == "edsl":
        g = Tv(tdef.edsl_fcall, tq, ct=ct, id=id, locus=f.locus)
    elif getattr(f, "fqnp_sw_EDSL", False):
        g = Tv(tdef.EDSL_fcall, tq, ct=ct, id=id, locus=f.locus)
    else:
        g = Tv(tdef.fcall, tq, ct=f.ct, id=f.id, locus=f.locus)
        if fqnp:
            g.fqnp = fqnp
    g.args = a
    return g


def match_scalar_unop(f):
    if f.t != tdef.unop:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    ct = getattr(f, "ct", 0)
    if not ct:
        return
    id = getattr(f, "id", "")
    if not is_unop(id):
        return
    args = getattr(f, "args", ())
    if len(args) != 1:
        return
    expr = match_expr(args[0])
    if not expr:
        return
    g = Tv(tdef.unop, tq, ct=ct, id=id, locus=f.locus)
    g.args = [expr]
    return g


def match_field_unop(f):
    id = getattr(f, "id", "")
    if not id.startswith("operator"):
        return
    if getattr(f, "fqnp", "") != "EDSL":
        return
    op = id[8:]
    if not is_unop(op):
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.field:
        return
    ct = getattr(f, "ct", 0)
    if not ct:
        return
    args = getattr(f, "args", ())
    if len(args) != 1:
        return
    expr = match_expr(args[0])
    if not expr:
        return
    g = Tv(tdef.unop, tq, ct=ct, id=op, locus=f.locus)
    g.args = [expr]
    return g


def match_scalar_binop_assign(f):
    y = match_scalar_binop(f)
    if not y:
        return
    if not is_assign_op(y.id):
        return
    if y.tq != tqdef.scalar:
        die("internal error")
    g = y
    g.t = tdef.scalar_assign
    g.lhs = g.args[0]
    g.rhs = g.args[1]
    delattr(g, "args")
    return g


def match_scalar_assign(f):
    y = match_scalar_binop_assign(f)
    if y:
        return y
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    (ct, id) = (getattr(f, "ct", ""), getattr(f, "id", ""))
    if not id.startswith("operator"):
        return
    op = f.id[8:]
    if not is_assign_op(op):
        return
    args = getattr(f, "args", ())
    if len(args) != 2:
        return
    lhs = match_scalar_ref(args[0]) or match_var_access(args[0])
    if not lhs:
        return
    rhs = match_expr(args[1], tqdef.scalar)
    if not rhs:
        return
    g = Tv(tdef.scalar_assign, tq, ct=ct, id=op, locus=f.locus)
    g.lhs = lhs
    g.rhs = rhs
    return g


def strip_field_cast_expr(f):
    if (
        not isinstance(f, Tv)
        or f.t != tdef.field_cast_expr
        or f.expr.t != tdef.field_spec
    ):
        return f
    return f.expr


def strip_scalar_ctor(f):
    if f.t != tdef.scalar_ctor:
        return f
    args = getattr(f, "args", ())
    if len(args) == 1:
        return args[0]
    return f


def strip_field_ctor(f):
    if f.t != tdef.field_ctor:
        return f
    args = getattr(f, "args", ())
    if len(args) == 1:
        return args[0]
    return f


def match_operator_binop(f):
    y = match_fcall(f)
    if not y:
        return
    if y.t != tdef.EDSL_fcall:
        return
    id = getattr(y, "id", "")
    if not id.startswith("operator"):
        return
    op = f.id[8:]
    if not is_binop(op):
        return
    args = getattr(f, "args", ())
    if len(args) != 2:
        return
    lhs = match_expr(args[0])
    if not lhs:
        return
    rhs = match_expr(args[1])
    if not rhs:
        return
    tq = getattr(y, "tq", 0)
    if not tq:
        return
    ct = getattr(y, "ct", "")
    if not ct:
        return
    g = Tv(tdef.binop, tq=tq, ct=f.ct, id=op, locus=f.locus)
    g.args = [strip_field_cast_expr(lhs), strip_field_cast_expr(rhs)]
    return g


def match_operator_assign(f):
    g = match_operator_binop(f)
    if not g:
        return
    op = getattr(g, "id", "")
    if not is_assign_op(op):
        return
    g.t = tdef.var_assign
    return g


def match_field_binop(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.field:
        return
    id = getattr(f, "id", "")
    if not id.startswith("operator"):
        return
    op = f.id[8:]
    if not is_binop(op):
        return
    args = getattr(f, "args", ())
    if len(args) != 2:
        return
    lhs = match_expr(args[0])
    if not lhs:
        return
    rhs = match_expr(args[1])
    if not rhs:
        return
    g = Tv(tdef.binop, tq=tq, ct=f.ct, id=op, locus=f.locus)
    g.args = [strip_field_cast_expr(lhs), strip_field_cast_expr(rhs)]
    return g


def match_scalar_binop(f):
    if f.t != tdef.binop:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    (ct, id) = (getattr(f, "ct", ""), getattr(f, "id", ""))
    if not is_binop(id):
        return
    args = getattr(f, "args", ())
    if len(args) != 2:
        return
    lhs = match_expr(args[0], tqdef.scalar)
    if not lhs:
        return
    rhs = match_expr(args[1], tqdef.scalar)
    if not rhs:
        return
    g = Tv(tdef.scalar_binop, tq, ct=ct, id=id, locus=f.locus)
    g.args = [lhs, rhs]
    return g


def match_field2scalar_conv_from_mre(f):
    if f.t != tdef.member_ref_expr:
        return
    fqnp = getattr(f, "fqnp", "")
    if not fqnp.startswith("EDSL::Field"):
        return
    id = getattr(f, "id", "")
    if not f.id.startswith("operator "):
        return
    op = f.id[9:]
    if not is_scalar_type(uqt=op):
        return
    ct = op
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    expr = match_field_expr(v[0])
    if not expr:
        return
    conv = Tv(tdef.field2scalar_conv, get_tq(ct), ct=ct, locus=f.locus)
    conv.expr = expr
    return conv


def match_field2scalar_conv_from_ce(f):
    if f.t != tdef.call_expr:
        return
    fqnp = getattr(f, "fqnp", "")
    if not fqnp.startswith("EDSL::Field"):
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.scalar:
        return
    id = getattr(f, "id", "")
    if not id.startswith("operator "):
        return
    op = id[9:]
    if not is_scalar_type(uqt=op):
        return
    ct = getattr(f, "ct", "")
    if op != ct:
        return False
    args = getattr(f, "args", ())
    if len(args) != 1:
        return
    mre = match_field2scalar_conv_from_mre(args[0])
    if not mre:
        return
    # we have nothing to add to mre:
    return mre


def match_scalar_expr(f):
    tq = getattr(f, "tq", 0)
    if tq and tq != tqdef.scalar:
        return
    y = (
        match_literal(f)
        or match_init_list(f)
        or match_scalar_ref(f)
        or match_scalar_ctor(f)
        or match_scalar_binop(f)
        or match_scalar_unop(f)
        or match_vector_subscript_expr(f)
        or match_scalar_fcall(f)
        or match_field2scalar_conv_from_ce(f)
        or match_vmap_access(f)
    )
    if not y:
        return
    # remove ctor:
    y = strip_scalar_ctor(y)
    g = Tv(tdef.scalar_expr, tq=y.tq, ct=y.ct, locus=f.locus)
    g.expr = y
    return g


def match_field_dot_product(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    id = getattr(f, "id", "")
    if tq != tqdef.field:
        return
    if not getattr(f, "fqnp_sw_EDSL", False):
        return
    if f.id != "dot_product":
        return
    args = getattr(f, "args", ())
    if len(args) != 2:
        return
    a = []
    for x in args:
        y = match_expr(x, tqdef.field)
        if not y:
            return
        a.append(y)
    dp = Tv(tdef.dot_product, tq, ct=getattr(f, "ct", ""), locus=f.locus)
    dp.args = a
    return dp


def match_mre_field_init(f):
    if f.t != tdef.member_ref_expr:
        return False
    id = getattr(f, "id", "")
    if id != "init":
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    fspec = match_field_spec(v[0])
    if fspec:
        g = Tv(tdef.mre_field_init, fspec.tq, ct=fspec.ct, locus=f.locus)
        g.field_spec = fspec
        return g

    fref = match_field_ref(v[0])
    if fref:
        g = Tv(tdef.mre_field_init, fref.tq, ct=fref.ct, locus=f.locus)
        g.field_ref = fref
        return g

    return


def match_mre_set_field(f):
    if f.t != tdef.member_ref_expr:
        return False
    id = getattr(f, "id", "")
    if not id in ("field", "id"):
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    se = match_set_expr(v[0])
    return se


def match_set_method(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.field:
        return
    id = getattr(f, "id", "")
    if not id in ("field", "id"):
        return
    args = getattr(f, "args", ())
    if len(args) != 1:
        return
    se = match_mre_set_field(args[0])
    g = Tv(tdef.set_method, tq, ct=getattr(f, "ct", ""), id=id, locus=f.locus)
    assert se.tq == tqdef.set
    g.expr = se
    return g


def match_field_init(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.field:
        return
    id = getattr(f, "id", "")
    if id != "init":
        return
    args = getattr(f, "args", ())
    if not args:
        return
    mre = match_mre_field_init(args[0])
    a = []
    for x in args[1:]:
        y = match_expr(x)
        if not y:
            return
        a.append(y)
    g = Tv(tdef.field_init, tq, ct=getattr(f, "ct", ""), locus=f.locus)
    g.args = a
    fs = getattr(mre, "field_spec", None)
    if fs:
        g.field_spec = fs
        return g

    fr = getattr(mre, "field_ref", None)
    if fr:
        g.field_ref = fr
        return g
    return


def match_paren_expr(f):
    if f.t != tdef.paren_expr:
        return
    v = getattr(f, "v", ())
    if len(v) != 1:
        return
    expr = match_expr(v[0])
    if not expr:
        die("internal error: unexpected unresolved expression")
    g = Tv(
        tdef.paren_expr, tq=getattr(f, "tq", 0), ct=getattr(f, "ct", ""), locus=f.locus
    )
    g.expr = expr
    return g


def match_field_call_expr(f):
    if f.t != tdef.call_expr:
        return
    tq = getattr(f, "tq", 0)
    if tq != tqdef.field:
        return
    y = (
        match_field_spec(f)
        or match_field_binop(f)
        or match_field_dot_product(f)
        or match_field_init(f)
        or match_field_ctor(f)
        or match_set_method(f)
        or match_field_access(f)
        or match_field_reduction(f)
        or match_fcall(f)
    )
    if not y:
        return
    y = strip_field_ctor(y)
    return y


def match_field_expr(f):
    return (
        match_field_unop(f)
        or match_field_ref(f)
        or match_field_call_expr(f)
        or match_field_cast_expr(f)
        or match_field_reducton_mre(f)
    )


def match_expr(f, tq=0, error_msg=None):
    if tq and tq != getattr(f, "tq", 0):
        return
    return (
        match_paren_expr(f)
        or match_operator_assign(f)
        or match_operator_binop(f)
        or match_reduction_weights(f)
        or match_reduction_offsets(f)
        or match_scalar_expr(f)
        or match_field_expr(f)
        or match_set_expr(f)
        or match_map_expr(f)
        or match_fcall(f)
        or match_gridspace_expr(f)
        or handle_unresolved(f, error_msg)
    )


def match_field_assign(f):
    if f.t != tdef.call_expr:
        return
    (tq, ct, id, args) = (
        getattr(f, "tq", 0),
        getattr(f, "ct", ""),
        getattr(f, "id", ""),
        getattr(f, "args", ()),
    )
    if tq != tqdef.field or len(args) != 2:
        return
    if id.startswith("operator"):
        id = id[8:]
    if not is_assign_op(id):
        return
    lhs = match_field_ref(args[0]) or match_field_access(args[0])
    if not lhs:
        return myerr(f, "field_assign: lhs problem")
    rhs = match_expr(args[1])
    if not rhs:
        return myerr(f, "field_assign: rhs problem")
    g = Tv(tdef.field_assign, tq, ct=ct, id=id, locus=f.locus)
    g.lhs = lhs
    g.rhs = rhs
    return g


def match_assign_stmt(f):
    return match_field_assign(f) or match_scalar_assign(f)


def match_return_stmt(f):
    if f.t != tdef.return_stmt:
        return
    v = getattr(f, "v", ())
    if not v:
        g = Tv(tdef.return_stmt, locus=f.locus)
        return g
    if len(v) != 1:
        return
    expr = match_expr(v[0])
    if not expr:
        reject("invalid return statement")
    g = Tv(tdef.return_stmt, expr.tq, ct=expr.ct, locus=f.locus)
    g.expr = expr
    return g


def match_if_stmt(f):
    if f.t != tdef.if_stmt:
        return
    v = getattr(f, "v", ())
    if len(v) < 2:
        return
    cond = match_expr(v[0], tqdef.scalar)
    if not cond:
        return
    tcase = match_stmt(v[1])
    if not tcase:
        return
    if len(v) > 2:
        assert len(v) == 3
        fcase = match_stmt(v[2])
    else:
        fcase = None

    g = Tv(tdef.if_stmt, locus=f.locus)
    g.cond = cond
    if tcase.t == tdef.block_stmt:
        g.tcase = tcase.stmts
    else:
        g.tcase = [tcase]
    if fcase:
        if fcase.t == tdef.block_stmt:
            g.fcase = fcase.stmts
        else:
            g.fcase = [fcase]
    return g


def match_stmt(f, error_msg=None):
    return (
        match_decl_stmt(f)
        or match_iter_stmt(f)
        or match_assign_stmt(f)
        or match_if_stmt(f)
        or match_return_stmt(f)
        or match_block_stmt(f)
        or match_field_spec_stmt(f)
        or match_expr(f)
        or handle_unresolved(f, error_msg)
    )


def match_block_stmt(f):
    if isinstance(f, (list, tuple)):
        v = f
    elif isinstance(f, Tv):
        if f.t != tdef.block_stmt:
            return
        v = f.v
    else:
        return
    a = []
    for stmt in v:
        y = match_stmt(stmt, error_msg="Statement expected.")
        if y:
            a.append(y)
        else:
            die("unexpected case")
    g = Tv(tdef.block_stmt, locus=f.locus)
    g.stmts = a
    return g


def match_parm_decl(f):
    if f.t != tdef.parm_decl:
        return
    g = copy(f)
    g.t = tdef.var_decl
    y = match_var_decl(g)
    return y


def match_parameters(f):
    if f.t != tdef.parameters:
        return
    v = getattr(f, "v", ())
    a = []
    for x in v:
        y = match_parm_decl(x)
        if not y:
            return
        a.append(y)
    g = Tv(tdef.parameters, locus=f.locus)
    g.parm_decls = a
    return g


def match_function_decl(f):
    if f.t != tdef.function_decl:
        return
    tq = getattr(f, "tq", 0)
    ct = getattr(f, "ct", "")
    id = getattr(f, "id", "")
    g = Tv(tdef.function_decl, tq, ct=ct, id=id, cx=f.cx, locus=f.locus)
    v = getattr(f, "v", ())
    if len(v) < 1 or len(v) > 2:
        ICE()
    params = match_parameters(v[0])
    if params.t != tdef.parameters:
        ICE()
    g.parm_decls = params.parm_decls
    if len(v) == 2:
        block = v[1]
        if block.t != tdef.block_stmt:
            ICE()
        y = match_block_stmt(block)
        if y:
            g.stmts = y.stmts
    return g


def match_decl(decl, error_msg=None):
    y = (
        match_function_decl(decl)
        or match_var_decl(decl)
        or match_type_decl(decl)
        or handle_unresolved(decl, error_msg)
    )
    if not y:
        return
    return y


def match_edsl_namespace(f):
    assert f.t == tdef.namespace
    if f.id != "edsl":
        return
    body = f.v
    if body.t != tdef.namespace_body:
        return
    a = []
    for decl in body.v:
        y = match_decl(decl, error_msg="Expected declaration.")
        if y:
            a.append(y)
        else:
            return myerr(f, "Expected declaration.")
    g = Tv(tdef.namespace, locus=f.locus)
    g.body = a
    return g


def match_translation_unit(f):
    assert f.t == tdef.translation_unit
    a = []
    for ns in f.v:
        if ns.t != tdef.namespace:
            return myerrr(f, "expected namespace")
        y = match_edsl_namespace(ns)
        if y:
            a.extend(y.body)
        else:
            return myerr(f, "expected edsl namespace")
    g = Tv(tdef.translation_unit, id=f.id, locus=f.locus)
    g.body = a
    g.filename = getattr(f, "parse_filename", None)
    g.cwd = getattr(f, "parse_cwd", None)
    g.edsl_extension_src = getattr(f, "edsl_extension_src", None)
    return g


def get_parse_tree(cf_tree):
    parse_tree = match_translation_unit(cf_tree)
    if parse_tree:
        return parse_tree
    die("parsing failed")
