#
# SIR -> Itfir ("interface itermediate representation")
#

from collections.abc import Iterable

import dawn4py
from dawn4py.serialization import SIR, AST

from common import *

g_itfir_id = 0


# types:


def new_itfir_id():
    global g_itfir_id
    g_itfir_id += 1
    return g_itfir_id


class Itfir(DispFormGen):
    def __init__(
        self,
        grid_type_str,
        filename,
        stencils,
        gdecls,
        backend_name,
        ofile_name,
        have_cdsl_support,
    ):
        self._id = new_itfir_id()
        self.grid_type_str = grid_type_str
        self.filename = filename
        self.stencils = stencils
        self.gdecls = gdecls
        self.backend_name = backend_name
        self.ofile_name = ofile_name or None
        self.have_cdsl_support = have_cdsl_support
        self.LibTag = None

        if grid_type_str == "Cartesian":
            self.enable_name_mangling = have_cdsl_support
            if not backend_name in ("cxxnaive", "cuda"):
                user_error("Unsuitable backend for Cartesion grid:", backend_name)
        elif grid_type_str == "Unstructured":
            self.enable_name_mangling = False
            if backend_name == "cxxnaiveico":
                self.LibTag = "atlasInterface::atlasTag"
            if not backend_name in ("cxxnaiveico", "cudaico"):
                user_error("Unsuitable backend for unstructured grid:", backend_name)
        if backend_name in ("cuda", "cudaico"):
            self.is_cuda_backend = True
        elif backend_name in ("cxxnaive", "cxxnaiveico"):
            self.is_cuda_backend = False
        else:
            user_error("Unknown backend:", backend_name)

    @classmethod
    def fromSIR(cls, sir, backend_name, ofile_name, have_cdsl_support):
        (grid_type_str, filename, stencils, gdecls) = dispatch(sir)
        return cls(
            grid_type_str,
            filename,
            stencils,
            gdecls,
            backend_name,
            ofile_name,
            have_cdsl_support,
        )


class Dnode(DispFormGen):
    # simplifies SIR inspection
    def __init__(self, s, resolve=False, resolve_attr=None):
        self._node_name = sir_type2name(s)
        attr = [x[0].name for x in s.ListFields()]
        if resolve_attr:
            resolve = True
        self._attr = attr
        if resolve:
            for k in attr:
                if resolve_attr and not k in resolve_attr:
                    continue
                v = getattr(s, k, None)
                assert v is not None
                dv = dispatch(v)
                setattr(self, k, dv)


class ParmDecl(DispFormGen):
    def __init__(self, var_type, var_name, is_struct, is_vertical, is_dense):
        self.Type = var_type
        self.name = var_name
        self.is_struct = is_struct
        self.is_vertical = is_vertical
        self.is_dense = is_dense


class Stencil(DispFormGen):
    def __init__(self, name, parm_decls):
        self.name = name
        self.parm_decls = parm_decls
        self.gdecls = None
        is_struct_p = [(1 if pd.is_struct else 0) for pd in parm_decls]
        if sum(is_struct_p) == len(parm_decls):
            self.is_struct = True
        elif sum(is_struct_p) == 0:
            self.is_struct = False
        else:
            die(
                "Unexpected mixed struct and unstruct parameters in stencil "
                + name
                + "."
            )


class GlobalDecl:
    # single global variable declaration
    def __init__(self, type_name, var_name, init_value):
        self.type_name = type_name
        self.var_name = var_name
        self.init_value = init_value


# global consts:


def init_const():
    global g_LocationType_v2k, g_LocationType_k2v
    g_LocationType_v2k = {}
    g_LocationType_k2v = {}
    for k, v in AST.LocationType.items():
        assert g_LocationType_v2k.get(v) is None
        g_LocationType_k2v[k] = v
        g_LocationType_v2k[v] = k


init_const()


def sir_type2name(s):
    stype = str(type(s))
    p0 = stype.find("'SIR.SIR_pb2.") + 13
    p1 = stype.find("'", p0)
    name = stype[p0:p1]
    name = name.replace(".", "_")
    return name


# dispatcher funs:


def dfun_SIR(f):
    if f.gridType == AST.GridType.Value("Cartesian"):
        grid_type_str = "Cartesian"
    elif f.gridType == AST.GridType.Value("Unstructured"):
        grid_type_str = "Unstructured"
    else:
        ICE()

    filename = getattr(f, "filename", "")
    gdecls = dispatch(f.global_variables)
    stencils = [dfun_Stencil(s) for s in f.stencils]
    # Here we inject the access information about global variables into
    # each stencil. This follows the scheme of the Dawn generated code.
    for s in stencils:
        s.gdecls = gdecls
    return (grid_type_str, filename, stencils, gdecls)


def dfun_statements_pb2_GlobalVariableMap(f):
    gdecls = []
    for k, v in f.map.items():
        if v.is_constexpr:
            continue  # there is no run-time access to const globals
        if v.HasField("boolean_value"):
            t = "bool"
            init_value = v.boolean_value
        elif v.HasField("integer_value"):
            t = "int"
            init_value = v.integer_value
        elif v.HasField("double_value"):
            t = "double"
            init_value = v.double_value
        elif v.HasField("float_value"):
            t = "float"
            init_value = v.float_value
        else:
            die("unsuported case")
        gd = GlobalDecl(type_name=t, var_name=k, init_value=init_value)
        gdecls.append(gd)
    return gdecls


def dfun_Stencil(f):
    name = f.name
    parm_decls = []
    for x in f.fields:
        if x.is_temporary:
            continue
        y = dispatch(x)
        assert isinstance(y, ParmDecl)
        parm_decls.append(y)
        stencil = Stencil(name, parm_decls)
    return stencil


def dfun_statements_pb2_Field(f):
    name = f.name
    field_dimensions = dispatch(f.field_dimensions)
    parm_decl = ParmDecl(
        field_dimensions._storage_type,
        name,
        field_dimensions.is_struct,
        field_dimensions.is_vertical,
        field_dimensions.is_dense,
    )
    return parm_decl


def dfun_statements_pb2_FieldDimensions(f):
    tmp = Dnode(f, resolve=True)
    struct_hdims = getattr(tmp, "cartesian_horizontal_dimension", ())
    unstruct_hdims = getattr(tmp, "unstructured_horizontal_dimension", ())
    mask_k = getattr(tmp, "mask_k", 0)
    g = Dnode(f)
    is_struct = bool(struct_hdims)
    is_unstruct = bool(unstruct_hdims)
    if mask_k and not (is_struct or is_unstruct):
        # "pure vertical" (does not apply for struct grids yet)
        is_vertical = True
    else:
        is_vertical = False

    g.is_struct = is_struct
    g.is_unstruct = is_unstruct
    g.is_vertical = is_vertical

    if is_struct:
        g.mask_h = struct_hdims._mask_cart
        storage_type = "storage_"
        if g.mask_h[0]:
            storage_type += "i"
        if g.mask_h[1]:
            storage_type += "j"
        if mask_k:
            storage_type += "k"
        g._storage_type = storage_type + "_t"
        g.is_dense = True
    elif is_unstruct:
        dense_element_name = g_LocationType_v2k[unstruct_hdims.chain[0]].lower()
        is_dense = unstruct_hdims.is_dense
        g.is_dense = is_dense
        if is_dense:
            g._storage_type = "dawn::" + dense_element_name + "_field_t"
        else:
            g._storage_type = "dawn::sparse_" + dense_element_name + "_field_t"
    elif is_vertical:
        g.is_dense = None
        g._storage_type = "dawn::vertical_field_t"
    else:
        ICE()
    return g


def dfun_statements_pb2_UnstructuredDimension(f):
    g = Dnode(f, resolve=True)
    assert not g.iter_space.include_center  # for now
    g.chain = g.iter_space.chain
    nc = len(g.chain)
    assert nc > 0
    g.is_dense = nc == 1
    return g


def dfun_statements_pb2_UnstructuredIterationSpace(f):
    g = Dnode(f, resolve=True)
    g.include_center = getattr(f, "include_center", False)
    if g.include_center:
        die("not supported yet: unstruct chain with center")
    return g


def dfun_statements_pb2_CartesianDimension(f):
    # a CartesianDimension only covers the structured (i,j) space
    # (not the vertical, see Dawn: statements.proto)
    g = Dnode(f, resolve=True)
    mc = (getattr(g, "mask_cart_i", 0), getattr(g, "mask_cart_j", 0))
    ij_code = ""
    if mc[0] == 1:
        ij_code += "i"
    if mc[1] == 1:
        ij_code += "j"
    if not ij_code:
        ij_code = "empty"
    storage_type = "storage_" + ij_code + "_t"
    g._mask_cart = mc
    g._storage_type = storage_type
    g.is_struct = True
    return g


# dispatcher:


def dispatch(f):
    if isinstance(f, (str, bool, int, float)):
        return f
    elif isinstance(f, Iterable):
        a = []
        for x in f:
            y = dispatch(x)
            if y == None:
                die("bad case")
            a.append(y)
        return a
    else:
        key = sir_type2name(f)
        fun = dispatcher.get(key, None)
        if fun is not None:
            # we have no locus attribute in SIR form
            # g_locus_stack.push(f.locus)
            result = fun(f)
            assert result is not None
            return result
        else:
            print("unknown key:", key)
            ICE()


dispatcher = {}


def init_dispatcher():
    global dispatcher
    for k in list(globals().keys()):
        if not k.startswith("dfun_"):
            continue
        name = k[5:]
        v = globals()[k]
        if callable(v):
            dispatcher[name] = v


init_dispatcher()
