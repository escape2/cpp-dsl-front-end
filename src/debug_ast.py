#!/usr/bin/env python3
# Distribution: see LICENSE.txt

from enum import Enum
from common import *

__ALL__ = ["Terminal", "Fcall"]


def reset_sir_globals():
    global g_sir_globals
    g_sir_globals = ""


class Terminal:
    def __init__(self, terminal):
        self.t = terminal

    def str(self):
        return self.t


class Fcall(DispFormGen):
    def __init__(self, fname, *args, **kwargs):
        assert not (args and kwargs)
        self.fname = fname
        self.args = args
        self.kwargs = kwargs

class BuiltinType(Enum):
    Boolean = "AST.BuiltinType.Boolean"
    Double = "AST.BuiltinType.Double"
    Float = "AST.BuiltinType.Float"
    Integer = "AST.BuiltinType.Integer"


class VerticalRegion(Enum):
    Forward = "AST.VerticalRegion.Forward"
    Backward = "AST.VerticalRegion.Backward"


class Interval(Enum):
    Start = "AST.Interval.Start"
    End = "AST.Interval.End"


class GridType:
    def Value(arg):
        return Fcall("AST.GridType.Value", arg)


class LocationType:
    def Value(arg):
        return Fcall("AST.LocationType.Value", arg)


