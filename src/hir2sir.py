#!/usr/bin/env python3
# Distribution: see LICENSE.txt

from inspect import currentframe, getframeinfo
from copy import copy
from collections import OrderedDict
import os

import cdsl_config
from common import *
import HIR
from ext_sir import ExtSIR

CDSL_DEBUG_SIR_STAGE = not (os.environ.get("CDSL_DEBUG_SIR_STAGE") or "").lower() in (
    "",
    "0",
    "false",
    "disabled",
)
if CDSL_DEBUG_SIR_STAGE:
    import debug_sir as SIR
    import debug_ast as AST
    import debug_sir_utils as sir_utils
else:
    try:
        from dawn4py.serialization import SIR, AST
        from dawn4py.serialization import utils as sir_utils
    except ImportError:
        general_error("Could not import Dawn modules, please check environment.")

from node import Node
from HIR import Hnode, BinOp
import sa2hir
from sa2hir import hir_str, id2unop
from common import *

hir_type2sir = {
    "bool": AST.BuiltinType.Boolean,  # HIR-extension
    "int": AST.BuiltinType.Integer,
    "float": AST.BuiltinType.Float,
    "double": AST.BuiltinType.Double,
}

hir_LoopOrder2sir = {
    "forward": AST.VerticalRegion.Forward,
    "backward": AST.VerticalRegion.Backward,
    #'parallel' : SIR.VerticalRegion.Parallel, # not supported yet ?
}


hir_binop2sir = {v: k for k, v in sa2hir.id2binop.items()}
assert len(hir_binop2sir) == len(sa2hir.id2binop)


hir_unop2sir = {v: k for k, v in sa2hir.id2unop.items()}
assert len(hir_unop2sir) == len(sa2hir.id2unop)

def get_GridDimension(node):
    if isinstance(node,Offset):
        return node.ContentsModel[0]
    else:
        return node.GridDimension


class VRlist(Node):
    def __init__(self, vrlist):
        super().__init__("VRlist")
        self.list = vrlist


class Dim(DispFormGen):
    def __init__(self, gd):
        dimension_type = getattr(gd, "dimension_type", None)
        self.name = gd.name
        self.GridDimension = gd
        self.is_struct = dimension_type is None
        self.is_horizontal = dimension_type is not None
        self.is_vertical = not self.is_horizontal
        self.is_dense = dimension_type == "dense"
        self.pos = None

    def str(self):
        a = []
        for k in self.__dict__.keys():
            a.append(str(k) + "=" + str(getattr(self, k)))
        s = str(type(self)) + "(" + ", ".join(a) + ")"
        return s


class DomainParallelDim(Dim):
    def __init__(self, gd):
        super().__init__(gd)
        self.is_horizontal = True
        self.is_vertical = not self.is_horizontal


class VerticalDim(Dim):
    def __init__(self, gd):
        super().__init__(gd)
        self.is_horizontal = False
        self.is_vertical = not self.is_horizontal


class ParallelDim(Dim):
    def __init__(self, gd):
        super().__init__(gd)


#class SirContainer(DispFormGen):
#    def __init__(self, sir, gridtype):
#        self.sir = sir
#        self.gridtype = gridtype



def init0():
    global unop2id
    unop2id = {}
    for (k, v) in id2unop.items():
        unop2id[v] = k


def reset():
    global dimset, cur_struct_dim2pos, cur_struct_pos2dim, StencilDecls, DomCompScope
    global verticaldim, g_GridType
    dimset = {}
    cur_struct_dim2pos = {}
    cur_struct_pos2dim = [None, None, None]
    StencilDecls = {}
    DomCompScope = False
    verticaldim = None
    g_GridType = None


init0()


def set_current_dim_pos(name, pos):
    global cur_struct_dim2pos, cur_struct_pos2dim
    if pos != None:
        assert pos >= 0 and pos <= 2
        assert cur_struct_dim2pos.get(name) == None
        assert cur_struct_pos2dim[pos] == None
    cur_struct_dim2pos[name] = pos
    if pos != None:
        cur_struct_pos2dim[pos] = name
    else:
        cur_struct_pos2dim = [None, None, None]
        for (d, p) in cur_struct_dim2pos.items():
            if p != None:
                cur_struct_pos2dim[p] = d


def struct_FieldAccess(h):
    Offset = getattr(h, "ContentsModel", ())
    assert len(Offset) <= 3
    distvec = [0, 0, 0]
    for os in Offset:
        gd = os.get_GridDimension()
        dist = os.distance
        idist = int(dist)
        assert str(idist) == dist
        pos = cur_struct_dim2pos.get(gd.name)
        assert pos != None
        distvec[pos] = idist
    decl = StencilDecls.get(h.name) or die("bad case")
    decl_cm = getattr(decl, "ContentsModel", (None,))
    ftype = decl_cm[0] or die("bad case")
    gdims = decl_cm[1:] or die("bad case")
    for gd in gdims:
        pos = cur_struct_dim2pos.get(gd.name)
        assert pos != None and 0 <= pos and pos <= 2
    return sir_utils.make_field_access_expr(name=h.name, offset=distvec)


def unstruct_FieldAccess(h):
    sir_name = h.name
    offset_list = getattr(h, "ContentsModel", ())
    sir_utils.make_unstructured_offset(has_offset=True)
    sir_horizontal_offset = sir_utils.make_unstructured_offset(has_offset=h.indirect_dense_access)
    sir_vertical_shift = 0
    if offset_list:
        if len(offset_list) != 1: die("unexpected number of offsets")
        offset = offset_list[0]
        offset_gdim = offset.get_GridDimension()
        if offset_gdim.getcm() or offset_gdim.chain: die("unexpected case: offset to horizontal dimension")
        sir_vertical_shift = str2int(offset.distance)
        sir_vertical_indirection = None
    return sir_utils.make_unstructured_field_access_expr(
        name=sir_name, horizontal_offset=sir_horizontal_offset, vertical_shift=sir_vertical_shift
    )


def dfun_FieldAccess(h):
    is_struct = getattr(h, "_is_struct", None)
    if is_struct is None:
        decl = StencilDecls.get(h.name) or die("bad case")
        is_struct = decl._is_struct
    if is_struct:
        s = struct_FieldAccess(h)
    else:
        s = unstruct_FieldAccess(h)
    return s


def dfun_Type(h):
    s = hir_type2sir.get(h.name) or die("bad case")
    return s


def dfun_Literal(h):
    cm = h.getcm()
    h_type = cm[0]
    value = h.value
    s_type = dispatch(h_type)
    assert s_type
    s = sir_utils.make_literal_access_expr(value=value, type=s_type)
    return s


def sir_unop(h, OpName):
    op_sir = unop2id.get(OpName) or die("bad case")
    cm = getattr(h, "ContentsModel", (None,))
    assert len(cm) == 1
    expr = cm[0]
    expr_sir = dispatch(expr)
    s = sir_utils.make_unary_operator(op_sir, expr_sir)
    return s


def unused_sir_binop(h, OpName):
    op_sir = binop2id.get(OpName) or die("bad case")
    (lhs, rhs) = h.ContentsModel
    lhs_sir = dispatch(lhs)
    rhs_sir = dispatch(rhs)
    s = sir_utils.make_binary_operator(lhs_sir, op_sir, rhs_sir)
    return s


def old_dfun_binaryOpModel(h):
    sir_op = hir_binop2sir.get(h.op) or die("unknown binop: " + h.op)
    args = h.getcm()
    (left_expr, right_expr) = dispatch(args)
    s = sir_utils.make_binary_operator(left_expr, sir_op, right_expr)
    return s

def dfun_BinaryOp(h):
    sir_op = hir_binop2sir.get(h.op) or die("unknown binop: " + h.op)
    args = h.getcm()
    (left_expr, right_expr) = dispatch(args)
    s = sir_utils.make_binary_operator(left=left_expr, op=sir_op, right=right_expr)
    return s


def dfun_UnaryOp(h):
    sir_op = hir_unop2sir.get(h.op) or die("unknown unop: " + h.op)
    args = h.getcm()
    assert len(args) == 1
    sir_expr = dispatch(args[0])
    s = sir_utils.make_unary_operator(op=sir_op, operand=sir_expr)
    return s


def dfun_mulOp(h):
    return sir_binop(h, "mulOp")


def dfun_plusOp(h):
    return sir_binop(h, "plusOp")


def dfun_minusOp(h):
    return sir_binop(h, "minusOp")


def dfun_unaryMinus(h):
    return sir_unop(h, "unaryMinus")


def dfun_AssignmentStmt(h):
    assert isinstance(h,HIR.AssignmentStmt)
    (lhs, exprModel) = getattr(h, "ContentsModel", ())
    lhs_sir = dispatch(lhs)
    rhs = exprModel
    rhs_sir = dispatch(rhs)
    op = getattr(h, "op", "=")
    s = sir_utils.make_assignment_stmt(left=lhs_sir, right=rhs_sir, op=op)
    return s


def gen_sir_interval(di):
    bounds = copy(di.getcm())
    for b in bounds:
        if isinstance(b,HIR.Literal):
            b._mark = AST.Interval.Start
            b._offset = int(b.value)
        elif isinstance(b, HIR.DimensionLevel):
            cm = b.getcm()
            assert len(cm) == 1 or len(cm) == 2
            va = cm[0]
            vname = va.name
            if len(cm) == 2:
                offset = str2int(cm[1].value)
                assert offset is not None
            else:
                offset = 0
            if vname == "start_level":
                b._mark = AST.Interval.Start
                b._offset = offset
            elif vname == "end_level":
                b._mark = AST.Interval.End
                b._offset = offset
            else:
                die("bad case")
        else:
            die("unexpected case")
    interval_sir = sir_utils.make_interval(
        lower_level=bounds[0]._mark,
        upper_level=bounds[1]._mark,
        lower_offset=bounds[0]._offset,
        upper_offset=bounds[1]._offset
    )
    return interval_sir


def dfun_VerticalRegion(h):
    set_current_dim_pos(verticaldim.name, 2)

    (divals, comps) = h.getcm() or die("bad case")
    assert len(divals) and len(comps)
    assert len(divals) == len(comps) or len(comps) == 1
    vrlist = []
    for dival_pos in range(len(divals)):
        di = divals[dival_pos]
        order = getattr(di, "LoopOrder", "forward")
        order_sir = hir_LoopOrder2sir.get(order)
        assert order != None
        ival_sir = gen_sir_interval(di)
        ast = comps[dival_pos]
        ast_sir = dispatch(ast)
        vr_sir = sir_utils.make_vertical_region_decl_stmt(ast=ast_sir, interval=ival_sir, loop_order=order_sir)
        vrlist.append(vr_sir)
    set_current_dim_pos(verticaldim.name, None)
    return VRlist(vrlist)


def dfun_Computation(h):
    cm = getattr(h, "ContentsModel", ())
    (gdims, bstmt) = cm
    for gd in gdims:
        dim = dimset[gd.name]
        set_current_dim_pos(dim.name, dim.pos)
    stmts = bstmt.getcm()
    stmts_sir = dispatch(stmts)
    for gd in gdims:
        set_current_dim_pos(gd.name, None)
    comp_sir = sir_utils.make_ast(root=stmts_sir)
    return comp_sir


def dfun_BlockStmt(h):
    cm = getattr(h, "ContentsModel", ())
    stmts_sir = dispatch(cm)
    bstmt_sir = sir_utils.make_block_stmt(stmts_sir)
    return bstmt_sir


def gen_sir_field_dimensions(h):
    field_cm = getattr(h, "ContentsModel", ())
    struct_hdims = []
    unstruct_hdims = []
    vertical_dim = None
    other_dims = []
    for x in field_cm:
        if isinstance(x, Hnode) and isinstance(x, HIR.GridDimension):
            dim = dimset[x.name]
            if dim.is_horizontal:
                if dim.is_struct:
                    struct_hdims.append(dim)
                else:
                    unstruct_hdims.append(dim)
            elif dim.is_vertical:
                vertical_dim = dim
            else:
                other_dims.append(dim)
    mask = [1, 1, 1]
    if vertical_dim == None:
        mask[2] = 0
    if struct_hdims:
        assert not unstruct_hdims
        assert len(struct_hdims) == 2
        return sir_utils.make_field_dimensions_cartesian(mask=mask)
    elif unstruct_hdims:
        assert not struct_hdims
        # This is a special point:
        # The font-end separates dense and sparse aspects in diffferent dimensions,
        # the SIR does not. Here we must adapt to SIR style.
        # For this we reinterpret the non-iterative orig_dims of our last unstruct dimension
        # as active iterations and then drop all previous unstructured dimensions.
        # This is possible because we have a single chain iteration concept.
        # If we want to have multiple sparse dimensions in the future then we have to
        # revisit this point and maybe use the font-end style everywhere.
        if len(unstruct_hdims) > 1:
            unstruct_hdims = [unstruct_hdims[-1]]
        chain_sir = []
        lt_chain = None
        for d in unstruct_hdims:
            gd = d.GridDimension
            assert lt_chain is None
            lt_chain = getattr(gd, "chain", None) or ICE()
            sir_lt_chain = dispatch(lt_chain)
        field_dims_sir = sir_utils.make_field_dimensions_unstructured(
           locations=sir_lt_chain, mask_k=mask[2], include_center=False
        )
        return field_dims_sir
    elif vertical_dim:
        if g_GridType == "Cartesian":
            mask = [0, 0, 1]
            return sir_utils.make_field_dimensions_cartesian(mask=mask)
        elif g_GridType == "Unstructured":
            my_sir = sir_utils.make_field_dimensions_vertical()
            return my_sir
        else:
            ICE()
    else:
        die("A field declaration without spatial dimensions is not supported yet.")

def is_struct_field(field):
    known_state = getattr(field, "_is_struct", None)
    if known_state != None:
        return known_state
    cm = getattr(field, "ContentsModel", ())
    has_unstruct_dim = False
    for gd in cm[1:]:
        if getattr(gd, "dimension_type", None):
            has_unstruct_dim = True

    return not has_unstruct_dim


def is_unstruct_field(field):
    known_state = getattr(field, "_is_struct", None)
    if known_state != None:
        return known_state
    cm = getattr(field, "ContentsModel", ())
    has_unstruct_dim = False
    for gd in cm[1:]:
        if getattr(gd, "dimension_type", None):
            has_unstruct_dim = True
    return has_unstruct_dim


def is_vertical_field(field):
    cm = getattr(field, "ContentsModel", ())
    if len(cm) != 2:
        return False
    gd = cm[1]
    if gd.name == verticaldim.name:
        return True
    return False


def dfun_ScopedProgram(h):
    global StencilDecls
    stencil_name = h.name
    parm_decls = getattr(h, "parm_decls", ())
    field_decls = getattr(h, "field_decls", ())
    for x in parm_decls:
        if not isinstance(x, HIR.FieldDecl):
            die("non field stencil arguments are not supported")
        x.is_ParmDecl = True
    for x in field_decls:
        x.is_ParmDecl = False
    StencilDecls = {}
    stencil_fields_sir = []
    for field in parm_decls + field_decls:
        if not isinstance(field,HIR.FieldDecl):
            continue
        is_vertical = is_vertical_field(field)
        if g_GridType == "Unstructured":
            expect(
                is_unstruct_field(field) or is_vertical,
                "unstruct or vertical field " + field.name,
            )
            field._is_struct = False
        else:
            assert is_struct_field(field)
            field._is_struct = True
        StencilDecls[field.name] = field
        dims_sir = gen_sir_field_dimensions(field)
        is_tmp = not field.is_ParmDecl
        if is_vertical and g_GridType == "Unstructured":
            field_sir = sir_utils.make_vertical_field(name=field.name, is_temporary=is_tmp)
        else:
            field_sir = sir_utils.make_field(name=field.name, dimensions=dims_sir, is_temporary=is_tmp)
        stencil_fields_sir.append(field_sir)

    stmts = getattr(h.BlockStmt, "ContentsModel", ())
    if not stmts:
        warn("No AST statements in " + h.name + ".")
    ast = []
    for stmt in stmts:
        assert not isinstance(stmt, (list, tuple))
        y = dispatch(stmt)
        assert not isinstance(y, (list, tuple))
        if isinstance(y, VRlist):
            ast.extend(y.list)
        else:
            if y:
                ast.append(y)

    StencilDecls = {}
    ast_sir = sir_utils.make_ast(root=ast)
    stencil_sir = sir_utils.make_stencil(name=stencil_name, ast=ast_sir, fields=stencil_fields_sir)
    return stencil_sir


def gen_sir_global_variables(vdecls):
    globals_sir = sir_utils.GlobalVariableMap()
    for v in vdecls:
        assert isinstance(v,HIR.VarDecl)
        v_name = v.name
        v_cm = v.getcm()
        v_type_name = v_cm[0].name
        v_init = getattr(v, "initialization", None)
        v_is_const = getattr(v, "is_const", False)
        if v_init is not None:
            if not isinstance(v_init, HIR.Literal):
                error("expected literal in global constant", v.locus)
            v_init_val = v_init.value
        else:
            v_init_val = None
        if v_type_name == "int":
            val = str2int(v_init_val) if v_init_val is not None else 0
            globals_sir.map[v_name].integer_value = val
        elif v_type_name == "double":
            val = str2float(v_init_val) if v_init_val is not None else 0.0
            globals_sir.map[v_name].double_value = val
        elif v_type_name == "float":
            val = str2float(v_init_val) if v_init_val is not None else 0.0
            globals_sir.map[v_name].float_value = val
        elif v_type_name == "bool":
            val = str2int(v_init_val) if v_init_val is not None else 0
            globals_sir.map[v_name].boolean_value = bool(val)
        else:
            die("Unsupported type of global variable: " + v_type_name)
        if v_is_const:
            globals_sir.map[v_name].is_constexpr = True
    return globals_sir


def dfun_Program(h):
    global dimset, verticaldim, g_GridType
    filename = h.source or "UNSPECIFIED_SOURCE"  # input filename
    prog_cm = getattr(h, "ContentsModel", ())
    assert len(prog_cm) == 5
    p = 0
    n = len(prog_cm)
    (gdims, domain, fdecls, vdecls, SpOrEks) = prog_cm
    dom_cm = getattr(domain, "ContentsModel", ())
    assert len(dom_cm) == 3
    (DomainParallelDimensions, VerticalDimension, ParallelDimensions) = dom_cm
    struct_hdims = []
    unstruct_hdims = []
    for gd in DomainParallelDimensions:
        dim = DomainParallelDim(gd)
        dimset[dim.name] = dim
        if dim.is_horizontal:
            if dim.is_struct:
                struct_hdims.append(dim.name)
            else:
                unstruct_hdims.append(dim.name)
    if not (bool(struct_hdims) != bool(unstruct_hdims)):
        ICE()
    if struct_hdims:
        g_GridType = "Cartesian"
    else:
        g_GridType = "Unstructured"

    dim = VerticalDim(VerticalDimension)
    dimset[dim.name] = dim
    verticaldim = dim

    for d in ParallelDimensions:
        dim = ParallelDim(gd)
        dimset[dim.name] = dim
    if struct_hdims:
        assert len(struct_hdims) == 2
        dimset[DomainParallelDimensions[0].name].pos = 0
        dimset[DomainParallelDimensions[1].name].pos = 1
        dimset[VerticalDimension.name].pos = 2
    for gd in gdims:
        name = gd.name
        if not name in dimset:
            dim = Dim(gd)
            dimset[name] = dim
    stencils = []
    for kernel in SpOrEks:
        assert isinstance(kernel, HIR.ScopedProgram)
        stencil = dispatch(kernel)
        stencils.append(stencil)
    globals_sir = gen_sir_global_variables(vdecls)
    sir = sir_utils.make_sir(
        filename=filename,
        grid_type=AST.GridType.Value(g_GridType),
        stencils=stencils,
        global_variables=globals_sir,
    )
    return sir


def dfun_VarDecl(h):
    cm = h.getcm()
    type_hir = cm[0]
    type_sir = sir_utils.make_type(builtin_type_or_name=type_hir.name)
    ndims = getattr(h, "ndims", 0)
    init_hir = getattr(h, "initialization", None)
    init_sir = dispatch(init_hir)
    if init_sir:
        s = sir_utils.make_var_decl_stmt(
            type=type_sir, name=h.name, dimension=ndims, init_list=init_sir
        )
    else:
        s = sir_utils.make_var_decl_stmt(type=type_sir, name=h.name, dimension=ndims)
    return s


def dfun_VarAccess(h):
    cm = h.getcm()
    if cm:
        assert len(cm) == 1
        sir_index = dispatch(cm[0])
    else:
        sir_index = None
    is_external = getattr(h, "is_nonlocal_ref", False)
    s = sir_utils.make_var_access_expr(name=h.name, index=sir_index, is_external=is_external)
    return s


def unwrap_elements(elems):
    assert isinstance(elems, (list, tuple))
    a = []
    for x in elems:
        if isinstance(x, VRlist):
            a.extend(x.list)
        else:
            a.append(x)
    return a


def dfun_IfStmt(h):
    cm = h.getcm()
    if len(cm) == 3:
        (hir_Condition, hir_Then, hir_Else) = cm
    elif len(cm) == 2:
        (hir_Condition, hir_Then) = cm
        hir_Else = None
    else:
        die("bad case")
    sir_cond = dispatch(hir_Condition)
    sir_cond = sir_utils.make_expr_stmt(expr=sir_cond)

    sir_then = dispatch(hir_Then)
    sir_then = unwrap_elements(sir_then)
    sir_then = sir_utils.make_block_stmt(statements=sir_then)

    if hir_Else:
        sir_else = dispatch(hir_Else)
        sir_else = unwrap_elements(sir_else)
        sir_else = sir_utils.make_block_stmt(statements=sir_else) if sir_else != None else None
    else:
        sir_else = None

    s = sir_utils.make_if_stmt(cond_part=sir_cond, then_part=sir_then, else_part=sir_else)
    return s


def dfun_SparseIfStmt(h):
    warn("SparseIfStmt is currently mapped to normal IfStmt.")
    return dfun_IfStmt(h)


def dfun_FctCall(f):
    callee = f.name
    args = f.getcm()
    args_sir = dispatch(args)
    s = sir_utils.make_fun_call_expr(callee=callee, arguments=args_sir)
    return s


def dispatch(h):
    if h == None:
        return None
    if isinstance(h, (list, tuple)):
        a = []
        for x in h:
            y = dispatch(x)
            if y:
                a.append(y)
        return a
    elif isinstance(h, Hnode):
        t = h._kind.name
        dfun = dispatcher.get(t)
        if dfun != None:
            g_locus_stack.push(h.locus)
            s = dfun(h)
            g_locus_stack.pop()
            return s
        else:
            print("Missing dispatcher function implementation for t=", t)
            assert False
    else:
        print("bad case 2: h=", h)
        assert False


def dfun_ReturnStmt(f):
    hir_exprModel = getattr(f, "exprModel", None)
    if hir_exprModel == None:
        return
    die("non empty return stmt not supported yet")
    tstop(getframeinfo(currentframe()))


def dfun_LocationType(f):
    return AST.LocationType.Value(f.location)

def hir_literal2numeric_value(f):
    assert isinstance(f,HIR.Literal)
    f_type = f.get_Type().name
    if f_type == "int":
        num_val = str2int(f.value)
    elif f_type == "float":
        num_val = str2float(f.value)
    else:
        die("unexpected type: " + f_type)
    return num_val
    
def wrap_sir_utils_make_reduction_over_neighbor_expr(**kwargs):
    if "offsets" in kwargs:
        if kwargs['offsets'] is None:
            # This helps to get identical SIR hashes for master and offsetReduction branches of Dawn:
            del kwargs['offsets']
        else:
            if kwargs['offsets'] and not cdsl_config.CDSL_HAS_OFFSET_REDUCTION:
                die("SIR make_reduction_over_neighbor_expr does not support offsets")
    return sir_utils.make_reduction_over_neighbor_expr(**kwargs)

def proc_single_NeighbourReduce(red_gdim, init, expr, weights, offsets):
    dim_info = dimset.get(red_gdim.name) or ICE()
    chain = getattr(red_gdim, "chain", None) or die("bad case")
    assert len(chain) >= 2 or die("bad case")
    sir_chain = dispatch(chain)
    sir_init = dispatch(init)
    sir_expr = dispatch(expr)
    sir_weights = dispatch(weights) or None
    int_offsets = [hir_literal2numeric_value(x) for x in offsets] or None
    op = "+"  # TODO: gather op info
    include_center = False # for now

    #s = sir_utils.make_reduction_over_neighbor_expr(
    s = wrap_sir_utils_make_reduction_over_neighbor_expr(
        op=op, rhs=sir_expr, init=sir_init, chain=sir_chain, weights=sir_weights, include_center=include_center, offsets=int_offsets
    )
    return s


def dfun_NeighbourReduce(f):
    cm = copy(f.getcm())
    assert len(cm) >= 4
    p1 = 0
    for p in range(len(cm)):
        cm_p = cm[p]
        if isinstance(cm_p, Hnode):
            if isinstance(cm_p, HIR.GridDimension):
                continue
        p1 = p
        break
    gdims = cm[:p1]
    (init, exprModel, weights, offsets) = cm[p1:]
    expr = exprModel
    for red_gdim in gdims:
        red_expr = proc_single_NeighbourReduce(red_gdim, init, expr, weights, offsets)
        expr = red_expr
    return expr


def dfun_LoopOn(f):
    cm = f.getcm()
    assert len(cm) == 2
    (gdims, block) = cm
    assert len(gdims) == 1
    d = gdims[0]
    assert d.dimension_type == "sparse"
    block_sir = dispatch(block)
    chain_sir = dispatch(d.chain)
    loop_sir = sir_utils.make_loop_stmt(block=block_sir, chain=chain_sir, include_center=False)
    return loop_sir


def dfun_DotProduct(f):
    error("DotProduct not supported yet by SIR", f.locus)


dispatcher = {}


def init_dispatcher():
    global dispatcher
    for k in list(globals().keys()):
        v = globals()[k]
        if callable(v) and k.startswith("dfun_"):
            t = k[5:]
            dispatcher[t] = v


init_dispatcher()


def get_sir(h):
    assert isinstance(h, HIR.Program)
    reset()
    s = dispatch(h)
    return s


def get_ext_sir(h):
    assert isinstance(h, HIR.Program)
    reset()
    s = dispatch(h)
    xs = ExtSIR(s, h.edsl_extension_src)
    return xs
