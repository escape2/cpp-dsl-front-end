# Distribution: see LICENSE.txt

from collections import namedtuple
import re
import sys
import copy

from clang.cindex import Cursor
from parser_config import (
    is_field_type,
    is_scalar_type,
    is_set_type,
    is_map_type,
    is_scope_guard_type,
    die,
)
from common import *

debug_mode = 0

CtypeProps_pattern_str = r"^\s*(const\s+)?([\w:]+)\s*(\[.+\])?\s*$"
CtypeProps_pattern_re = re.compile(CtypeProps_pattern_str)

# CtypeProps are not related to the Python ctypes library
class CtypeProps(DispFormGen):
    def __init__(self, ct):
        self.ct = ct
        match = CtypeProps_pattern_re.match(ct)
        self.is_const = False
        is_valid = False
        self.uqt = ""  # unqualified type (minimal c type)
        self.fqnp = ""
        self.is_intrinsic = False
        self.has_idx = False
        if match:
            g = match.groups()
            is_valid = True
            self.const = bool(g[0])
            if bool(g[2]):
                idx_str = g[2][1:-1].strip()
                idx = str2int(idx_str)
                if idx != None:
                    self.has_idx = True
                    self.idx = idx
                    self.idx_is_int = True
                else:
                    self.idx = idx_str
                    self.idx_is_int = False
            uqt = g[1].strip()
            split_uqt = uqt.split("::")
            if len(split_uqt) == 1:
                self.uqt = uqt
            elif len(split_uqt) > 1:
                uqt = split_uqt.pop()
                fqnp = "::".join(split_uqt)
                self.fqnp = fqnp
                if uqt.startswith("Intrinsic_"):
                    self.is_intrinsic = True
                    uqt = uqt[10:].capitalize()
                self.uqt = uqt
        if not is_valid:
            self.is_invalid = True

    def gen_display_form(self):
        return Display_form(self, drop=("ct",))

    def str(self):
        a = []
        for k, v in vars(self).items():
            if v != None:
                a.append(k + ":" + str(v))
        s = "{" + ", ".join(a) + "}"
        return s

    def show(self):
        for k, v in vars(self).items():
            dprint("k,v=", k, v)


def match_tq(fqnp, uqt):
    if not fqnp and uqt:
        if uqt in ("bool", "int", "float", "double"):
            return tqdef.scalar
    if fqnp != "EDSL" or not uqt:
        return tqdef.no_tq
    if is_field_type(uqt=uqt):
        return tqdef.field
    if is_scalar_type(uqt=uqt):
        return tqdef.scalar
    if is_set_type(uqt=uqt):
        return tqdef.set
    if is_map_type(uqt=uqt):
        return tqdef.map
    if is_scope_guard_type(uqt=uqt):
        return tqdef.scope_guard
    return tqdef.other


def get_tq(ct, ctp=None):
    if ct and not ctp:
        ctp = CtypeProps(ct)
        ctp_is_invalid = getattr(ctp, "is_invalid", False)
    if not ctp or ctp_is_invalid:
        return tqdef.no_tq
    m = match_tq(ctp.fqnp, ctp.uqt)
    if m:
        return m
    return tqdef.no_tq


def is_tv(f):
    return isinstance(f, Tv)


def gen_tv(t, tq, ct, id, v):
    global last_tv
    last_tv = Tv(t, tq, ct, id, v)
    if not isinstance(tq, int) or tq > 7:
        die("bad tq")
    return last_tv


def gen_tfun(t):
    def my_gen_tv(tq, ct, id, v, t=t):
        return gen_tv(t, tq, ct, id, v)

    return my_gen_tv


def common_ct(a):
    ct = None
    if is_lot(a):
        for f in a:
            if not isinstance(f, Tv):
                return no_ct
            if ct is None:
                ct = f.ct
            else:
                if ct != f.ct:
                    return no_ct
        if ct is None:
            return no_ct
        return ct
    else:
        if not isinstance(f, Tv):
            return no_ct
        return f.ct


def tokens_short_str(a):
    if len(a) < 10:
        return str(a)
    else:
        return str(a[:9]) + ", ..."


full_tv_str_mode = True


def set_full_tv_str_mode(mode):
    global full_tv_str_mode
    full_tv_str_mode = mode


def tv_str(tv, prefix=""):
    if not tv:
        return str(tv)
    if isinstance(tv, Tv):
        alist = []
        rlist = []
        for k in tv.__dict__.keys():
            a = getattr(tv, k)
            if hasattr(a, "str") and callable(a.str):
                a = a.str()
            elif k == "t":
                if not a:
                    continue
                a = tname[a]
            elif k in ("ct", "id"):
                if not a:
                    continue
            elif not full_tv_str_mode and k.startswith("_"):
                continue
            elif isinstance(a, CtypeProps):
                a = a.str()
            elif isinstance(a, (Locus, CtypeProps)):
                continue
            elif k == "tq":
                if not a:
                    continue
                a = tqname[a]

            if isinstance(a, (str, float, int, bool)):
                alist.append(k + ":" + str(a))
            elif not a:
                alist.append(k + ":" + str(a))
            elif k == "v":
                if tv.t == tdef.tokens:
                    rlist.append((k, tokens_short_str(tv.v)))
                else:
                    rlist.append((k, a))
            else:
                rlist.append((k, a))
        alist_str = ", ".join(alist)
        if not rlist:
            return "(" + alist_str + ")"
        head = "(" + alist_str + "), "
        s = head
        prefix2 = prefix + pfi
        for (k, a) in rlist:
            if is_lot(a):
                s += "\n" + prefix2 + k + ":"
                s += "\n" + prefix2 + pfi + "|" + tv_str(a, prefix2 + pfi + "|")
            elif isinstance(a, Tv):
                s += "\n" + prefix2 + k + ": " + tv_str(a, prefix + pfi)
            elif isinstance(a, Cursor):
                # we ignore cursors
                pass
            elif isinstance(a, CtypeProps):
                s += "CTP"
            else:
                s += prefix + str(a)
        return s
    elif is_lot(tv):
        a = []
        n = 0
        for x in tv:
            a.append(tv_str(x, prefix))
            n += 1
        sep = "\n" + prefix
        return sep.join(a)
    elif isinstance(tv, (str, float, int, bool)):
        return str(tv)
    return "fallback(" + str(tv) + ")"


def zero_tv():
    global last_tv
    last_tv = nil_tv
    return last_tv


def show_tv(tv, prefix=""):
    s = tv_str(tv, prefix)
    dprint(s)


def is_high_level_tree(f):
    if is_tv(f):
        if f.t < min_high_level_t:
            dprint("error: found low level node:", tname[f.t])
            return False
        return is_high_level_tree(f.v)
    if is_lot(f):
        for a in f:
            if not is_high_level_tree(a):
                return False
        return True
    return True


pfi = "  "  # prefix increment

short_tv_str = False

# low and intermediate node types
low_level_tname = [
    "nil",  ## nil must be at pos 0
    "any",  # type kind that matches any other types
    "anyof",  # type kind that matches any of the types in the value part
    "array_subscript_expr",
    "rejected",
    "scope_guard_decl",
    "scope_guard_deref",
    #'noise', # a harmless node that passed the filter but is of no interest
    "member_ref_expr",
    "decl_ref_expr",
    "iterator",
    "tokens",
    "typedef_decl",
    "type_ref",
    "class_decl",
    "struct_decl",
    "type_alias_decl",
    "template_ref",
    "other",
    "unknown_terminal",
    "unknown_nonterminal",
    #'fqnp',
    "list",
    "from_get_arguments",
    "UNRESOLVED",
    "CKV",  # [CurserKind, value] pair
]

#  C++ pre-HIR types:
high_level_tname = [
    "bool_literal",
    "integer_literal",
    "floating_literal",
    "string_literal",
    "dim_key",
    "dim_keys",
    "dim_spec",
    "scalar_assign",
    "scalar_expr",
    "scalar_decl",
    "scalar_def",
    "scalar_ref",
    "scalar_ctor",
    "field_ctor",
    "field_def",
    "field_decl",
    "field_spec",
    "field_spec_stmt",
    "field_access",
    #'field_keys',
    "field_ref",
    "field_expr",
    "field_assign",
    "field2scalar_conv",
    "field_init",
    "mre_field_init",
    "edsl_fcall",
    "EDSL_fcall",
    "fcall",
    "if_stmt",
    "sparse_if_stmt",
    "likely",
    "unlikely",
    "set_ref",
    "set_expr",
    "set_assign",
    #'set_mask',
    "set_op",
    "set_op_mre",
    "set_id_field",
    "set_method",
    #'subset',
    "subset_op_expr",
    "subset_m_expr",
    "mre_op_set_expr",
    "op_set_expr",
    "set_access",
    "set_iter_deref",
    "vector_subscript_expr",
    "vmap",
    "vmap_spec",
    "vmap_decl",
    "vmap_ref",
    "vmap_access",
    "intrinsics",
    "expl_dof",  # explicit degree of freedom
    "dsl_decl_head",
    "dsl_decl",
    #'arguments',
    "assignment",
    "ternop",
    "binop",
    "scalar_binop",
    "field_binop",
    "set_binop",
    "unop",
    "redop",
    "namespace_body",
    "call_expr",
    "compute_on",
    "loop_on",
    "loop_var_decl",
    "vertical_region",
    "set_vertical_region",
    "range_loop",
    "function_decl",
    "function_def",
    "var_decl",
    "var_def",
    "var_ref",
    "var_assign",
    "var_access",
    "var_ctor",
    "gridspace_decl",
    "gridspace_ref",
    "retyped_field_decl",
    "retyped_field_ref",
    "type_decl",
    #'literal',# not used
    "decl_stmt",
    "expr",
    "init_value",
    "init_list",
    "paren_expr",
    "parameters",
    "parm_decl",
    "return_stmt",
    "nreduce",
    "nreduce_mre",
    "dot_product",
    "field_cast_expr",
    "value",
    "op",
    "order",
    "translation_unit",
    "unary_op",
    "unexposed_expr",
    "namespace",
    "result_type",
    "block_stmt",
    "fun_head",
    "embedded_set",
    "reduction_weights",
    "reduction_offsets",
]

tname = low_level_tname + high_level_tname

max_low_level_t = len(low_level_tname) - 1
min_high_level_t = max_low_level_t + 1

Tdef = namedtuple("Tdef", tname)
tdef = Tdef(*range(len(tname)))

no_ct = ""
no_id = ""
no_v = []


# type qualifier
tqname = [
    "no_tq",  # must be at pos 0
    #'bool',      # literal type
    #'integer',   # literal type
    #'floating',  # literal type
    "scalar",  # any scalar type
    "field",
    "set",
    "map",
    "scope_guard",  # DSL internal
    "other",  # other EDSL types
]

Tqdef = namedtuple("Tqdef", tqname)
tqdef = Tqdef(*range(len(tqname)))
no_tq = tqdef.no_tq
assert no_tq == 0

tfun = Tdef(*(gen_tfun(t) for t in range(len(tname))))


class Tv(DispFormGen):
    def __init__(
        self, t=tdef.nil, tq=no_tq, ct=no_ct, id=no_id, v=None, cx=None, locus=None
    ):
        self.t = t
        self.tq = tq
        self.ct = ct
        self.id = id

        if ct and not hasattr(self, "_ctp"):
            self._ctp = CtypeProps(ct)
        if v != None:
            self.v = v
        if locus:
            self.locus = locus
        if cx:
            self.cx = cx
            if not locus:
                locus = Locus(cx.location, cx.extent)
        if not locus:
            die("no locus information")
        assert isinstance(locus, Locus)

    def gen_display_form(self):
        head = ", ".join(
            [
                "t:" + tname[self.t],
                "tq:" + tqname[self.tq],
                "id:" + self.id,
                "ct:" + self.ct,
            ]
        )
        return Display_form(
            self, first=(("head", head),), drop=("t", "tq", "id", "ct", "cx", "locus")
        )

    def display_form(self):
        if getattr(self, "has_display_form", None) is not None:
            return self
        g = copy.copy(self)
        g.t = tname[g.t]
        g.tq = tqname[g.tq]
        g.has_display_form = True
        return g

    def set(self, attr, value):
        setattr(self, attr, value)
        return self

    def str(self):
        return tv_str(self)


no_locus = Locus(None, None)
last_tv = Tv(0, no_tq, no_ct, no_id, locus=no_locus)
nil_tv = Tv(tdef.nil, no_tq, no_ct, no_id, locus=no_locus)
assert nil_tv.t == 0

get_tvs_count = 0
get_tvs_result = nil_tv
forbid_id_search = 0
