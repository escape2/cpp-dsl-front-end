# Distribution: see LICENSE.txt

import sys
import dim_config

pfi = "  "


def print_empty_class(c, prefix=""):
    inh = ""
    if "inherits" in c:
        inh = " : public " + c["inherits"]
    print(prefix + "class", c["name"] + inh, "{};")


def print_class(c, prefix=""):
    if not "members" in c:
        return print_empty_class(c, prefix)
    inh = ""
    if "inherits" in c:
        inh = " : public " + c["inherits"]
    print(prefix + "class", c["name"] + inh, "{")
    print(prefix + "public:")
    pref = prefix + pfi
    for d in c["members"]:
        print(pref, d + ";")
    print(prefix + "};")
    k = "nonmembers"
    if k in c:
        for d in c[k]:
            print(prefix + d + ";")
    print()


def gen_hpp_output(classes):
    print("// Generated automatically by " + sys.argv[0] + " - do not edit.")
    print()
    for c in classes:
        if not "members" in c:
            print_empty_class(c)
    print()
    for c in classes:
        if "members" in c:
            print_class(c)
