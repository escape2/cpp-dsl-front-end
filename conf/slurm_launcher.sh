#!/bin/bash

# check if we have a slurm allocation
if [[ -z "$SLURM_JOB_ID" ]]; then
    echo "*** $0: SLURM_JOB_ID not set. Please provide a SLURM allocation." 1>&2
    exit 1
fi

module purge

module load cuda/10.0.130

#srun which nvcc
#srun nvidia-smi
#srun /home/dkrz/k202076/test/cuda-10.0.130/NVIDIA_CUDA-10.0_Samples/0_Simple/cdpSimplePrint/cdpSimplePrint

srun $*
