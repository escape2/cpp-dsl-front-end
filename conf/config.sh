#!/usr/bin/env bash

# --- support section:

bad_case() {
    echo "$0: ${FUNCNAME[*]}"
    if [[ ! -z "$*" ]]; then
        echo "$*" >&2
    fi
    exit 1
}

is_true() {
    if [[ -z "$1" ]] || [[ "$1" == "0" ]]; then
        false
    elif [[ "$1" == "1" ]]; then
        true
    fi
}

extend_pythonpath() {
    local pp=$1
    if [[ -z "$PYTHONPATH" ]]; then
        export PYTHONPATH=$pp
    else
        case ":$PYTHONPATH:" in
            *":$pp:"*) :;; # do nothing
            *) export PYTHONPATH="$pp:$PYTHONPATH" ;;
        esac
    fi
}

extend_modulepath() {
    local mp=$1
    if [[ -z "$MODULEPATH" ]]; then
        export MODULEPATH=$mp
    else
        case ":$MODULEPATH:" in
            *":$mp:"*) :;; # do nothing
            *) export MODULEPATH="$mp:$MODULEPATH" ;;
        esac
    fi
}

# --- individual host (put here as examples) section:

darkstar_conf() {
    extend_pythonpath /usr/lib/python3/dist-packages
    echo "$0: host_conf = ${FUNCNAME[0]}"
    if [[ -z "$VIRTUAL_ENV" ]]; then
        source $HOME/dawn_venv/bin/activate
    fi
    export ATLAS=$HOME/work/install/atlas_0.20.0
    export DAWN=$HOME/dawn
    export GridTools_INCLUDE_PATH=$DAWN/build/_deps/gridtools-src/include
    export CDSL_INSTALL_DIR=$HOME/work/install/cdsl
    export DAWN_HAS_OFFSET_REDUCTION=1
}

streamline_conf() {
    echo "$0: host_conf = ${FUNCNAME[0]}"
    if [[ -z "$VIRTUAL_ENV" ]]; then
        source $HOME/dawn_venv/bin/activate
    fi
    export ATLAS=$HOME/work/install/atlas_0.20.0
    export DAWN=$HOME/dawn
    export GridTools_INCLUDE_PATH=$DAWN/build/_deps/gridtools-src/include
    export CDSL_INSTALL_DIR=$HOME/work/install/cdsl
}

btlogin1_bullx_conf() {
    echo "$0: host_conf = ${FUNCNAME[0]}"
    local TEST_HOME=/home/dkrz/k202076
    if [[ -z "$VIRTUAL_ENV" ]]; then
        source $TEST_HOME/dawn_venv/bin/activate
    fi
    # current test installations:

    local SPACK_MODULES=$TEST_HOME/spack_modules
    extend_modulepath $SPACK_MODULES/linux-rhel6-haswell
    module purge

    #module load llvm-10.0.0-gcc-9.1.0-q24msmy # has python bindings
    module load llvm/9.0.0-gcc-9.1.0 # this one doesn't
    
    module load \
           cmake-3.18.0-gcc-9.1.0-qt4u54x \
           netcdf-fortran-4.5.2-gcc-9.1.0-6k2pk72 \
           gcc/9.1.0-gcc-7.4.0 \
           git/2.9.0
    
    export BOOST_ROOT=/sw/spack-rhel6/boost-1.63.0-sifvou

    # confirm compiler selection (modules might have changed this)
    export CXX=g++
    export FC=gfortran
    export ATLAS=$TEST_HOME/work/install/atlas_0.20.0
    export DAWN=$TEST_HOME/dawn
    export GridTools_INCLUDE_PATH=$DAWN/build/_deps/gridtools-src/include
    #export CDSL_INSTALL_DIR=$HOME/work/install/cdsl
}


host_conf() {
    echo "using host conf $1"
    source $CDSL_SOURCE_DIR/conf/hosts/$1
}

# --- system-variant (or distribution) section:

RedHat6_conf() {
    echo "$0: distribution_conf = ${FUNCNAME[0]}"
    local case_expr=$(hostname -f)
    case $case_expr in
        btlogin1.bullx*)
            btlogin1_bullx_conf
            ;;
        mlogin*.hpc.dkrz.de)
            host_conf mistral_gcc71.sh
            ;;
        *)
            bad_case ${FUNCNAME[0]} $case_expr
            ;;
    esac
}

Ubuntu_20_04_conf() {
    echo "$0: distribution_conf = ${FUNCNAME[0]}"
    local case_expr=$(hostname -f)
    case $case_expr in
        darkstar*)
            darkstar_conf
            ;;
        # no default case: missing information should be handled by cmake
    esac

}

Ubuntu_18_04_conf() {
    echo "$0: distribution_conf = ${FUNCNAME[0]}"
    echo "PYTHONPATH=$PYTHONPATH"
    local case_expr=$(hostname -f)
    case $case_expr in
        streamline*)
            streamline_conf
            ;;
        # no default case: missing information should be handled by cmake
    esac

}

# --- OS section:

linux_conf() {
    # default settings:
    export CXX=g++
    export FC=gfortran
    local case_expr=$(lsb_release -ds)
    case $case_expr in
        *Red\ Hat*release*6*)
            RedHat6_conf
            ;;
        Ubuntu\ 20.04*)
            Ubuntu_20_04_conf
            ;;
        Ubuntu\ 18.04*)
            Ubuntu_18_04_conf
            ;;
        *)
            bad_case unknown system ${FUNCNAME[0]} $case_expr
            ;;
    esac
}

# --- OS selector:

case $(uname -o) in
    GNU/Linux)
        linux_conf
        ;;
    *)
        echo "bad case";
        exit 1
esac
