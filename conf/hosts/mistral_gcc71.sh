#!/usr/bin/env bash

PROJECT_DIR=/work/ka1189

module purge

module load \
       cmake-3.19.5-gcc-10.2.0-nktbmrb \
       llvm-10.0.0-gcc-10.1.0-7uao3u2 \
       gcc/7.1.0 \
       netcdf-fortran-4.4.3-gcc71 \
       git \
       \
       protobuf-3.11.2-gcc-10.1.0-jv5g7cc \
       py-protobuf-3.11.2-gcc-10.1.0-ozfqaiq

if [[ "$CDSL_WITH_CUDA" == "1" ]]; then
    module load cuda/10.0.130
    cuda_or_empty="CUDA"
    export CDSL_CUDA_LAUNCHER=$PROJECT_DIR/cdsl/conf/slurm_launcher.sh
    export CMAKE_CUDA_ARCHITECTURES=37
else
    cuda_or_empty=""
fi

if [[ -z "$CDSL_DAWN_BRANCH" ]]; then
    CDSL_DAWN_BRANCH="offsetReduction"
fi

if [[ "$CDSL_DAWN_BRANCH" == "offsetReduction" ]]; then
    # Matthias offsetReduction branch:
    export DAWN=$PROJECT_DIR/dawn_offsetReduction
    export DAWN_VENV=$PROJECT_DIR/dawn_offsetReduction_venv
    export DAWN_HAS_OFFSET_REDUCTION=1
    export DAWN_HAS_FLEX_STRUCT_HALOS=1
    export CDSL_INSTALL_DIR=$PROJECT_DIR/sw/install/cdsl${cuda_or_empty}_gcc71
else
    export DAWN=$PROJECT_DIR/dawn
    export DAWN_VENV=$PROJECT_DIR/dawn_venv
    export DAWN_HAS_OFFSET_REDUCTION=0
    export CDSL_INSTALL_DIR=$PROJECT_DIR/sw/install/cdsl${cuda_or_empty}_dawn_master_gcc71
fi

export DAWN_HAS_FLEX_STRUCT_HALOS=1

echo "$0: DAWN=$DAWN"

if [[ -z "$VIRTUAL_ENV" ]]; then
    source $DAWN_VENV/bin/activate
fi

# confirm compiler selection (modules might have changed this)
export CXX=g++
export FC=gfortran
export ATLAS=$PROJECT_DIR/sw/install/atlas_0.20.0_gcc71
export GridTools_INCLUDE_PATH=$PROJECT_DIR/sw/install/gridtools-1.1.3/include
export BOOST_ROOT=/work/ka1189/sw/spack/install/linux-rhel6-haswell/gcc-7.1.0/boost-1.74.0-i6wsqyh5yavm3gbasjffjd2asiad7sen
