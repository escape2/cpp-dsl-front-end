#!/usr/bin/env perl

use strict;
use warnings;

my @conf_keys = qw(
    CDSL_atlas_DIR
    CDSL_atlas_PKGCONFIG_DIR
    CDSL_BASH_PRG
    CDSL_Boost_INCLUDE_DIR
    CDSL_CLANG_MODULE_DIR
    CDSL_CLANGPP_PRG
    CDSL_DAWN_INCLUDE_DIR
    CDSL_eckit_BASE_DIR
    CDSL_GIT_SHA1
    CDSL_GIT_SHA1_SHORT
    CDSL_GridTools_INCLUDE_PATH
    CDSL_HAS_OFFSET_REDUCTION
    CDSL_LD_LIBRARY_PATH
    CDSL_LIBCLANG_PATH
    CDSL_PERL_PRG
    CDSL_PYTHONPATH
    CDSL_PYTHON_PRG
    CDSL_ROOT
    CDSL_VERSION
    );

my @conf_cpp_keys = qw(
    CDSL_WITH_CUDA
    );

my $max_len = 0;
for my $k (sort @conf_keys) {
    my $len = length($k);
    $max_len = $len if $len > $max_len;
}

my $conf_sh_file = "conf/cdsl_config.sh.in";
open(CONF_SH,">$conf_sh_file") or die("Cannot open file $conf_sh_file");
print CONF_SH "# CDSL configuration file for bash\n";

my $conf_py_file = "conf/cdsl_config.py.in";
open(CONF_PY,">$conf_py_file") or die("Cannot open file $conf_py_file");
print CONF_PY "# CDSL configuration file for python\n";

my $conf_cmake_file = "conf/cdsl-config.cmake.in";
open(CONF_CMAKE,">$conf_cmake_file") or die("Cannot open file $conf_cmake_file");
print CONF_CMAKE "# CDSL configuration file for cmake\n";

my $conf_txt_file = "conf/cdsl_config.txt.in";
open(CONF_TXT,">$conf_txt_file") or die("Cannot open file $conf_txt_file");

for my $k (sort @conf_keys) {
  my $fk = sprintf("%-${max_len}s",$k);
  print CONF_SH "export $k=\@$k\@\n";
  print CONF_PY "$fk = \"\@$k\@\"\n";
  print CONF_CMAKE "set($k \@$k\@)\n";
  print CONF_TXT "   $fk : \@$k\@\n";
}

# for python: convert to proper type:
print CONF_PY "# type conversions:\n";
my @conf_py_add = qw( CDSL_HAS_OFFSET_REDUCTION );
for my $k (@conf_py_add) {
  print CONF_PY "$k = $k.lower() in ['true', '1', 't', 'y', 'yes']";
}

close CONF_SH;
close CONF_PY;
close CONF_CMAKE;
close CONF_TXT;

# compile defs - now replaced with target_compile_definitions in cmake files
#
# my $conf_cpp_file = "conf/cdsl_config.h.in";
# open(CONF_CPP,">$conf_cpp_file") or die("Cannot open file $conf_cpp_file");
# print CONF_CPP "#ifndef _CDSL_CONFIG_H\n";
# print CONF_CPP "#define _CDSL_CONFIG_H\n\n";
# for my $k (sort @conf_cpp_keys) {
#   print CONF_CPP "#cmakedefine $k\n";
# }
# print CONF_CPP "\n#endif\n";
# close CONF_CPP;
