#!/bin/bash

# - example config for Ubuntu 20.04 with given Dawn installation
# - script becomes effective when copied to ../user_config.sh

# add path to Python clang bindings
export PYTHONPATH=/usr/lib/python3/dist-packages:$PYTHONPATH
if [[ -z "$VIRTUAL_ENV" ]]; then
    source $HOME/dawn-dusk-venv/bin/activate
fi

export ATLAS=$HOME/atlas/install
export DAWN=$HOME/dawn
export GridTools_INCLUDE_PATH=$DAWN/build/_deps/gridtools-src/include
export CDSL_INSTALL_DIR=$HOME/cdsl
export DAWN_HAS_OFFSET_REDUCTION=0
export CDSL_INSTALL_DIR=$HOME/cpp-dsl-front-end/install

