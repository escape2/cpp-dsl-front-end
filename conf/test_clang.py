#!/usr/bin/env python3

import sys
import os
from enum import Enum
from ctypes import *


class PathKind:
    invalid = 0
    abs_dir_path = 1
    abs_file_path = 2
    rel_file_path = 3


def die(*reason):
    print(*reason, file=sys.stderr)
    exit(1)


def test_import():
    global clang, Index, Config
    try:
        import clang
    except:
        die("Could not import clang Python module. Please check environment.")

    try:
        from clang.cindex import Index, Config, CursorKind, TypeKind, Cursor
    except:
        die(
            "Could not import objects from clang.cindex. Please check clang installation."
        )


def load_libclang(file_or_dir):

    if os.path.isabs(file_or_dir):
        if os.path.isdir(file_or_dir):
            kind = PathKind.abs_dir_path
        else:
            kind = PathKind.abs_file_path
        if not os.path.exists(file_or_dir):
            die(libclang_filepath, "does not exist.")
    elif file_or_dir.endswith(".so"):
        kind = PathKind.rel_file_path
    else:
        kind = PathKind.invalid
        die("Invalid directory or file path for libclang:", file_or_dir)

    if kind == PathKind.abs_dir_path:
        Config.set_library_path(file_or_dir)
    else:
        Config.set_library_file(file_or_dir)

    try:
        index = Index.create()
    except clang.cindex.LibclangError:
        die("Cannot load libclang.")

    try:
        libclang = clang.cindex.conf.get_cindex_library()
    except:
        die("Unexpected failure of get_cindex_library().")

    if kind == PathKind.abs_file_path:
        filepath = file_or_dir
    else:
        try:
            from dlinfo import DLInfo
        except ModuleNotFoundError:
            die("Cannot import Python module: dlinfo")

        dlinfo = DLInfo(libclang)
        filepath = dlinfo.path
        assert os.path.isfile(filepath)
    return filepath


def get_clang_binding_path():
    clang_init_file = os.path.abspath(clang.__file__)
    if not clang_init_file.endswith("/clang/__init__.py"):
        die(
            "Unexpected clang module filename (expected to end with clang/__init__.py):",
            clang_init_file,
        )
    clang_python_path = os.path.dirname(clang_init_file)
    return clang_python_path


if __name__ == "__main__":

    test_import()

    default_libclang = clang.cindex.conf.get_filename() or die("Internal error")

    # get path tp libclang file or diectory
    if len(sys.argv) < 2:
        die(sys.argv[0] + ": Command Line Argument missing.")
    path = sys.argv[1]

    libclang_filepath = load_libclang(path)
    clang_binding_dir = get_clang_binding_path()

    print(";".join([clang_binding_dir, libclang_filepath, default_libclang]))
