#!/usr/bin/env python3
import sys
import os

try:
    import dawn4py
except ImportError:
    print(
        "Could not import dawn4py Python module. Please check environment.",
        file=sys.stderr,
    )
    exit(1)

try:
    from dawn4py.serialization import SIR
except ImportError:
    print(
        "Could not import SIR from dawn4py.serialization. Please check Dawn installation.",
        file=sys.stderr,
    )
    exit(1)

try:
    from dawn4py.serialization import utils as sir_utils
except ImportError:
    print(
        "Could not import utils from dawn4py.serialization. Please check Dawn installation.",
        file=sys.stderr,
    )
    exit(1)


dawn4py_init_file = os.path.abspath(dawn4py.__file__)

if not dawn4py_init_file.endswith("/dawn4py/__init__.py"):
    print(
        "Unexpected dawn4py module filename (expected to end with dawn4py/__init__.py):",
        dawn4py_init_file,
        file=sys.stderr,
    )
    exit(1)

# return the module directory
dawn4py_dir = os.path.dirname(dawn4py_init_file)
print(dawn4py_dir)
