#include "dsl.hpp"

using namespace EDSL;

namespace edsl {

  Gridspace volume(latitudes,longitudes,levels);
  Gridspace plane(latitudes,longitudes);

  void fun(Field u, Field lap) {
    u.spec(volume); lap.spec(volume);
    //Field lap(volume);
    vertical_region(start_level,end_level) {
      compute_on(plane) {
        Intrinsic_longitude i;
        Intrinsic_latitude j;
        lap = -4*u + u(i+1) + u(i-1) + u(j-1) + u(j+1);
        u = -4*lap + lap(1+i) + lap(i-1) + lap(j-1) + lap(1+j);
      }
    }
  } // end of function fun

} //end of namespace edsl
