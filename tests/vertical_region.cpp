#include "dsl.hpp"

using namespace EDSL;

namespace edsl {


  void fun(Field alpha) {
    alpha.spec(longitudes, latitudes,levels);
    vertical_region(start_level+1,end_level-1) {
      compute_on(longitudes, latitudes) {
        alpha = 2;
      }
    }
  }

} //end of namespace edsl
