#include "dsl.hpp"
#include <math.h> 
using namespace EDSL;


namespace edsl {
  // tested: abs, sqrt, sin, cos, exp, log

  
  void elemental_math(Field alpha, Field beta) {
    alpha.spec(latitudes,longitudes,levels);
    beta.spec(latitudes,longitudes,levels);
    vertical_region(start_level, end_level) {
      compute_on(latitudes,longitudes) {
        //alpha = log(exp(0.0001*beta)) + cos(beta);
        alpha = log(exp(beta));
      }
    }
  }
}
