#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void fun(Field f0) {
    f0.spec(cells, levels);
    //Field f1(cells, levels); // unused temp gives backend error

    vertical_region(start_level, end_level) {
      compute_on(cells) {
        f0(cells,levels) = 0;
      }
    }
  } // end of function fun

} //end of namespace edsl
