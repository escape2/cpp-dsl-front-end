#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void fun() {
    // exercise the embedding of sets into various iteration contexts:
    Field f1( edges.via(verts) );

    Field f2a( edges.via(verts.via(cells)) );
    Field f2b( edges.via(cells,verts) );

    Field f3( cells.edges.via(verts) );
    Field f4( cells.via(verts.edges) );
    Field f5( cells.edges.via(verts.edges) );

  }
 

} //end of namespace edsl
