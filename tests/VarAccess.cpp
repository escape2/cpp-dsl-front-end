#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void test_fun(Field alpha) {
    alpha.spec(cells);
    const int n = 1;
    double MyVec[] = {5,7};
    double M =  MyVec[3*n-2];
  }

} //end of namespace edsl
