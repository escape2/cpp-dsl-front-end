#include "dsl.hpp"

using namespace EDSL;

namespace edsl {

  void scalar_math(Field alpha, Field zmask) {
    alpha.spec(latitudes,longitudes,levels);
    zmask.spec(levels);
    vertical_region(start_level, end_level) {
      alpha = zmask;
    }
  }
}
