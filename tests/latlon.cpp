#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  
  void fun(Field u) {
    u.spec(latitudes,longitudes);
    Field lap(latitudes,longitudes);
    compute_on(latitudes, longitudes) {
      Intrinsic_longitude i;
      Intrinsic_latitude j;
      lap = -4*u + u(i+1) + u(i-1) + u(j-1) + u(j+1);
      u = -4*lap + lap(1+i) + lap(i-1) + lap(j-1) + lap(1+j);
    }

  } // end of function fun

} //end of namespace edsl
