#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void fun() {
    Field_int dolic_c(cells);
    Field_int alpha = levels.id() + 1;
    compute_on(cells,levels.lt(3).gt(5)) { alpha = 2; }
    compute_on(levels.lt(3).gt(5) ) {}
    compute_on(levels.interval(2,7).reversed() ) {}
    compute_on(levels.reversed()) {}

    Field hight(levels);
    compute_on(levels.where(hight < 1000)) {}
    compute_on(levels.where( levels.id() + 1 < 12 ) ) {}

  } // end of function fun

} //end of namespace edsl
