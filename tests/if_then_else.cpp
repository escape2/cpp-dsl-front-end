#include "dsl.hpp"
#include <cmath>
using namespace EDSL;

namespace edsl {

  bool cond = true;
  int kvar = 11;

  // Note: we currently do not support 'likely()'
  void fun(Field alpha) {
    alpha.spec(longitudes, latitudes);


    
    int k1 = 42;

    int k = 1;

    if (unlikely(cond))  k = 42;

    sparse_if(cond) {
      k = 1;
    } else {
      k = 2;
    }

    if (unlikely(cond)) {
      k = 3;
    } else {
      k = 4;
    }

    if (3.*kvar < 11) {
      k = 17;
    }

    compute_on(longitudes, latitudes) {
      if (alpha<1) alpha = 0;
      }

    return;

  } // end of function fun

} //end of namespace edsl
