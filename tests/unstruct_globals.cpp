#include "dsl.hpp"
using namespace EDSL;
namespace edsl {
  

  //const int my_int = 42;
  //float my_float  = 1.12345678901234567890;
  double my_double = 2.1234567890123;
  //bool my_bool = true;

  //string my_s = "abc";

  void test_fun(Field alpha) {
    alpha.spec(cells);
    compute_on(cells) {
        alpha = my_double;
    }
  }

}
