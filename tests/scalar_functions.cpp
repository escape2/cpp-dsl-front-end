#include "dsl.hpp"
#include <math.h>

using namespace EDSL;

namespace edsl {

  double sin2(const double x) {
    double s = sin(x);
    return s*s;
  }

  void fun() {
    Field beta(cells);
    double x=0,y=0;
    
    y = sin(x) + sin2(x);

  }
  
} //end of namespace edsl
