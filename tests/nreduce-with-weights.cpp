#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  
  void fun1(Field alpha, Field beta, Field stencil) {
    alpha.spec(edges);
    beta.spec(verts);
    stencil.spec(edges, cells.verts);
    const double wtest[4] = {1, -1, 2, -2};
    double cvar = 1234.567;
    cvar = 17;
    Field aux1(edges);
    aux1 = 2*alpha;
    compute_on(edges) {
      // should be okay (no check of weight size yet):
      alpha = nreduce(cells.verts, {alpha,2,3,4}, beta*stencil ) ;
      alpha = nreduce(cells.verts, {42, alpha,3,4}, beta*stencil ) ;
      alpha = nreduce(cells.verts, {}, beta*stencil ) ;
      alpha = nreduce(cells.verts, {42, 17.0, 21.3, 180}, beta*stencil ) ;
      alpha = nreduce(cells.verts, {aux1, alpha,3,4}, beta*stencil ); // fails in backend

      //not supported yet:
      //alpha = nreduce(cells.verts, {cvar, 42, 3, 4}, beta*stencil); // fails in backend
      //alpha = nreduce(cells.verts, wtest, beta*stencil); // fails in frontend
    }

  } // end of function
} //end of namespace edsl

