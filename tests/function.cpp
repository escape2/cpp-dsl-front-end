#include "dsl.hpp"

using namespace EDSL;

namespace edsl {

  Field myfun(Field gamma, int p) {
    Field beta2(edges);
    gamma.spec(cells);
    return gamma;
  }

  void fun(Field beta) {
    beta.spec(cells);
    beta = myfun(beta,2);
  }
  
} //end of namespace edsl
