
#include "dsl.hpp"
using namespace EDSL;
namespace edsl {
#define KIT000_DEFAULT 1
#define DOUBLE_DEFAULT 1.0
#define MY_BOOL_VALUE true
  const int kit000 = KIT000_DEFAULT;
  const double d17 = DOUBLE_DEFAULT;
  const bool b = MY_BOOL_VALUE;
  int  g_int = 1;

  void test_fun(Field alpha) {
    alpha.spec(latitudes,longitudes);
    compute_on(latitudes,longitudes) {
      alpha = d17;
    }
  }

}
