#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void copy_stencil_example(Field alpha, Field beta) {
    alpha.spec(latitudes,longitudes);
    beta.spec(latitudes,longitudes);

    compute_on(latitudes,longitudes) {
      alpha = beta;
    }

  }

} //end of namespace edsl
