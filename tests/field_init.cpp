#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void test_fun() {
    Field_int f1(cells_of_edge);
    f1.init(-1,1);
    auto f2 =  Field_int(cells_of_edge).init(-1,1);
  }

} //end of namespace edsl
