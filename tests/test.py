#!/usr/bin/env python3

import os
import sys
from collections import OrderedDict
import subprocess
import re
import filecmp
import argparse
import hashlib
# check where we are and import cdsl_config:
CDSL_CONF_PATH = "/".join([os.environ.get("CDSL_ROOT"), "conf"])
if not CDSL_CONF_PATH in sys.path:
    sys.path.append(CDSL_CONF_PATH)
import cdsl_config

# globals:
args = None
cdsl_root = None
cdsl_prg = None
test_stage = None
possible_stages = ("CLANG_FILTER", "PARSER", "AST", "SA", "HIR", "SIR", "CPP")
hash_file = "test_cases.dat"
out_dir = "test_out"

def error(*reason):
    print("Error:", *reason, file=sys.stderr)
    exit(1)


class TestCase:
    def __init__(self, filename, stage, ref_sha1=None):
        self.filename = filename
        self.stage = stage
        self.ref_sha1 = ref_sha1


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s",
        dest="stage",
        default="SIR",
        help="processing stage (one of: clang_filter, parser, AST, SA, HIR, SIR, CPP)",
        action="store",
    )
    parser.add_argument(
        "-root",
        dest="cdsl_root",
        help="CDSL installation root dir",
        action="store",
        required=False,
    )
    parser.add_argument(
        "-gen_sha1",
        dest="gen_sha1",
        help="generate sha1_hashes",
        action="store_true",
        required=False,
    )
    parser.add_argument(
        "files",
        metavar="file",
        help="one or more CDSL source code files",
        nargs="*",
    )
    return parser.parse_args()


def proc_test_case(test):
    if not (test.ref_sha1 or args.gen_sha1):
        print("Skipping test case", test.filename, "(no ref_sha1).")
        return
    my_env = os.environ.copy()
    my_env["CDSL_ROOT"] = cdsl_root
    opts = ["-nopath", "-s", test.stage]
    if test.stage == "SIR":
        my_env["CDSL_DEBUG_SIR_STAGE"] = "1"
    elif test.stage == "HIR":
        opts.append("-json")
    cmd = [cdsl_prg, *opts, test.filename]
    result = subprocess.run(cmd, capture_output=True, env=my_env)
    if result.returncode:
        print("stderr=",result.stderr.decode('utf-8'))
        error("failed cmd:", " ".join(cmd))
    out = result.stdout
    out_file = ".".join([test.filename,test.stage])
    out_path = "/".join([out_dir, out_file])
    with open(out_path, mode="w") as f:
        f.write(out.decode('utf-8'))
    sha1sum = hashlib.sha1(out).hexdigest()
    test_str = "test: " + '"' + " ".join(cmd) + '"'
    if args.gen_sha1:
        print(", ".join([test.filename, test.stage, sha1sum]))
        return
    if sha1sum != test.ref_sha1:
        print(test_str + ": FAILED")
        print("   sha1sum does not match reference:")
        print("   test:", sha1sum)
        print("   ref :", test.ref_sha1 or "(not available).")
        return False
    print(test_str + ": passed")
    return True

def read_test_cases():
    test_cases = OrderedDict()
    with open(hash_file, mode="r") as f:
        for line in f:
            line = line.strip()
            if not line or line.startswith("#"):
                continue
            rec = [x.strip() for x in line.split(",")]
            if len(rec) != 3:
                error("Invalid test_case:", line)
            (fname, stage, hash_) = rec
            if not os.path.isfile(fname):
                error("invalid filename in test case:", rec)
            if not stage in possible_stages:
                error("invalid stage in testcase:", rec)
            test_cases[fname]=TestCase(fname, stage, hash_)
    return test_cases

def gen_out_dir():
    if not os.path.exists(out_dir):    
        try:
            os.mkdir(out_dir)
        except OSError:
            print ("Cannot create directory",out_dir)
    if not os.path.isdir(out_dir):
        error(out_dir, "is not a director")

def run():
    global args, cdsl_prg, cdsl_root
    args = get_args()
    cdsl_root = (
        args.cdsl_root
        or os.environ.get("CDSL_ROOT")
        or error("Unspecified CDSL_ROOT directory.")
    )
    cdsl_root = os.path.abspath(cdsl_root)
    print("cdsl_root=", cdsl_root)
    cdsl_prg = cdsl_root + "/bin/cdsl"
    gen_out_dir()
    test_cases = read_test_cases()
    test_stage = args.stage.lower()
    if args.files:
        files = args.files
    else:
        files = test_cases.keys()
    passed = 0
    skipped = 0
    failed = 0

    for fname in files:
        test_case = test_cases.get(fname)
        if not test_case:
            print("# No test case for file:", fname)
            continue
        if not cdsl_config.CDSL_HAS_OFFSET_REDUCTION:
            if test_case.filename == "offsetReduction.cpp":
                print("# Skip offsetReduction test:", fname)
                continue
        res = proc_test_case(test_case)
        if res is None:
            skipped += 1
        elif res:
            passed += 1
        else:
            failed += 1
    print(
        "PASSED = "
        + str(passed)
        + "; SKIPPED = "
        + str(skipped)
        + "; FAILED = "
        + str(failed)
    )
    if failed:
        exit(1)


if __name__ == "__main__":
    run()
