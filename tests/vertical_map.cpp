#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void fun() {
    Field alpha(levels), beta(levels);
    Vertical_map ikidx0(levels);
    ikidx0(1) = 2;
    alpha = beta(ikidx0);
  } // end of function fun

} //end of namespace edsl
