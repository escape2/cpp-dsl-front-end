#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  
  void fun(Field alpha, Field beta, Field stencil) {
    alpha.spec(edges);
    beta.spec(cells);
    stencil.spec(edges, verts.cells);
    compute_on(edges) {
      alpha = nreduce(verts.cells, beta*stencil);
      alpha = nreduce(verts.cells.via(edges), beta*stencil);
    }

  } // end of function
} //end of namespace edsl
