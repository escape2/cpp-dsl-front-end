#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void fun(Field_int f0) {
    f0.spec(cells, levels);
    Field_int f1(cells, levels);

    vertical_region(start_level, end_level) {
      compute_on(cells) {
        f0(cells,levels) = 0;
      }
    }
  } // end of function fun

} //end of namespace edsl
