#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void test_fun(Field alpha, Field beta) {
    alpha.spec(cells);
    beta.spec(cells);
    compute_on(cells) {
      alpha = beta;
    }

  }

} //end of namespace edsl
