#include "dsl.hpp"

using namespace EDSL;


namespace edsl {

  Gridspace ek_space(edges,levels);
  define_field_type(EK_Field,ek_space);

  Gridspace eck_space(edges,cells,levels);
  define_field_type(ECK_Field,eck_space);

  Gridspace ecek_space(edges,cells.edges,levels);
  define_field_type(ECEK_Field,ecek_space);

  void offset_reduce(ECEK_Field raw_diam_coeff, EK_Field out_vn_e, EK_Field prism_thick_e, ECK_Field e2c_aux) {

    out_vn_e = nreduce(cells.edges,
                       {e2c_aux, e2c_aux, e2c_aux, e2c_aux},
                       {0, 0, 1, 1},
                       raw_diam_coeff * prism_thick_e
                       );

  }

}
