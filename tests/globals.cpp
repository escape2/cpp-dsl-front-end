#include "dsl.hpp"
using namespace EDSL;
namespace edsl {
  
  //int my_g1[3] = {1,2,3};

  const int my_int = 42;
  float my_float  = 1.12345678901234567890;
  double my_double = 2.1234567890123;
  bool my_bool = true;

  //string my_s = "abc";

  void test_fun(Field alpha) {
    alpha.spec(latitudes,longitudes);
    compute_on(latitudes,longitudes) {
        alpha = my_double;
    }
  }

}
