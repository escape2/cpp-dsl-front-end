#include "dsl.hpp"

using namespace EDSL;

namespace edsl {


  void fun(Field beta) {
    beta.spec(cells,edges);
    beta = 0;
  }

  
} //end of namespace edsl
