/*
  Test usage of relative sets, e.g., edges_of_cell: 
  1. declaration with (cells, edges_of_cell) should be the same as with (cells, edges)
  2. The "edges_of_cell" reduction dimension should function here in the same way as "edges"
     bc compute_on(cells) provides a suitable iteration context.
  3. "edges.via(cells)" is more flexible and should be used instead of "edges_of_cell"
 */
#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void fun(Field f0a, Field f0b, Field f1a, Field f1b) {
    f0a.spec(cells, edges_of_cell);
    f0b.spec(cells, edges);
    f1a.spec(cells);
    f1b.spec(cells);
    
    compute_on(cells) {
      // old style:
      f1a = nreduce(edges_of_cell, f0a);
      // new style:
      f1b = nreduce(edges.via(cells), f0b);
      // minimal expression:
      f1b = nreduce(edges, f0b);
    }

  } // end of function fun

} //end of namespace edsl
