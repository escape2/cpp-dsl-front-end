#include "dsl.hpp"

using namespace EDSL;

namespace edsl {


  void fun() {
    Field f(tracers);
    for (auto k: tracers) {
      f(k) = 1;
    }
  } // end of function fun

} //end of namespace edsl
