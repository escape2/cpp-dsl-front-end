#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
    
  void fun() {
    /* the following require loop_on support in the SIR
    Field orientation(edges,cells);
    Field dist_vector(edges, cells, cart3d); 
    Field primal_cart_normal(cells, cart3d);
    */
    // without loop_on:
    Field orientation(edges);
    Field dist_vector(edges, cart3d); 
    Field primal_cart_normal(edges, cart3d);
    compute_on(edges) {
      //loop_on(cells) {
      orientation = dot_product(dist_vector,primal_cart_normal);
      //}
    }

  }

} //end of namespace edsl
