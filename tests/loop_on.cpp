#include "dsl.hpp"

using namespace EDSL;

namespace edsl {
  

  void fun(Field alpha, Field beta, Field gamma, Field delta) {
    alpha.spec(cells,edges);
    beta.spec(cells,edges);
    gamma.spec(edges);
    delta.spec(cells);
    compute_on(cells) {
      loop_on(edges) {
        // indirect access to dense field gamma
        alpha = beta + gamma;
      }
    }
  }

  
} //end of namespace edsl
