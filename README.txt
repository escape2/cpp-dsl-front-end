build:
======

The CDSL frontend functionality has been tested with the setting below.

   - cmake >=  3.18.6
   - current Dawn
     - GridTools in the version used by Dawn
   - Atlas (we only use the C++ support)
   - LLVM (tested versions: 9, 10)
   - Python 3.7.7, including the following modules (e.g. using a virtual environment)
      - dlinfo 1.2.0
      - clang (Python bindings for libclang) - only if not already provided by LLVM
        - if LLVM has been installed without Python bindings then
          the virtual env clang package should be installed in a matching version
          (pip install clang==<insert the LLVM major version number>)
   - Perl v5.30.0
   - netcdf-fortran 4.5.2 (used by tests/examples)
   - tested gcc versions: 7.1, 9.1, 10.2
   - CUDA examples: cuda/10.0.130 & gcc/7.1.0 

The script cmake_build.sh shows how the build & tests could be driven, e.g.:

  ./cmake_build.sh 1 && ./cmake_build.sh 2

configures and builds CDSL.


build on Ubuntu 20.04:
======================

- follow the ESCAPE-2:D2.5 build instructions for Dawn on Ubuntu
- install additional packages:
     sudo apt install gfortran python3-clang libnetcdff-dev
- Get CDSL sources:
     git clone ssh://git@git.ecmwf.int/escape/cpp-dsl-front-end.git
- cd cpp-dsl-front-end
- copy the file ./conf/user_config-example-ubuntu.sh to ./user_config.sh and adapt if necessary
- execute conf, build and test phases:
  ./cmake_build.sh 1 && ./cmake_build.sh 2 && ./cmake_build.sh 3
- installation step:
  ./cmake_build.sh 4
- cdsl can be found in the $CDSL_INSTALL_DIR/bin/ directory where
  CDSL_INSTALL_DIR is defined in your user_config.sh script, e.g.,
  for CDSL_INSTALL_DIR=$HOME/cpp-dsl-front-end/install the following should work:
  ~/cpp-dsl-front-end/install/bin/cdsl -s SIR -json tests/compute_on.cpp


tests:
======

./cmake_build.sh 3

runs all tests. The environment variable TEST_LD_LIBRARY_PATH can be set to provide additional paths
to the effective LD_LIBRARY_PATH. This variable is only evaluated at configure time.


major environment variables:
============================

CDSL_WITH_CUDA: switch to enable cuda support & tests
DAWN_HAS_OFFSET_REDUCTION : state if DAWN has the offsetReduction feature
DAWN_HAS_FLEX_STRUCT_HALOS: state if the DAWN CUDA bacakend has the flexible halos compatible with the cxxnaive backend

see examples in ./conf/ for further environment variables


licences:
=========

All source code is distributed under the BSD-3-Clause licence (contained in the file
LICENCE.txt in the root directory) unless a certain implementation has been derived
from another project. This is noted in the respective file, e.g.:

MIT License (MIT); Copyright (c) 2020 mroethlin:

    examples/unstruct_stencils/AtlasSupport.cpp
    support/atlasToGlobalGpuTriMesh.*

developer-documentation:

see notes online:

https://docs.google.com/document/d/16u-EElvQMx_wI9VBElSxxO7XJYIKyo7eWHF0AHiJsQI/edit?usp=sharing

