#ifndef _DSL_HPP
#define _DSL_HPP

#include <cmath>

// include required standard headers:
#include <initializer_list>

namespace EDSL {

  struct Dim_key {
    // a single element key: int dim position, dim_kind;
    // or a set key: int dim positions, dim_kind;
  };

  // an element is an atomic part of a mesh
  struct Element_ref {
    operator Dim_key() const;
  };

  struct Direct_element_base : Element_ref {
    // index
  };

  struct Indirect_element_base : Element_ref {
    // indirect index, access to connectivity map, ...
  };

  
  struct Dim_spec {
    // captures only the storage aspect of a set
    // int size;
    // int kind;
    // other properties ...
  };

  struct Set_expr {
    operator Dim_key() const;
  };

  //struct Set_ref : Set_expr, Dim_spec {
  struct Set_ref {
    // a reference to the full set
    // int size;
    // int kind;
    // other properties ...
    operator Dim_spec() const;
    operator Dim_key() const;
    operator Set_expr() const;
  };


  struct Dense_set_base : Set_ref {
    // Set parameters ...
  };

  struct Sparse_set_base : Set_ref {
    // Set parameters, including access to connectivity map ...
  };



  struct Dim_chain : Set_ref{
    // connected dims
  };


  // reduction operators:
  namespace op {
    enum Reduce_op_kind {
      plus,
      minus,
      max,
      min
    };
  };

  // loop order
  namespace order {
    enum Order_kind {
      forward,
      backward,
      parallel,
    };
  };

  // Gridspace type
  struct Gridspace {
    Gridspace(const Dim_spec& ds0);
    Gridspace(const Dim_spec& ds0, const Dim_spec& ds1);
    Gridspace(const Dim_spec& ds0, const Dim_spec& ds1, const Dim_spec& ds2);
    Gridspace(const Dim_spec& ds0, const Dim_spec& ds1, const Dim_spec& ds2, const Dim_spec& ds3);
    Gridspace(const Dim_spec& ds0, const Dim_spec& ds1, const Dim_spec& ds2, const Dim_spec& ds3,
              const Dim_spec& ds4);
    Gridspace(const Dim_spec& ds0, const Dim_spec& ds1, const Dim_spec& ds2, const Dim_spec& ds3,
              const Dim_spec& ds4, const Dim_spec& ds5);
    Gridspace(const Dim_spec& ds0, const Dim_spec& ds1, const Dim_spec& ds2, const Dim_spec& ds3,
              const Dim_spec& ds4, const Dim_spec& ds5, const Dim_spec& ds6);
    Gridspace(const Dim_spec& ds0, const Dim_spec& ds1, const Dim_spec& ds2, const Dim_spec& ds3,
              const Dim_spec& ds4, const Dim_spec& ds5, const Dim_spec& ds6, const Dim_spec& ds7);
  };

  // Reductionweights (currently only for Fields):
  class Field;
  struct ReductionWeights {
    ReductionWeights(const std::initializer_list<double> w);
    /*
    ReductionWeights(const std::initializer_list<float> w);
    ReductionWeights(const std::initializer_list<int> w);
    ReductionWeights(const std::initializer_list<bool> w);
    */
    ReductionWeights(); // we support this case only to simplify error analysis
  };

  struct ReductionOffsets {
    ReductionOffsets(const std::initializer_list<int> w);
    ReductionOffsets(); // we support this case only to simplify error analysis
  };
  
  // include auto-generated declarations
#include "fields_dsl.hpp"
#include "struct_dsl.hpp"
#include "unstruct_dsl.hpp"

  // GridspaceFields:
  int CDSL__gen_var2type_binding_id(const Gridspace &gs);
#define define_field_type(F,S) typedef Field F; const int CDSL__var2type_binding_of_##F = CDSL__gen_var2type_binding_id(S)
#define define_field_dp_type(F,S) typedef Field_dp F; const int CDSL__var2type_binding_of_##F = CDSL__gen_var2type_binding_id(S)
#define define_field_int_type(F,S) typedef Field_int F; const int CDSL__var2type_binding_of_##F = CDSL__gen_var2type_binding_id(S)
#define define_field_bool_type(F,S) typedef Field_bool F; const int CDSL__var2type_binding_of_##F = CDSL__gen_var2type_binding_id(S)

  class Vertical_map {
  public:
    Vertical_map(const Dim_spec& ds0, const Dim_spec& ds1);
    Vertical_map(const Dim_spec& ds0);
    // todo: we also need methods to set the map in the init phase (outside of the DSL)
    operator Dim_key() const;
    Level& operator() (const Level k);
    //Level get(Level k) const;
    //void set(Level k);
  };


  bool likely(const bool cond);
  bool unlikely(const bool cond);

#define sparse_if(...) if (unlikely(__VA_ARGS__))


  struct Intrinsics_scope_guard_type {};
 
  // The Intrinsics class is used by the compute_on abstraction.
  class Intrinsics {
    // intrinsic properties of an expression (intrinsic iterations)
  public:
    void add_key(const Dim_key& k0);
    Intrinsics();
    Intrinsics(const Dim_key& k0);
    ~Intrinsics();

    Intrinsics begin () const;
    Intrinsics end () const;
    const Intrinsics_scope_guard_type operator* ();
    bool operator != (const Intrinsics& other);
    const Intrinsics& operator++ ();

  };


  // explicit declarations to avoid template error messages
  Intrinsics set_intrinsics(const Set_expr& se0, const Set_expr& se1,
                            const Set_expr& se2, const Set_expr& se3,
                            const Set_expr& se4, const Set_expr& se5);

  Intrinsics set_intrinsics(const Set_expr& se0, const Set_expr& se1,
                            const Set_expr& se2, const Set_expr& se3,
                            const Set_expr& se4);

  Intrinsics set_intrinsics(const Set_expr& se0, const Set_expr& se1,
                            const Set_expr& se2, const Set_expr& se3);

  Intrinsics set_intrinsics(const Set_expr& se0, const Set_expr& se1,
                            const Set_expr& se2);

  Intrinsics set_intrinsics(const Set_expr& se0, const Set_expr& se1);

  Intrinsics set_intrinsics(const Set_expr& se0);

  Intrinsics set_intrinsics(const Gridspace& gs0);

  Intrinsics set_intrinsics();

  // support for compute_on:
#define compute_on(...) for(auto EDSL__compute_on_guard [[maybe_unused]] : EDSL::set_intrinsics(__VA_ARGS__))
#define loop_on(...) for(auto EDSL__loop_on_guard [[maybe_unused]] : EDSL::set_intrinsics(__VA_ARGS__))

  struct Vertical_region_scope_guard_type {};

  // explicitly bounded vertical region
  class Vertical_region : Dense_set_base {
  public:
    Vertical_region(const Level start_level, const Level end_level);
    Vertical_region(const Level start_level, const Level end_level, const order::Order_kind order);
    operator Dim_key() const;
    operator Dim_spec() const;
    Vertical_region begin () const;
    Vertical_region end () const;
    //const Level operator* ();
    const Vertical_region_scope_guard_type operator* ();
    bool operator != (const Vertical_region& other);
    const Vertical_region& operator++ ();
  };

  Vertical_region set_vertical_region(Level start_level, Level end_level);
  Vertical_region set_vertical_region(Level start_level, Level end_level, order::Order_kind order);
  //Vertical_region set_vertical_region(int start_level, int end_level);

#define vertical_region(...) for(auto EDSL__vertical_region_guard [[maybe_unused]] : EDSL::set_vertical_region(__VA_ARGS__))

  struct Declaration_region_scope_guard_type {};
  // declaration of function arguments
  class Declaration_region {
  public:
    Declaration_region();
    //Declaration_region(const Dim_spec& ds0);
    Declaration_region begin () const;
    Declaration_region end () const;
    const Declaration_region_scope_guard_type operator* ();
    bool operator != (const Declaration_region& other);
    const Declaration_region& operator++ ();
  };

  Declaration_region set_declaration_region(const Dim_spec& ds0);
  Declaration_region set_declaration_region();

  //#define field_declaration(...) for(auto EDSL__declaration_region_guard [[maybe_unused]] : EDSL::set_declaration_region(__VA_ARGS__))
#define declare for(auto EDSL__declaration_region_guard [[maybe_unused]] : EDSL::set_declaration_region())
}
#endif
